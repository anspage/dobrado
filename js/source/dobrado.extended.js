/*global dobrado: true, CKEDITOR: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.extended) {
  dobrado.extended = {};
}
(function() {

  'use strict';

  // Track if any fields in the custom settings form have changed.
  var customChange = false;
  // notify is passed through from modules that want to know if they need
  // to send out notifications even though content has not changed.
  var notify = false;
  // The name of the current media query for style rules.
  var media = '';
  // The array passed to autocomplete of existing media queries.
  var mediaList = [];
  // Style rules that need to be saved when the style editor is closed.
  var rules = {};
  // Autocomplete css properties.
  var cssProperties =
    ['align-content', 'align-items', 'align-self', 'background',
     'background-attachment', 'background-color', 'background-image',
     'background-position', 'background-repeat', 'border', 'border-bottom',
     'border-bottom-color', 'border-bottom-style', 'border-bottom-width',
     'border-collapse', 'border-color', 'border-image', 'border-left',
     'border-left-color', 'border-left-style', 'border-left-width',
     'border-radius', 'border-right', 'border-right-color',
     'border-right-style', 'border-right-width', 'border-spacing',
     'border-style', 'border-top', 'border-top-color', 'border-top-style',
     'border-top-width', 'border-width', 'bottom', 'box-shadow', 'box-sizing',
     'clear', 'clip', 'color', 'content', 'cursor', 'direction', 'display',
     'empty-cells', 'flex', 'flex-basis', 'flex-direction', 'flex-flow',
     'flex-grow', 'flex-shrink', 'flex-wrap', 'float', 'font', 'font-family',
     'font-size', 'font-style', 'font-variant', 'font-weight', 'grid',
     'grid-area', 'grid-auto-columns', 'grid-auto-flow', 'grid-auto-rows',
     'grid-column', 'grid-column-end', 'grid-column-gap', 'grid-column-start',
     'grid-gap', 'grid-row', 'grid-row-end', 'grid-row-gap', 'grid-row-start',
     'grid-template', 'grid-template-areas', 'grid-template-columns',
     'grid-template-rows', 'height', 'justify-content', 'justify-items',
     'justify-self', 'left', 'letter-spacing', 'line-height', 'list-style',
     'list-style-image', 'list-style-position', 'list-style-type', 'margin',
     'margin-bottom', 'margin-left', 'margin-right', 'margin-top', 'max-height',
     'max-width', 'min-height', 'min-width', 'opacity', 'order', 'orphans',
     'outline', 'outline-color', 'outline-style', 'outline-width', 'overflow',
     'padding', 'padding-bottom', 'padding-left', 'padding-right',
     'padding-top', 'page', 'page-break-after', 'page-break-before',
     'page-break-inside', 'place-content', 'place-items', 'place-self',
     'position', 'right', 'size', 'text-align', 'text-decoration',
     'text-indent', 'text-transform', 'top', 'vertical-align', 'visibility',
     'white-space', 'width', 'widows', 'word-spacing', 'z-index'];
  // Autocomplete css values from property names.
  var cssValues = {
    'align-content': ['flex-start', 'flex-end', 'center', 'space-between',
                      'space-around', 'space-evenly', 'stretch', 'start',
                      'end'],
    'align-items': ['auto', 'flex-start', 'flex-end', 'center', 'baseline',
                    'stretch', 'start', 'end'],
    'align-self': ['auto', 'flex-start', 'flex-end', 'center', 'baseline',
                   'stretch', 'start', 'end'],
    'background-attachment': ['scroll', 'fixed'],
    'background-position': ['left', 'center', 'right', 'top', 'bottom'],
    'background-repeat': ['repeat','repeat-x','repeat-y', 'no-repeat'],
    'border-collapse': ['collapse', 'separate'],
    'box-sizing': ['border-box', 'content-box'],
    'clear': ['none', 'left', 'right', 'both'],
    'content': ['normal', 'none', 'open-quote', 'close-quote',
                'no-open-quote', 'no-close-quote'],
    'cursor': ['auto', 'crosshair', 'default', 'pointer', 'move', 'e-resize',
               'ne-resize', 'nw-resize', 'n-resize', 'se-resize', 'sw-resize',
               's-resize', 'w-resize', 'text', 'wait', 'help', 'progress'],
    'direction': ['ltr', 'rtl'],
    'display': ['inline', 'block', 'none', 'flex', 'grid', 'list-item',
                'inline-block', 'inline-grid', 'table', 'inline-table',
                'table-row-group', 'table-header-group', 'table-footer-group',
                'table-row', 'table-column-group', 'table-column',
                'table-cell', 'table-caption'],
    'empty-cells': ['show', 'hide'],
    'float': ['left', 'right', 'none'],
    'flex-direction': ['column', 'colum-reverse', 'row', 'row-reverse'],
    'flex-wrap': ['nowrap', 'wrap', 'wrap-reverse'],
    'font-style': ['normal', 'italic', 'oblique'],
    'font-variant': ['normal', 'small-caps'],
    'font-weight': ['normal', 'bold', 'bolder', 'lighter', '100', '200',
                     '300', '400', '500', '600', '700', '800', '900'],
    'grid-auto-flow': ['row', 'column', 'row dense', 'column dense'],
    'justify-content': ['flex-start', 'flex-end', 'center', 'space-between',
                        'space-around', 'space-evenly', 'start', 'end'],
    'justify-items': ['start', 'end', 'center', 'stretch'],
    'justify-self': ['start', 'end', 'center', 'stretch'],
    'list-style-position': ['inside', 'outside'],
    'list-style-type': ['disc', 'circle', 'square', 'decimal',
                        'decimal-leading-zero', 'lower-roman', 'upper-roman',
                        'lower-greek', 'lower-latin', 'upper-latin', 'armenian',
                        'georgian', 'lower-alpha', 'upper-alpha', 'none'],
    'page-break-after': ['auto', 'always', 'avoid', 'left', 'right'],
    'page-break-before': ['auto', 'always', 'avoid', 'left', 'right'],
    'page-break-inside': ['avoid', 'auto'],
    'position': ['static', 'sticky', 'relative', 'absolute', 'fixed'],
    'text-align': ['left', 'right', 'center', 'justify'],
    'text-decoration': ['none', 'underline', 'overline',
                        'line-through', 'blink'],
    'text-transform': ['capitalize', 'uppercase', 'lowercase', 'none'],
    'vertical-align': ['baseline', 'sub', 'super', 'top', 'text-top',
                       'middle', 'bottom', 'text-bottom'],
    'visibility': ['visible', 'hidden', 'collapse'],
    'white-space': ['normal', 'pre', 'nowrap', 'pre-wrap', 'pre-line']
  };

  $(function() {
    // Center the dialog window horizontally, with a small offset at the top.
    $('.extended').dialog({
      show: true,
      autoOpen: false,
      width: 750,
      height: 500,
      position: { my: 'top', at: 'top+50', of: window },
      title: 'Editor',
      close: close,
      create: dobrado.fixedDialog });
    $('#colorpicker').addClass('ui-widget-content').hide();
  });

  function saveStyle() {
    if (!$.isEmptyObject(rules)) {
      // Call box_style.php, page_style.php or site_style.php.
      var call = $('.extended').data('mode') + '_style';
      // This checkbox is used in page mode.
      var updateAllPages = $('#extended-update-all-pages:checked').length;

      dobrado.log('Saving style changes.', 'info');
      $.post('/php/' + call + '.php',
             { style: JSON.stringify(rules), media: media,
               updateAllPages: updateAllPages,
               url: location.href, token: dobrado.token },
        function(response) {
          dobrado.checkResponseError(response, call);
        });
      rules = {};
    }
  }

  function resetStyleEditor() {
    var updateAllPages = '';
    var hidden = ' hidden';
    // Only show update-all-pages checkbox in page mode.
    if ($('.extended').data('mode') === 'page') {
      hidden = '';
    }
    if ($('#extended-style .update-all-pages').length !== 0) {
      updateAllPages = '<div class="update-all-pages' + hidden + '">' +
          '<label for="extended-update-all-pages">Copy changes to all pages?' +
          '</label>' +
          '<input id="extended-update-all-pages" type="checkbox">' +
        '</div>';
    }
    var html = '<div id="extended-style">' +
         updateAllPages +
         '<div class="form-spacing">' +
           '<label for="extended-media-query">Media query: </label>' +
           '<input id="extended-media-query" type="text">' +
         '</div>' +
         '<div class="style-editor">' +
           '<h3><a href="#">Add a new style group</a></h3>' +
           '<div class="form-spacing">' +
             '<label for="extended-new-selector">Selector: </label>' +
             '<input id="extended-new-selector" type="text">' +
           '</div>' +
         '</div>' +
      '</div>';
    $('#extended-style').remove();
    $('#extended-content').after(html);
  }

  function close() {
    
    function formatDate() {
      var d = new Date();
      var year = d.getFullYear();
      var month = d.getMonth() + 1;
      var day = d.getDate();
      if (month < 10) {
        month = '0' + month;
      }
      if (day < 10) {
        day = '0' + day;
      }
      return year + '-' + month + '-' + day;
    }

    saveStyle();
    media = '';
    var content = { dataOnly: false, notify: notify };
    $('#extended-custom-settings').find(':input').each(function() {
      var name = $(this).attr('name');
      if (name) {
        // Substitute 1 or 0 for radios and checkboxes.
        if ($(this).is(':radio') || $(this).is(':checkbox')) {
          if ($(this).is(':checked')) {
            content[name] = 1;
          }
          else {
            content[name] = 0;
          }
        }
        else {
          // If there is an input named title, assume it is a post module
          // and use it for the permalink page name to redirect to.
          if (name === 'title' && $(this).val() !== '') {
            // Remove characters from the title that don't belong in
            // permalinks, but replace spaces with underscores.
            var title = $(this).val().replace(/[^a-z0-9\s_-]/gi, '');
            title = title.replace(/\s/g, '_');
            dobrado.permalink = formatDate() + '-' + title;
          }
          if (name === 'enclosure') {
            if (!content.enclosure) {
              content.enclosure = [];
            }
            content.enclosure.push($(this).val());
          }
          else {
            content[name] = $(this).val();
          }
        }
      }
    });
    dobrado.closeEditor(content, customChange);
    $('#extended-tabs').tabs('destroy');
    if ($('.extended').data('mode') === 'box') {
      // The css added below will be added to all instances if not cleared.
      CKEDITOR.clearCss();
      // Restore the inline editors once the extended editor is closed.
      if (dobrado.editMode) {
        $('.dobrado-editable').each(function() {
          dobrado.inlineEditor($(this).get(0));
        });
      }
    }
    $('.extended').data('mode', '');
    resetStyleEditor();
    customChange = false;
  }

  function mediaQuery() {
    var mode = $('.extended').data('mode');
    var parameters = { request: 'extended', mode: mode, action: 'display',
                       media: media, url: location.href, token: dobrado.token };
    if (mode === 'box') {
      var id = dobrado.current;
      var content = { data: dobrado.editor.getData() };
      parameters.id = id;
      parameters.label = $(id).data('label');
      parameters.content = JSON.stringify(content);
    }
    dobrado.log('Loading media query.', 'info');
    $.post('/php/request.php', parameters,
      function(response) {
        if (dobrado.checkResponseError(response, 'mediaQuery')) {
          return;
        }
        var extended = JSON.parse(response);
        dobrado.extended.styleEditor(extended.style, extended.media);
      });
  }

  function selectMediaQuery(event, ui) {
    resetStyleEditor();
    media = ui.item.value;
    mediaQuery();
  }

  function loadMediaQuery(event) {
    if (event.keyCode !== 13) {
      return;
    }
    event.preventDefault();
    resetStyleEditor();
    media = $(this).val();
    mediaQuery();
  }

  function updateStyleRuleComponents() {
    $('.style-property').each(function() {
      $(this).one('click', dobrado.select);
      // Use autocomplete for new properties.
      if (!$(this).attr('readonly')) {
        $(this).autocomplete({ minLength: 1,
                               search: dobrado.fixAutoCompleteMemoryLeak,
                               source: cssProperties });
      }
      // Otherwise try to autocomplete values for existing properties.
      else if (cssValues[$(this).val()]) {
        $(this).parent().next().children().autocomplete({
          minLength: 1,
          search: dobrado.fixAutoCompleteMemoryLeak,
          source: cssValues[$(this).val()]});
      }
    });
    $('.remove-style-rule').button({
      icon: 'ui-icon-closethick',
      showLabel: false
    });
    // Initialise existing color fields.
    $('.style-value').each(function() {
      if (/^#[a-f0-9]+$/i.test($(this).val())) {
        $.farbtastic('#colorpicker').linkTo(this);
        $.farbtastic('#colorpicker').setColor($(this).val());
      }
    });
    $('.style-value').click(function() {
      if (/^#[a-f0-9]+$/i.test($(this).val())) {
        $.farbtastic('#colorpicker').linkTo(this);
        $('#colorpicker').show().position({
          of: this,
          my: 'top',
          at: 'bottom',
          collision: 'none'
        });
        $(this).blur(function() {
          $('#colorpicker').hide();
        });
      }
    });
  }

  function addNewSelector(event) {
    if (event.keyCode !== 13) {
      return;
    }
    event.preventDefault();
    var selector = $(this).val();
    selector = selector.replace(/>/g, '&gt;');
    selector = selector.replace(/</g, '&lt;');
    if (selector) {
      var newStyleGroup = '<h3><a href="#">' + selector +
        '</a></h3>' +
        '<div class="style-group">' +
          '<div class="style-rule">' +
            '<button class="remove-style-rule">remove</button>' +
            '<span class="ui-widget">' +
              '<input class="style-property" type="text" value="property">' +
            '</span>' +
            '<span class="ui-widget">' +
              '<input class="style-value" type="text" value="value">' +
            '</span>' +
          '</div>' +
          '<button class="new-style-rule">Add a new style rule</button>' +
        '</div>';
        $(this).val('');
        $('.style-editor').accordion('destroy');
        $(newStyleGroup).insertAfter($(this).parent());
        dobrado.extended.styleEditor();
    }
  }

  function addStyleRuleEvents() {

    function save(selector, property, value) {
      var current = {};
      if (rules[selector]) {
        current = rules[selector];
      }
      current[property] = value;
      rules[selector] = current;
    }

    function customSelector(text) {
      if (dobrado.current) {
        return text === 'General' ?
          dobrado.current : dobrado.current + ' ' + text;
      }
      return text;
    }

    function saveSelector(selector, property, value) {
      // Do nothing if the value wasn't changed. (ie still set to 'value')
      if (value === 'value') {
        return;
      }
      // Check if this style-group has a custom selector.
      var custom = customSelector(selector);
      $(custom).css(property.val(), value);
      save(custom, property.val(), value);
      // Once a property has been saved, make sure it is read-only as it
      // must be deleted to be removed, not 'written over'.
      property.attr('readonly', true);
    }

    $('.style-group').on('click', '.remove-style-rule', function() {
      var property = $(this).next('.ui-widget').children('.style-property');
      var selector = $(this).parents('.style-group').prev('h3').text();
      saveSelector(selector, property, '');
      $(this).parent().remove();
    });
    $('.style-group').one('click', '.style-property', dobrado.select).
      on('blur', '.style-property', function() {
        // Try to autocomplete values for the property.
        if (cssValues[$(this).val()]) {
          $(this).parent().next().children().autocomplete({
            minLength: 1,
            search: dobrado.fixAutoCompleteMemoryLeak,
            source: cssValues[$(this).val()] });
        }
      });
    $('.style-group').one('click', '.style-value', dobrado.select).
      on('blur keypress', '.style-value', function(event) {
        if (event.type === 'keypress' && event.keyCode !== 13) {
          return;
        }
        event.preventDefault();
        var selector = $(this).parents('.style-group').prev('h3').text();
        var property = $(this).parent().prev().children('.style-property');
        saveSelector(selector, property, $(this).val());
      });
    $('.new-style-rule').button().click(function() {
      var newStyleRule = '<div class="style-rule">' +
          '<button class="remove-style-rule">remove</button>' +
          '<span class="ui-widget">' +
            '<input class="style-property" type="text" value="property">' +
          '</span>' +
          '<span class="ui-widget">' +
            '<input class="style-value" type="text" value="value">' +
          '</span>' +
        '</div>';
      $(newStyleRule).insertBefore($(this));
      updateStyleRuleComponents();
    });
  }

  dobrado.extended.select = function(filename) {
    var enclosure = '<div class="form-spacing">' +
        '<button class="post-remove-photo">remove</button>' +
        '<input name="enclosure" type="text" maxlength="200" ' +
          'value="' + filename + '">' +
      '</div>';
    $(enclosure).insertBefore('#post-add-photo').children('.post-remove-photo').
      button({ icon: 'ui-icon-closethick',
               showLabel: false }).
      click(function() {
        $(this).parent().remove();
        return false;
      });
  };

  dobrado.extended.selectTab = function(event, ui) {
    if ($(ui.oldTab).text() === 'Style') {
      saveStyle();
    }
  };

  dobrado.extended.styleEditor = function(style, updateMedia) {
    if (style) {
      $('.style-editor').append(style);
    }
    if (updateMedia) {
      mediaList = updateMedia;
    }
    $('#extended-media-query').val(media);
    $('.style-editor').accordion({ heightStyle: 'content' });
    addStyleRuleEvents();
    updateStyleRuleComponents();
    $('#extended-media-query').autocomplete({ minLength: 1,
                                              search: dobrado.fixAutoCompleteMemoryLeak,
                                              source: mediaList,
                                              select: selectMediaQuery });
    $('#extended-media-query').keypress(loadMediaQuery);
    $('#extended-new-selector').keypress(addNewSelector);
    $('#extended-tabs').show().tabs({ activate: dobrado.extended.selectTab });
    // Disable the content tab when not used, which is when dobrado.editor is
    // not set and the content area is empty.
    if (!dobrado.editor && $('#extended-content > div.edit').html() === '') {
      $('#extended-tabs').tabs('disable', 1);
    }
    // Disable the custom tab if not used by this module.
    if ($('#extended-custom > div.custom').html() === '') {
      $('#extended-tabs').tabs('disable', 0);
      if (dobrado.editor || $('#extended-content > div.edit').html() !== '') {
        $('#extended-tabs').tabs('option', 'active', 1);
      }
      else {
        // When the first two tabs are disabled show the style tab.
        $('#extended-tabs').tabs('option', 'active', 2);
      }
    }
  };

  dobrado.extended.editor = function(response) {
    dobrado.editorLoading = false;
    if (dobrado.checkResponseError(response, 'extended.editor')) {
      $('.extended').dialog('close');
      return;
    }
    
    // Remove editors.
    dobrado.editor = null;
    $('.dobrado-editable').attr('contenteditable', false);
    $.each(CKEDITOR.instances, function() {
      this.destroy();
    });

    var extended = JSON.parse(response);
    // The content tab.
    var custom = '';
    var source = '';
    if (extended.content.custom) {
      custom = extended.content.custom;
    }
    if (extended.content.source) {
      source = extended.content.source;
    }
    // Reset notify in case it was used previously.
    notify = false;
    if (extended.content.notify) {
      notify = extended.content.notify;
    }
    // Rename the 'Custom' tab with the name of the current module.
    var label = $(dobrado.current).data('label');
    // Some module names aren't that readable, so customise them here.
    if (label === 'commenteditor') {
      label = 'Comment Manager';
    }
    else if (label === 'simple') {
      label = 'Text Area';
    }
    else {
      // Otherwise capitalize the first letter to match other tabs.
      label = label.charAt(0).toUpperCase() + label.slice(1);
    }
    $('#extended-tabs li a').first().html(label);
    $('#extended-custom').html('<div class="custom">' + custom + '</div>');
    $('#extended-content').html('<div class="edit">' + source + '</div>');
    // If the user changes the title of a post module, a redirect is done to
    // the new page, but only if they were on the old permalink page.
    dobrado.oldPermalink = extended.content.permalink;
    $('#extended-custom-settings').find(":input").change(function() {
      customChange = true;
    });
    $('#extended-custom-settings :submit').button().click(function() {
      $('.extended').dialog('close');
      return false;
    });
    // Add some custom actions for the post module.
    $('.post-remove-photo').
      button({ icon: 'ui-icon-closethick',
               showLabel: false }).
      click(function() {
        $(this).parent().remove();
        return false;
      });
    $('#post-add-photo').button().click(function() {
      dobrado.createModule('browser', 'browser', 'extended');
      return false;
    });
    if (extended.content.editor === true) {
      var edit = $('#extended-content > div.edit');
      // Copy the current module's style into the editor. Note that properties
      // affecting position have been removed from this list, and border styles
      // are not displayed either.
      var editorCssProperties =
        ['background', 'background-attachment', 'background-color',
         'background-image', 'background-position', 'background-repeat',
         'background-size', 'color', 'direction', 'empty-cells', 'font',
         'font-family', 'font-size', 'font-style', 'font-variant',
         'font-weight', 'letter-spacing', 'line-height', 'list-style',
         'list-style-image', 'list-style-position', 'list-style-type',
         'opacity', 'outline', 'outline-color', 'outline-style',
         'outline-width', 'size', 'text-align', 'text-decoration',
         'text-indent', 'text-transform', 'white-space', 'word-spacing'];

      var css = 'a.hidden { display: none; } .cke_editable { ';
      var current = $(dobrado.current);
      for (var i = 0; i < editorCssProperties.length; i++) {
        var property = editorCssProperties[i];
        var value = current.css(property);
        if (value) {
          css += property + ': ' + value + '; ';
        }
      }
      css += 'margin: 10px; }';
      CKEDITOR.addCss(css);
      // Don't want to use autogrow plugin here but there's no way to turn it
      // off, so set the min and max height to that of the edit area.
      let height = edit.height();
      if (!height) height = 240;
      dobrado.editor = CKEDITOR.replace(edit.get(0),
        { allowedContent: true,
          autoGrow_minHeight: height,
          autoGrow_maxHeight: height,
          autoGrow_onStartup: true,
          disableNativeSpellChecker: false,
          enterMode: CKEDITOR.ENTER_BR,
          filebrowserBrowseUrl: '/php/browse.php',
          removePlugins:
            'elementspath,tableselection,tabletools,contextmenu,liststyle',
          resize_enabled: false,
          toolbar: [[ 'Source', '-', 'NewPage' ],
                    [ 'Undo', 'Redo', '-', 'SelectAll', 'RemoveFormat' ],
                    [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea',
                      'Select', 'Button', 'ImageButton', 'HiddenField'],
                    [ 'Bold', 'Italic', 'Underline', 'Strike' ],
                    [ 'Subscript', 'Superscript'],
                    [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent',
                      'Blockquote', 'CreateDiv'],
                    [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
                      'JustifyBlock'],
                    [ 'Link', 'Unlink', 'Anchor'],
                    [ 'EmojiPanel', 'Image', 'Table', 'HorizontalRule',
                      'SpecialChar', 'PageBreak'],
                    [ 'Format', 'Font', 'FontSize'],
                    [ 'TextColor', 'BGColor'],
                    [ 'Maximize', 'ShowBlocks', '-', 'About']]
        });
    }
    // The style tab.
    dobrado.extended.styleEditor(extended.style, extended.media);
    // The history tab.
    $('#extended-history').html(extended.history);
    $('.extended').dialog('open');
    $('.extended').data('mode', 'box');
  };

}());

/*
 * @licstart The following is the entire license notice
 * for the JavaScript code in this page.
 *
 * Farbtastic Color Picker 1.2
 * © 2008 Steven Wittens
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @licend The above is the entire license notice
 * for the JavaScript code in this page.
 */

jQuery.fn.farbtastic = function (callback) {
  $.farbtastic(this, callback);
  return this;
};

jQuery.farbtastic = function (container, callback) {
  container = $(container).get(0);
  return container.farbtastic || (container.farbtastic = new jQuery._farbtastic(container, callback));
}

jQuery._farbtastic = function (container, callback) {
  // Store farbtastic object
  var fb = this;

  // Insert markup
  $(container).html('<div class="farbtastic"><div class="color"></div><div class="wheel"></div><div class="overlay"></div><div class="h-marker marker"></div><div class="sl-marker marker"></div></div>');
  var e = $('.farbtastic', container);
  fb.wheel = $('.wheel', container).get(0);
  // Dimensions
  fb.radius = 84;
  fb.square = 100;
  fb.width = 194;

  /**
   * Link to the given element(s) or callback.
   */
  fb.linkTo = function (callback) {
    // Unbind previous nodes
    if (typeof fb.callback == 'object') {
      $(fb.callback).unbind('keyup', fb.updateValue);
    }

    // Reset color
    fb.color = null;

    // Bind callback or elements
    if (typeof callback == 'function') {
      fb.callback = callback;
    }
    else if (typeof callback == 'object' || typeof callback == 'string') {
      fb.callback = $(callback);
      fb.callback.bind('keyup', fb.updateValue);
      if (fb.callback.get(0).value) {
        fb.setColor(fb.callback.get(0).value);
      }
    }
    return this;
  }
  fb.updateValue = function (event) {
    if (this.value && this.value != fb.color) {
      fb.setColor(this.value);
    }
  }

  /**
   * Change color with HTML syntax #123456
   */
  fb.setColor = function (color) {
    var unpack = fb.unpack(color);
    if (fb.color != color && unpack) {
      fb.color = color;
      fb.rgb = unpack;
      fb.hsl = fb.RGBToHSL(fb.rgb);
      fb.updateDisplay();
    }
    return this;
  }

  /**
   * Change color with HSL triplet [0..1, 0..1, 0..1]
   */
  fb.setHSL = function (hsl) {
    fb.hsl = hsl;
    fb.rgb = fb.HSLToRGB(hsl);
    fb.color = fb.pack(fb.rgb);
    fb.updateDisplay();
    return this;
  }

  /////////////////////////////////////////////////////

  /**
   * Retrieve the coordinates of the given event relative to the center
   * of the widget.
   */
  fb.widgetCoords = function (event) {
    var x, y;
    var el = event.target || event.srcElement;
    var reference = fb.wheel;

    if (typeof event.offsetX != 'undefined') {
      // Use offset coordinates and find common offsetParent
      var pos = { x: event.offsetX, y: event.offsetY };

      // Send the coordinates upwards through the offsetParent chain.
      var e = el;
      while (e) {
        e.mouseX = pos.x;
        e.mouseY = pos.y;
        pos.x += e.offsetLeft;
        pos.y += e.offsetTop;
        e = e.offsetParent;
      }

      // Look for the coordinates starting from the wheel widget.
      var e = reference;
      var offset = { x: 0, y: 0 }
      while (e) {
        if (typeof e.mouseX != 'undefined') {
          x = e.mouseX - offset.x;
          y = e.mouseY - offset.y;
          break;
        }
        offset.x += e.offsetLeft;
        offset.y += e.offsetTop;
        e = e.offsetParent;
      }

      // Reset stored coordinates
      e = el;
      while (e) {
        e.mouseX = undefined;
        e.mouseY = undefined;
        e = e.offsetParent;
      }
    }
    else {
      // Use absolute coordinates
      var pos = fb.absolutePosition(reference);
      x = (event.pageX || 0*(event.clientX + $('html').get(0).scrollLeft)) - pos.x;
      y = (event.pageY || 0*(event.clientY + $('html').get(0).scrollTop)) - pos.y;
    }
    // Subtract distance to middle
    return { x: x - fb.width / 2, y: y - fb.width / 2 };
  }

  /**
   * Mousedown handler
   */
  fb.mousedown = function (event) {
    // Capture mouse
    if (!document.dragging) {
      $(document).bind('mousemove', fb.mousemove).bind('mouseup', fb.mouseup);
      document.dragging = true;
    }

    // Check which area is being dragged
    var pos = fb.widgetCoords(event);
    fb.circleDrag = Math.max(Math.abs(pos.x), Math.abs(pos.y)) * 2 > fb.square;

    // Process
    fb.mousemove(event);
    return false;
  }

  /**
   * Mousemove handler
   */
  fb.mousemove = function (event) {
    // Get coordinates relative to color picker center
    var pos = fb.widgetCoords(event);

    // Set new HSL parameters
    if (fb.circleDrag) {
      var hue = Math.atan2(pos.x, -pos.y) / 6.28;
      if (hue < 0) hue += 1;
      fb.setHSL([hue, fb.hsl[1], fb.hsl[2]]);
    }
    else {
      var sat = Math.max(0, Math.min(1, -(pos.x / fb.square) + .5));
      var lum = Math.max(0, Math.min(1, -(pos.y / fb.square) + .5));
      fb.setHSL([fb.hsl[0], sat, lum]);
    }
    return false;
  }

  /**
   * Mouseup handler
   */
  fb.mouseup = function () {
    // Uncapture mouse
    $(document).unbind('mousemove', fb.mousemove);
    $(document).unbind('mouseup', fb.mouseup);
    document.dragging = false;
  }

  /**
   * Update the markers and styles
   */
  fb.updateDisplay = function () {
    // Markers
    var angle = fb.hsl[0] * 6.28;
    $('.h-marker', e).css({
      left: Math.round(Math.sin(angle) * fb.radius + fb.width / 2) + 'px',
      top: Math.round(-Math.cos(angle) * fb.radius + fb.width / 2) + 'px'
    });

    $('.sl-marker', e).css({
      left: Math.round(fb.square * (.5 - fb.hsl[1]) + fb.width / 2) + 'px',
      top: Math.round(fb.square * (.5 - fb.hsl[2]) + fb.width / 2) + 'px'
    });

    // Saturation/Luminance gradient
    $('.color', e).css('backgroundColor', fb.pack(fb.HSLToRGB([fb.hsl[0], 1, 0.5])));

    // Linked elements or callback
    if (typeof fb.callback == 'object') {
      // Set background/foreground color
      $(fb.callback).css({
        backgroundColor: fb.color,
        color: fb.hsl[2] > 0.5 ? '#000' : '#fff'
      });

      // Change linked value
      $(fb.callback).each(function() {
        if (this.value && this.value != fb.color) {
          this.value = fb.color;
        }
      });
    }
    else if (typeof fb.callback == 'function') {
      fb.callback.call(fb, fb.color);
    }
  }

  /**
   * Get absolute position of element
   */
  fb.absolutePosition = function (el) {
    var r = { x: el.offsetLeft, y: el.offsetTop };
    // Resolve relative to offsetParent
    if (el.offsetParent) {
      var tmp = fb.absolutePosition(el.offsetParent);
      r.x += tmp.x;
      r.y += tmp.y;
    }
    return r;
  };

  /* Various color utility functions */
  fb.pack = function (rgb) {
    var r = Math.round(rgb[0] * 255);
    var g = Math.round(rgb[1] * 255);
    var b = Math.round(rgb[2] * 255);
    return '#' + (r < 16 ? '0' : '') + r.toString(16) +
           (g < 16 ? '0' : '') + g.toString(16) +
           (b < 16 ? '0' : '') + b.toString(16);
  }

  fb.unpack = function (color) {
    if (color.length == 7) {
      return [parseInt('0x' + color.substring(1, 3)) / 255,
        parseInt('0x' + color.substring(3, 5)) / 255,
        parseInt('0x' + color.substring(5, 7)) / 255];
    }
    else if (color.length == 4) {
      return [parseInt('0x' + color.substring(1, 2)) / 15,
        parseInt('0x' + color.substring(2, 3)) / 15,
        parseInt('0x' + color.substring(3, 4)) / 15];
    }
  }

  fb.HSLToRGB = function (hsl) {
    var m1, m2, r, g, b;
    var h = hsl[0], s = hsl[1], l = hsl[2];
    m2 = (l <= 0.5) ? l * (s + 1) : l + s - l*s;
    m1 = l * 2 - m2;
    return [this.hueToRGB(m1, m2, h+0.33333),
        this.hueToRGB(m1, m2, h),
        this.hueToRGB(m1, m2, h-0.33333)];
  }

  fb.hueToRGB = function (m1, m2, h) {
    h = (h < 0) ? h + 1 : ((h > 1) ? h - 1 : h);
    if (h * 6 < 1) return m1 + (m2 - m1) * h * 6;
    if (h * 2 < 1) return m2;
    if (h * 3 < 2) return m1 + (m2 - m1) * (0.66666 - h) * 6;
    return m1;
  }

  fb.RGBToHSL = function (rgb) {
    var min, max, delta, h, s, l;
    var r = rgb[0], g = rgb[1], b = rgb[2];
    min = Math.min(r, Math.min(g, b));
    max = Math.max(r, Math.max(g, b));
    delta = max - min;
    l = (min + max) / 2;
    s = 0;
    if (l > 0 && l < 1) {
      s = delta / (l < 0.5 ? (2 * l) : (2 - 2 * l));
    }
    h = 0;
    if (delta > 0) {
      if (max == r && max != g) h += (g - b) / delta;
      if (max == g && max != b) h += (2 + (b - r) / delta);
      if (max == b && max != r) h += (4 + (r - g) / delta);
      h /= 6;
    }
    return [h, s, l];
  }

  // Install mousedown handler (the others are set on the document on-demand)
  $('*', e).mousedown(fb.mousedown);

    // Init color
  fb.setColor('#000000');

  // Set linked elements/callback
  if (callback) {
    fb.linkTo(callback);
  }
}
