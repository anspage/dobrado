/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2017 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.more) {
  dobrado.more = {};
}
(function() {

  'use strict';

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.more').length === 0) {
      return;
    }

    // Center the dialog window horizontally, with a small offset at the top.
    $('.more').dialog({
      show: true,
      width: 500,
      height: 400,
      position: { my: 'top', at: 'top+50', of: window },
      title: 'Add a new module',
      create: dobrado.fixedDialog,
      close: function() { $('.more').remove(); }
    });
    $('#more-menu a').each(function() {
      $(this).click(dobrado.addModule);
    });
    // Check if tabs are being used to also show install options.
    if ($('#more-tabs').length) {
      $('#more-tabs').tabs();
      $('#more-installer-form > button.submit').button().click(install);
    }
  });

  function install(event) {
    event.preventDefault();
    dobrado.log('Installing module.', 'info');
    $.post('/php/installer.php',
           { label: $('#more-module-label').val(),
             version: $('#more-module-version').val(),
             title: $('#more-module-title').val(),
             description: $('#more-module-description').val(),
             display: $('#more-display-input:checked').length,
             url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'installer')) {
          return;
        }
        $('.more').dialog('close');
      });
    return false;
  }

}());
