// @source: /js/source/dobrado.login.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2018 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.login){dobrado.login={};}
(function(){'use strict';$(function(){if($('.login').length===0){return;}
$('.login-form > input:submit').button();$('.email-reset > input:submit').button();$('.login-title').click(function(){if($('.login-form').is(':hidden')){$('.login-title').hide();$('.login-form').show('slide',{direction:'up'},'slow');$('.login-help').show('slow');}
return false;});$('.login-help').click(function(){$('.email-reset').toggle('slide',{direction:'up'},'slow');return false;});});}());