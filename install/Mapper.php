<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Mapper extends Base {

  public function Add($id) {
    // Add default values to mapper table so that ViewSettings doesn't need to
    // check if there was a result from the query.
    $mysqli = connect_db();
    $query = 'INSERT INTO mapper VALUES ' .
      '("' . $this->owner . '", ' . $id . ', 0.0, 0.0, 0, "", "", "", "")';
    if (!$mysqli->query($query)) {
      $this->Log('Mapper->Add: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Callback() {
    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'init') return $this->Init();
    if ($us_action === 'settings') {
      if ($this->user->canEditPage) return $this->Settings();
      return ['error' => 'You don\'t have permission to view mapper settings.'];
    }
    if ($us_action === 'addMarker') {
      if ($this->user->canEditPage) return $this->AddMarker();
      return ['error' => 'You don\'t have permission to add markers.'];
    }
    if ($us_action === 'removeMarker') {
      if ($this->user->canEditPage) return $this->RemoveMarker();
      return ['error' => 'You don\'t have permission to remove markers.'];
    }
    if ($us_action === 'saveLocation') {
      if ($this->user->canEditPage) return $this->SaveLocation();
      return ['error' => 'You don\'t have permission to edit mapper settings.'];
    }
    if ($us_action === 'saveAPI') {
      if ($this->user->canEditPage) return $this->SaveAPI();
      return ['error' => 'You don\'t have permission to edit mapper settings.'];
    }
    return ['error' => 'Unknown action.'];
  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $edit = '';
    if ($this->user->canEditPage) {
      $edit = '<button class="edit-button"></button>' .
        '<div id="mapper-editor-' . $id . '" class="mapper-editor"></div>';
    }
    return $edit . '<div id="mapper-view-' . $id . '" class="view"></div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {

  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $this->AppendScript($path, 'dobrado.mapper.js');
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS mapper (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'latitude FLOAT(8, 5) NOT NULL,' .
      'longitude FLOAT(8, 5) NOT NULL,' .
      'default_zoom TINYINT NOT NULL,' .
      'tile_url VARCHAR(200) NOT NULL,' .
      'tile_id VARCHAR(50) NOT NULL,' .
      'access_token VARCHAR(200) NOT NULL,' .
      'attribution TEXT,' .
      'PRIMARY KEY(user, box_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Mapper->Install 1: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS mapper_marker (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'marker_id INT UNSIGNED NOT NULL AUTO_INCREMENT,' .
      'latitude FLOAT(8, 5) NOT NULL,' .
      'longitude FLOAT(8, 5) NOT NULL,' .
      'content TEXT,' .
      'PRIMARY KEY(user, box_id, marker_id)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Mapper->Install 2: ' . $mysqli->error);
    }
    $site_style = ['"", ".mapper > .view", "height", "300px"',
                   '"", ".marker-remove", "margin-left", "2em"',
                   '"", "#mapper-settings-form label", "width", "7em"',
                   '"", "#mapper-marker-not-set", "font-style", "italic"',
                   '"", "#mapper-marker-not-set", "font-size", "0.8em"',
                   '"", "#mapper-marker-add", "margin-left", "7.3em"',
                   '"", "#mapper-location-save", "margin-left", "7.3em"',
                   '"", "#mapper-api-save", "margin-left", "7.3em"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.mapper.js');
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function AddMarker() {
    include 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);
    $us_content = $purifier->purify($_POST['content']);

    $latitude = (float)$_POST['latitude'];
    $longitude = (float)$_POST['longitude'];
    $box_id = (int)substr($_POST['id'], 9);

    $mysqli = connect_db();
    $content = $mysqli->escape_string($us_content);
    $query = 'INSERT INTO mapper_marker (user, box_id, latitude, longitude, ' .
      'content) VALUES ("' . $this->owner . '", ' . $box_id . ', ' . $latitude .
      ', ' . $longitude . ', "' . $content . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Mapper->AddMarker: ' . $mysqli->error);
    }
    $id = $mysqli->insert_id;
    $mysqli->close();
    $content = $this->MarkerContent($id, $latitude, $longitude, $us_content);
    return ['id' => $id, 'content' => $content];
  }

  private function Init() {
    $settings = [];
    $mysqli = connect_db();
    $query = 'SELECT mapper.box_id, latitude, longitude, default_zoom, ' .
      'tile_url, tile_id, access_token, attribution FROM mapper LEFT JOIN ' .
      'modules ON mapper.user = modules.user WHERE ' .
      'mapper.user = "' . $this->owner . '" AND ' .
      'page = "' . $this->user->page . '"';
    if ($result = $mysqli->query($query)) {
      while ($mapper = $result->fetch_assoc()) {
        $box_id = $mapper['box_id'];
        $settings[$box_id] = ['latitude' => (float)$mapper['latitude'],
                              'longitude' => (float)$mapper['longitude'],
                              'zoom' => (int)$mapper['default_zoom'],
                              'url' => $mapper['tile_url'],
                              'id' => $mapper['tile_id'],
                              'token' => $mapper['access_token'],
                              'attribution' => $mapper['attribution'],
                              'marker' => $this->MarkerSettings($box_id)];
      }
      $result->close();
    }
    else {
      $this->Log('Mapper->Init: ' . $mysqli->error);
    }
    $mysqli->close();
    return $settings;
  }

  private function MarkerContent($id, $latitude, $longitude, $us_content) {
    return '<div class="form-spacing">' .
        '<button id="mapper-marker-remove-' . $id . '" class="marker-remove">' .
          'remove</button> ' .
        '<input type="text" readonly="readonly" ' .
          'value="' . htmlspecialchars($us_content) . '"> Location: ' .
        '<var>' . $latitude . '</var>, <var>' . $longitude . '</var> ' .
      '</div>';
  }

  private function MarkerSettings($box_id) {
    $marker = [];
    $mysqli = connect_db();
    $query = 'SELECT marker_id, latitude, longitude, content FROM ' .
      'mapper_marker WHERE user = "' . $this->owner . '" AND ' .
      'box_id = ' . $box_id;
    if ($result = $mysqli->query($query)) {
      while ($mapper_marker = $result->fetch_assoc()) {
        $id = (int)$mapper_marker['marker_id'];
        $marker[$id] = ['latitude' => (float)$mapper_marker['latitude'],
                        'longitude' => (float)$mapper_marker['longitude'],
                        'content' => $mapper_marker['content']];
      }
      $result->close();
    }
    else {
      $this->Log('MarkerSettings: ' . $mysqli->error);
    }
    $mysqli->close();
    return $marker;
  }

  private function RemoveMarker() {
    $box_id = (int)substr($_POST['id'], 9);
    $marker_id = (int)substr($_POST['marker'], 22);
    $mysqli = connect_db();
    $query = 'DELETE FROM mapper_marker WHERE user = "' . $this->owner . '" ' .
      'AND box_id = ' . $box_id . ' AND marker_id = ' . $marker_id;
    if (!$mysqli->query($query)) {
      $this->Log('Mapper->RemoveMarker: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function SaveAPI() {
    $mysqli = connect_db();
    $box_id = (int)substr($_POST['id'], 9);
    $tile_url = $mysqli->escape_string($_POST['tileURL']);
    $tile_id = $mysqli->escape_string($_POST['tileID']);
    $access_token = $mysqli->escape_string($_POST['accessToken']);
    $attribution = $mysqli->escape_string($_POST['attribution']);
    $query = 'UPDATE mapper SET tile_url = "' . $tile_url . '", ' .
      'tile_id = "' . $tile_id . '", access_token = "' . $access_token . '", ' .
      'attribution = "' . $attribution . '" WHERE ' .
      'user = "' . $this->owner . '" AND box_id = ' . $box_id;
    if (!$mysqli->query($query)) {
      $this->Log('Mapper->SaveAPI: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function SaveLocation() {
    $box_id = (int)substr($_POST['id'], 9);
    $latitude = (float)$_POST['latitude'];
    $longitude = (float)$_POST['longitude'];
    $zoom = (int)$_POST['zoom'];
    $mysqli = connect_db();
    $query = 'UPDATE mapper SET latitude = ' . $latitude . ', ' .
      'longitude = ' . $longitude . ', default_zoom = ' . $zoom . ' WHERE ' .
      'user = "' . $this->owner . '" AND box_id = ' . $box_id;
    if (!$mysqli->query($query)) {
      $this->Log('Mapper->SaveLocation: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function Settings() {
    $box_id = (int)substr($_POST['id'], 9);
    $view = $this->ViewSettings($box_id);
    $marker = $this->MarkerSettings($box_id);
    $marker_content = '';
    foreach ($marker as $id => $attributes) {
      $marker_content .= $this->MarkerContent($id, $attributes['latitude'],
                                              $attributes['longitude'],
                                              $attributes['content']);
    }
    $hidden = $marker_content === '' ? '' : ' class="hidden"';
    $content = '<form id="mapper-settings-form">' .
        '<fieldset><legend>Location markers</legend>' .
          '<p id="mapper-marker-not-set"' . $hidden . '>' .
            'There are no location markers set.</p>' .
          '<div id="mapper-marker-list">' . $marker_content . '</div>' .
          '<hr>' .
          '<div class="form-spacing">' .
            '<label for="mapper-marker-latitude">Latitude:</label>' .
            '<input type="text" id="mapper-marker-latitude">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="mapper-marker-longitude">Longitude:</label>' .
            '<input type="text" id="mapper-marker-longitude">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="mapper-marker-content">Content:</label>' .
            '<textarea id="mapper-marker-content"></textarea>' .
          '</div>' .
          '<button id="mapper-marker-add">add marker</button>' .
        '</fieldset>' .
        '<fieldset><legend>Location shown when loading map</legend>' .
          '<div class="form-spacing">' .
            '<label for="mapper-latitude">Latitude:</label>' .
            '<input type="text" id="mapper-latitude" ' .
              'value="' . $view['latitude'] . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="mapper-longitude">Longitude:</label>' .
            '<input type="text" id="mapper-longitude" ' .
              'value="' . $view['longitude'] . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="mapper-default-zoom">Default Zoom:</label>' .
            '<input type="text" id="mapper-default-zoom" ' .
              'value="' . $view['default_zoom'] . '">' .
          '</div>' .
          '<button id="mapper-location-save">save</button>' .
        '</fieldset>' .
        '<fieldset><legend>Tile API settings</legend>' .
          '<div class="form-spacing">' .
            '<label for="mapper-tile-url">URL:</label>' .
            '<input type="text" id="mapper-tile-url" ' .
              'value="' . $view['tile_url'] . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="mapper-tile-id">ID:</label>' .
            '<input type="text" id="mapper-tile-id" ' .
              'value="' . $view['tile_id'] . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="mapper-access-token">Access Token:</label>' .
            '<input type="text" id="mapper-access-token" ' .
              'value="' . $view['access_token'] . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="mapper-attribution">Attribution:</label><br>' .
            '<textarea id="mapper-attribution">' . $view['attribution'] .
            '</textarea>' .
          '</div>' .
          '<button id="mapper-api-save">save</button>' .
        '</fieldset>' .
      '</form>';
    return ['id' => $box_id, 'content' => $content];
  }

  private function ViewSettings($box_id) {
    $mapper = [];
    $mysqli = connect_db();
    $query = 'SELECT latitude, longitude, default_zoom, tile_url, tile_id, ' .
      'access_token, attribution FROM mapper WHERE ' .
      'user = "' . $this->owner . '" AND box_id = ' . $box_id;
    if ($result = $mysqli->query($query)) {
      $mapper = $result->fetch_assoc();
      $result->close();
    }
    else {
      $this->Log('Mapper->ViewSettings: ' . $mysqli->error);
    }
    $mysqli->close();
    return $mapper;
  }

}
