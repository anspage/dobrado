<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Members extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'You don\'t have permission to view members.'];
    }

    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'list') return $this->AllData();
    if ($us_action === 'save') return $this->Save();
    if ($us_action === 'history') return $this->History();
    if ($us_action === 'download') return $this->Download();
    return ['error' => 'Unknown action'];
  }

  public function CanAdd($page) {
    return !$this->AlreadyOnPage('members', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $wide_grid_button = ' class="hidden"';
    if (isset($this->user->settings['members']['displayWideGridButton']) &&
        $this->user->settings['members']['displayWideGridButton'] === 'display')
    {
      $wide_grid_button = '';
    }
    $label = $this->Substitute('detail-description-label');
    $description = '';
    if ($label !== '') {
      $description =
        '<div class="form-spacing">' .
          '<input type="checkbox" id="members-column-description">' .
          '<label for="members-column-description">' . $label . '</label>' .
        '</div>';
    }
    $admin_content = '';
    $admin_data = '';
    // Only allow showing some restricted data if user is in admin group.
    if ($this->GroupMember('admin', 'admin')) {
      if ($this->Substitute('display-membership-reminder') === 'true') {
        $admin_content .=
          '<div class="form-spacing">' .
            '<input type="checkbox" id="members-column-membership">' .
            '<label for="members-column-membership">Membership Due</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="members-column-repeat">' .
            '<label for="members-column-repeat">Membership Repeats</label>' .
          '</div>';
      }
      $admin_content .=
        '<div class="form-spacing">' .
          '<input type="checkbox" id="members-column-balance">' .
          '<label for="members-column-balance">Balance</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input type="checkbox" id="members-column-date">' .
          '<label for="members-column-date">Last Attendance</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input type="checkbox" id="members-column-active">' .
          '<label for="members-column-active">Active Status</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input type="checkbox" id="members-column-deposit">' .
          '<label for="members-column-deposit">Paid Deposit</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input type="checkbox" id="members-column-buyer">' .
          '<label for="members-column-buyer">Buyer Rates</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input type="checkbox" id="members-column-reference">' .
          '<label for="members-column-reference">Account Reference</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input type="checkbox" id="members-column-accountName">' .
          '<label for="members-column-accountName">Account Name</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input type="checkbox" id="members-column-accountNumber">' .
          '<label for="members-column-accountNumber">Account Number</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input type="checkbox" id="members-column-bsb">' .
          '<label for="members-column-bsb">Account BSB</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input type="checkbox" id="members-column-credit">' .
          '<label for="members-column-credit">Receive Credit</label>' .
        '</div>';
      $admin_data = '<input type="radio" id="members-data-inactive-accounts" ' .
          'name="members-data-accounts">' .
        '<label for="members-data-inactive-accounts">inactive</label>';
    }
    return '<p id="members-info"></p>' .
      '<div id="members-buttons">' .
        '<span' . $wide_grid_button . '>' .
          '<input type="checkbox" id="members-wide-grid">' .
          '<label for="members-wide-grid">Wide grid</label>' .
        '</span>' .
        '<button id="members-open-display-form">Edit Display</button>' .
        '<button id="members-history">Account History</button>' .
        '<button id="members-download">Download</button> ' .
      '</div>' .
      '<form id="members-display" class="hidden">' .
        '<input type="checkbox" id="members-help">' .
        '<label for="members-help">Help</label> ' .
        '<div class="members-help-info hidden">' .
          $this->Substitute('members-help-content') .
        '</div>' .
        '<div id="members-data">' .
          '<h4>Change the data displayed in the grid:</h4>' .
          '<fieldset><legend>Account Type:</legend>' .
            '<input type="radio" id="members-data-member-accounts" ' .
              'name="members-data-accounts">' .
            '<label for="members-data-member-accounts">members</label>' .
            '<input type="radio" id="members-data-supplier-accounts" ' .
              'name="members-data-accounts">' .
            '<label for="members-data-supplier-accounts">supplier only</label>'.
            $admin_data .
          '</fieldset>' .
        '</div>' .
        '<h4>Change the columns displayed in the grid:</h4>' .
        '<div id="members-columns">' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="members-column-first">' .
            '<label for="members-column-first">First Name</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="members-column-last">' .
            '<label for="members-column-last">Last Name</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="members-column-email">' .
            '<label for="members-column-email">Email</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="members-column-phone">' .
            '<label for="members-column-phone">Phone Number</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="members-column-address">' .
            '<label for="members-column-address">Address</label>' .
          '</div>' .
          $description .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="members-column-delivery">' .
            '<label for="members-column-delivery">Delivery Group</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="members-column-notes">' .
            '<label for="members-column-notes">Notes</label>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<input type="checkbox" id="members-column-supplier">' .
            '<label for="members-column-supplier">Supplier Only</label>' .
          '</div>' .
          $admin_content .
        '</div>' .
      '</form>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.members.js to the existing dobrado.js file.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.members.js', false);

    $site_style = ['"","#members-info","text-align","center"',
                   '"","#members-buttons","display","flex"',
                   '"","#members-buttons","justify-content","space-evenly"',
                   '"","#members-buttons","align-items","start"',
                   '"","label[for=members-help]","float","right"',
                   '"","#members-columns","display","grid"',
                   '"","#members-columns","grid-template-columns","50% 50%"',
                   '"","#members-columns label","float","none"',
                   '"","#members-data label","margin-right","10px"'];

    $this->AddSiteStyle($site_style);

    $us_content = '<p>This page lists all the accounts that have been ' .
        'created in your group. You can change most of the values by ' .
        'clicking the cells in the grid and editing them. Press the ' .
        '<b>enter key</b> to save, or simply click another cell. The ' .
        '<b>Download</b> button will create a file containing the columns ' .
        'currently displayed on the screen.</p>' .
      '<p>Only some information is shown by default, you can change the ' .
        'data displayed using this dialog. The first option allows you to ' .
        'select the type of accounts you want to list. Member accounts can ' .
        'also be suppliers if you add their products to the stock page, so ' .
        '<b>Supplier Only</b> is used for accounts that do not purchase from ' .
        'your group. These accounts are also added to the import list on the ' .
        'stock page.</p>' .
      '<p>Setting an account as inactive is for normal members who have left ' .
        'the group, and this will hide them in the system and will mean they ' .
        'are no longer sent invoices.</p>' .
      '<p>The next section in this dialog allows you to select the columns ' .
        'displayed in the grid. The options shown here depends on the ' .
        'permissions for your account. If you\'re a group admin you will ' .
        'have access to more account details.</p>';
    $mysqli = connect_db();
    $content = $mysqli->escape_string($us_content);
    $mysqli->close();

    $this->AddTemplate(['"members-help-content","","' . $content . '"']);
    $this->AddSettingTypes(['"members","displayWideGridButton",' .
                              '"display,hidden","radio","When the wide grid ' .
                              'button is hidden the grid is set to the full ' .
                              'width of the window automatically."']);
    return $this->Dependencies(['banking', 'detail', 'payment', 'purchase']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // This is called when the version of the module is updated,
    // to provide a way to update or modify tables etc..
  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.members.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function AllData() {
    // Users need to be a member of the admin group to edit member data and
    // to view some information.
    $can_edit = $this->GroupMember('admin', 'admin');
    $detail = new Detail($this->user, $this->owner);
    $all_users = $detail->AllUsers(true);
    $us_column_options = json_decode($_POST['columns'], true);
    $us_sort_options = json_decode($_POST['sort'], true);
    $account_type = $_POST['accountType'];
    $data = [];
    $all_most_recent = [];
    $all_outstanding = [];
    $all_settings = [];
    $show_first = in_array('first', $us_column_options);
    $show_last = in_array('last', $us_column_options);
    $show_email = in_array('email', $us_column_options);
    $show_phone = in_array('phone', $us_column_options);
    $show_address = in_array('address', $us_column_options);
    $show_description = in_array('description', $us_column_options);
    $show_delivery = in_array('delivery', $us_column_options);
    $show_notes = in_array('notes', $us_column_options);
    $show_supplier = in_array('supplier', $us_column_options);
    $show_active = false;
    $show_membership = false;
    $show_repeat = false;
    $show_balance = false;
    $show_date = false;
    $show_reference = false;
    $show_account_name = false;
    $show_account_number = false;
    $show_bsb = false;
    $show_credit = false;
    $show_surcharge = false;
    $show_deposit = false;
    $show_buyer = false;
    
    if ($can_edit) {
      $show_active = in_array('active', $us_column_options);
      $show_reference = in_array('reference', $us_column_options);
      $show_account_name = in_array('accountName', $us_column_options);
      $show_account_number = in_array('accountNumber', $us_column_options);
      $show_bsb = in_array('bsb', $us_column_options);
      $show_credit = in_array('credit', $us_column_options);
      $show_surcharge = in_array('surcharge', $us_column_options);
      $show_deposit = in_array('deposit', $us_column_options);
      $show_buyer = in_array('buyer', $us_column_options);
      if ($this->Substitute('display-membership-reminder') === 'true') {
        $show_membership = in_array('membership', $us_column_options);
        $show_repeat = in_array('repeat', $us_column_options);
      }
      $purchase = new Purchase($this->user, $this->owner);
      if (in_array('balance', $us_column_options)) {
        $show_balance = true;
        $payment = new Payment($this->user, $this->owner);
        $payment_totals = $payment->AllTotals(true);
        $all_outstanding =
          $purchase->AllOutstanding($payment_totals, time(), true);
      }
      if (in_array('date', $us_column_options)) {
        $show_date = true;
        $all_most_recent = $purchase->AllMostRecent(time(), true);
      }
      if ($show_reference || $show_account_name ||
          $show_account_number || $show_bsb || $show_credit ||
          $show_surcharge || $show_deposit || $show_buyer) {
        $banking = new Banking($this->user, $this->owner);
        $all_settings = $banking->AllSettings();
      }
    }
    else {
      // admin permission required to view inactive account so make sure it
      // wasn't requested here.
      if ($account_type === 'inactive') $account_type = 'member';
    }

    foreach ($all_users as $username => $details) {
      if ($account_type === 'member' &&
          (!$details['active'] || $details['supplierOnly'])) {
        continue;
      }
      if ($account_type === 'supplier' &&
          (!$details['active'] || !$details['supplierOnly'])) {
        continue;
      }
      if ($account_type === 'inactive' && $details['active']) {
        continue;
      }

      $row = ['username' => $username];
      if ($show_first) {
        $row['first'] = htmlspecialchars_decode($details['first']);
      }
      if ($show_last) {
        $row['last'] = htmlspecialchars_decode($details['last']);
      }
      if ($show_email) {
        $row['email'] = htmlspecialchars_decode($details['email']);
      }
      if ($show_phone) {
        $row['phone'] = htmlspecialchars_decode($details['phone']);
      }
      if ($show_address) {
        $row['address'] = htmlspecialchars_decode($details['address']);
      }
      if ($show_description) {
        $row['description'] = htmlspecialchars_decode($details['description']);
      }
      if ($show_delivery) {
        $row['delivery'] = htmlspecialchars_decode($details['delivery']);
      }
      if ($show_notes) {
        $row['notes'] = htmlspecialchars_decode($details['notes']);
      }
      if ($show_supplier) {
        $row['supplier'] = $details['supplierOnly'];
      }
      if ($can_edit) {
        if ($show_membership) {
          $row['membership'] = $details['reminderTime'] * 1000;
        }
        if ($show_repeat) {
          $row['repeat'] = $details['reminderRepeat'];
        }
        if ($show_active) {
          $row['active'] = $details['active'];
        }
        if ($show_balance) {
          $row['balance'] = isset($all_outstanding[$username]) ?
            number_format($all_outstanding[$username] * -1, 2, '.', '') :
            '0.00';
        }
        if ($show_date) {
          $row['date'] = isset($all_most_recent[$username]) ?
            (int)$all_most_recent[$username] * 1000 : 0;
        }
        if (isset($all_settings[$username])) {
          if ($show_reference) {
            $row['reference'] = $all_settings[$username]['reference'];
          }
          if  ($show_account_name) {
            $row['accountName'] = $all_settings[$username]['name'];
          }
          if ($show_account_number) {
            $row['accountNumber'] = $all_settings[$username]['number'];
          }
          if ($show_bsb) {
            $row['bsb'] = $all_settings[$username]['bsb'];
          }
          if ($show_credit) {
            $row['credit'] = $all_settings[$username]['credit'];
          }
          if ($show_surcharge) {
            $row['surcharge'] = $all_settings[$username]['surcharge'];
          }
          if ($show_deposit) {
            $row['deposit'] = $all_settings[$username]['deposit'];
          }
          if ($show_buyer) {
            $row['buyer'] = $all_settings[$username]['buyerGroup'];
          }
        }
      }
      $data[] = $row;
    }
    if (is_array($us_sort_options) && count($us_sort_options) > 0) {
      $args = [];
      foreach ($us_sort_options as $sort) {
        $args[] = array_column($data, $sort['columnId']);
        $args[] = $sort['sortAsc'] ? SORT_ASC : SORT_DESC;
      }
      $args[] = &$data;
      call_user_func_array('array_multisort', $args);
    }
    return ['canEdit' => $can_edit, 'descriptionLabel' =>
              $this->Substitute('detail-description-label'), 'data' => $data];
  }

  private function Download() {
    $filename = 'members-'.date('Y-m-d').'.csv';
    $result = $this->AllData();
    // If membership data is included, it will be converted to a date.
    $this->CreateCSV($filename, $result['data'], true, 'membership');
    return ['filename' => $filename];
  }

  private function History() {
    $mysqli = connect_db();
    $username = $mysqli->escape_string($_POST['username']);
    $mysqli->close();

    $purchase = new Purchase($this->user, $this->owner);
    $payment = new Payment($this->user, $this->owner);
    $purchase_data = $purchase->Search($username, 0, '', '', 0, '', '', 0,
                                       time(), true);
    $sold_data = $purchase->Search('', 0, '', $username, 0, '', '', 0,
                                   time(), true);
    $payment_data = $payment->Search($username);
    $total_purchase = 0;
    $total_sold = 0;
    $total_payments = 0;
    // Create a new array, merging the three sets of data when their entries
    // share the same day.
    $data = [];
    for ($i = 0; $i < count($purchase_data); $i++) {
      $date = date('Y-m-d', $purchase_data[$i]['date'] / 1000);
      $total = $purchase_data[$i]['total'];
      $total_purchase += $total;
      if (isset($data[$date])) {
        $data[$date]['purchases'] += $total;
      }
      else {
        $data[$date] = ['purchases' => $total, 'sold' => '', 'payments' => ''];
      }
    }
    for ($i = 0; $i < count($sold_data); $i++) {
      $date = date('Y-m-d', $sold_data[$i]['date'] / 1000);
      $total = $sold_data[$i]['total'];
      $total_sold += $total;
      if (isset($data[$date])) {
        $data[$date]['sold'] += $total;
      }
      else {
        $data[$date] = ['purchases' => '', 'sold' => $total, 'payments' => ''];
      }
    }
    for ($i = 0; $i < count($payment_data['search']); $i++) {
      $date = date('Y-m-d', $payment_data['search'][$i]['date'] / 1000);
      $amount = $payment_data['search'][$i]['amount'];
      $total_payments += $amount;
      if (isset($data[$date])) {
        $data[$date]['payments'] = $amount;
      }
      else {
        $data[$date] = ['purchases' => '', 'sold' => '', 'payments' => $amount];
      }
    }
    krsort($data);
    // Now just need to format as an indexed array for CreateCSV.
    $indexed = [];
    foreach ($data as $date => $values) {
      $indexed[] = ['date' => $date, 'purchases' => $values['purchases'],
                    'sold' => $values['sold'],
                    'payments' => $values['payments']];
    }
    // And add some extra entries for totals and balance.
    $indexed[] = ['date' => '', 'purchases' => '', 'sold' => '',
                  'payments' => ''];
    $indexed[] = ['date' => 'TOTAL', 'purchases' => $total_purchase,
                  'sold' => $total_sold, 'payments' => $total_payments];
    $indexed[] = ['date' => '', 'purchases' => '', 'sold' => '',
                  'payments' => ''];
    $balance = $total_payments + $total_sold - $total_purchase;
    $indexed[] = ['date' => 'BALANCE', 'purchases' => '', 'sold' => '',
                  'payments' => price_string($balance)];
    $date = date('Y-m-d');
    $filename = 'members-' . $username . '-' . $date . '.csv';
    $this->CreateCSV($filename, $indexed, false);
    return ['filename' => $filename];
  }

  private function Save() {
    if (!$this->GroupMember('admin', 'admin')) {
      return ['error' => 'Permission denied editing members details.'];
    }

    $organiser = new Organiser($this->user, $this->owner);
    $mysqli = connect_db();
    $user = $mysqli->escape_string(htmlspecialchars($_POST['username']));
    $id = $mysqli->escape_string(htmlspecialchars($_POST['id']));
    $value = $mysqli->escape_string(htmlspecialchars(trim($_POST['value'])));
    $mysqli->close();

    if (!$organiser->MatchUser($user)) {
      return ['error' => 'User not found.'];
    }

    $detail_fields = ['first', 'last', 'phone', 'address', 'description',
                      'delivery', 'notes', 'supplier', 'membership', 'repeat'];
    if (in_array($id, $detail_fields)) {
      $detail = new Detail($this->user, $this->owner);
      if ($id === 'delivery') {
        $id = 'delivery_group';
      }
      else if ($id === 'membership') {
        $id = 'reminder_time';
        $value /= 1000;
      }
      else if ($id === 'repeat') {
        $id = 'reminder_repeat';
      }
      else if ($id === 'supplier') {
        $id = 'supplier_only';
        $value = $value === 'true' ? 1 : 0;
      }
      return $detail->UpdateField($user, $id, $value);
    }

    $banking_fields = ['reference', 'accountName', 'accountNumber', 'bsb',
                       'credit', 'surcharge', 'deposit', 'buyer'];
    if (in_array($id, $banking_fields)) {
      if ($id === 'accountName') $id = 'name';
      else if ($id === 'accountNumber') $id = 'number';
      else if ($id === 'buyer') $id = 'buyer_group';
      else if ($id === 'deposit') $value = $value === 'true' ? 1 : 0;
      else if ($id === 'credit') $value = $value === 'true' ? 1 : 0;
      $banking = new Banking($this->user, $this->owner);
      return $banking->UpdateField($user, $id, $value);
    }

    $mysqli = connect_db();
    if ($id === 'email') {
      $query = 'UPDATE users SET email = "'.$value.'" WHERE user = "'.$user.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Members->Save 1: '.$mysqli->error);
      }
    }
    else if ($id === 'active') {
      $active = $value === 'true' ? 1 : 0;
      $query = 'UPDATE users SET active = '.$active.' WHERE user = "'.$user.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Members->Save 2: '.$mysqli->error);
      }
    }
    $mysqli->close();
    return ['done' => true];
  }

}
