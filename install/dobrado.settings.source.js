/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2018 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.settings) {
  dobrado.settings = {};
}
(function() {

  'use strict';

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.settings').length === 0) {
      return;
    }

    $('.settings input[type=checkbox]').checkboxradio();
    $('.settings input[type=radio]').checkboxradio();
    $('.settings .datepicker').datepicker({
      dateFormat: dobrado.dateFormat });
    $('.settings .show-password').button().click(function() {
      if ($(this).siblings('input').attr('type') === 'password') {
        $(this).siblings('input').attr('type', 'text');
      }
      else {
        $(this).siblings('input').attr('type', 'password');
      }
    });
    $('.settings :input').change(save);
  });

  function save() {
    dobrado.log('Saving setting.', 'info');
    $.post('/php/request.php',
           { request: 'settings', action: 'save', id: $(this).attr('id'),
             value: $(this).val(), url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'settings save')) {
          return false;
        }
      });
  }

}());
