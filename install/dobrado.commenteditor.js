// @source: /js/source/dobrado.commenteditor.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2017 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.commenteditor){dobrado.commenteditor={};}
(function(){'use strict';$(function(){if($('.commenteditor').length===0){return;}
$('#comment-form > button.submit').button().click(submitComment);$('.comments-editor').button({icon:'ui-icon-pencil',showLabel:false}).click(extendedEditor);dobrado.notify('comment',dobrado.commenteditor.update);});function submitComment(event){event.preventDefault();dobrado.log('Adding comment...','info');var id='#'+$('.commenteditor').attr('id');$.post('/php/request.php',{id:id,request:'commenteditor',mode:'submit',name:$('#comment-name').val(),website:$('#comment-url').val(),content:$('#comment-content').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'submitComment')){return;}
$('#comment-name').val('');$('#comment-url').val('');$('#comment-content').val('');$.post('/php/request.php',{request:'post',action:'newComment',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'newComment')){return;}});});return false;}
function extendedEditor(){dobrado.log('Editor loading...','info');dobrado.current='#'+$(this).parents('.commenteditor').attr('id');$(dobrado.current).data('label','commenteditor');$.post('/php/request.php',{id:dobrado.current,label:'commenteditor',content:'',request:'extended',mode:'box',url:location.href,token:dobrado.token},dobrado.extended.editor);}
dobrado.commenteditor.update=function(action){$.post('/php/request.php',{request:'comment',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'latestComment')){return;}
var comment=JSON.parse(response);if($('#'+comment.id).length===0){$('<div>'+comment.content+'</div>').attr({id:comment.id}).addClass('comment').insertBefore('.commenteditor');$('#'+comment.id).data('label','comment');}
else{$('#'+comment.id).html(comment.content);}
$('#'+comment.id).addClass('highlight');setTimeout(function(){$('#'+comment.id).removeClass('highlight');},3000);});};}());