/*global dobrado: true, Slick: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.members) {
  dobrado.members = {};
}
(function() {

  'use strict';

  var members = {};
  var grid = null;
  var gridId = '';
  // Store the selected user in the grid for downloading member history.
  var selectedUser = '';
  // List of all columns that can be displayed in the grid, the user can
  // decide which columns they want to show. It's populated in gridSetup once
  // we know if there's a grid module on the page so that Slick is available.
  var allColumns = [];

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.members').length === 0) {
      return;
    }

    let mobile = $('.dobrado-mobile').is(':visible');
    if (!mobile) {
      $('#members-display').dialog({
        show: true, autoOpen: false, width: 760, height: 450,
        position: { my: 'top', at: 'top+50', of: window },
        title: 'Accounts Display',
        create: dobrado.fixedDialog });
    }

    $('#members-open-display-form').button().click(function() {
      if (mobile) {
        $('#members-display').toggle();
        // Simplify the page on mobile by hiding the grid when members-display
        // is open.
        $(gridId).toggle();
      }
      else {
        $('#members-display').dialog('open');
      }
    });
    $('#members-data input').click(updateDisplay);
    // Clear column display option checkboxes and set from local storage.
    $('#members-columns input').prop('checked', false).click(updateColumns);
    $('#members-history').button().click(history);
    $('#members-download').button().click(download);
    $('#members-help').button().click(function() {
      $('.members-help-info').toggle();
    });
    // If columns isn't set in local storage create a default.
    var columns = JSON.stringify(['username','first','last','email','phone']);
    var sort = '';
    var accountType = 'member';
    if (dobrado.localStorage) {
      if (localStorage.membersColumns) {
        columns = localStorage.membersColumns;
      }
      if (localStorage.membersSortColumns) {
        sort = localStorage.membersSortColumns;
      }
      if (localStorage.membersAccountType) {
        accountType = localStorage.membersAccountType;
      }
      $('#members-data-' + accountType + '-accounts').prop('checked', true);
    }

    $('#members-info').html('Viewing <b>' + accountType + '</b> accounts.' +
                            ' Click Edit Display to change data.');
    dobrado.log('Loading accounts...', 'info');
    $.post('/php/request.php',
           { request: 'members', action: 'list', columns: columns,
             sort: sort, accountType: accountType,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'members list')) {
          return;
        }
        members = JSON.parse(response);
        if ($('.grid').length !== 0) {
          gridSetup();
        }
        else {
          alert('Please add a grid module to this page.');
        }
      });
  });

  function history() {
    if (selectedUser === '') {
      alert('Please select an account from the grid.');
      return;
    }

    dobrado.log('Downloading account history...', 'info');
    $.post('/php/request.php',
           { request: 'members', action: 'history', username: selectedUser,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'members history')) {
          return;
        }
        var result = JSON.parse(response);
        if (result.filename) {
          location.href = "/php/private.php?file=" + result.filename;
        }
      });
  }

  function download() {
    var columns = '';
    var sort = '';
    var accountType = 'member';
    if (dobrado.localStorage) {
      if (localStorage.membersColumns) {
        columns = localStorage.membersColumns;
      }
      if (localStorage.membersSortColumns) {
        sort = localStorage.membersSortColumns;
      }
      if (localStorage.membersAccountType) {
        accountType = localStorage.membersAccountType;
      }
    }
    else {
      alert('Could not find selected columns.');
      return;
    }

    dobrado.log('Downloading account data...', 'info');
    $.post('/php/request.php',
           { request: 'members', action: 'download', columns: columns,
             sort: sort, accountType: accountType,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'members download')) {
          return;
        }
        var result = JSON.parse(response);
        if (result.filename) {
          location.href = "/php/private.php?file=" + result.filename;
        }
      });
  }
  
  function gridSetup() {
    gridId = '#' + $('.grid').attr('id');
    var columns = [];
    var columnIds = [];
    // Set editable based on permission given from the server.
    var options = { autoHeight: true, editable: members.canEdit,
                    forceFitColumns: true, multiColumnSort: true };
    allColumns = [{ id: 'username', name: 'Username', field: 'username',
                    width: 100, sortable: true },
                  { id: 'first', name: 'First Name', field: 'first',
                    width: 150, sortable: true, editor: Slick.Editors.Text },
                  { id: 'last', name: 'Last Name', field: 'last',
                    width: 150, sortable: true, editor: Slick.Editors.Text },
                  { id: 'email', name: 'Email', field: 'email',
                    width: 100, sortable: true, editor: Slick.Editors.Text },
                  { id: 'phone', name: 'Phone Number', field: 'phone',
                    width: 100, sortable: true, editor: Slick.Editors.Text },
                  { id: 'address', name: 'Address', field: 'address',
                    width: 200, sortable: true,
                    editor: Slick.Editors.LongText },
                  { id: 'description', name: members.descriptionLabel,
                    field: 'description', width: 200, sortable: true,
                    editor: Slick.Editors.LongText },
                  { id: 'delivery', name: 'Delivery Group', field: 'delivery',
                    width: 100, sortable: true, editor: Slick.Editors.Text },
                  { id: 'notes', name: 'Notes', field: 'notes',
                    width: 200, sortable: true,
                    editor: Slick.Editors.LongText },
                  { id: 'supplier', name: 'Supplier Only',
                    field: 'supplier', width: 100, sortable: true,
                    formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox },
                  { id: 'membership', name: 'Membership Due',
                    field: 'membership', width: 100, sortable: true,
                    formatter: Slick.Formatters.Timestamp,
                    editor: Slick.Editors.Date },
                  { id: 'repeat', name: 'Membership Repeats',
                    field: 'repeat', width: 100, sortable: true,
                    editor: Slick.Editors.MembershipEditor },
                  { id: 'balance', name: 'Balance', field: 'balance',
                    width: 80, sortable: true,
                    formatter: Slick.Formatters.Dollar },
                  { id: 'date', name: 'Last Attendance',
                    field: 'date', width: 80, sortable: true,
                    formatter: Slick.Formatters.Timestamp },
                  { id: 'deposit', name: 'Paid Deposit', field: 'deposit',
                    width: 50, sortable: true,
                    formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox },
                  { id: 'buyer', name: 'Buyer Rates', field: 'buyer',
                    width: 50, sortable: true,
                    editor: Slick.Editors.BuyerEditor },
                  { id: 'active', name: 'Active', field: 'active',
                    width: 40, sortable: true,
                    formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox },
                  { id: 'reference', name: 'Account Reference',
                    field: 'reference', width: 100, sortable: true,
                    editor: Slick.Editors.Text },
                  { id: 'accountName', name: 'Account Name',
                    field: 'accountName', width: 100, sortable: true,
                    editor: Slick.Editors.Text },
                  { id: 'accountNumber', name: 'Account Number',
                    field: 'accountNumber', width: 100, sortable: true,
                    editor: Slick.Editors.Text },
                  { id: 'bsb', name: 'Account BSB', field: 'bsb',
                    width: 100, sortable: true, editor: Slick.Editors.Text },
                  { id: 'credit', name: 'Receive Credit', field: 'credit',
                    width: 80, sortable: true,
                    formatter: Slick.Formatters.Checkmark,
                    editor: Slick.Editors.Checkbox }];

    if (dobrado.localStorage) {
      if (localStorage.membersColumns) {
        columnIds = JSON.parse(localStorage.membersColumns);
      }
      else {
        // If columns isn't set in local storage create a default.
        columnIds = ['username', 'first', 'last','email','phone'];
        localStorage.membersColumns = JSON.stringify(columnIds);
      }
      $.each(columnIds, function(i, id) {
        $('#members-column-' + id).prop('checked', true);
        $.each(allColumns, function(i, item) {
          if (item.id === id) {
            columns.push(item);
            return false;
          }
        });
      });
    }
    grid = dobrado.grid.instance(gridId, members.data, columns, options);
    grid.setSelectionModel(new Slick.RowSelectionModel());
    grid.onClick.subscribe(function(e, item) {
      selectedUser = members.data[item.row].username;
    });
    grid.onCellChange.subscribe(updateMember);
    grid.onSort.subscribe(function(e, args) {
      var cols = args.sortCols;
      members.data.sort(function(row1, row2) {
        for (var i = 0; i < cols.length; i++) {
          var field = cols[i].sortCol.field;
          var sign = cols[i].sortAsc ? 1 : -1;
          var value1 = row1[field];
          var value2 = row2[field];
          if (field === 'balance' || field === 'date') {
            value1 = value1 ? parseFloat(value1) : 0;
            value2 = value2 ? parseFloat(value2) : 0;
          }
          if (value1 === value2) {
            continue;
          }
          if (value1 > value2) {
            return sign;
          }
          else {
            return sign * -1;
          }
        }
        return 0;
      });
      grid.invalidate();
      if (dobrado.localStorage) {
        localStorage.membersSortColumns = JSON.stringify(grid.getSortColumns());
      }
    });
    $('#members-wide-grid').prop('checked', false).
      checkboxradio({ icon: false }).click(function() {
        dobrado.grid.setup('members', 'wide', grid, gridId);
      });
    if (dobrado.localStorage) {
      if (localStorage.membersWideGrid === 'true') {
        // Want to call toggleWideGrid and check the checkbox, this does both.
        $('#members-wide-grid').click();
      }
      if (localStorage.membersSortColumns) {
        grid.setSortColumns(JSON.parse(localStorage.membersSortColumns));
      }
    }
    dobrado.grid.setup('members', 'fixed', grid, gridId);
  }

  function updateColumns() {
    var reload = false;
    // When updating columns always add the username column first.
    var columns = [allColumns[0]];
    var columnIds = ['username'];
    $('#members-columns input:checked').each(function() {
      var id = $(this).attr('id').match(/([^-]+)$/)[1];
      $.each(allColumns, function(i, item) {
        if (item.id === id) {
          columns.push(item);
          columnIds.push(id);
          return false;
        }
      });
    });
    if (dobrado.localStorage) {
      let currentColumns = JSON.parse(localStorage.membersColumns);
      // If the column count hasn't changed then don't do anything.
      // (ie not interested in this click event.)
      if (columnIds.length === currentColumns.length) {
        return;
      }

      // Fetch new data when a column is added.
      if (columnIds.length > currentColumns.length) {
        reload = true;
      }
      else {
        // When a column is being removed, need to reset the sort order.
        localStorage.membersSortColumns = '';
      }
      localStorage.membersColumns = JSON.stringify(columnIds);
    }
    if (grid) {
      grid.resetActiveCell();
      grid.setSelectedRows([]);
      grid.setColumns(columns);
      if (reload) {
        updateDisplay();
      }
      else {
        grid.setSortColumns([]);
        grid.render();
      }
    }
  }

  function updateDisplay() {
    var sort = '';
    var columns = '';
    var accountType = $('input[name=members-data-accounts]:checked').attr('id');
    if (accountType === 'members-data-supplier-accounts') {
      accountType = 'supplier';
    }
    else if (accountType === 'members-data-inactive-accounts') {
      accountType = 'inactive';
    }
    else {
      accountType = 'member';
    }
    if (dobrado.localStorage) {
      if (localStorage.membersSortColumns) {
        sort = localStorage.membersSortColumns;
      }
      if (localStorage.membersColumns) {
        columns = localStorage.membersColumns;
      }
      localStorage.membersAccountType = accountType;
    }

    dobrado.log('Loading new data...', 'info');
    $.post('/php/request.php',
           { request: 'members', action: 'list', columns: columns,
             sort: sort, accountType: accountType,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'members list')) {
          return;
        }
        members = JSON.parse(response);
        grid.setData(members.data);
        if (sort !== '') {
          grid.setSortColumns(JSON.parse(sort));
        }
        grid.updateRowCount();
        grid.render();
        $('#members-info').html('Viewing <b>' + accountType + '</b> accounts.' +
                                ' Click Edit Display to change data.');

      });
  }

  function updateMember(e, args) {
    // Get the id of the column that contains the updated cell.
    var columns = grid.getColumns();
    var id = columns[args.cell].id;

    dobrado.log('Updating account.', 'info');
    $.post('/php/request.php',
           { request: 'members', action: 'save', id: id,
             value: args.item[id], username: args.item.username,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'members save')) {
          return;
        }
      });
  }

  dobrado.members.settingsCallback = function(settings) {
    if (dobrado.localStorage && settings.displayWideGridButton === 'hidden') {
      localStorage.membersWideGrid = 'true';
    }
  };

}());
