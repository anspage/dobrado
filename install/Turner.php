<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Turner extends Base {

  public function Add($id) {
    // Add default content here so that SetContent doesn't
    // need to check if a table row already exists.
    $this->Insert($id);
  }

  public function Callback() {
    $object = [];
    $mysqli = connect_db();
    // 'mode' is used by the Extended module, which calls this function.
    if (isset($_POST['mode']) && $_POST['mode'] === 'box') {
      $id = isset($_POST['id']) ? (int)substr($_POST['id'], 9) : 0;
      $query = 'SELECT content FROM turner WHERE user = "'.$this->owner.'" '.
        'AND box_id = '.$id;
      if ($result = $mysqli->query($query)) {
        if ($turner = $result->fetch_assoc()) {
          $object['editor'] = true;
          $object['source'] = $turner['content'];
        }
        $result->close();
      }
      else {
        $this->Log('Turner->Callback: '.$mysqli->error);
      }
    }
    $mysqli->close();
    return $object;
  }

  public function CanAdd($page) {
    // Must have admin access to add the turner module.
    return $this->user->canEditSite;
  }

  public function CanEdit($id) {
    return true;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    return '<div class="dobrado-editable">'.$this->PlainContent($id).'</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    $this->Insert($id, $this->PlainContent($old_id, $old_owner, true));
    $this->CopyStyle($id, $old_owner, $old_id);
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.turner.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.turner.js');
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS turner ('.
      'user VARCHAR(50) NOT NULL,'.
      'box_id INT UNSIGNED NOT NULL,'.
      'content TEXT,'.
      'timestamp INT(10) UNSIGNED NOT NULL,'.
      'PRIMARY KEY(user, box_id)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Turner->Install 1: '.$mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS turner_history ('.
      'user VARCHAR(50) NOT NULL,'.
      'box_id INT UNSIGNED NOT NULL,'.
      'content TEXT,'.
      'timestamp INT(10) UNSIGNED NOT NULL,'.
      'modified_by VARCHAR(50) NOT NULL,'.
      'PRIMARY KEY(user, box_id, timestamp)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Turner->Install 2: '.$mysqli->error);
    }
    $mysqli->close();
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $query = 'DELETE FROM turner WHERE user = "'.$this->owner.'" '.
        'AND box_id = '.$id;
      if (!$mysqli->query($query)) {
        $this->Log('Turner->Remove 1: '.$mysqli->error);
      }
      $query = 'DELETE FROM turner_history WHERE user = "'.$this->owner.'" '.
        'AND box_id = '.$id;
      if (!$mysqli->query($query)) {
        $this->Log('Turner->Remove 2: '.$mysqli->error);
      }
    }
    else {
      $query = 'DELETE FROM turner WHERE user = "'.$this->owner.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Turner->Remove 3: '.$mysqli->error);
      }
      $query = 'DELETE FROM turner_history WHERE user = "'.$this->owner.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Turner->Remove 4: '.$mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {
    if ($us_content['data'] === $this->PlainContent($id)) return;

    $time = time();
    $mysqli = connect_db();
    $data = $mysqli->escape_string($us_content['data']);
    $query = 'UPDATE turner SET content = "'.$data.'", timestamp = '.$time.
      ' WHERE user = "'.$this->owner.'" AND box_id = '.$id;
    if (!$mysqli->query($query)) {
      $this->Log('Turner->SetContent 1: '.$mysqli->error);
    }

    $query = 'INSERT INTO turner_history VALUES ("'.$this->owner.'", '.
      $id.', "'.$data.'", '.$time.', "'.$this->user->name.'")';
    if (!$mysqli->query($query)) {
      $this->Log('Turner->SetContent 2: '.$mysqli->error);
    }
    $mysqli->close();
  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Append dobrado.turner.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.turner.js');
  }

  // Private functions below here ////////////////////////////////////////////

  private function Insert($id, $content='') {
    $time = time();
    $mysqli = connect_db();
    $query = 'INSERT INTO turner VALUES '.
      '("'.$this->owner.'", '.$id.', "'.$content.'", '.$time.')';
    if (!$mysqli->query($query)) {
      $this->Log('Turner->Insert 1: '.$mysqli->error);
    }

    $query = 'INSERT INTO turner_history VALUES ("'.$this->owner.'", '.
      $id.', "'.$content.'", '.$time.', "'.$this->user->name.'")';
    if (!$mysqli->query($query)) {
      $this->Log('Turner->Insert 2: '.$mysqli->error);
    }
    $mysqli->close();
  }

  private function PlainContent($id, $user = '', $escape = false) {
    if ($user === '') {
      $user = $this->owner;
    }
    $content = '';
    $mysqli = connect_db();
    $query = 'SELECT content FROM turner WHERE user = "'.$user.'" '.
      'AND box_id = '.$id;
    if ($result = $mysqli->query($query)) {
      if ($turner = $result->fetch_assoc()) {
        $content = $escape ? $mysqli->escape_string($turner['content']) :
          $turner['content'];
      }
      $result->close();
    }
    else {
      $this->Log('Turner->PlainContent: '.$mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

}
