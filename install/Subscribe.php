<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Subscribe extends Base {

  public function Add($id) {

  }

  public function Callback() {

  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $current_page = $this->user->page;
    if (!isset($_GET['feed']) || $_GET['feed'] === '') {
      $this->user->page = 'index';
    }
    else if (preg_match('/^[a-z0-9_-]{1,200}$/i', $_GET['feed'])) {
      $this->user->page = $_GET['feed'];
    }
    // Only show content if the feed page is published or visible to the
    // currently logged in user.
    $published_page = false;
    $mysqli = connect_db();
    $query = 'SELECT published FROM published WHERE ' .
      'user = "' . $this->owner . '" AND page = "' . $this->user->page . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($published = $mysqli_result->fetch_assoc()) {
        $published_page = $published['published'] === '1';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Subscribe->Content: ' . $mysqli->error);
    }
    $mysqli->close();

    if (!$published_page) {
      $user = new User();
      $user->SetPermission($this->user->page, $this->user->name);
      if (!$user->canViewPage) return false;
    }

    $reader = new Reader($this->user, $this->owner);
    $feed_list = $reader->CreateFeedList('mf2');
    $this->user->page = $current_page;
    return '<div class="h-feed">' . $feed_list . '</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {

  }

  public function Group() {

  }

  public function IncludeScript() {
    return false;
  }

  public function Install($path) {
    $site_style = ['"",".subscribe .h-card","margin","10px"',
                   '"",".subscribe .h-card","padding","10px"',
                   '"",".subscribe .h-card","border-radius","5px"',
                   '"",".subscribe .h-card","color","#222222"',
                   '"",".subscribe .h-card","background-color","#efefef"',
                   '"",".subscribe .h-card .thumb","height","50px"',
                   '"",".subscribe .h-card .thumb","float","left"',
                   '"",".subscribe .h-card .thumb","margin-right","5px"',
                   '"",".subscribe .h-card .thumb","border-radius","5px"',
                   '"",".subscribe .h-card a.p-name","font-size","1.2em"',
                   '"",".subscribe .h-card a.p-name","text-decoration","none"',
                   '"",".subscribe .h-card a.p-name","color","#222222"',
                   '"",".subscribe .h-card a.p-name:hover","color","#999999"',
                   '"",".subscribe .h-card .clear","clear","both"'];
    $this->AddSiteStyle($site_style);
    return $this->Dependencies(['reader']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {

  }

}
