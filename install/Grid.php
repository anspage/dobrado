<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Grid extends Base {

  public function Add($id) {

  }

  public function Callback() {

  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    return '<div class="view"></div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $this->AppendScript($path, 'dobrado.grid.js', false);
    $site_style = ['"",".grid","margin-top","10px"',
                   '"",".grid","clear","both"',
                   '"",".grid","border","1px solid #aaaaaa"',
                   '"",".grid","border-radius","2px"',
                   '"",".grid .view","width","100%"',
                   '"",".slick-header","height","24px"',
                   '"",".slick-header.fixed-style","position","fixed"',
                   '"",".slick-header.fixed-style","top","48px"',
                   '"",".slick-header.fixed-style","z-index","12"',
                   '"",".slick-headerrow.fixed-style","position","fixed"',
                   '"",".slick-headerrow.fixed-style","top","74px"',
                   '"",".slick-headerrow.fixed-style","z-index","12"',
                   '"",".slick-headerrow-column input","box-sizing",' .
                     '"border-box"',
                   '"",".slick-headerrow-column input","height","100%"',
                   '"",".slick-headerrow-column input","width","100%"',
                   '"",".slick-headerrow-column.ui-state-default","padding",' .
                     '"0"',
                   '"",".slick-headerrow-column.ui-state-default",' .
                     '"box-sizing","border-box"',
                   '"",".slick-headerrow-column.ui-state-default","height",' .
                     '"100%"',
                   '"",".slick-cell.selected","background-color","#fdffaf"',
                   '"",".slick-cell > input[type=text]","padding","0"',
                   '"",".slick-cell > input[type=text]","border","none"',
                   '"",".slick-cell > input[type=text]","width","100%"',
                   '"",".slick-cell > input[type=text]","height","100%"',
                   '"",".slick-cell > a","text-decoration","none"',
                   '"","button.long-text-save","float","right"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.grid.js', false);
  }

}
