<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Viewanalytics extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $start = (int)$_POST['start'] / 1000;
    $end = (int)$_POST['end'] / 1000;
    if ($end === 0) $end = time();
    if ($start === 0) $start = $end - (7 * 86400);
    // Logs are stored with UTC timestamps, so convert the given start and
    // end dates in local time to UTC. Want to display the data with local
    // timestamps however, so also calculate the offset to local time.
    $offset = strtotime('UTC') - strtotime('now');
    $start = strtotime(date('F j Y 00:00:00', $start) . ' UTC');
    $end = strtotime(date('F j Y 23:59:59', $end) . ' UTC');
    if (!$start || !$end || $start > $end) {
      return ['error' => 'Start and End dates required.'];
    }

    $data = [];
    $analytics = new Analytics($this->user, $this->owner);
    $referers = $analytics->Referers($start, $end);
    // Want to show up to 10 data points in the graph, so find the number of
    // hours being shown and calculate the number of hours in each data point.
    $hours = (int)ceil(($end - $start) / 3600);
    $x_axis = $hours > 48 ? '%d %b' : '%d %b %#I%p';
    $segment = $hours > 10 ? (int)ceil($hours / 10) : 1;
    $next = $start + ($segment * 3600);
    for ($i = 0; $i < 10; $i++) {
      // Make sure $next isn't greater than the requested end date.
      if ($next > $end) $next = $end;
      $data[] = [($start - $offset) * 1000, $analytics->Total($start, $next)];
      $start = $next + 1;
      $next += $segment * 3600;
    }
    return ['referers' => $referers, 'series' => [['showMarker' => false]],
            'data' => ['type' => 'time', 'x_axis' => $x_axis,
                       'y_axis' => '%d', 'min' => 0, 'data' => [$data]]];
  }

  public function CanAdd($page) {
    return $this->user->canEditSite;
  }

  public function CanEdit($id) {
    return true;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    return '<form id="viewanalytics-total-search">' .
        'Enter dates to view total visits:' .
        '<div class="form-spacing">' .
          '<label for="viewanalytics-total-start">Start:</label>' .
          '<input id="viewanalytics-total-start" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="viewanalytics-total-end">End:</label>' .
          '<input id="viewanalytics-total-end" maxlength="50">' .
        '</div>' .
        '<button id="viewanalytics-total-button">submit</button>' .
      '</form>' .
      '<div class="viewanalytics-total-graph"></div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Need to call AppendScript here if module uses javascript.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.viewanalytics.js', false);
    $site_style = ['"","#viewanalytics-total-search label","width","4em"',
                   '"","#viewanalytics-total-button","margin-left","3.9em"'];
    return $this->Dependencies(['analytics']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.viewanalytics.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

}
