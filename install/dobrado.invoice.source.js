/*global dobrado: true, Slick: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.invoice) {
  dobrado.invoice = {};
}
(function() {

  'use strict';

  // This is a representation of users in json.
  var userDetails = [];
  // This is the current search data loaded into the grid.
  var gridData = [];
  // This is an instance of slick grid, if available on the page.
  var grid = null;

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.invoice').length === 0) {
      return;
    }

    $('#invoice-date-input').change(clearSearchDates).
      datepicker({ dateFormat: dobrado.dateFormat });
    $('#invoice-start-input').val('').change(clearDate).
      datepicker({ dateFormat: dobrado.dateFormat });
    $('#invoice-end-input').val('').change(clearDate).
      datepicker({ dateFormat: dobrado.dateFormat });
    $('#invoice-form .toggle-search-options').click(function() {
      clearDate();
      clearSearchDates();
      $('#invoice-form .search-options').toggle();
      return false;
    });
    $('#invoice-date-options').selectmenu({ change: changeSearchDates });
    $('#invoice-form .default-action').click(search);
    $('#invoice-form .search').button().click(search);
    $('#invoice-form .submit').button().click(submit);
    $('#invoice-form .clear').button().click(clearForm);
    $('#invoice-form .remove').button().click(remove);

    dobrado.log('Loading data...', 'info');
    $.post('/php/request.php',
           { request: 'invoice', action: 'search', exportData: 0, username: '',
             invoiceDate: '', invoiceNumber: '', startDate: '', endDate: '',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'default search')) {
          return;
        }
        var search = JSON.parse(response);
        userDetails = search.userDetails;
        autocompleteSetup();
        gridData = search.data;
        if ($('.grid').length !== 0) {
          gridSetup();
        }
      });
  });

  function autocompleteSetup() {

    function usernameUpdate(event, ui) {
      var fullname = '';
      var user = $('#invoice-username-input').val();
      $('#invoice-fullname-input').val('');
      $('#invoice-email-input').val('');
      // de-select grid row.
      if (grid) {
        grid.setSelectedRows([]);
      }
      if (ui) {
        user = ui.item.value;
      }
      if (userDetails[user]) {
        let details = userDetails[user];
        if (details.first) {
          fullname = details.first;
        }
        if (details.last) {
          if (fullname !== '') {
            fullname += ' ';
          }
          fullname += details.last;
        }
        $('#invoice-fullname-input').val(dobrado.decode(fullname));
        $('#invoice-email-input').val(dobrado.decode(details.email));
      }
    }

    function fullnameUpdate(event, ui) {
      var fullname = $('#invoice-fullname-input').val();
      $('#invoice-username-input').val('');
      $('#invoice-email-input').val('');
      // de-select grid row.
      if (grid) {
        grid.setSelectedRows([]);
      }
      if (ui) {
        fullname = ui.item.value;
      }
      $.each(userDetails, function(key, value) {
        var check = value.first;
        if (value.last !== '') {
          if (check !== '') {
            check += ' ';
          }
          check += value.last;
        }
        if (fullname === dobrado.decode(check)) {
          $('#invoice-username-input').val(key);
          $('#invoice-email-input').val(dobrado.decode(value.email));
          return false;
        }
      });
    }

    var fullname = '';
    var fullnameList = [];
    var usernameList = [];
    $.each(userDetails, function(key, value) {
      fullname = '';
      if (value.first) {
        fullname = dobrado.decode(value.first);
      }
      if (value.last) {
        if (fullname !== '') {
          fullname += ' ';
        }
        fullname += dobrado.decode(value.last);
      }
      if (fullname !== '') {
        fullnameList.push(fullname);
      }
      usernameList.push(key);
    });
    $('#invoice-username-input').autocomplete({ minLength: 1,
                                                search: dobrado.fixAutoCompleteMemoryLeak,
                                                source: usernameList,
                                                select: usernameUpdate });
    $('#invoice-username-input').change(usernameUpdate);
    $('#invoice-fullname-input').autocomplete({ minLength: 1,
                                                search: dobrado.fixAutoCompleteMemoryLeak,
                                                source: fullnameList,
                                                select: fullnameUpdate });
    $('#invoice-fullname-input').change(fullnameUpdate);
  }

  function changeSearchDates(event, ui) {
    var option = $('#invoice-date-options').val();
    if (option === 'current' || option === 'previous') {
      let today = new Date();
      let year = today.getFullYear();
      if (today.getMonth() < 6) {
        // The current financial year started last year.
        year--;
      }
      if (option === 'previous') {
        // Take another year off for the previous financial year.
        year--;
      }
      $('#invoice-start-input').datepicker('setDate',
                                           dobrado.formatDate(year, 7, 1));
      $('#invoice-end-input').datepicker('setDate',
                                         dobrado.formatDate(year + 1, 6, 30));
      return;
    }

    // Otherwise option is the number of days back to set the start date.
    var count = parseInt(option, 10);
    if (isNaN(count) || count === 0) {
      $('#invoice-start-input').val('');
      $('#invoice-end-input').val('');
      return;
    }

    let end = Date.now();
    let start = end - count * 86400000;
    $('#invoice-start-input').datepicker('setDate', dobrado.formatDate(start));
    $('#invoice-end-input').datepicker('setDate', dobrado.formatDate(end));
  }

  function clearDate() {
    $('#invoice-date-input').val('');
  }

  function clearForm() {
    $('#invoice-username-input').val('');
    $('#invoice-fullname-input').val('');
    $('#invoice-email-input').val('');
    $('#invoice-date-input').val('');
    $('#invoice-number-input').val('');
    $('#invoice-start-input').val('');
    $('#invoice-end-input').val('');
    return false;
  }

  function clearSearchDates() {
    $('#invoice-start-input').val('');
    $('#invoice-end-input').val('');
  }

  function gridSetup() {
    var columns =
      [{ id : 'date', name: 'Date', field: 'date', width: 110, sortable: true,
         formatter: Slick.Formatters.Timestamp },
       { id : 'name', name: 'Username', field: 'name', width: 140,
         sortable: true },
       { id : 'purchases', name: 'Purchases', field: 'purchases', width: 80,
         sortable: true, formatter: Slick.Formatters.Dollar }];
    if ($('.dobrado-mobile').is(':hidden')) {
      columns.push({ id : 'surcharge', name: 'Surcharge', field: 'surcharge',
                     width: 80, sortable: true,
                     formatter: Slick.Formatters.Dollar });
      columns.push({ id : 'number', name: 'Invoice Number', field: 'number',
                     width: 120, sortable: true });
    }
    var options = { autoHeight: true, forceFitColumns: true };
    var id = '#' + $('.grid').attr('id');
    grid = dobrado.grid.instance(id, gridData, columns, options);
    grid.setSelectionModel(new Slick.RowSelectionModel());
    grid.onClick.subscribe(function(e, item) {
      show(gridData[item.row]);
    });
    grid.onSelectedRowsChanged.subscribe(function(e, item) {
      if (item.rows.length === 1) {
        show(gridData[item.rows[0]]);
      }
      else {
        clearForm();
      }
    });
    grid.onSort.subscribe(function (e, args) {
      gridData.sort(function(row1, row2) {
        var field = args.sortCol.field;
        var sign = args.sortAsc ? 1 : -1;
        var value1 = row1[field];
        var value2 = row2[field];
        if (field !== 'name') {
          value1 = parseFloat(value1);
          value2 = parseFloat(value2);
        }
        if (value1 === value2) {
          return 0;
        }
        if (value1 > value2) {
          return sign;
        }
        else {
          return sign * -1;
        }
      });
      grid.invalidate();
    });
  }

  function remove() {
    var data = [];
    var selected = [];
    selected = grid.getSelectedRows();
    if (selected.length === 0) {
      return false;
    }

    var message = 'Are you sure you want to remove this invoice and ' +
      'all associated purchase data?';
    if (selected.length > 1) {
      message = 'Are you sure you want to remove ' + selected.length +
        ' invoices and all associated purchase data?';
    }
    if (!confirm(message)) {
      return false;
    }

    $.each(selected, function(i, row) {
      data.push(gridData[row]);
    });
    dobrado.log('Removing invoice...', 'info');
    $.post('/php/request.php',
           { request: 'invoice', action: 'remove', data: JSON.stringify(data),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'remove')) {
          return;
        }
        clearForm();
        // Run search to update the grid with the selected invoices removed.
        search();
      });
    return false;
  }

  function show(item) {
    var username = item.name;
    if (userDetails[username]) {
      let fullname = userDetails[username].first;
      if (fullname !== '') {
        fullname += ' ' + userDetails[username].last;
      }
      $('#invoice-fullname-input').val(fullname);
      $('#invoice-email-input').val(userDetails[username].email);
    }
    $('#invoice-username-input').val(username);
    $('#invoice-date-input').datepicker('setDate',
                                        dobrado.formatDate(item.date));
    $('#invoice-number-input').val(item.number);
    $('#invoice-purchase-id-input').val(item.purchaseId);
    $('.invoice .search-options').hide();
  }

  function search() {
    var exportData = $('#invoice-export-data:checked').length;
    var startDate = $('#invoice-start-input').val();
    var endDate = $('#invoice-end-input').val();
    if (startDate === '' && endDate !== '') {
      alert('Please pick a start date.');
      return false;
    }
    if (startDate !== '' && endDate === '') {
      alert('Please pick an end date.');
      return false;
    }

    dobrado.log('Searching...', 'info');
    $.post('/php/request.php',
           { request: 'invoice', action: 'search', exportData: exportData,
             username: $('#invoice-username-input').val(),
             invoiceDate: $('#invoice-date-input').val(),
             invoiceNumber: $('#invoice-number-input').val(),
             startDate: startDate, endDate: endDate,
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'search')) {
          return;
        }
        var search = JSON.parse(response);
        gridData = search.data;
        if (grid) {
          grid.setData(gridData);
          grid.updateRowCount();
          grid.render();
          grid.setSelectedRows([]);
        }
        if (search.filename) {
          // Redirect to the download.
          location.href = '/php/private.php?file=' + search.filename;
        }
      });
    return false;
  }

  function submit() {
    if (!grid) {
      alert('There was a problem loading the grid. Please reload the page.');
      return false;
    }

    var emailSelf = $('#invoice-email-self:checked').length === 1;
    var emailUser = $('#invoice-email-user:checked').length === 1;
    var data = [];
    var selected = [];
    selected = grid.getSelectedRows();
    if (selected.length === 0) {
      return false;
    }
    if (!emailSelf && !emailUser) {
      alert('Please select who the invoice should be sent to.');
      return false;
    }

    if (selected.length > 1) {
      let message = 'Send ' + selected.length + ' invoices to ';
      if (emailSelf && emailUser) {
        message += 'yourself and the selected users?';
      }
      else {
        message += emailSelf ? 'yourself?' : 'selected users?';
      }
      if (!confirm(message)) {
        return false;
      }
    }

    $.each(selected, function(i, row) {
      data.push(gridData[row]);
    });
    dobrado.log('Sending invoice...', 'info');
    $.post('/php/request.php',
           { request: 'invoice', action: 'submit', data: JSON.stringify(data),
             customEmail: $('#invoice-email-input').val(), emailSelf: emailSelf,
             emailUser: emailUser, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'submit')) {
          return;
        }
        var invoiceNumber = JSON.parse(response);
        // Update the invoice numbers in the grid.
        $.each(selected, function(i, row) {
          if (invoiceNumber[i]) {
            gridData[row].number = invoiceNumber[i];
          }
        });
        grid.invalidateRows(selected);
        grid.render();
        grid.setSelectedRows(selected);
        clearForm();
        $('#invoice-form .info').html('Invoice sent.');
        setTimeout(function() { $('#invoice-form .info').html(''); }, 5000);
      });
    return false;
  }

}());
