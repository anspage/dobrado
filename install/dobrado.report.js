// @source: /js/source/dobrado.report.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2018 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.report){dobrado.report={};}
(function(){'use strict';var report={};var quota=[];var quotaGrid=null;var quotaGridId='';var account=[];var accountGrid=null;var accountGridId='';var graphId='';var purchaseGraphId='';var receivedPaymentGraphId='';var outgoingPaymentGraphId='';var salesGraphId='';var attendanceGraphId='';$(function(){if($('.report').length===0){return;}
$('#report-group-select').change(changeGroup);if($('.grid').length!==0){gridSetup();}
if($('.graph').length===1){graphId='#'+$('.graph').attr('id');$(graphId+' .graph-area').each(function(index){if(index===0){purchaseGraphId=$(this).attr('id');$('#'+purchaseGraphId).parent().hide();}
if(index===1){receivedPaymentGraphId=$(this).attr('id');$('#'+receivedPaymentGraphId).parent().hide();}
if(index===2){outgoingPaymentGraphId=$(this).attr('id');$('#'+outgoingPaymentGraphId).parent().hide();}
if(index===3){salesGraphId=$(this).attr('id');$('#'+salesGraphId).parent().hide();}
if(index===4){attendanceGraphId=$(this).attr('id');$('#'+attendanceGraphId).parent().hide();}});if(!attendanceGraphId){alert('5 graph areas required, found: '+
$(graphId+' .graph-area').length);}}
$.post('/php/request.php',{request:'report',action:'init',group:$('#report-group-select').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'report init')){return;}
report=JSON.parse(response);init();});});function gridSetup(){$('.grid').each(function(index){if(index===0){quotaGridId='#'+$(this).attr('id');}
if(index===1){accountGridId='#'+$(this).attr('id');}});var quotaColumns=[{id:'product',name:'Product',field:'name',width:300,sortable:true},{id:'supplier',name:'Supplier',field:'supplier',width:200,sortable:true},{id:'quota',name:'Quota',field:'fraction',width:300,sortable:true,formatter:Slick.Formatters.Quota}];var quotaOptions={autoHeight:true,forceFitColumns:true};quotaGrid=dobrado.grid.instance(quotaGridId,[],quotaColumns,quotaOptions);quotaGrid.onSort.subscribe(function(e,args){quota.sort(function(row1,row2){var field=args.sortCol.field;var sign=args.sortAsc?1:-1;var value1=row1[field];var value2=row2[field];if(field==='fraction'){value1=parseFloat(value1)/row1.size;value2=parseFloat(value2)/row2.size;}
if(value1===value2){return 0;}
if(value1>value2){return sign;}
else{return sign* -1;}});quotaGrid.invalidate();});if($('.grid').length>=2){var accountColumns=[{id:'user',name:'Username',field:'user',width:150,sortable:true}];if($('.dobrado-mobile').is(':hidden')){accountColumns.push({id:'fullname',name:'Full Name',field:'fullname',width:200,sortable:true});accountColumns.push({id:'email',name:'Email',field:'email',width:150,sortable:true});}
accountColumns.push({id:'data',name:'Data',field:'data',width:100,sortable:true});var accountGridOptions={autoHeight:true,forceFitColumns:true};accountGrid=dobrado.grid.instance(accountGridId,[],accountColumns,accountGridOptions);accountGrid.onSort.subscribe(function(e,args){account.sort(function(row1,row2){var field=args.sortCol.field;var sign=args.sortAsc?1:-1;var value1=row1[field];var value2=row2[field];if(field==='data'){value1=value1===''?0:parseFloat(value1);value2=value2===''?0:parseFloat(value2);}
if(value1===value2){return 0;}
if(value1>value2){return sign;}
else{return sign* -1;}});accountGrid.invalidate();});}
$('.grid').hide();}
function init(){var products=[];var suppliers=[];$.each(report.products,function(index,item){if($.inArray(item.name,products)===-1){products.push(item.name);}
if($.inArray(item.user,suppliers)===-1){suppliers.push(item.user);}});$('#report-collate-start').datepicker({dateFormat:dobrado.dateFormat}).val('');$('#report-collate-end').datepicker({dateFormat:dobrado.dateFormat}).val('');$('#report-collate-orders').button().click(collateOrders);$('#report-quotas-button').button().click(showQuotas);$('#report-order-download').button().click(showOrder);$('#report-purchase-start').datepicker({dateFormat:dobrado.dateFormat}).val('');$('#report-purchase-end').datepicker({dateFormat:dobrado.dateFormat}).val('');$('#report-purchase-button').button().click(purchaseHistory);$('#report-received-payment-start').datepicker({dateFormat:dobrado.dateFormat}).val('');$('#report-received-payment-end').datepicker({dateFormat:dobrado.dateFormat}).val('');$('#report-received-payment-button').button().click(function(){paymentHistory("received");});$('#report-outgoing-payment-start').datepicker({dateFormat:dobrado.dateFormat}).val('');$('#report-outgoing-payment-end').datepicker({dateFormat:dobrado.dateFormat}).val('');$('#report-outgoing-payment-button').button().click(function(){paymentHistory("outgoing");});$('#report-product').val('').change(function(){setTimeout(function(){showProductFromMenu();},10);});$('#report-product').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:products,select:showProductFromMenu});$('#report-supplier').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:suppliers});$('#report-sales-start').datepicker({dateFormat:dobrado.dateFormat}).val('');$('#report-sales-end').datepicker({dateFormat:dobrado.dateFormat}).val('');$('#report-sales-button').button().click(salesHistory);$('#report-attendance-start').datepicker({dateFormat:dobrado.dateFormat}).val('');$('#report-attendance-end').datepicker({dateFormat:dobrado.dateFormat}).val('');$('#report-attendance-button').button().click(attendanceHistory);$('#report-accounts-select').val('').change(accountOptions);}
function changeGroup(){dobrado.log('Loading report.','info');$.post('/php/request.php',{request:'report',action:'changeGroup',group:$('#report-group-select').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'report ChangeGroup')){return;}
report=JSON.parse(response);if(quotaGrid){$(quotaGridId).appendTo('body').hide();}
if(graphId){if(purchaseGraphId){$('#'+purchaseGraphId).parent().hide().appendTo(graphId);}
if(receivedPaymentGraphId){$('#'+receivedPaymentGraphId).parent().hide().appendTo(graphId);}
if(outgoingPaymentGraphId){$('#'+outgoingPaymentGraphId).parent().hide().appendTo(graphId);}
if(salesGraphId){$('#'+salesGraphId).parent().hide().appendTo(graphId);}
if(attendanceGraphId){$('#'+attendanceGraphId).parent().hide().appendTo(graphId);}}
$('.report-content').html(report.content);init();});}
function collateOrders(){dobrado.log('Collating orders...','info');var start=0;var end=0;if($('#report-collate-start').length!==0){start=parseInt($.datepicker.formatDate('@',$('#report-collate-start').datepicker('getDate')),10);}
if($('#report-collate-end').length!==0){end=parseInt($.datepicker.formatDate('@',$('#report-collate-end').datepicker('getDate')),10);}
$.post('/php/request.php',{request:'report',action:'collateOrders',start:start,end:end,group:$('#report-group-select').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'report collateOrders')){return;}
var info=JSON.parse(response);$('#report-collate-info').html('Order processing done.');$('#report-format-select').html(info.content);$('#report-order-result').html('');});}
function showProductFromMenu(event,ui){var product=$('#report-product').val();var supplier=$('#report-supplier').val();var supplierFound=false;if(ui){product=ui.item.value;}
if(product===''){return false;}
$.each(report.products,function(index,item){if(item.name===product&&(supplier===''||supplier===item.user)){$('#report-supplier').val(item.user);supplierFound=true;return false;}});if(!supplierFound){$.each(report.products,function(index,item){if(item.name===product){$('#report-supplier').val(item.user);return false;}});}}
function showQuotas(){dobrado.log('Loading quotas.','info');$.post('/php/request.php',{request:'report',action:'showQuotas',group:$('#report-group-select').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'report showQuotas')){return;}
quota=JSON.parse(response);if(quotaGrid){$(quotaGridId).appendTo('.report-quotas');$(quotaGridId).show();quotaGrid.setData(quota);quotaGrid.updateRowCount();quotaGrid.render();}});}
function showOrder(){dobrado.log('Showing order list.','info');$('#report-order-result').html('');$.post('/php/request.php',{request:'report',action:'showOrder',group:$('#report-group-select').val(),format:$('#report-format-select').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'report showOrder')){return;}
var order=JSON.parse(response);$('#report-order-result').html(order.content);$('#report-supplier-download').button().click(function(){var file=$('#report-supplier-select').val();if(file){location.href='/php/private.php?file='+file;}});if(order.name){location.href='/php/private.php?file='+order.name;}});}
function purchaseHistory(){var start=parseInt($.datepicker.formatDate('@',$('#report-purchase-start').datepicker('getDate')),10);if(!start){alert('Start date required.');return false;}
var end=parseInt($.datepicker.formatDate('@',$('#report-purchase-end').datepicker('getDate')),10);if(!end){alert('End date required.');return false;}
dobrado.log('Loading purchase history.','info');$.post('/php/request.php',{request:'report',action:'purchaseHistory',group:$('#report-group-select').val(),start:start,end:end,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'report purchaseHistory')){return;}
var purchase=JSON.parse(response);$('.report-purchase-date').html(purchase.date);$('.report-purchase-total').html(purchase.total);$('.report-purchase').show();if(purchase.taxable){$('.report-taxable-total').html(purchase.taxable).show();}
if(purchase.surcharge){$('.report-surcharge-total').html(purchase.surcharge).show();}
if(purchaseGraphId){$('#'+purchaseGraphId).html('').parent().hide();$('#'+purchaseGraphId).parent().appendTo('.report-purchase-graph');if(purchase.data){var legend={show:true};$('#'+purchaseGraphId).parent().show();dobrado.graph.loadData(purchaseGraphId,purchase.data,purchase.series,legend);}}});}
function paymentHistory(type){var start=parseInt($.datepicker.formatDate('@',$('#report-'+type+'-payment-start').datepicker('getDate')),10);if(!start){alert('Start date required.');return false;}
var end=parseInt($.datepicker.formatDate('@',$('#report-'+type+'-payment-end').datepicker('getDate')),10);if(!end){alert('End date required.');return false;}
dobrado.log('Loading '+type+' payment history.','info');$.post('/php/request.php',{request:'report',action:'paymentHistory',group:$('#report-group-select').val(),start:start,end:end,type:type,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'report paymentHistory')){return;}
var payment=JSON.parse(response);$('.report-'+type+'-payment-date').html(payment.date);$('.report-'+type+'-payment-total').html(payment.total);var paymentGraphId=receivedPaymentGraphId;if(type==='outgoing'){paymentGraphId=outgoingPaymentGraphId;}
if(paymentGraphId){$('#'+paymentGraphId).html('').parent().hide();$('#'+paymentGraphId).parent().appendTo('.report-'+type+'-payment-graph');if(payment.data){$('#'+paymentGraphId).parent().show();dobrado.graph.loadData(paymentGraphId,payment.data,payment.series);}}});}
function salesHistory(){var start=parseInt($.datepicker.formatDate('@',$('#report-sales-start').datepicker('getDate')),10);if(!start){alert('Start date required.');return false;}
var end=parseInt($.datepicker.formatDate('@',$('#report-sales-end').datepicker('getDate')),10);if(!end){alert('End date required.');return false;}
var supplier=$('#report-supplier').val();if(!supplier){alert('Supplier required.');return false;}
dobrado.log('Loading sales history.','info');$.post('/php/request.php',{request:'report',action:'salesHistory',group:$('#report-group-select').val(),product:$('#report-product').val(),supplier:supplier,start:start,end:end,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'report salesHistory')){return;}
var sales=JSON.parse(response);$('.report-sales-date').html(sales.date);$('.report-sales-total').html(sales.total);$('.report-sales').show();if(salesGraphId){$('#'+salesGraphId).html('').parent().hide();$('#'+salesGraphId).parent().appendTo('.report-sales-graph');if(sales.data){$('#'+salesGraphId).parent().show();dobrado.graph.loadData(salesGraphId,sales.data,sales.series);}}});}
function attendanceHistory(){var start=parseInt($.datepicker.formatDate('@',$('#report-attendance-start').datepicker('getDate')),10);if(!start){alert('Start date required.');return false;}
var end=parseInt($.datepicker.formatDate('@',$('#report-attendance-end').datepicker('getDate')),10);if(!end){alert('End date required.');return false;}
dobrado.log('Loading attendance history.','info');$.post('/php/request.php',{request:'report',action:'attendanceHistory',group:$('#report-group-select').val(),start:start,end:end,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'report attendanceHistory')){return;}
var attendance=JSON.parse(response);$('.report-attendance-date').html(attendance.date);$('.report-attendance-total').html(attendance.total);$('.report-attendance').show();if(attendanceGraphId){$('#'+attendanceGraphId).html('').parent().hide();$('#'+attendanceGraphId).parent().appendTo('.report-attendance-graph');if(attendance.data){$('#'+attendanceGraphId).parent().show();dobrado.graph.loadData(attendanceGraphId,attendance.data,attendance.series);}}});}
function accountOptions(){var option=$('#report-accounts-select').val();if(option===''){return false;}
dobrado.log('Loading account information.','info');var download=$('#report-download-accounts:checked').length;$.post('/php/request.php',{request:'report',action:option,group:$('#report-group-select').val(),download:download,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'report accountOptions')){return;}
var result=JSON.parse(response);account=result.data;if(accountGrid){$(accountGridId).appendTo('.report-accounts');var columns=accountGrid.getColumns();if(option==='membershipReminders'||option==='lastAttendance'){columns[3].name='Date';columns[3].formatter=Slick.Formatters.Timestamp;}
else if(option==='accountsPayable'){columns[3].name='Payment';columns[3].formatter=Slick.Formatters.Dollar;}
else if(option==='activeMembers'){columns[3].name='Active';columns[3].formatter=Slick.Formatters.Checkmark;}
else if(option==='paidDeposit'){columns[3].name='Deposit';columns[3].formatter=Slick.Formatters.Checkmark;}
if(account&&account.length!==0){$(accountGridId).show();accountGrid.setColumns(columns);accountGrid.setData(account);accountGrid.updateRowCount();accountGrid.render();}
else{$(accountGridId).hide();}
$('.report-accounts-info').html(result.info);}
if(download===1&&result.filename){location.href="/php/private.php?file="+result.filename;}});}}());