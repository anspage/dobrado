/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2017 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.contact) {
  dobrado.contact = {};
}
(function() {

  'use strict';

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.contact').length === 0) {
      return;
    }

    $('.contact-form .submit').button().click(submit);
  });

  function submit() {
    var content = {};
    var form = $(this).parent();
    form.find(':input').each(function() {
      var name = $(this).attr('name');
      if (name) {
        content[name] = $(this).val();
      }
    });
    if (!content.email || !/@/.test(content.email)) {
      form.find('.contact-info').show().html('<i>Please provide your email ' +
                                             'address.</i>');
      return false;
    }

    $('.contact-form .submit').button('option', 'disabled', true);
    $.post('/php/request.php',
           { request: 'contact', mode: 'contact',
             content: JSON.stringify(content), url: location.href,
             token: dobrado.token },
      function(response) {
        $('.contact-form .submit').button('option', 'disabled', false);
        if (dobrado.checkResponseError(response, 'contact')) {
          form.find('.contact-info').show().html('<i>There was an error ' +
                                                 'sending your message.</i>');
          return;
        }
        // Reset the form fields on success.
        form.find(':input').each(function() {
          if ($(this).attr('type') !== 'hidden') {
            $(this).val('');
          }
        });
        form.find('.contact-info').show().html('<i>Thanks your message ' +
                                               'has been sent.</i>');
      });
    return false;
  }

}());
