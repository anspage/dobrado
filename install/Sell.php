<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Sell extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      $error = 'You don\'t have permission to view point of sale.';
      return ['error' => $error];
    }

    $action = isset($_POST['action']) ? $_POST['action'] : '';

    if ($action === 'list') {
      $stock = new Stock($this->user, $this->owner);
      $banking = new Banking($this->user, $this->owner);
      list($users, $buyerGroup) = $banking->AllBuyers();
      return ['data' => [], 'processed' => [], 'userIndex' => [],
              'newSales' => [], 'users' => $users, 'buyerGroup' => $buyerGroup,
              'products' => $stock->AvailableProducts(),
              'wholesalePercent' =>
                (float)$this->Substitute('stock-wholesale-percent'),
              'retailPercent' =>
                (float)$this->Substitute('stock-retail-percent')];
    }
    if ($action === 'listUser') {
      $purchase = new Purchase($this->user, $this->owner);
      // List all purchases with today's date.
      $start = strtotime(date('F j Y 00:00:00'));
      $end = $start + 86400;
      $mysqli = connect_db();
      $username = $mysqli->escape_string($_POST['username']);
      $mysqli->close();
      return $purchase->Data($start, $end, true, $username);
    }
    if ($action === 'save') {
      $purchase = new Purchase($this->user, $this->owner);
      // The session variable is given precedence because the transaction has
      // been checked. Otherwise method could end up as 'check transaction'.
      if (isset($_SESSION['sell-payment-method'])) {
        $purchase->SaveSales($_SESSION['sell-payment-method']);
      }
      else {
        $mysqli = connect_db();
        $payment = $mysqli->escape_string($_POST['method']);
        $mysqli->close();
        $purchase->SaveSales($payment);
      }
      $_SESSION['sell-payment-method'] = null;
      return ['done' => true];
    }
    if ($action === 'checkout') {
      return $this->Checkout();
    }
    if ($action === 'payment-done') {
      // This allows the page to refresh without the query string added by
      // the payment gateway.
      return ['location' => $this->Url()];
    }
    if ($action === 'connectSettings') {
      return $this->ConnectSettings();
    }
    return ['error' => 'Unknown action.'];
  }

  public function CanAdd($page) {
    // Need admin privileges to add the purchase module.
    if (!$this->user->canEditSite) return false;
    // Can only have one sell module on a page, also no purchase module.
    if ($this->AlreadyOnPage('purchase', $page)) return false;
    return !$this->AlreadyOnPage('sell', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $content = '';
    $first = '';
    $img = '<img class="thumb" src="/images/default_thumb.jpg">';
    $payment_methods = '';
    $cart = new Module($this->user, $this->owner, 'cart');
    $_SESSION['sell-payment-method'] = null;

    // Check if returning from making a payment.
    if (isset($_GET['payment'])) {
      if ($_GET['payment'] === 'paypal') {
        // TODO: There is no checking done on paypal return values,
        // that requires supporting PDT and maybe IPN in the future.
        $content .= '<div class="sell-payment-message">'.
          'Thankyou for your purchase!<br><button>dismiss</button></div>';
        $_SESSION['sell-payment-method'] = 'paypal';
      }
      else if ($_GET['payment'] === 'credit') {
        if ($cart->IsInstalled()) {
          $gateway = $cart->Factory('Gateway', 'credit');
          if ($gateway === true) {
            $gateway = 'Thanks your payment has been processed.';
          }
          $content .= '<div class="sell-payment-message">' . $gateway .
            '<br><button>dismiss</button></div>';
          $_SESSION['sell-payment-method'] = 'credit';
        }
        else {
          $content .= '<div class="sell-payment-message">' .
            'There was an error checking the payment.<br>' .
            'Cart module must be installed.<br><button>dismiss</button></div>';
        }
      }
    }
    if ($cart->IsInstalled()) {
      $payment_methods = $cart->Factory('PaymentMethods',
                                        [true, 'purchase-payment-input']);
    }
    else {
      $payment_methods = '<label for="purchase-payment-input">Payment:</label>'.
        '<select id="purchase-payment-input">' .
          '<option value=""></option>' .
          '<option value="cash">Cash</option>' .
          '<option value="eftpos">Eftpos</option>' .
          '<option value="account">Account</option>' .
        '</select>';
    }
    $detail = new Detail($this->user, $this->owner);
    $user_details = $detail->User();
    if (is_array($user_details)) {
      // The user doesn't have to match the account details due to
      // users having shared accounts, so check who is on the roster.
      $roster = new Roster($this->user, $this->owner);
      $first = $roster->FirstName();
      if ($first === '' || $first === $this->user->name) {
        $first = $user_details['first'];
        $img = $user_details['thumbnail'];
      }
    }
    // If the user hasn't filled out their details just display their username.
    if ($first === '') {
      $first = $this->user->name;      
    }
    $purchase_update_quantity = '';
    if ($this->Substitute('sell-update-quantity') === 'true') {
      $purchase_update_quantity = '<span id="purchase-connect">' .
          '<label for="purchase-update-quantity">' .
            '<span class="ui-icon ui-icon-transferthick-e-w"></span>' .
          '</label>' .
          '<input id="purchase-update-quantity" type="checkbox">' .
          '<button id="purchase-connect-settings">settings</button>' .
        '</span>';
    }

    $content .= '<div class="message">' .
        $img . 'Hello ' . $first . '<br>' .
      '</div>' .
      '<div class="warning ui-state-highlight ui-corner-all hidden">' .
        '<b>Warning:</b> This purchasing system would work better if you ' .
        'upgraded your browser. Thanks!' .
      '</div>' .
      '<form id="purchase-form" autocomplete="off">' .
        '<div class="form-spacing">' .
          '<label for="purchase-name-input">Username:</label>' .
          '<input id="purchase-name-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="purchase-product-input">Product:</label>' .
          '<input id="purchase-product-input" type="text" maxlength="100">' .
          '<button class="view-all">View all</button>' .
        '</div>' .
        '<div id="purchase-grower-info"></div>' .
        '<div class="form-spacing">' .
          '<label for="purchase-quantity-input">Quantity:</label>' .
          '<input id="purchase-quantity-input" type="text" maxlength="8">' .
          $purchase_update_quantity .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="purchase-price-input">Price:</label>' .
          '<input id="purchase-price-input" type="text" ' .
            'maxlength="50" readonly="true">' .
        '</div>' .
        '<button class="add">add</button>' .
        '<button class="remove">remove</button>' .
      '</form>' .
      '<span class="order">' .
        'The current total is: $<span class="total">0.00</span>' .
      '</span>' .
      '<button class="done">purchase done</button>' .
      '<button class="reset hidden">create new sale</button>' .
      '<button class="save hidden">save cash purchases</button>' .
      '<div id="sell-checkout-dialog" class="hidden">' .
        '<div id="sell-checkout-message">Please enter your contact details:' .
        '</div>' .
        '<form id="sell-customer-details-form" autocomplete="off">' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-name">Name:</label>' .
            '<input id="customer-detail-name" type="text" maxlength="100">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-email">Email:</label>' .
            '<input id="customer-detail-email" type="text" maxlength="100">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-phone">Phone:</label>' .
            '<input id="customer-detail-phone" type="text" maxlength="100">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-address">Address:</label>' .
            '<input id="customer-detail-address" type="text" maxlength="100">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-postcode">Postcode:</label>' .
            '<input id="customer-detail-postcode" type="text" maxlength="32">' .
            '<label for="customer-detail-city">City:</label>' .
            '<input id="customer-detail-city" type="text" maxlength="100">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-state">State:</label>' .
            '<input id="customer-detail-state" type="text" maxlength="100">' .
            '<label for="customer-detail-country">Country:</label>' .
            '<input id="customer-detail-country" type="text" maxlength="50" ' .
              'value="' . $this->Substitute('cart-default-country') . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="customer-detail-notes">Notes:</label>' .
            '<textarea id="customer-detail-notes"></textarea>' .
          '</div>' .
          '<button id="customer-detail-submit">Submit</button>' .
          '<div id="sell-checkout-info"></div>' .
        '</form>' .
      '</div>' .
      '<div class="purchase-connect-settings hidden"></div>' .
      '<div class="sell-finished hidden">' .
        '<div class="total-text">' .
          'The total is: $<span class="total">0.00</span>' .
        '</div>' .
        'How would you like to pay?' .
        '<div class="form-spacing">' . $payment_methods . '</div>' .
        '<input id="sell-auto-save" type="checkbox" checked="checked">' .
        'Save each purchase ' .
        '(<a href="#" class="sell-describe-auto-save">what\'s this?</a>)<br>' .
        '<div class="sell-auto-save-description hidden">Purchases are ' .
          'normally saved when you click the <b>done button</b> below. ' .
          'By un-checking this box, you can use this page offline for ' .
          '<i>cash-only</i> sales. A new button will be displayed so you can ' .
          'save all purchases when you next have internet access.' .
        '</div>' .
        '<button class="sell-finished-back">back</button>' .
        '<button class="sell-finished-done">done</button>' .
      '</div>';

    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $this->AppendScript($path, 'dobrado.sell.js', false);

    $template = ['"sell-items-description","","Purchases made on !date."',
                 '"sell-checkout","","Thanks for providing your ' .
                 'details !name."'];
    $this->AddTemplate($template);
    $description = ['sell-items-description' => 'A description used by the ' .
                      'Point of Sale Checkout when submitting a payment. ' .
                      'Will substitute !date.',
                    'sell-checkout' => 'A message to display at the Point of ' .
                      'Sale Checkout, above the list of purchases. Will ' .
                      'substitute all the details the user provided: !name, ' .
                      '!phone, !address, !postcode, !city, !state, !country, ' .
                      '!notes.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"","#sell-customer-details-form","background-color",' .
                     '"#eeeeee"',
                   '"","#sell-customer-details-form","border",' .
                     '"1px solid #aaaaaa"',
                   '"","#sell-customer-details-form","border-radius","2px"',
                   '"","#sell-customer-details-form","padding","5px"',
                   '"","#sell-customer-details-form","margin","5px"',
                   '"","#sell-customer-details-form label","width","7em"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.sell.js', false);
  }

  // Private functions below here ////////////////////////////////////////////

  private function Checkout() {
    $us_name = htmlspecialchars($_POST['name']);
    $us_email = htmlspecialchars($_POST['email']);
    $us_phone = htmlspecialchars($_POST['phone']);
    $us_address = htmlspecialchars($_POST['address']);
    $us_city = htmlspecialchars($_POST['city']);
    $us_postcode = htmlspecialchars($_POST['postcode']);
    $us_state = htmlspecialchars($_POST['state']);
    $us_country = htmlspecialchars($_POST['country']);
    $us_notes = nl2br(htmlspecialchars($_POST['notes']), false);
    $total = (float)$_POST['total'];
    $us_method = htmlspecialchars($_POST['method']);
    $us_items = $this->Substitute('sell-items-description', '/!date/',
                                  date('M jS, Y \a\t g:i a'));
    $patterns = ['/!name/', '/!phone/', '/!address/','/!postcode/', '/!city/',
                 '/!state/', '/!country/', '/!notes/'];
    $replacements = [$us_name, $us_phone, $us_address, $us_postcode, $us_city,
                     $us_state, $us_country, $us_notes];
    $result = ['content' => $this->Substitute('sell-checkout', $patterns,
                                              $replacements)];
    $cart = new Module($this->user, $this->owner, 'cart');
    if ($cart->IsInstalled()) {
      $processing = $cart->Factory('ProcessingCost', $total);
      $result['content'] .= '<p>Payment will be $' .
        number_format($total, 2, '.', '');
      if ($processing > 0) {
        $result['content'] .= ' plus $' .
          number_format($processing, 2, '.', '') . ' processing fee.';
      }
      $result['content'] .= '</p>';
      $result['content'] .=
        $cart->Factory('Payment', [$us_items, $total + $processing, $us_name,
                                   $us_email, $us_phone, $us_address, $us_city,
                                   $us_postcode, $us_state, $us_country,
                                   $us_notes, $us_method]);
    }
    else {
      $result['content'] .= 'Cannot process payment. Cart module must be ' .
        'installed.';
    }
    return $result;
  }

  private function ConnectSettings() {
    $spark = new Module($this->user, $this->owner, 'spark');
    if ($spark->IsInstalled()) {
      return ['settings' => $spark->Factory('ListConnections',
                                            $this->user->group)];
    }
    return ['error' => 'Spark module is not installed.'];
  }

}
