<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Settings extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $mysqli = connect_db();
    $id = explode('-', $mysqli->escape_string($_POST['id']));
    $value = $mysqli->escape_string($_POST['value']);
    $label = '';
    $name = '';
    if (count($id) >= 3) {
      $label = $id[1];
      $name = $id[2];
      if (count($id) === 4) {
        // When a fourth part of the id is given, it represents the position
        // in the list of values available for this setting.
        $value = $this->Value($label, $name, (int)$id[3]);
      }
    }
    $display = explode(',', $this->Substitute('settings-display'));
    if (!in_array($label, $display)) {
      return ['error' => 'Module not listed in settings to display.'];
    }

    if ($label !== '' && $name !== '') {
      $query = 'INSERT INTO settings VALUES ("' . $this->user->name . '", ' .
        '"' . $label . '", "' . $name . '", "' . $value . '") ' .
        'ON DUPLICATE KEY UPDATE value = "' . $value . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Settings->Callback: ' . $mysqli->error);
      }
      if ($label === 'account' && $name === 'forward') {
        // This is a special case. When the user requests forwarding, send an
        // email so that it gets sent to the forwarding account, and then a
        // confirmation can be requested to forward from that account.
        $mysqli->close();
        return $this->ConfirmForwarding($value);
      }
    }
    $mysqli->close();

    if ($label === '') return ['error' => 'Module name not provided.'];
    if ($name === '') return ['error' => 'Setting name not provided.'];
    return ['done' => true];
  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    // Allow the modules to display settings for to be customised per group.
    $display = explode(',', $this->Substitute('settings-display'));

    $mysqli = connect_db();
    $all_settings = [];
    $label_query = '';
    foreach ($display as $label) {
      if ($label_query !== '') $label_query .= ' OR ';
      $label_query .= 'label = "' . $mysqli->escape_string($label) . '"';
    }
    $query = 'SELECT label, name, value, input_type, input_info FROM ' .
      'setting_types WHERE ' . $label_query;
    if ($mysqli_result = $mysqli->query($query)) {
      while ($setting_types = $mysqli_result->fetch_assoc()) {
        $label = $setting_types['label'];
        $name = $setting_types['name'];
        $value = $setting_types['value'];
        $input_type = $setting_types['input_type'];
        $input_info = $setting_types['input_info'];
        if (isset($all_settings[$label])) {
          $all_settings[$label][$name] = ['value' => $value,
            'input_type' => $input_type, 'input_info' => $input_info];
        }
        else {
          $all_settings[$label] = [$name => ['value' => $value,
            'input_type' => $input_type, 'input_info' => $input_info]];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Settings->Content 1: ' . $mysqli->error);
    }

    $user_settings = [];
    $query = 'SELECT label, name, value FROM settings WHERE ' .
      'user = "' . $this->user->name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($settings = $mysqli_result->fetch_assoc()) {
        $label = $settings['label'];
        $name = $settings['name'];
        $value = $settings['value'];
        if (isset($user_settings[$label])) {
          $user_settings[$label][$name] = $value;
        }
        else {
          $user_settings[$label] = [$name => $value];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Settings->Content 2: ' . $mysqli->error);
    }
    $mysqli->close();

    $content = '';
    foreach ($display as $label) {
      if (!isset($all_settings[$label])) continue;

      $section = '';
      foreach ($all_settings[$label] as $name => $attributes) {
        $user_value = isset($user_settings[$label][$name]) ?
          $user_settings[$label][$name] : '';
        $section .= $this->FormatInput($label, $name, $attributes, $user_value);
      }
      if ($section !== '') {
        $content .= '<div class="settings-section"><h2>' . ucfirst($label) .
          '</h2>' . $section . '</div>';
      }
    }

    if ($content === '') {
      return '<p>Please edit the settings-display template.</p>';
    }
    return '<p>Settings are listed below grouped by the modules they ' .
      'apply to. After making any changes please reload the page.</p>' .
      $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if ($fn === 'AddMember') return $this->AddMember();
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS setting_defaults (' .
      'label VARCHAR(50) NOT NULL,' .
      'name VARCHAR(200) NOT NULL,' .
      'value TEXT,' .
      'group_name VARCHAR(250) NOT NULL,' .
      'PRIMARY KEY(label, name(50), group_name(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Settings->Install: ' . $mysqli->error);
    }
    $mysqli->close();

    $this->AppendScript($path, 'dobrado.settings.js', false);
    // Add all modules that call AddSettingTypes here as a default.
    $this->AddTemplate(['"settings-display","","account,control,members,'.
                          'purchase,reader,stock"']);
    $site_style = ['"",".settings-section","background-color","#eeeeee"',
                   '"",".settings-section","border","1px solid #aaaaaa"',
                   '"",".settings-section","margin","5px"',
                   '"",".settings-section","padding","5px"',
                   '"",".settings-section h2","margin-top","5px"',
                   '"",".settings fieldset","margin-bottom","5px"',
                   '"",".settings-info","font-size","0.9em"',
                   '"",".settings-info","padding","2px 0 0 5px"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.settings.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  public function AddMember() {
    $mysqli = connect_db();
    $query = 'INSERT IGNORE INTO settings (user, label, name, value) ' .
      'SELECT "' . $this->user->name . '", label, name, value FROM ' .
      'setting_defaults WHERE group_name = "' . $this->user->group . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Settings->AddMember: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function ConfirmForwarding($value) {
    $email = '';
    $mysqli = connect_db();
    $query = 'SELECT email FROM users WHERE user = "' . $this->user->name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($users = $mysqli_result->fetch_assoc()) {
        $email = $users['email'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Settings->ConfirmForwarding: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($email === '') {
      return ['error' => 'Email address not set.'];
    }
    if ($this->Substitute('mail-forward-bcc') === '') {
      return ['error' => 'Mail forwarding not available.'];
    }

    $action = $value === 'forward email' ? 'start' : 'stop';
    $subject = $action . ' forwarding requested';
    $message = 'This email was sent to acknowledge that ' .
      $this->user->name . ' requested email forwarding ' . $action . '.';
    $server = $this->user->config->ServerName();
    $sender = $this->Substitute('system-sender', '/!host/', $server);
    $sender_name = $this->Substitute('system-sender-name');
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $headers = "MIME-Version: 1.0\r\n" .
      "Content-type: text/html; charset=utf-8\r\n" .
      'From: ' . $sender_name . "\r\n";
    dobrado_mail($email, $subject, $message, $headers, '', $sender,
                 $this->user->name, 'settings', 'forward');
    return ['done' => true];
  }

  private function FormatInput($label, $name, $attributes, $user_value) {
    $display_name = preg_replace('/([[:upper:]])/', ' $1', $name);
    if ($attributes['input_type'] === 'checkbox') {
      $content = '<fieldset><legend>' . ucfirst($display_name) . ':</legend>';
      $values = explode(',', $attributes['value']);
      for ($i = 0; $i < count($values); $i++) {
        $checked = $user_value === $values[$i] ? ' checked="checked"' : '';
        $content .= '<input type="checkbox" ' .
            'id="settings-' . $label . '-' . $name . '-' . $i . '" ' .
            'name="checkbox-' . $label . '-' . $name . '"' . $checked . '>' .
          '<label for="settings-' . $label . '-' . $name . '-' . $i . '">' .
            ucfirst($values[$i]) . '</label>';
      }
      return $content . '<div class="settings-info">' .
        $attributes['input_info'] . '</div></fieldset>';
    }
    if ($attributes['input_type'] === 'radio') {
      $content = '<fieldset><legend>' . ucfirst($display_name) . ':</legend>';
      $values = explode(',', $attributes['value']);
      for ($i = 0; $i < count($values); $i++) {
        $checked = $user_value === $values[$i] ? ' checked="checked"' : '';
        $content .= '<input type="radio" ' .
            'id="settings-' . $label . '-' . $name . '-' . $i . '" ' .
            'name="radio-' . $label . '-' . $name . '"' . $checked . '>' .
          '<label for="settings-' . $label . '-' . $name . '-' . $i . '">' .
            ucfirst($values[$i]) . '</label>';
      }
      return $content . '<div class="settings-info">' .
        $attributes['input_info'] . '</div></fieldset>';
    }
    if ($attributes['input_type'] === 'date') {
      $value = $user_value === '' ? $attributes['value'] : $user_value;
      return '<div class="form-spacing">' .
        '<label for="settings-' . $label . '-' . $name . '">' .
          ucfirst($display_name) . ':</label>' .
        '<input type="text" class="datepicker" ' .
          'id="settings-' . $label . '-' . $name . '" value="' . $value . '">' .
          '<div class="settings-info">' . $attributes['input_info'] .
          '</div></div>';
    }
    if ($attributes['input_type'] === 'password') {
      $value = $user_value === '' ? $attributes['value'] : $user_value;
      return '<div class="form-spacing">' .
        '<label for="settings-' . $label . '-' . $name . '">' .
          ucfirst($display_name) . ':</label>' .
        '<input type="password" id="settings-' . $label . '-' . $name . '" ' .
          'value="' . $value . '">' .
          '<button class="show-password">show password</button>' .
          '<div class="settings-info">' . $attributes['input_info'] .
          '</div></div>';
    }
    if ($attributes['input_type'] === 'select') {
      $content = '<div class="form-spacing">' .
        '<label for="settings-' . $label . '-' . $name . '">' .
          ucfirst($display_name) . ':</label>' .
        '<select id="settings-' . $label . '-' . $name . '">';
      $values = explode(',', $attributes['value']);
      for ($i = 0; $i < count($values); $i++) {
        $selected = $user_value === $values[$i] ? ' selected="selected"' : '';
        $content .= '<option value="' . $values[$i] . '"' . $selected . '>' .
          ucfirst($values[$i]) . '</option>';
      }
      return '</select>' . $content . '<div class="settings-info">' .
        $attributes['input_info'] . '</div></div>';
    }
    if ($attributes['input_type'] === 'text') {
      $value = $user_value === '' ? $attributes['value'] : $user_value;
      return '<div class="form-spacing">' .
        '<label for="settings-' . $label . '-' . $name . '">' .
          ucfirst($display_name) . ':</label>' .
        '<input type="text" id="settings-' . $label . '-' . $name . '" ' .
          'value="' . $value . '">' .
          '<div class="settings-info">' . $attributes['input_info'] .
          '</div></div>';
    }
    if ($attributes['input_type'] === 'textarea') {
      $value = $user_value === '' ? $attributes['value'] : $user_value;
      return '<div class="form-spacing">' .
          '<label for="settings-' . $label . '-' . $name . '">' .
            ucfirst($display_name) . ':</label>' .
          '<textarea id="settings-' . $label . '-' . $name . '">' . $value .
          '</textarea>' .
          '<div class="settings-info">' . $attributes['input_info'] .
        '</div></div>';
    }
    return '';
  }

  private function Value($label, $name, $position) {
    $value = [];
    $mysqli = connect_db();
    $query = 'SELECT value FROM setting_types WHERE label = "' . $label . '" ' .
      'AND name = "' . $name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($setting_types = $mysqli_result->fetch_assoc()) {
        $value = explode(',', $setting_types['value']);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Settings->Value: ' . $mysqli->error);
    }
    $mysqli->close();
    return $position < count($value) ? $value[$position] : '';
  }

}
