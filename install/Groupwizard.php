<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Groupwizard extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if ($this->GroupMember('admin', 'admin')) {
      $us_action = isset($_POST['action']) ? $_POST['action'] : '';
      if ($us_action === 'saveSetting') {
        return $this->SaveSetting();
      }
      if ($us_action === 'createGroup') {
        return $this->CreateGroup();
      }
      if ($us_action === 'changeGroup') {
        return $this->ChangeGroup();
      }
      if ($us_action === 'updateMarkup') {
        return $this->UpdateMarkup();
      }
    }
    return ['error' => 'Permission denied using wizard.'];
  }

  public function CanAdd($page) {
    if ($this->GroupMember('admin', 'admin')) {
      // If the module is to be permanently added to the page, make sure it
      // hasn't already been added.
      if ($page === $this->Substitute('groupwizard-page')) {
        return !$this->AlreadyOnPage('groupwizard', $page);
      }
      else {
        return true;
      }
    }
    return false;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    // The module can be permanently added to the page when groupwizard-page is
    // used, so this provides a way to remove it in that case.
    return $this->GroupMember('admin', 'admin') &&
      $this->user->page === $this->Substitute('groupwizard-page');
  }

  public function Content($id) {
    $_SESSION['groupwizard-all-groups'] = false;
    // Content is called every time this module is added. Before checking
    // current settings, want to make sure the default page for this group
    // has a Banking module on it.
    $page = $this->Substitute('group-default-page');
    if ($page === '') {
      return '<div>Your group does not have a default page. Please create a '.
          'page that includes a Banking module, and set the name in the '.
          '<b>group-default-page</b> template before continuing.' .
        '</div>';
    }

    $banking = false;
    $mysqli = connect_db();
    $query = 'SELECT label FROM modules WHERE user = "admin" AND ' .
      'page = "' . $page . '" AND label = "banking" AND deleted = 0';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows === 1) {
        $banking = true;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Groupwizard->Content: ' . $mysqli->error);
    }
    $mysqli->close();

    if (!$banking) {
      return '<div>Your group\'s default page: <b>admin/' . $page . '</b> ' .
          'doesn\'t have a Banking module. Please add it before continuing.' .
        '</div>';
    }

    $default_group = $this->user->group;
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
    }
    // These arrays are turned into options for selects below.
    $weekdays = ['', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
                 'Friday', 'Saturday'];
    $hours = ['', '12am', '1am', '2am', '3am', '4am', '5am', '6am', '7am',
              '8am', '9am', '10am', '11am', '12pm', '1pm', '2pm', '3pm', '4pm',
              '5pm', '6pm', '7pm', '8pm', '9pm', '10pm', '11pm'];
    $banking = ['', 'retail', 'wholesale'];
    $group_format = ['', 'product', 'user'];
    $surcharge = $this->Substitute('surcharge') === 'true';
    $invite = new Invite($this->user, $this->owner);
    $created = $invite->Created();
    $invite_group = false;
    $group_settings = '';
    $select_ignore_group = '';
    $rounding = 'invoice-group-rounding';
    $organisation = false;
    $organiser = new Organiser($this->user, $this->owner);
    $groups = $organiser->Siblings();
    if (count($groups) > 1) {
      $organisation = true;
      $parent = $organiser->Parent();
      $parent_text = $parent === false || $parent === '' ? '' :
        ' in <b>' . ucfirst($parent) . '</b>';
      $group_text = $this->user->group === '' ? '' :
        ' group, <b>' . ucfirst($this->user->group) . '</b>';
      // Allow this module to update all groups in an organisation.
      $group_settings = '<p>You can use this wizard to update settings for ' .
          'all groups' . $parent_text . ', or just your own' . $group_text .
        '.</p>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-all-groups">Update all groups?</label>' .
          '<input type="checkbox" id="groupwizard-all-groups">' .
        '</div>';
      // Add the empty option to invoice-ignore-group so that all groups can
      // be de-selected if required.
      array_unshift($groups, '');
      $select_ignore_group = '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-ignore-group">Select a group ' .
            'that doesn\'t make purchases:</label>' .
          '<select id="groupwizard-invoice-ignore-group">' .
            $this->Options('invoice-ignore-group', $groups) .
          '</select>' .
        '</div>';
      $rounding = 'invoice-organisation-rounding';
    }
    else if (in_array($this->user->group, $created)) {
      $invite_group = true;
      $group_settings = 'This buying group can invite other groups to join. ' .
        'You can specify members from your group who are allowed to send out ' .
        'invites. To do this, <a href="#" class="groupwizard-invite">click ' .
        'here to edit the "invite-notifications" group</a>.';
    }

    $content = '<div id="groupwizard-info"></div>' .
      '<div class="groupwizard-content">' .
        '<button class="previous">previous</button>' .
        '<button class="next">next</button>' .
        $this->ExistingGroups($default_group, $created) .

      '<div class="groupwizard-1 hidden"><b>Section 1:</b> Group Details.' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-group-name">' .
            'Descriptive name for the group:</label>' .
          '<input id="groupwizard-group-name" type="text" maxlength="100" ' .
            'value="' . $this->Value('group-name') . '">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-group-description">' .
            'Summary describing this group:</label><br>' .
          '<textarea id="groupwizard-group-description">' .
            $this->Value('group-description') . '</textarea>' .
        '</div>' .
        $group_settings .
      '</div>' .

      '<div class="groupwizard-2 hidden"><b>Section 2:</b> Configure the ' .
        'days that members can collect their items.' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-co-op-day">The day to collect:' .
          '</label>' .
          '<select id="groupwizard-co-op-day-select">' .
            $this->Options('co-op-day', $weekdays) .
          '</select>' .
        '</div>' .
        '<div class="form-spacing hidden">' .
          '<label for="groupwizard-co-op-day-input">The day to collect:' .
          '</label>' .
          '<input id="groupwizard-co-op-day-input" type="text" maxlength="50" '.
            'value="' . $this->Value('co-op-day', $weekdays) . '">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-co-op-day-date">Switch to calendar date ' .
          '(or multiple days)</label>' .
          '<input type="checkbox" id="groupwizard-co-op-day-date">' .
        '</div>' .
        '<p>You can leave the collection day blank if you\'re not sure when ' .
          'the order will close (for example if you want to wait until ' .
          'quotas are full). Then once you\'re ready, you can come back here ' .
          'and set the date.</p>' .
      '</div>' .

      '<div class="groupwizard-3 hidden"><b>Section 3:</b> Configure the ' .
        'days that members can order.' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-pre-order">Does this group pre-order?' .
          '</label>' .
          '<input type="checkbox" id="groupwizard-pre-order"' .
            $this->Checked('pre-order') . '>' .
        '</div>' .
        '<div class="groupwizard-pre-order-settings hidden">' .
          '<div class="form-spacing">' .
            '<label for="groupwizard-pre-order-open-select">' .
              'The day pre-order opens:</label>' .
            '<select id="groupwizard-pre-order-open-select">' .
              $this->Options('pre-order-open', $weekdays) .
            '</select>' .
          '</div>' .
          '<div class="form-spacing hidden">' .
            '<label for="groupwizard-pre-order-open-input">The day ' .
              'pre-order opens:</label>' .
            '<input id="groupwizard-pre-order-open-input" type="text" ' .
              'maxlength="50" value="' . $this->Value('pre-order-open',
                                                      $weekdays) . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="groupwizard-pre-order-open-date">Switch to calendar ' .
              'date</label>' .
            '<input type="checkbox" id="groupwizard-pre-order-open-date">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="groupwizard-pre-order-open-time">The time ' .
              'pre-order opens:</label>' .
            '<select id="groupwizard-pre-order-open-time">' .
              $this->Options('pre-order-open-time', $hours) .
            '</select>' .
          '</div>' .
          'If you haven\'t set a collection date, leave the closing date ' .
          'for the order blank too, and set them both when you\'re ready. ' .
          '<div class="form-spacing">' .
            '<label for="groupwizard-pre-order-final-select">The day ' .
              'pre-order closes:</label>' .
            '<select id="groupwizard-pre-order-final-select">' .
              $this->Options('pre-order-final', $weekdays) .
            '</select>' .
          '</div>' .
          '<div class="form-spacing hidden">' .
            '<label for="groupwizard-pre-order-final-input">The day ' .
              'pre-order closes:</label>' .
            '<input id="groupwizard-pre-order-final-input" type="text" ' .
              'maxlength="50" value="' . $this->Value('pre-order-final',
                                                      $weekdays) . '">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="groupwizard-pre-order-final-date">' .
              'Switch to calendar date</label>' .
            '<input type="checkbox" id="groupwizard-pre-order-final-date">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="groupwizard-pre-order-final-time">The time ' .
              'pre-order closes:</label>' .
            '<select id="groupwizard-pre-order-final-time">' .
              $this->Options('pre-order-final-time', $hours) .
            '</select>' .
          '</div>' .
          'When pre-order is not open, display a message to members to ' .
          'return later.' .
          '<div class="form-spacing">' .
            '<label for="groupwizard-pre-order-unavailable">Message:</label>' .
            '<textarea id="groupwizard-pre-order-unavailable">' .
              $this->Value('pre-order-unavailable') . '</textarea>' .
          '</div>' .
          'If you would like to show tabs on the purchase page, you can ' .
          'enter the categories you want to use here. They should be ' .
          'separated by commas and match the categories used on the stock ' .
          'page.' .
          '<div class="form-spacing">' .
            '<label for="groupwizard-purchase-categories"> ' .
              'Purchase categories:</label>' .
            '<input id="groupwizard-purchase-categories" type="text" ' .
              'value="' . $this->Value('purchase-categories') . '">' .
          '</div>' .
          'You can set special accounts that are allowed to order for other ' .
          'members. <a href="#" class="groupwizard-purchase-other-order">' .
            'Click here to edit this group</a>.' .
        '</div>' .
      '</div>' .

      '<div class="groupwizard-4 hidden"><b>Section 4:</b> Order cycle ' .
        'settings.' .
        '<p>For groups that meet on a <i>weekly basis</i>:</p>' .
        'If you allow ordering for the following week <i>on the same day ' .
        'that you collect</i>, you can force the purchase page to remain ' .
        'in order mode. You can then add selected accounts to the ' .
        '<b>admin/purchase-volunteer</b> group, which becomes the only way ' .
          'to access the volunteer purchasing mode.' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-purchase-force-order">Force the purchase ' .
            'page to always stay in order mode?</label>' .
          '<input type="checkbox" id="groupwizard-purchase-force-order"' .
            $this->Checked('purchase-force-order') . '>' .
        '</div>' .
        '<a href="#" class="groupwizard-purchase-volunteer">Click here to ' .
          'edit the admin group.</a><br>(When the dialog opens, type ' .
          '<b>admin/purchase-volunteer</b> as the group name.)' .
        '<hr>' .
        '<p>For groups that meet on a <i>fortnightly basis</i>:</p>' .
        '<p>You need to enter calendar dates to order once a fortnight, but ' .
          'once they\'re set they can be automatically updated to the next ' .
          'fortnight after invoices have been sent. (ie pick the following ' .
          'day here.)</p>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-update-order-cycle">The day to update ' .
            'the order cycle:</label>' .
          '<input id="groupwizard-update-order-cycle" type="text" ' .
            'maxlength="50" value="' . $this->Value('update-order-cycle') .'">'.
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-update-order-cycle-time">The time to ' .
            'update the order cycle:</label>' .
          '<select id="groupwizard-update-order-cycle-time">' .
            $this->Options('update-order-cycle-time', $hours) .
          '</select>' .
        '</div>' .
      '</div>' .

      '<div class="groupwizard-5 hidden"><b>Section 5:</b> Payment settings.' .
        $this->PaymentSettings($invite_group, $surcharge) .
      '</div>' .

      '<div class="groupwizard-6 hidden"><b>Section 6:</b> Invoice ' .
        'configuration.' .
        $this->InvoiceConfiguration($invite_group, $rounding) .
      '</div>' .

      '<div class="groupwizard-7 hidden"><b>Section 7:</b> Invoice formatting.'.
        $this->InvoiceFormat($invite_group, $surcharge) .
      '</div>' .

      '<div class="groupwizard-8 hidden"><b>Section 8:</b> Settings to send ' .
        'order emails to members.' .
        '<p>Make sure orders are sent after ordering closes. Also, if you ' .
          'are using calendar dates, you should set a <b>lead time</b> if ' .
          'delivery will be more than a week after orders are sent. ' .
          '(Set it to the number of days between when the order is sent ' .
          'and when the delivery will arrive.)</p>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-send-orders-select">The day to ' .
            'send orders:</label>' .
          '<select id="groupwizard-invoice-send-orders-select">' .
            $this->Options('invoice-send-orders', $weekdays) .
          '</select>' .
        '</div>' .
        '<div class="form-spacing hidden">' .
          '<label for="groupwizard-invoice-send-orders-input">The day to ' .
            'send out orders:</label>' .
          '<input id="groupwizard-invoice-send-orders-input" type="text" ' .
            'maxlength="50" value="' . $this->Value('invoice-send-orders',
                                                    $weekdays) . '">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-send-orders-date">Switch to ' .
            'calendar date</label>' .
          '<input type="checkbox" id="groupwizard-invoice-send-orders-date">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-send-orders-time">The time to send '.
            'out orders:</label>' .
          '<select id="groupwizard-invoice-send-orders-time">' .
            $this->Options('invoice-send-orders-time', $hours) .
          '</select>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-order-lead-time">The number of ' .
            'days lead time:</label>' .
          '<input id="groupwizard-invoice-order-lead-time" type="text" ' .
           'maxlength="5" value="'.$this->Value('invoice-order-lead-time').'">'.
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-order-subject">Orders email ' .
            'subject:</label>' .
          '<input id="groupwizard-invoice-order-subject" type="text" ' .
           'maxlength="100" value="'.$this->Value('invoice-order-subject').'">'.
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-order-start">Orders email ' .
            'start:</label>' .
          '<input id="groupwizard-invoice-order-start" type="text" ' .
            'maxlength="100" value="'.$this->Value('invoice-order-start').'">' .
        '</div>' .
      '</div>' .

      '<div class="groupwizard-9 hidden"><b>Section 9:</b> Settings for the ' .
        'order email for the group.' .
        '<p>Make sure orders are sent after ordering closes. Also, if you ' .
          'are using calendar dates, you should set a <b>lead time</b> if ' .
          'delivery will be more than a week after orders are sent. ' .
          '(Set it to the number of days between when the order is sent ' .
          'and when the delivery will arrive.)</p>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-group-order-select">The day to ' .
            'send the group order:</label>' .
          '<select id="groupwizard-invoice-group-order-select">' .
            $this->Options('invoice-group-order', $weekdays) .
          '</select>' .
        '</div>' .
        '<div class="form-spacing hidden">' .
          '<label for="groupwizard-invoice-group-order-input">The day to ' .
            'send the group order:</label>' .
          '<input id="groupwizard-invoice-group-order-input" type="text" ' .
            'maxlength="50" value="' . $this->Value('invoice-group-order',
                                                    $weekdays) . '">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-group-order-date">Switch to ' .
            'calendar date</label>' .
          '<input type="checkbox" id="groupwizard-invoice-group-order-date">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-group-order-time">The time to send '.
            'the group order:</label>' .
          '<select id="groupwizard-invoice-group-order-time">' .
            $this->Options('invoice-group-order-time', $hours) .
          '</select>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-group-lead-time">The number of ' .
            'days lead time:</label>' .
          '<input id="groupwizard-invoice-group-lead-time" type="text" ' .
           'maxlength="5" value="'.$this->Value('invoice-group-lead-time').'">'.
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-group-email">The address to send ' .
            'to:</label>' .
          '<input id="groupwizard-invoice-group-email" type="text" ' .
            'maxlength="100" value="'.$this->Value('invoice-group-email').'">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-group-subject">The email subject:' .
          '</label>' .
          '<input id="groupwizard-invoice-group-subject" type="text" ' .
           'maxlength="100" value="'.$this->Value('invoice-group-subject').'">'.
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-group-format">Sort the group ' .
            'order by product or user:</label>' .
          '<select id="groupwizard-invoice-group-format">' .
            $this->Options('invoice-group-format', $group_format) .
          '</select>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-group-attach-files">Attach ' .
            'supplier lists to the group order?</label>' .
          '<input type="checkbox" id="groupwizard-invoice-group-attach-files"' .
            $this->Checked('invoice-group-attach-files') . '>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-intro-orders">The start of the ' .
            'group email:</label><br>' .
          '<textarea id="groupwizard-invoice-intro-orders">' .
            $this->Value('invoice-intro-orders') . '</textarea>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-intro-no-orders">The start of the ' .
            'email when no orders:</label><br>' .
          '<textarea id="groupwizard-invoice-intro-no-orders">' .
            $this->Value('invoice-intro-no-orders') . '</textarea>' .
        '</div>' .
      '</div>' .

      '<div class="groupwizard-10 hidden"><b>Section 10:</b> ' .
        'Settings for the order email for the organisation or host.' .
        $this->OrganisationEmail($organisation, $invite_group,
                                 $select_ignore_group) .
      '</div>' .

      '<div class="groupwizard-11 hidden"><b>Section 11:</b> Stock settings.' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-stock-order-available">Show purchase and ' .
            'order availability?</label>' .
          '<input type="checkbox" id="groupwizard-stock-order-available"' .
            $this->Checked('stock-order-available') . '>' .
        '</div>' .
        '<div class="form-spacing hidden">When both availabilities are ' .
          'shown, you can also have two sets of prices to allow for week to ' .
          'week changes in availability lists from your suppliers. To use ' .
          'this feature, you will also need to set the day when your current ' .
          'order settings are used to update the purchase settings. Make ' .
          'sure this happens after invoices are sent.<br>' .
          '<label for="groupwizard-stock-order-update">Select the day to ' .
            'update availability:</label>' .
          '<select id="groupwizard-stock-order-update">' .
            $this->Options('stock-order-update', $weekdays) .
          '</select>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-stock-track-quantity">Track stock levels?' .
          '</label>' .
          '<input type="checkbox" id="groupwizard-stock-track-quantity"' .
            $this->Checked('stock-track-quantity') . '>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-stock-track-quantity">Unavailable to order '.
          'when out of stock?</label>' .
          '<input type="checkbox" id="groupwizard-stock-limited"' .
            $this->Checked('stock-limited') . '>' .
        '</div>' .
        '<p><b>Note:</b> All percentages below should be a value between ' .
          '<b>0</b> and <b>100</b> when provided.</p>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-stock-tax-percent">The percentage of ' .
            'tax included:</label>' .
          '<input id="groupwizard-stock-tax-percent" type="text" ' .
            'maxlength="5" value="' . $this->Value('stock-tax-percent') . '">' .
        '</div>' .
        '<p>Edit the default markup to apply to supplier prices. Leave ' .
          'either option empty to hide it on the stock page. You can also ' .
          'set the options to \'0\' to show it on the stock page and not ' .
          'have it apply markup automatically.</p>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-stock-wholesale-percent">The markup for ' .
            'wholesale:</label>' .
          '<input id="groupwizard-stock-wholesale-percent" type="text" ' .
           'maxlength="5" value="'.$this->Value('stock-wholesale-percent').'">'.
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-stock-retail-percent">The markup for ' .
            'retail:</label>' .
          '<input id="groupwizard-stock-retail-percent" type="text" ' .
            'maxlength="5" value="' .$this->Value('stock-retail-percent').'">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-banking-default-group">Default markup ' .
            'setting for new members:</label>' .
          '<select id="groupwizard-banking-default-group">' .
            $this->Options('banking-default-group', $banking) .
          '</select>' .
        '</div>' .
        '<p>Once you\'ve modified the markup percentages above, you can ' .
          'update the markup for all products by clicking the button below. ' .
          'Note that this should only be used if you want to update the ' .
          'markup for your entire stock list.</p>' .
        '<button id="groupwizard-update-markup">update markup for all products'.
        '</button>' .
        '<div id="groupwizard-update-markup-info"></div>' .
      '</div>' .

      '<div class="groupwizard-12 hidden"><b>Section 12:</b> Summary pages.' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-summary-pre-order">Pre-order text:' .
          '</label>' .
          '<input id="groupwizard-summary-pre-order" type="text" ' .
            'maxlength="100" value="' . $this->Value('summary-pre-order') .'">'.
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-summary-balance">Balance text:' .
          '</label>' .
          '<input id="groupwizard-summary-balance" type="text" ' .
            'maxlength="100" value="' . $this->Value('summary-balance') . '">' .
        '</div>' .
      '</div>' .

      '<div class="groupwizard-13 hidden"><b>Section 13:</b> ' .
        'Organiser settings.' . $this->OrganiserSettings($invite_group) .
      '</div>' .

      '<div class="groupwizard-14 hidden"><b>Section 14:</b> New users.' .
        $this->NewUsers($invite_group) .
      '</div></div>';

    $this->user->group = $default_group;
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {
    if (!$this->Run(date('H:00:00'))) return;

    $mysqli = connect_db();
    foreach ($this->RunGroups('update-order-cycle') as $run_group) {
      $group = $run_group['group'];
      $this->user = new User($this->owner, $group, $run_group['timezone']);
      $update_labels = ['co-op-day', 'pre-order-open', 'pre-order-final',
                        'update-order-cycle', 'invoice-send-orders',
                        'invoice-group-order', 'invoice-day',
                        'invoice-remove-orders'];
      foreach ($update_labels as $label) {
        if ($timestamp = strtotime($this->Substitute($label))) {
          // Add a fortnight to the current value and convert back to a date.
          $value = date('j M Y', $timestamp + 1209600);
          $query = 'INSERT INTO template VALUES ("' . $label . '", ' .
            '"' . $group . '", "' . $value . '") ON DUPLICATE KEY UPDATE ' .
            'content = "' . $value . '"';
          if (!$mysqli->query($query)) {
            $this->Log('Groupwizard->Cron: ' . $mysqli->error);
          }
        }
      }
    }
    $mysqli->close();
  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.groupwizard.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.groupwizard.js', false);
    // The first rule is to avoid showing any content until the dialog is ready.
    $site_style = ['"", ".groupwizard", "display", "none"',
                   '"", "#groupwizard-info", "background-color", "inherit"',
                   '"", "#groupwizard-info", "border", "none"',
                   '"", "#groupwizard-info", "color", "#aaaaaa"',
                   '"", "#groupwizard-info", "font-style", "italic"',
                   '"", "#groupwizard-info", "margin", "0"',
                   '"", "#groupwizard-info", "text-align", "right"',
                   '"", ".groupwizard > div", "padding", "5px"',
                   '"", ".groupwizard > div", "margin", "5px"',
                   '"", ".groupwizard > div", "color", "#222222"',
                   '"", ".groupwizard > div", "background-color", "#eeeeee"',
                   '"", ".groupwizard > div", "border", "1px solid #aaaaaa"',
                   '"", ".groupwizard > div", "border-radius", "2px"',
                   '"", ".groupwizard .form-spacing", "padding", ' .
                     '"2px 2px 2px 5px"',
                   '"", ".groupwizard .next", "float", "right"',
                   '"", "#groupwizard-existing-group", "float", "right"',
                   '"","#groupwizard-invoice-order-lead-time","width","80px"',
                   '"","#groupwizard-invoice-group-lead-time","width","80px"',
                   '"","#groupwizard-invoice-day-count","width","80px"',
                   '"","#groupwizard-invoice-order-rounding","width","80px"',
                   '"","#groupwizard-stock-tax-percent","width","80px"',
                   '"","#groupwizard-stock-wholesale-percent","width","80px"',
                   '"","#groupwizard-stock-retail-percent","width","80px"',
                   '"", ".groupwizard textarea", "width", "600px"',
                   '"", ".groupwizard textarea", "height", "100px"',
                   '"", "label[for=groupwizard-invoice-subject]", ' .
                     '"width", "16em"',
                   '"", "label[for=groupwizard-invoice-no-purchase-subject]", '.
                     '"width", "16em"',
                   '"", "label[for=groupwizard-invoice-sender]", ' .
                     '"width", "16em"',
                   '"", "label[for=groupwizard-invoice-sender-name]", ' .
                     '"width", "16em"',
                   '"","label[for=groupwizard-invoice-surcharge-description]",'.
                     '"width", "16em"',
                   '"", "label[for=groupwizard-invoice-group-email]", ' .
                     '"width", "12em"',
                   '"", "label[for=groupwizard-invoice-group-subject]", ' .
                     '"width", "12em"',
                   '"", ".groupwizard-5 label", "width", "16em"',
                   '"", ".groupwizard-6 label", "width", "18em"',
                   '"", ".groupwizard-8 label", "width", "16em"',
                   '"", ".groupwizard-9 label", "width", "18em"',
                   '"", ".groupwizard-10 label", "width", "14em"',
                   '"", ".groupwizard-11 label", "width", "22em"',
                   '"", ".groupwizard-12 label", "width", "8em"',
                   '"", ".groupwizard-14 label", "width", "18em"',
                   '"","#groupwizard-update-markup","margin-left","160px"',
                   '"", "label[for=groupwizard-organiser-sender]", ' .
                     '"width", "14em"',
                   '"", "label[for=groupwizard-organiser-sender-name]", ' .
                     '"width", "14em"'];
    $this->AddSiteStyle($site_style);
    return $this->Dependencies(['invite', 'payment', 'purchase', 'stock']);
  }

  public function Placement() {
    return $this->user->page === $this->Substitute('groupwizard-page') ?
      'middle' : 'outside';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // This is called when the version of the module is updated,
    // to provide a way to update or modify tables etc..
  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.groupwizard.js', false);
  }

  // Private functions below here ////////////////////////////////////////////

  private function Checked($label) {
    if ($this->Substitute($label) === 'true') {
      return ' checked="checked"';
    }
    return '';
  }

  private function Options($label, $values) {
    $text = '';
    $current = '';
    // co-op-day is a special case that needs to be checked.
    if ($label !== 'co-op-day' || $this->PurchaseDate()) {
      $current = $this->Substitute($label);
    }
    foreach ($values as $option) {
      if ($option === $current) {
        $text .= '<option value="' . $option . '" selected="selected">' .
          $option . '</option>';
      }
      else {
        $text .= '<option value="' . $option . '">' . $option . '</option>';
      }
    }
    return $text;
  }

  private function Value($label, $excluded = null) {
    // co-op-day is a special case that needs to be checked.
    if ($label === 'co-op-day' && !$this->PurchaseDate()) {
      return '';
    }
    $current = $this->Substitute($label);
    if (isset($excluded) && in_array($current, $excluded)) {
      return '';
    }
    return $current;
  }

  private function PurchaseDate() {
    // This function is used to hide the date when co-op-day is more than
    // 6 months away, which signifies that ordering will be closed later.
    // It's also used to update the value when it's not currently set.
    $co_op_day = $this->Substitute('co-op-day');
    if ($co_op_day === '') {
      // When co-op-day is not set, give it a default of one year away.
      $twelve_months = time() + (86400 * 365);
      $co_op_day = date('j M Y', $twelve_months);
      $organiser = new Organiser($this->user, $this->owner);
      $mysqli = connect_db();
      foreach ($organiser->Siblings() as $group) {
        $query = 'INSERT INTO template VALUES ("co-op-day", ' .
          '"' . $group . '", "' . $co_op_day . '") ON DUPLICATE KEY ' .
          'UPDATE content = "' . $co_op_day . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Groupwizard->PurchaseDate: ' . $mysqli->error);
        }
      }
      $mysqli->close();
      return false;
    }
    $six_months = time() + (86400 * 182);
    if (strtotime($co_op_day) > $six_months) {
      return false;
    }
    return true;
  }

  private function SaveSetting() {
    include 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);
    $default_group = $this->user->group;
    if (isset($_SESSION['purchase-group'])) {
      $this->user->group = $_SESSION['purchase-group'];
    }
    $mysqli = connect_db();
    $label = $mysqli->escape_string($_POST['label']);
    $value = $mysqli->escape_string($purifier->purify($_POST['value']));
    // Special case for co-op-day, update existing orders to new date.
    if ($label === 'co-op-day') {
      $weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
                   'Friday', 'Saturday'];
      $co_op_day = $this->Substitute('co-op-day');
      if (in_array($co_op_day, $weekdays)) {
        $co_op_day = 'next ' . $co_op_day;
      }
      $old_timestamp = strtotime($co_op_day);
      $new_timestamp = false;
      if ($value === '') {
        // When a new value for co-op-day isn't given update the template and
        // also the order to one year away.
        $new_timestamp = time() + (86400 * 365);
        $value = date('j M Y', $new_timestamp);
      }
      else if (in_array($value, $weekdays)) {
        $new_timestamp = strtotime('next ' . $value);
      }
      else {
        $new_timestamp = strtotime($value);
      }
      // Only update timestamps for future orders, not existing purchases.
      if ($old_timestamp && $new_timestamp &&
          $old_timestamp > strtotime('23:59:59')) {
        $purchase = new Purchase($this->user, $this->owner);
        $purchase->UpdateTimestamp($old_timestamp, $new_timestamp);
      }
    }

    // Payment values are also a special case, update them in Payment module.
    $payment = NULL;
    if (strpos($label, 'payment') === 0) {
      // Remove the 'payment-' prefix before calling the Payment module.
      $label = substr($label, 8);
      $payment = new Payment($this->user, $this->owner);
    }
    if ($_SESSION['groupwizard-all-groups']) {
      $organiser = new Organiser($this->user, $this->owner);
      foreach ($organiser->Siblings() as $group) {
        if (isset($payment)) {
          $payment->UpdateSetting($label, $value, $group);
        }
        else {
          $query = 'INSERT INTO template VALUES ("' . $label . '", ' .
            '"' . $group . '", "' . $value . '") ON DUPLICATE KEY UPDATE ' .
            'content = "' . $value . '"';
          if (!$mysqli->query($query)) {
            $this->Log('Groupwizard->SaveSetting 1: ' . $mysqli->error);
          }
        }
      }
    }
    else {
      if (isset($payment)) {
        $payment->UpdateSetting($label, $value);
      }
      else {
        $query = 'INSERT INTO template VALUES ("' . $label . '", ' .
          '"' . $this->user->group . '", "' . $value . '") ON DUPLICATE KEY ' .
          'UPDATE content = "' . $value . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Groupwizard->SaveSetting 2: ' . $mysqli->error);
        }
      }
    }
    $mysqli->close();
    if ($label === 'all-groups') {
      $_SESSION['groupwizard-all-groups'] = $value === 'true';
    }
    $this->user->group = $default_group;
    return ['done' => true];
  }

  private function CreateGroup() {
    $mysqli = connect_db();
    $group = $mysqli->escape_string($_POST['group']);
    $mysqli->close();
    if (!preg_match("/^[a-z0-9_-]{1,50}$/i", $group)) {
      return ['error' => 'Group name has the wrong format.'];
    }

    $result = [];
    $default_group = $this->user->group;
    $this->user->group = $group;
    // Check if a group with this name already exists in an organisation.
    $organiser = new Organiser($this->user, $this->owner);
    if ($organiser->Parent() !== false) {
      $result['error'] = 'Group name is not available.';
    }
    else {
      $invite = new Invite($this->user, $this->owner);
      if ($invite->Open($default_group)) {
        $_SESSION['purchase-group'] = $this->user->group;
        $_SESSION['purchase-group-changed'] = true;
        $this->user->group = $default_group;
        $result['content'] = $this->Content(0);
      }
      else {
        $result['error'] = 'Group name is not available.';
      }
    }
    $this->user->group = $default_group;
    return $result;
  }

  private function ChangeGroup() {
    $mysqli = connect_db();
    $group = $mysqli->escape_string($_POST['group']);
    $mysqli->close();
    // If this isn't the user's default group, check that they created it.
    $invite = new Invite($this->user, $this->owner);
    if ($group === $this->user->group || in_array($group, $invite->Created())) {
      $_SESSION['purchase-group'] = $group;
      $_SESSION['purchase-group-changed'] = true;
      return ['content' => $this->Content(0)];
    }
    else {
      return ['error' => 'Permission denied.'];
    }
  }

  private function ExistingGroups($default_group, $created) {
    $existing_groups = '';
    if (count($created) !== 0) {
      $existing_groups = '<div class="groupwizard-existing-group-info hidden">'.
        '<label for"groupwizard-exiting-group-select">Choose a group:</label>' .
        '<select id="groupwizard-existing-group-select">' .
          '<option value="" selected="selected"></option>' .
          '<option value="' . $default_group . '">' . $default_group .
          '</option>';
      foreach ($created as $value) {
        $existing_groups .= '<option value="' . $value . '">' . $value .
          '</option>';
      }
      $existing_groups .= '</select></div>';
      // Also add a hidden div when multiple groups are available, which is
      // used to update the dialog's title. This is so the user can always
      // see which group they are updating the settings for.
      $existing_groups .= '<div class="groupwizard-selected-group hidden">' .
        $this->Substitute('group-name') . '</div>';
    }
    // The Groupwizard module can be displayed as a dialog or in the page.
    $intro = '<p>This dialog will help you configure your group settings.</p>' .
      '<p>Your changes will be automatically saved as you navigate through ' .
        'each section, and when this dialog is closed.</p>';
    if ($this->user->page === $this->Substitute('groupwizard-page')) {
      $intro = '<p>This page will help you configure your group settings.</p>' .
        '<p>Your changes will be automatically saved as you navigate through ' .
          'each section.</p>';
    }
    return '<div class="groupwizard-0">' . $intro .
      '<p>Do you want to create a new buying group, or update an existing ' .
        'one?<br>(You can just click next to update your default group.)</p>' .
      '<button id="groupwizard-new-group">create a new group</button>' .
      '<button id="groupwizard-existing-group">update existing group' .
      '</button>' .
      '<div class="groupwizard-new-group-info hidden">' .
        'The new group should be given a short name to identify it here, no ' .
        'spaces or punctutation. You can give it a descriptive name in the ' .
        'next section.<br>(<i>Note:</i> if you haven\'t yet configured your ' .
        'default group, please click \'update existing group\' above.)' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-new-group-input">New group:</label>' .
          '<input id="groupwizard-new-group-input" type="text" maxlength="50">'.
        '</div>' .
        '<button id="groupwizard-create-group">create</button>' .
        '</div>' .
        $existing_groups .
      '</div>';
  }

  private function InvoiceConfiguration($invite_group, $rounding) {
    $weekdays = ['', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
                 'Friday', 'Saturday'];
    $hours = ['', '12am', '1am', '2am', '3am', '4am', '5am', '6am', '7am',
              '8am', '9am', '10am', '11am', '12pm', '1pm', '2pm', '3pm', '4pm',
              '5pm', '6pm', '7pm', '8pm', '9pm', '10pm', '11pm'];

    $content = '';
    if ($invite_group) {
      $content .= '<p>Some invoice settings only need to be configured for ' .
        'your default group.</p><p>Purchases from this buying group will be ' .
        'included in the next invoice your members receive. If you have not ' .
        'configured your default group to send weekly invoices, you should ' .
        'update the date that invoices are sent to soon after this buying ' .
        'group closes.</p>';
    }
    else {
      $content .= '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-day-select">The day to send out ' .
            'invoices:</label>' .
          '<select id="groupwizard-invoice-day-select">' .
            $this->Options('invoice-day', $weekdays) .
          '</select>' .
        '</div>' .
        '<div class="form-spacing hidden">' .
          '<label for="groupwizard-invoice-day-input">The day to send out ' .
            'invoices:</label>' .
          '<input id="groupwizard-invoice-day-input" type="text" ' .
           'maxlength="50" value="'.$this->Value('invoice-day', $weekdays).'">'.
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-day-date">Switch to calendar date' .
          '</label>' .
          '<input type="checkbox" id="groupwizard-invoice-day-date">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-day-time">The time to send out ' .
            'invoices:</label>' .
          '<select id="groupwizard-invoice-day-time">' .
            $this->Options('invoice-day-time', $hours) .
          '</select>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-look-forward">Invoices are sent ' .
            'out <b>before</b> the order is collected:</label>' .
          '<input type="checkbox" id="groupwizard-invoice-look-forward" ' .
            $this->Checked('invoice-look-forward') . '>' .
        '</div>' .
        '<p>If the day invoices are sent is different from when purchases are '.
          'collected, enter the number of days here between sending the ' .
          'invoice and collection. This field can also be used to include ' .
          'more than one day of purchases in the invoice.</p>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-day-count">Number of days covered ' .
            'by invoice:</label>' .
          '<input id="groupwizard-invoice-day-count" type="text" ' .
            'maxlength="5" value="' . $this->Value('invoice-day-count') . '">' .
        '</div>';
    }
    if ($this->Substitute('groupwizard-remove-orders') === 'true') {
      $content .= '<p>Orders can be removed from the system after they\'ve ' .
          'been processed, so that unavailable products don\'t get included ' .
          'in purchases. The date orders are removed should be set to just ' .
          'before purchases will be entered, so that members can be reminded ' .
          'via their summary page that they have an order placed.</p>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-remove-orders-select">The day to ' .
            'remove orders:</label>' .
          '<select id="groupwizard-invoice-remove-orders-select">' .
            $this->Options('invoice-remove-orders', $weekdays) .
          '</select>' .
        '</div>' .
        '<div class="form-spacing hidden">' .
          '<label for="groupwizard-invoice-remove-orders-input">The day to ' .
            'remove orders:</label>' .
          '<input id="groupwizard-invoice-remove-orders-input" type="text" ' .
            'maxlength="50" value="' . $this->Value('invoice-remove-orders',
                                                    $weekdays) . '">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-remove-orders-date">Switch to ' .
            'calendar date</label>' .
          '<input type="checkbox" id="groupwizard-invoice-remove-orders-date">'.
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-remove-orders-time">' .
            'The time to remove orders:</label>' .
          '<select id="groupwizard-invoice-remove-orders-time">' .
            $this->Options('invoice-remove-orders-time', $hours) .
          '</select>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-purchase-check-quota">Only remove orders ' .
            'for products where quota wasn\'t met:</label>' .
          '<input type="checkbox" id="groupwizard-purchase-check-quota"' .
            $this->Checked('purchase-check-quota') . '>' .
        '</div>';
    }
    $content .= '<p>Orders for a product can be generated as a fraction or ' .
        'rounded to the nearest whole quantity. Enter a value between <b>0 ' .
        'and 1</b>, or leave this empty to keep as fractions.</p>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-order-rounding">Round down when ' .
          'fraction is less than:</label>' .
        '<input id="groupwizard-invoice-order-rounding" type="text" ' .
          'maxlength="5" value="' . $this->Value('invoice-order-rounding').'">'.
      '</div>' .
      'If you set a rounding value, you can use it to automatically adjust ' .
      'individual orders for your members. This means total orders will ' .
      'exactly match the number of boxes calculated when rounding.' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-' . $rounding . '">' .
          'Automatically adjust orders?</label>' .
        '<input type="checkbox" id="groupwizard-' . $rounding . '"' .
          $this->Checked($rounding) . '>' .
      '</div>' .
      'You can be notified when invoices are sent, ' .
      '<a href="#" class="groupwizard-invoice-notifications">' .
        'click here to edit this group</a>.' .
      '<input type="hidden" id="groupwizard-current-group" ' .
        'value="' . $this->user->group . '">';
    return $content;
  }

  private function InvoiceFormat($invite_group, $surcharge) {
    if ($invite_group) {
      return '<p>Invoices only need to be configured for your default group.' .
        '</p><p>Purchases from this buying group will be included in the ' .
        'next invoice your members receive. If you have not configured your ' .
        'default group to send weekly invoices, you should update the date ' .
        'that invoices are sent to soon after this buying group closes.</p>' .
        '<p>The settings below will be used when sending orders for this ' .
        'buying group.</p>' .
        '<i>The sender email address must be on the current domain.</i>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-sender">Order email sender:' .
          '</label>' .
          '<input id="groupwizard-invoice-sender" type="text" ' .
            'maxlength="100" value="' . $this->Value('invoice-sender') . '">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-sender-name">Order email ' .
            'sender name:</label>' .
          '<input id="groupwizard-invoice-sender-name" type="text" ' .
            'maxlength="100" value="' .$this->Value('invoice-sender-name').'">'.
        '</div>';
    }

    $membership_reminder = '';
    if ($this->Substitute('display-membership-reminder') === 'true') {
      $membership_reminder = '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-membership-reminder">' .
            'Message when membership is due:</label>' .
          '<textarea id="groupwizard-invoice-membership-reminder">' .
            $this->Value('invoice-membership-reminder') . '</textarea>' .
        '</div>';
    }
    $surcharge_label = '';
    if ($surcharge) {
      $surcharge_label = '<div class="form-spacing">' .
          '<label for="groupwizard-invoice-surcharge-description">' .
            'Description for surcharge:</label>' .
          '<input id="groupwizard-invoice-surcharge-description" type="text" ' .
            'maxlength="100" ' .
            'value="' . $this->Value('invoice-surcharge-description') . '">' .
        '</div>';
    }

    return '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-subject">Invoice email subject:' .
        '</label>' .
        '<input id="groupwizard-invoice-subject" type="text" ' .
          'maxlength="100" value="' . $this->Value('invoice-subject') . '">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-no-purchase-subject">' .
          'Account Statement email subject:</label>' .
        '<input id="groupwizard-invoice-no-purchase-subject" type="text" ' .
          'maxlength="100" ' .
          'value="' . $this->Value('invoice-no-purchase-subject') . '">' .
      '</div>' .
      '<i>The sender email address must be on the current domain.</i>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-sender">Invoice email sender:' .
        '</label>' .
        '<input id="groupwizard-invoice-sender" type="text" ' .
          'maxlength="100" value="' . $this->Value('invoice-sender') . '">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-sender-name">Invoice email ' .
          'sender name:</label>' .
        '<input id="groupwizard-invoice-sender-name" type="text" ' .
          'maxlength="100" value="' . $this->Value('invoice-sender-name') .'">'.
      '</div>' .
      $surcharge_label .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-start-purchase">Start of invoice ' .
          'when purchases:</label>' .
        '<textarea id="groupwizard-invoice-start-purchase">' .
          $this->Value('invoice-start-purchase') . '</textarea>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-start-no-purchase">Start of ' .
          'invoice when no purchases:</label>' .
        '<textarea id="groupwizard-invoice-start-no-purchase">' .
          $this->Value('invoice-start-no-purchase') . '</textarea>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-tax-included">Summary of tax ' .
          'included in purchases:</label>' .
        '<textarea id="groupwizard-invoice-tax-included">' .
          $this->Value('invoice-tax-included') . '</textarea>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-after-purchase">' .
          'Message after purchases are displayed:</label>' .
        '<textarea id="groupwizard-invoice-after-purchase">' .
          $this->Value('invoice-after-purchase') . '</textarea>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-user-info">' .
          'Member account information:</label>' .
        '<textarea id="groupwizard-invoice-user-info">' .
          $this->Value('invoice-user-info') . '</textarea>' .
      '</div>' .
      $membership_reminder .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-current-balance">' .
          'Message including current balance:</label>' .
        '<textarea id="groupwizard-invoice-current-balance">' .
          $this->Value('invoice-current-balance') . '</textarea>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-old-balance">' .
          'Message including balance with previous processing date:</label>' .
        '<textarea id="groupwizard-invoice-old-balance">' .
          $this->Value('invoice-old-balance') . '</textarea>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-sign-off">Invoice sign off:' .
        '</label>' .
        '<textarea id="groupwizard-invoice-sign-off">' .
          $this->Value('invoice-sign-off') . '</textarea>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-roster-reminder">Add a reminder to ' .
          'invoices to volunteer on the roster?</label>' .
        '<input type="checkbox" id="groupwizard-roster-reminder"' .
          $this->Checked('roster-reminder') . '>' .
      '</div>';
  }

  private function OrganisationEmail($organisation,
                                     $invite_group, $select_ignore_group) {
    if (!$organisation && !$invite_group) {
      return '<p>This section is used for invite orders that were created by '.
        'your group or the group is part of a larger organisation. An orders '.
        'email is sent that collates the total orders for all groups.</p>';
    }

    $organisation_format = ['', 'horizontal', 'vertical'];
    $content = '';
    if ($organisation) {
      $content .= '<p>As your group is part of a larger organisation, an ' .
          'orders email is sent that collates the total orders for all groups.'.
        '</p>';
    }
    else {
      $content .= '<p>As your group is the host for this invite order, an ' .
        'email is sent that collates the total orders for all groups.</p>';
    }

    return $content.
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-organisation-email">The address ' .
          'to send to:</label>' .
        '<input id="groupwizard-invoice-organisation-email" type="text" ' .
          'maxlength="100" ' .
          'value="' . $this->Value('invoice-organisation-email') . '">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-organisation-subject">The subject ' .
          'for the email:</label>' .
        '<input id="groupwizard-invoice-organisation-subject" type="text" ' .
          'maxlength="100" ' .
          'value="' . $this->Value('invoice-organisation-subject') . '">' .
      '</div>' .
      '<i>The sender email address must be on the current domain.</i>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-organisation-sender">The address ' .
          'to send from:</label>' .
        '<input id="groupwizard-invoice-organisation-sender" type="text" ' .
          'maxlength="100" ' .
          'value="'. $this->Value('invoice-organisation-sender') . '">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-organisation-sender-name">The name ' .
          'of the sender:</label>' .
        '<input id="groupwizard-invoice-organisation-sender-name" type="text" '.
          'maxlength="100" ' .
          'value="' . $this->Value('invoice-organisation-sender-name') . '">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-organisation-format">Format of ' .
          'the organisation email:</label>' .
        '<select id="groupwizard-invoice-organisation-format">' .
          $this->Options('invoice-organisation-format', $organisation_format) .
        '</select>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-invoice-extra-column">Extra column in ' .
          'spreadsheet:</label>' .
        '<input id="groupwizard-invoice-extra-column" type="text" ' .
          'maxlength="50" value="' . $this->Value('invoice-extra-column') .'">'.
      '</div>' .
      $select_ignore_group;
  }

  private function NewUsers($invite_group) {
    $content = '<p>Default timezone for this group.</p>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-timezone">Timezone:</label>' .
        '<input id="groupwizard-timezone" type="text" ' .
          'value="' . $this->Substitute('timezone') . '">' .
      '</div>';
    if (!$invite_group) {
      $content .= '<p>Settings for the email to send to new members.<br>' .
        '<i>The sender email address must be on the current domain.</i></p>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-new-user-sender">The address to send ' .
            'email from:</label>' .
          '<input id="groupwizard-new-user-sender" type="text" ' .
            'maxlength="100" value="' . $this->Value('new-user-sender') . '">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-new-user-sender-name">' .
            'The name of the sender:</label>' .
          '<input id="groupwizard-new-user-sender-name" type="text" ' .
            'value="' . $this->Value('new-user-sender-name') . '">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-new-user-info">Text to add to the bottom ' .
            'of the email:</label><br>' .
          '<textarea id="groupwizard-new-user-info">' .
            $this->Value('new-user-info') . '</textarea>' .
        '</div>';
    }
    return $content;
  }

  private function OrganiserSettings($invite_group) {
    if ($invite_group) {
      return '<p>Organiser settings only need to be configured for your ' .
        'default group.</p>';
    }

    return '<p><i>The sender email address must be on the current domain.</i>' .
      '</p>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-organiser-sender">The address to ' .
          'send from:</label>' .
        '<input id="groupwizard-organiser-sender" type="text" ' .
          'maxlength="100" value="' . $this->Value('organiser-sender') . '">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-organiser-sender-name">The name of the ' .
          'sender:</label>' .
        '<input id="groupwizard-organiser-sender-name" type="text" ' .
          'maxlength="100" value="' .$this->Value('organiser-sender-name').'">'.
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-organiser-email-body">Email template:' .
        '</label><br>' .
          '<textarea id="groupwizard-organiser-email-body">' .
            $this->Value('organiser-email-body') . '</textarea>' .
      '</div>';
  }

  private function PaymentSettings($invite_group, $surcharge) {
    if ($invite_group) {
      return '<p>Payment settings only need to be configured for your ' .
        'default group.</p>';
    }

    $payment = new Payment($this->user, $this->owner);
    $payment_settings = $payment->AllSettings();
    $stock = new Stock($this->user, $this->owner);
    $surcharge_info = '';
    if ($stock->SurchargeSupplier() === '') {
      $surcharge_info = '<p>You have selected to add a surcharge but ' .
        'don\'t currently have a supplier account set for it. Please go to ' .
        'the stock page and <b>add the surcharge as a product</b>. It is ' .
        'recommended that you create a new supplier for this purpose.</p><hr>';
    }
    else {
      $surcharge_info = '<div>Enter a surcharge value of $1 or above for a ' .
          'fixed surcharge.<br>Enter a surcharge as a decimal between 0 and ' .
          '1 for a percentage surcharge. (The min and max surcharge values ' .
          'only apply to a percentage surcharge)' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-payment-surcharge">Surcharge:</label>' .
          '<input id="groupwizard-payment-surcharge" type="text" ' .
            'value="' . $payment_settings['surcharge'] . '" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-payment-min_surcharge">Minimum Surcharge:' .
          '</label>' .
          '<input id="groupwizard-payment-min_surcharge" type="text" ' .
            'value="' . $payment_settings['min_surcharge'] . '" ' .
            'maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="groupwizard-payment-max_surcharge-">Maximum Surcharge:' .
          '</label>' .
          '<input id="groupwizard-payment-max_surcharge" type="text" ' .
            'value="' . $payment_settings['max_surcharge'] . '" ' .
            'maxlength="50">' .
        '</div>';
    }
    $surcharge_checked = $surcharge ? 'checked = "checked"' : '';
    return '<div class="form-spacing">' .
        '<label for="groupwizard-surcharge">Do members pay a surcharge?' .
        '</label>' .
        '<input type="checkbox" id="groupwizard-surcharge" ' .
          $surcharge_checked . '>' .
      '</div>' .
      '<div class="groupwizard-payment-settings hidden">' .
        $surcharge_info .
      '</div>' .
      '<p>The amount set here for the deposit will be deducted from new ' .
        'accounts as they are created. Members will initially owe that ' .
        'amount until it\'s been paid.</p>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-payment-deposit">Deposit:</label>' .
        '<input id="groupwizard-payment-deposit" type="text" ' .
          'value="' . $payment_settings['deposit'] . '" maxlength="50">' .
      '</div>' .
      '<p>Members will be shown a message when ordering and collecting ' .
        'when their balance is owing more than the level indicated here.</p>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-payment-info">Reminder Level:</label>' .
        '<input id="groupwizard-payment-info" type="text" ' .
          'value="' . $payment_settings['info'] . '" maxlength="50">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="groupwizard-payment-warning">Warning Level:</label>'.
        '<input id="groupwizard-payment-warning" type="text" ' .
          'value="' . $payment_settings['warning'] . '" maxlength="50">' .
      '</div>';
  }

  private function UpdateMarkup() {
    $stock = new Stock($this->user, $this->owner);
    $stock->UpdateMarkup();
    return ['content' => 'All products updated.'];
  }

}
