<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Domaincheck extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'lookup') return $this->Lookup();
    if ($us_action === 'submit') return $this->Submit();
    return ['error' => 'Unknown action'];
  }

  public function CanAdd($page) {
    return !$this->AlreadyOnPage('domaincheck', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    return '<form id="domaincheck-form" autocomplete="off">' .
        '<div class="form-spacing">' .
          '<label for="domaincheck-input">Domain Name:</label>' .
          '<input id="domaincheck-input" type="text" maxlength="100">' .
        '</div>' .
        '<button id="domaincheck-button">Check</button>' .
        '<div id="domaincheck-info"></div>' .
      '</form>' .
      '<form id="domaincheck-email-form" class="hidden" autocomplete="off">' .
        '<div class="form-spacing">' .
          '<label for="domaincheck-email">Email Address:</label>' .
          '<input id="domaincheck-email" type="text" maxlength="100">' .
        '</div>' .
        '<button id="domaincheck-email-submit">Submit</button>' .
        '<div id="domaincheck-email-info"></div>' .
      '</form>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if ($fn === 'ListDomains') return $this->ListDomains();
    if ($fn === 'Confirm') return $this->Confirm($p);
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $this->AppendScript($path, 'dobrado.domaincheck.js');

    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS domaincheck (' .
      'domain VARCHAR(100) NOT NULL,' .
      'email VARCHAR(100) NOT NULL,' .
      'verification VARCHAR(100) NOT NULL,' .
      'confirmed TINYINT(1) NOT NULL,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(domain)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Domaincheck->Install: ' . $mysqli->error);
    }
    $mysqli->close();

    $description = ['domaincheck-verification' => 'The name of the TXT ' .
                      'record to look for in DNS settings to verify an ' .
                      'email address.',
                    'domaincheck-ip' => 'The IP address to set the A record ' .
                      'to in DNS settings which the domain will be hosted on.',
                    'domaincheck-login-code' => 'The body of an email sent ' .
                      'to a moderator, so that a login code can be generated ' .
                      'for a confirmed user. Substitutes !domain and !email.',
                    'domaincheck-hosting' => 'The body of an email sent to ' .
                      'an unconfirmed user, so that they can add a ' .
                      'verification string to their DNS settings. ' .
                      'Substitutes: !domain, !email and !verification.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"","#domaincheck-form","background-color","#eeeeee"',
                   '"","#domaincheck-form","border","1px solid #aaaaaa"',
                   '"","#domaincheck-form","border-radius","2px"',
                   '"","#domaincheck-form","padding","20px"',
                   '"","#domaincheck-form","margin","20px"',
                   '"","#domaincheck-button","margin-left","7.2em"',
                   '"","#domaincheck-info","margin-top","20px"',
                   '"","#domaincheck-email-form","background-color","#eeeeee"',
                   '"","#domaincheck-email-form","border","1px solid #aaaaaa"',
                   '"","#domaincheck-email-form","border-radius","2px"',
                   '"","#domaincheck-email-form","padding","20px"',
                   '"","#domaincheck-email-form","margin","20px"',
                   '"","#domaincheck-email-submit","margin-left","7.2em"',
                   '"","#domaincheck-email-info","margin-top","20px"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.domaincheck.js');
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function Confirm($us_domain) {
    $mysqli = connect_db();
    $query = 'UPDATE domaincheck SET confirmed = 1 WHERE ' .
      'domain = "' . $mysqli->escape_string($us_domain) . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Domaincheck->Confirm: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function ListDomains() {
    $result = [];
    $mysqli = connect_db();
    $query = 'SELECT domain, email, verification, confirmed, timestamp FROM ' .
      'domaincheck ORDER BY timestamp DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($domaincheck = $mysqli_result->fetch_assoc()) {
        $domaincheck['confirmed'] = (int)$domaincheck['confirmed'];
        $domaincheck['timestamp'] = (int)$domaincheck['timestamp'] * 1000;
        $result[] = $domaincheck;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Domaincheck->ListDomains: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  private function Lookup() {
    $us_domain = trim($_POST['domain'], ' /');
    // Remove the scheme if provided.
    if (preg_match('/\/\/([^\/]+)/', $us_domain, $match)) {
      $us_domain = $match[1];
    }
    if (substr_count($us_domain, '.') === 0) {
      return ['info' => 'Sorry the input you provided is not a domain name.'];
    }

    $result_list = dns_get_record($us_domain, DNS_A + DNS_TXT);
    if (!$result_list) {
      return ['info' => 'Sorry but no domain records were found for the ' .
                        'input you provided.'];
    }

    $ip = '';
    $ip_ttl = 0;
    $txt = '';
    $txt_ttl = 0;
    $check_verification = $this->Substitute('domaincheck-verification');
    foreach ($result_list as $result) {
      if ($result['type'] === 'A') {
        $ip = $result['ip'];
        $ip_ttl = ceil((int)$result['ttl'] / 3600);
        continue;
      }

      if ($result['type'] === 'TXT' &&
          strpos($result['txt'], $check_verification) !== false) {
        $txt = $result['txt'];
        $txt_ttl = ceil((int)$result['ttl'] / 3600);
      }
    }
    $check_ip = $this->Substitute('domaincheck-ip');
    if ($ip === '') {
      $info = 'Nice domain name!<br>It looks like there\'s no IP address ' .
        'set. If you would like it hosted here, please set it to: <b>' .
        $check_ip . '</b> in your DNS settings. (The current settings are ' .
        'cached for ';
      if ($ip_ttl > 1) $info .= $ip_ttl . ' hours.)';
      else $info .= '1 hour.)';
      return ['info' => $info];
    }

    if ($ip !== $check_ip) {
      $info = 'Nice domain name!<br>The IP address is currently set to: <b>' .
        $ip . '</b>. If you would like it hosted here, please change it to: ' .
        '<b>' . $check_ip . '</b> in your DNS settings. (The current ' .
        'settings are cached for ';
      if ($ip_ttl > 1) $info .= $ip_ttl . ' hours.)';
      else $info .= '1 hour.)';
      return ['info' => $info];
    }

    // If ip is set correctly check if we have already recorded the domain.
    $mysqli = connect_db();
    $verification = '';
    $confirmed = false;
    $domain = $mysqli->escape_string($us_domain);
    $query = 'SELECT verification, confirmed FROM domaincheck WHERE ' .
      'domain = "' . $domain . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($domaincheck = $mysqli_result->fetch_assoc()) {
        $verification = $domaincheck['verification'];
        $confirmed = $domaincheck['confirmed'] === '1';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Domaincheck->Lookup: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($confirmed) {
      $_SESSION['domaincheck-domain'] = $us_domain;
      $info = 'It looks like your domain name is currently hosted here. If ' .
        'you need a new login code, please provide the email address you ' .
        'registered with using the form below and one will be emailed to you.';
      return ['info' => $info, 'email' => true];
    }

    if ($verification === '') {
      $_SESSION['domaincheck-domain'] = $us_domain;
      $info = 'Nice domain name!<br>If you would like it hosted here, please ' .
        'provide the email address you want to use to log in to your website.';
      return ['info' => $info, 'email' => true];
    }

    // If a verification string is set but the domain is not confirmed, then
    // it's waiting in a queue to be set up. Can at least let them know if they
    // have set the verification string properly.
    if ($verification === $txt) {
      return ['info' => 'Your DNS settings have been entered correctly, but ' .
                        'setup for your website is waiting in a queue. You ' .
                        'will receive an email once your website has been ' .
                        'created.'];
    }

    $info = 'Setup for your website is in a queue, thanks for waiting! ' .
      'In the mean time, please set the verification string in your DNS ' .
      'settings that was sent to your email address. This will allow you ' .
      'to log in to your website when setup is complete.';
    if ($txt_ttl > 1) {
      $info .= ' (The current settings are cached for ' . $txt_ttl . ' hours.)';
    }
    else if ($txt_ttl === 1) {
      $info .= ' (The current settings are cached for 1 hour.)';
    }
    return ['info' => $info];
  }

  private function Submit() {
    if (!isset($_SESSION['domaincheck-domain'])) {
      return ['error' => 'Domain name not set.'];
    }

    $us_domain = $_SESSION['domaincheck-domain'];
    $us_email = trim($_POST['email']);
    if ($us_email === '') return ['error' => 'Email not provided.'];

    $mysqli = connect_db();
    $domain = $mysqli->escape_string($us_domain);
    $email = $mysqli->escape_string($us_email);
    $check_email = '';
    $confirmed = false;
    $query = 'SELECT email, confirmed FROM domaincheck WHERE ' .
      'domain = "' . $domain . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($domaincheck = $mysqli_result->fetch_assoc()) {
        $check_email = $domaincheck['email'];
        $confirmed = $domaincheck['confirmed'] === '1';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Domaincheck->Submit 1: ' . $mysqli->error);
    }
    $mysqli->close();

    $sender = $this->Substitute('new-user-sender');
    $sender_name = $this->Substitute('new-user-sender-name');
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $cc = $this->Substitute('new-user-sender-cc');
    $bcc = $this->Substitute('new-user-sender-bcc');

    // If the domain is confirmed, email a moderator to create a new login code.
    if ($confirmed) {
      if ($us_email !== $check_email) {
        return ['info' => 'Sorry the email address you provided does not ' .
                          'match our records.'];
      }

      $subject = 'Login code requested';
      $us_content = $this->Substitute('domaincheck-login-code',
                                      ['/!domain/', '/!email/'],
                                      [$us_domain, $us_email]);
      $us_message = '<html><head><title>' . $subject . '</title></head><body>' .
        $us_content . '</body></html>';
      $headers = "MIME-Version: 1.0\r\n" .
        "Content-type: text/html; charset=utf-8\r\n" .
        'From: ' . $sender_name . "\r\n";
      if ($cc !== '') {
        $headers .= 'Cc: ' . $cc . "\r\n";
      }
      if ($bcc !== '') {
        $headers .= 'Bcc: ' . $bcc . "\r\n";
      }
      $moderator = $this->Substitute('new-user-moderator');
      if ($moderator === '') {
        $this->Log('Domaincheck->Submit 2: Moderator not set, message = ' .
                   $us_message);
      }
      else {
        mail($moderator, wordwrap($us_message), $headers, '-f ' . $sender);
      }
      $_SESSION['domaincheck-domain'] = NULL;
      return ['info' => 'Thanks! A login code will be emailed to you soon.'];
    }

    $mysqli = connect_db();
    // Otherwise the user has set the IP address correctly and now needs a
    // verification code to continue.
    $us_verification = $this->Substitute('domaincheck-verification') . '=' .
      bin2hex(openssl_random_pseudo_bytes(16));
    $verification = $mysqli->escape_string($us_verification);
    $query = 'INSERT INTO domaincheck VALUES ("' . $domain . '", ' .
      '"' . $email . '", "' . $verification . '", 0, ' . time() . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Domaincheck->Submit 3: ' . $mysqli->error);
    }
    $mysqli->close();

    $subject = 'Hosting requested';
    $us_content = $this->Substitute('domaincheck-hosting', ['/!domain/',
                                      '/!email/', '/!verification/'],
                                    [$us_domain, $us_email, $us_verification]);
    $us_message = '<html><head><title>' . $subject . '</title></head><body>' .
      $us_content . '</body></html>';
    $headers = "MIME-Version: 1.0\r\n" .
      "Content-type: text/html; charset=utf-8\r\n" .
      'From: ' . $sender_name . "\r\n";
    if ($cc !== '') {
      $headers .= 'Cc: ' . $cc . "\r\n";
    }
    if ($bcc !== '') {
      $headers .= 'Bcc: ' . $bcc . "\r\n";
    }
    mail($us_email, $subject, wordwrap($us_message), $headers, '-f ' . $sender);
    $_SESSION['domaincheck-domain'] = NULL;
    return ['info' => 'Thanks! Instructions on what to do next have been ' .
                      'sent to your email address.'];
  }

}
