// @source: /js/source/dobrado.hosting.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.hosting){dobrado.hosting={};}
(function(){'use strict';var hosting={};var grid=null;var selectedRow=0;$(function(){if($('.hosting').length===0){return;}
$('.hosting .setup-start').button().click(start);$('.hosting .setup-finish').button().click(finish);$.post('/php/request.php',{request:'hosting',action:'list',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'hosting list')){return;}
hosting=JSON.parse(response);if($('.grid').length!==0){gridSetup();}
else{alert('Please add a grid module to this page.');}});});function gridSetup(){var gridId='#'+$('.grid').attr('id');var columns=[{id:'domain',name:'Domain',field:'domain',width:200,sortable:true},{id:'email',name:'Email',field:'email',width:200,sortable:true},{id:'confirmed',name:'Confirmed',field:'confirmed',width:100,sortable:true,formatter:Slick.Formatters.Checkmark},{id:'timestamp',name:'Registered',field:'timestamp',width:100,sortable:true,formatter:Slick.Formatters.Timestamp}];var options={autoHeight:true,editable:false,forceFitColumns:true,multiColumnSort:true};grid=dobrado.grid.instance(gridId,hosting,columns,options);grid.setSelectionModel(new Slick.RowSelectionModel());grid.onClick.subscribe(function(e,item){showHosting(item.row);});grid.onSort.subscribe(function(e,args){var cols=args.sortCols;hosting.sort(function(row1,row2){for(var i=0;i<cols.length;i++){var field=cols[i].sortCol.field;var sign=cols[i].sortAsc?1:-1;var value1=row1[field];var value2=row2[field];if(field==='timestamp'){value1=value1?parseFloat(value1):0;value2=value2?parseFloat(value2):0;}
if(value1===value2){continue;}
if(value1>value2){return sign;}
else{return sign* -1;}}
return 0;});grid.invalidate();});}
function finish(){if(!hosting||!hosting[selectedRow]){return;}
var domain=hosting[selectedRow].domain;$.post('/php/request.php',{request:'hosting',action:'finishSetup',domain:domain,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'hosting finish')){return;}
let info=JSON.parse(response);$('#hosting-info').append(info.output);$('.setup-finish').show();});}
function start(){if(!hosting||!hosting[selectedRow]){return;}
var domain=hosting[selectedRow].domain;dobrado.log('Starting setup...','info');$.post('/php/request.php',{request:'hosting',action:'startSetup',domain:domain,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'hosting start')){return;}
let info=JSON.parse(response);$('#hosting-info').append(info.output);$('.setup-start').hide();$('.setup-finish').show();});}
function showHosting(row){if(!hosting||!hosting[row]){return;}
if(hosting[row].confirmed===0){$('#hosting-info').html('Start set up for domain: '+
hosting[row].domain);$('.setup-start').show();selectedRow=row;}
else{$('.setup-start').hide();}}}());