/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2013 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.gift) {
  dobrado.gift = {};
}
(function() {

  'use strict';

  $(function() {
    // Actions for users claiming gifts.
    $(".claim-form").hide();
    $(".claim-form > button.submit").button().click(claimSubmit);
    $(".claim > button").button().click(toggleClaim);
    $(".claim-left").each(function() {
      if ($(this).html() === "0") {
        // Disable the button if there are none available of this type.
        $(this).siblings("button").button({ disabled: true });
      }
    });

    // Actions for managing gift registry.
    $("#email-message-form > button.submit").button().click(updateEmailMessage);
    $("#moderators-form > button.submit").button().click(updateModerators);
    $("#gift-type-form > button.submit").button().click(updateGiftType);
    $("#gift-type-select").change(giftTypeSelectChange);

    // Actions for clicking links on a map.
    $("#location > a").click(showCategory);

    // Add titles to links showing user's messages.
    addMessages();
  });

  function toggleClaim() {
    var list = $(this).attr("id").split('-');
    var action = list[0];
    var type = list[1];
    if (action === "claim") {
      $("#claim-" + type  + "-form").toggle();
      $("#claim-" + type  + "-form").siblings(".reply").hide();
    }
    else if (action === "change") {
      var id = $(this).parent().attr("id");
      // Note can safely remove this claim_id because a valid email must've
      // been received initially to generate the page with the token.
      $.post("/php/request.php",
             { request: "gift",
               type: type,
               claim_id: id,
               mode: "remove",
               url: location.href,
               token: dobrado.token },
        function(response) {
          if (dobrado.checkResponseError(response, "gift remove")) {
            return;
          }
          $(".reply").html("Thankyou, please select another gift.<br/>" +
                           "Redirecting you to the main page now...").show();
          setTimeout(function() {
            // Reload without the parameters.
            location.href = document.URL.split('?')[0];
          }, 3000);
        });
    }
  }

  function claimSubmit(event) {
    event.preventDefault();
    var name = "";
    var email = "";
    var message = "";
    var form = $(this).parent();
    var type = form.attr("id").split('-')[1];

    form.children(":input").each(function() {
      var input = $(this);
      if (input.attr("name") === "claim-name") {
        name = input.val();
      }
      else if (input.attr("name") === "claim-email") {
        email = input.val();
      }
      else if (input.attr("name") === "claim-message") {
        message = input.val();
      }
    });
    if (name === "") {
      alert("Please enter your name.");
      return false;
    }
    if (email === "") {
      alert("Please enter your email.");
      return false;
    }
    $.post("/php/request.php",
           { request: "gift",
             type: type,
             name: name,
             email: email,
             message : message,
             mode: "claim",
             url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, "gift claim")) {
          return;
        }
        form.hide();
        var available = form.siblings(".claim").first();
        var claim = JSON.parse(response);
        if (claim.unavailable === true) {
          form.siblings(".reply").html("Sorry this is no longer available.");
          available.children("button").button({ disabled: true });
        }
        else {
          available.children(".claim-left").html(claim.remaining);
          form.siblings(".reply").html("Thankyou " + name +
                                       ", please check your email.");
        }
        form.siblings(".reply").show("slide", { direction: "up" }, "slow");
        
        if (claim.id) {
          // Also email the moderators about the new claim.
          $.post("/php/request.php",
                 { request: "gift",
                   mode: "moderators",
                   claimId: claim.id,
                   url: location.href,
                   token: dobrado.token },
            function(response) {
              if (dobrado.checkResponseError(response, "gift moderators")) {
                return;
              }
            });
        }
      });
    return false;
  }

  function updateEmailMessage(event) {
    event.preventDefault();
    $.post("/php/request.php",
           { request: "gift",
             type: "email-message",
             message: $("#email-message-form > textarea").val(),
             mode: "edit",
             url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, "update email message")) {
          return;
        }
        var reply = JSON.parse(response);
        manageResponse(reply.content, "email");
      });
    return false;
  }

  function updateModerators(event) {
    event.preventDefault();
    $.post("/php/request.php",
           { request: "gift",
             type: "moderators",
             message: $("#moderators-input").val(),
             mode: "edit",
             url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, "update moderators")) {
          return;
        }
        var reply = JSON.parse(response);
        manageResponse(reply.content, "moderators");
      });
    return false;
  }

  function updateGiftType(event) {
    event.preventDefault();
    var name = $("#gift-type-name-input").val();
    if (name === "") {
      manageResponse("Gift name is empty", "gift");
      return false;
    }
    $.post("/php/request.php",
           { request: "gift",
             type: "gift-type",
             name: name,
             category: $("#gift-type-category-input").val(),
             title: $("#gift-type-title-input").val(),
             message: $("#gift-type-form > textarea").val(),
             image: $("#gift-type-image-input").val(),
             price: $("#gift-type-price-input").val(),
             claimMax: $("#gift-type-claim-max-input").val(),
             claimCount: $("#gift-type-claim-count-input").val(),
             mode: "edit",
             url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, "update gift-type")) {
          return;
        }
        var reply = JSON.parse(response);
        manageResponse(reply.content, "gift");
        // Update the gift type select.
        var giftType = $("#gift-type-select > option:selected");
        // If title was empty, remove the gift type from the select.
        if ($("#gift-type-title-input").val() === "") {
          giftType.remove();
        }
        // If a new gift type was selected, add the new name to the select.
        else if (giftType.val() === "") {
          $("#gift-type-select").append('<option value="' + name + '">' +
                                        name + '</option>');
          $("#gift-type-select > option:last").select();
        }
      });
    return false;
  }

  function giftTypeSelectChange() {
    var type = $("#gift-type-select > option:selected").val();
    // Reset the input areas to enter a new gift.
    if (type === "") {
      $("#gift-type-name-input").val("");
      $("#gift-type-category-input").val("");
      $("#gift-type-title-input").val("");
      $("#gift-type-form > textarea").val("");
      $("#gift-type-image-input").val("");
      $("#gift-type-price-input").val("");
      $("#gift-type-claim-max-input").val("");
      $("#gift-type-claim-count-input").val("0");
      return;
    }
    $.post("/php/request.php",
           { request: "gift",
             type: type,
             mode: "change",
             url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, "gift type select change")) {
          return;
        }
        var reply = JSON.parse(response);
        $("#gift-type-name-input").val(reply.name);
        $("#gift-type-category-input").val(reply.category);
        $("#gift-type-title-input").val(reply.title);
        $("#gift-type-form > textarea").val(reply.description);
        $("#gift-type-image-input").val(reply.image);
        $("#gift-type-price-input").val(reply.price);
        $("#gift-type-claim-max-input").val(reply.claim_max);
        $("#gift-type-claim-count-input").val(reply.claim_count);
      });
  }

  function manageResponse(content, type) {
    if (content === "") {
      return;
    }

    $(".manage-response." + type).html(content).show();
    setTimeout(function() {
      $(".manage-response." + type).fadeOut();
    }, 3000);
  }

  function showCategory() {
    var name = $(this).attr("id");
    $.post("/php/request.php",
           { request: "gift",
             mode: "display",
             category: name,
             url: location.href,
             token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, "gift display")) {
          return;
        }
        var category = JSON.parse(response);
        $("<div>" + category.content + "</div>").attr({
          id: "gift-category",
          className: "gift"
        }).prependTo("body");
        $("#gift-category").dialog({
          close: close,
          modal: true,
          title: "Gifts available for " + name,
          width: 720,
          height: 500
        });
        // Activate events in new dialog.
        $(".claim-form").hide();
        $(".claim-form > button.submit").button().click(claimSubmit);
        $(".claim > button").button().click(toggleClaim);
        $(".claim-left").each(function() {
          if ($(this).html() === "0") {
            // Disable the button if there are none available of this type.
            $(this).siblings("button").button({ disabled: true });
          }
        });
      });
  }

   function addMessages() {
     $.post("/php/request.php",
            { request: "gift",
              mode: "messages",
              url: location.href,
              token: dobrado.token },
       function(response) {
         if (dobrado.checkResponseError(response, "gift messages")) {
           return;
         }
         var messages = JSON.parse(response);
         $("#location > a").each(function() {
             var title = messages[$(this).attr("id")];
             if (title !== "") {
               $(this).attr("title", title);
             }
         });
       });
   }

  function close() {
    $("#gift-category").remove();
  }

})();
