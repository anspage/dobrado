<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Indieauth extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $domain = trim($_POST['domain']);
    if ($domain === '') {
      return ['info' => 'Please provide a web address.'];
    }

    $rels = ['authorization_endpoint', 'token_endpoint', 'micropub', 'feed',
             'webmention'];
    $endpoint = discover_endpoint($domain, $rels);
    foreach ($endpoint as $name) {
      if ($name !== '' && stripos($name, 'http') !== 0) {
        return ['info' => $name];
      }
    }

    // Use indieauth.com as a fallback if no endpoint was found.
    if ($endpoint['authorization_endpoint'] === '') {
      $endpoint['authorization_endpoint'] = 'https://indieauth.com/auth';
    }
    // Store endpoints and state for when the user is returned.
    $_SESSION['authorization-endpoint'] = $endpoint['authorization_endpoint'];
    $_SESSION['token-endpoint'] = $endpoint['token_endpoint'];
    $_SESSION['micropub-endpoint'] = $endpoint['micropub'];
    $_SESSION['indieauth-state'] = bin2hex(openssl_random_pseudo_bytes(8));
    // This allows the Reader module to automatically add the user's feed, try
    // their login url if rel=feed wasn't found above.
    if ($this->Substitute('indieauth-add-feed') === 'true') {
      $_SESSION['indieauth-feed'] = $endpoint['feed'] === '' ?
        $domain : $endpoint['feed'];
    }
    // Also store the user input to compare to the returned 'me' value.
    $_SESSION['indieauth-input'] = $domain;
    $client = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
      'https://' : 'http://';
    $client .= $_SERVER['SERVER_NAME'];
    $location = $endpoint['authorization_endpoint'] .
      '?me=' . urlencode($domain) . '&redirect_uri=' .
      urlencode($client . '/php/auth.php') .
      '&client_id=' . urlencode($client) .
      '&state=' . $_SESSION['indieauth-state'];
    if ($endpoint['micropub'] !== '') {
      $location .= '&scope=create&response_type=code';
    }
    if ($this->Substitute('indieauth-debug') === 'true') {
      $this->Log('Indieauth->Callback: ' . $location);
    }
    return ['location' => $location];
  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    return '<form id="indieauth-form">' .
        '<div class="indieauth-description">' .
          $this->Substitute('indieauth-description') .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="indieauth-url">Web Address:</label>' .
          '<input id="indieauth-url" type="text">' .
        '</div>' .
        '<button id="indieauth-submit">Log In</button>' .
        '<div id="indieauth-info"></div>' .
      '</form>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $this->AppendScript($path, 'dobrado.indieauth.js');
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // This is called when the version of the module is updated,
    // to provide a way to update or modify tables etc..
  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.indieauth.js');
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

}
