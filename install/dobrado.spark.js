// @source: /js/source/dobrado.spark.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2018 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.spark){dobrado.spark={};}
(function(){'use strict';$(function(){if($(".spark").length===0){return;}
$(".spark-remove").button({icon:'ui-icon-closethick',showLabel:false}).click(removeSpark);$(".spark-edit-name").button().click(editName);$(".spark-api-info").click(function(){$(".spark-api-credentials").toggle();});$(".spark-group-remove").button({icon:'ui-icon-closethick',showLabel:false}).click(removeGroup);$(".spark-group-add").click(addGroup);$(".spark-group-save").button().click(saveGroup);$(".spark-wifi-remove").button({icon:'ui-icon-closethick',showLabel:false}).click(removeWiFi);$(".spark-wifi-add").click(addWiFi);$(".spark-wifi-save").button().click(saveWiFi);$(".spark-wifi-update").button({disabled:true}).click(updateSpark);dobrado.notify("spark",dobrado.spark.updateDone);});function removeSpark(){if(!confirm("Are you sure you want to remove settings for this spark?")){return false;}
var form=$(this).parent();dobrado.log("Removing spark.","info");$.post("/php/request.php",{request:"spark",action:"removeSpark",id:dobrado.moduleID(this),sparkID:$(this).siblings(".spark-id").html(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,"removeSpark")){return;}
form.remove();});return false;}
function editName(){dobrado.log("Saving spark name.","info");$.post("/php/request.php",{request:"spark",action:"editName",id:dobrado.moduleID(this),sparkID:$(this).siblings(".spark-id").html(),name:$(this).siblings(".spark-name").val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,"spark")){return;}});return false;}
function removeGroup(){var group=$(this).parent();var name=$(this).siblings(".spark-group-name").html();var sparkID=$(this).parent().siblings(".spark-id").html();dobrado.log("Removing group.","info");$.post("/php/request.php",{request:"spark",action:"removeGroup",id:dobrado.moduleID(this),sparkID:sparkID,name:name,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,"spark")){return;}
group.remove();});return false;}
function addGroup(){$(".spark-group-details").toggle();return false;}
function saveGroup(){var sparkID=$(this).parent().siblings(".spark-id").html();var formID=$(this).parents(".spark-details").attr("id");dobrado.log("Adding group.","info");$.post("/php/request.php",{request:"spark",action:"saveGroup",id:dobrado.moduleID(this),sparkID:sparkID,name:$("#spark-group-new").val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,"spark")){return;}
var group=JSON.parse(response);$(group.content).insertBefore("#"+formID+" .spark-group-details");$("#"+formID+" .spark-group-remove").button({icon:'ui-icon-closethick',showLabel:false}).click(removeGroup);$("#spark-group-new").val("");$("#"+formID+" .spark-group-details").hide();});return false;}
function removeWiFi(){var wifi=$(this).parent();var name=$(this).siblings(".spark-wifi-name").html();var sparkID=$(this).parent().siblings(".spark-id").html();dobrado.log("Removing wireless access point.","info");$.post("/php/request.php",{request:"spark",action:"removeWiFi",id:dobrado.moduleID(this),sparkID:sparkID,name:name,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,"spark")){return;}
wifi.remove();$(".spark-wifi-update").button("option","label","update spark");$(".spark-wifi-update").button("option","disabled",false);});return false;}
function addWiFi(){$(".spark-wifi-details").toggle();return false;}
function saveWiFi(){var sparkID=$(this).parent().siblings(".spark-id").html();dobrado.log("Adding wireless access point.","info");$.post("/php/request.php",{request:"spark",action:"saveWiFi",id:dobrado.moduleID(this),sparkID:sparkID,name:$("#spark-wifi-new").val(),password:$("#spark-wifi-password").val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,"spark")){return;}
var wifi=JSON.parse(response);$(wifi.content).insertBefore(".spark-wifi-details");$(".spark-wifi-remove").button({icon:'ui-icon-closethick',showLabel:false}).click(removeWiFi);$("#spark-wifi-new").val("");$("#spark-wifi-password").val("");$(".spark-wifi-details").hide();$(".spark-wifi-update").button("option","label","update spark");$(".spark-wifi-update").button("option","disabled",false);});return false;}
function updateSpark(){var sparkID=$(this).parent().siblings(".spark-id").html();dobrado.log("Setting update required.","info");$.post("/php/request.php",{request:"spark",action:"updateSpark",id:dobrado.moduleID(this),sparkID:sparkID,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,"spark")){return;}
alert("Please restart your spark to update.");$(".spark-wifi-update").button("option","label","updating next restart");$(".spark-wifi-update").button("option","disabled",true);});return false;}
dobrado.spark.updateDone=function(){function updateDone(){$(".spark-wifi-update").button("option","label","update done");$(".spark-wifi-update").button("option","disabled",true);alert("Your spark has been updated.");}
setTimeout(updateDone,5000);};})();