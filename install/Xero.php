<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Xero extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'You don\'t have permission to view xero.'];
    }

    $action = $_POST['action'];
    if ($action === 'diagnostics') return $this->Diagnostics();
    if ($action === 'submit') return $this->Submit();
  }

  public function CanAdd($page) {
    if (!$this->user->canEditSite) return false;
    return !$this->AlreadyOnPage('xero', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $config = $this->Config();
    $public_key_input = '';
    $private_key_input = '';
    $public_key_toggle = 'hidden';
    $private_key_toggle = 'hidden';
    if ($config['rsa_public_key'] !== '') {
      $public_key_toggle = '';
      $public_key_input = 'hidden';
    }
    if ($config['rsa_private_key'] !== '') {
      $private_key_toggle = '';
      $private_key_input = 'hidden';
    }
    $checked = $config['enabled'] ? 'checked="checked" ' : '';
    $token_toggle = $config['consumer_key'] === '' ? 'hidden' : '';
    $secret_toggle = $config['shared_secret'] === '' ? 'hidden' : '';
    return '<form id="xero-form">' .
      '<div class="form-spacing">' .
        '<label for="xero-enabled-input">Send all invoices to Xero:</label>' .
        '<input id="xero-enabled-input" ' . $checked . 'type="checkbox">' .
      '</div>' .
      '<a href="#" class="xero-token-toggle ' . $token_toggle . '">' .
        'Show Consumer Key</a>' .
      '<div class="form-spacing">' .
        '<label for="xero-token-input">Consumer Key:</label>' .
        '<input id="xero-token-input" ' .
          'value="' . $config['consumer_key'] . '" type="password">' .
      '</div>' .
      '<a href="#" class="xero-secret-toggle ' . $secret_toggle . '">' .
        'Show Consumer Secret</a>' .
      '<div class="form-spacing">' .
        '<label for="xero-secret-input">Consumer Secret:</label>' .
        '<input id="xero-secret-input" ' .
          'value="' . $config['shared_secret'] . '" type="password">' .
      '</div>' .
      '<a href="#" class="xero-public-key-toggle ' . $public_key_toggle . '">' .
        'Upload a new Public Key Certificate</a><br>' .
      '<div class="form-spacing ' . $public_key_input . '">' .
        '<label for="xero-public-key-input">Public Key Certificate:</label>' .
        '<input id="xero-public-key-input" value="" type="file">' .
      '</div>' .
      '<a href="#" class="xero-private-key-toggle ' . $private_key_toggle .'">'.
        'Upload a new Private Key Certificate</a><br>' .
      '<div class="form-spacing ' . $private_key_input . '">' .
        '<label for="xero-private-key-input">Private Key Certificate:</label>' .
        '<input id="xero-private-key-input" value="" type="file">' .
      '</div>' .
      '<button id="xero-submit">submit</button>' .
      '</form>' .
      '<span class="xero-diagnostics-title">Check your connection to Xero:' .
      '</span><button id="xero-diagnostics">diagnostics</button>' .
      '<p class="xero-diagnostics-info"></p>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if ($fn === 'Enabled') return $this->Enabled();
    if ($fn === 'SendInvoice') return $this->SendInvoice($p);
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }
  
  public function Install($path) {
    // Append dobrado.writer.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.xero.js', false);
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS xero (' .
      'token VARCHAR(200),' .
      'secret VARCHAR(200),' .
      'enabled TINYINT(1),' .
      'system_group VARCHAR(50),' .
      'PRIMARY KEY(system_group)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Xero->Install: ' . $mysqli->error);
    }
    $mysqli->close();

    $site_style = ['"",".xero-public-key-toggle","display","block"',
                   '"",".xero-private-key-toggle","display","block"',
                   '"","label[for=xero-enabled-input]","float","none"',
                   '"","#xero-form label","width","12em"',
                   '"","#xero-form","margin","20px"',
                   '"","#xero-submit","margin-left","11.3em"',
                   '"","#xero-diagnostics","margin-left","1em"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // This is called when the version of the module is updated,
    // to provide a way to update or modify tables etc..
  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.xero.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function Config() {
    $config = ['enabled' => false, 'consumer_key' => '', 'shared_secret' => '',
               'rsa_private_key' => '', 'rsa_public_key' => ''];
    $path = $this->user->config->PrivatePath() .'/' .
      $this->user->config->ServerName() .'/group/' . $this->user->group;
    if (is_file($path.'/xero_public.key')) {
      $config['rsa_public_key'] = $path . '/xero_public.key';
    }
    if (is_file($path.'/xero_private.key')) {
      $config['rsa_private_key'] = $path . '/xero_private.key';
    }

    $mysqli = connect_db();
    $query = 'SELECT token, secret, enabled FROM xero WHERE ' .
      'system_group = "' . $this->user->group . '"';
    if ($result = $mysqli->query($query)) {
      if ($xero = $result->fetch_assoc()) {
        $config['consumer_key'] = $xero['token'];
        $config['access_token'] = $xero['token'];
        $config['shared_secret'] = $xero['secret'];
        $config['access_token_secret'] = $xero['secret'];
        $config['enabled'] = (bool)$xero['enabled'];
      }
      $result->close();
    }
    else {
      $this->Log('Xero->Config: ' . $mysqli->error);
    }
    $mysqli->close();
    return $config;
  }

  private function Diagnostics() {
    include_once 'library/XeroOAuth.php';
    $oauth = new XeroOAuth($this->Config());
    return ['status' => $oauth->diagnostics()];
  }

  private function Enabled() {
    $enabled = false;
    $mysqli = connect_db();
    $query = 'SELECT enabled FROM xero WHERE system_group = ' .
      '"' . $this->user->group . '"';
    if ($result = $mysqli->query($query)) {
      if ($xero = $result->fetch_assoc()) {
        $enabled = (bool)$xero['enabled'];
      }
      $result->close();
    }
    else {
      $this->Log('Xero->Enabled: ' . $mysqli->error);
    }
    $mysqli->close();
    return $enabled;
  }

  private function SendInvoice($xml) {
    $config = $this->Config();
    if (!$config['enabled']) return;

    include_once 'library/XeroOAuth.php';

    $oauth = new XeroOAuth($config);
    $oauth->request('POST', $oauth->url('Invoices'), [], $xml);
    if ($oauth->response['code'] !== 200) {
      $this->Log('Xero->SendInvoice: ' . $oauth->response['response']);
    }
  }

  private function Submit() {
    $mysqli = connect_db();
    $enabled = (int)$_POST['enabled'];
    $token = $mysqli->escape_string($_POST['key']);
    $secret = $mysqli->escape_string($_POST['secret']);
    $query = 'INSERT INTO xero VALUES ("' . $token . '", "' . $secret . '", ' .
      $enabled . ', "' . $this->user->group . '") ON DUPLICATE KEY UPDATE ' .
      'token = "' . $token . '", secret = "' . $secret . '", enabled = ' .
      $enabled;
    if (!$mysqli->query($query)) {
      $this->Log('Xero->Submit: ' . $mysqli->error);
    }
    $mysqli->close();

    $max_file_size = $this->user->config->MaxFileSize();
    $path = $this->user->config->PrivatePath() . '/' .
      $this->user->config->ServerName() . '/group/' . $this->user->group;
    if (!is_dir($path)) mkdir($path, 0755, true);
    if (isset($_FILES['publicKeyCert'])) {
      if ($_FILES['publicKeyCert']['size'] > $max_file_size * 1000000) {
        return ['error' => 'Public key certificate is too large. (max ' .
                $max_file_size . 'M)'];
      }
      $tmp = $_FILES['publicKeyCert']['tmp_name'];
      if (!move_uploaded_file($tmp, $path.'/xero_public.key')) {
        return ['error' => 'Public key certificate was not uploaded.'];
      }
    }
    if (isset($_FILES['privateKeyCert'])) {
      if ($_FILES['privateKeyCert']['size'] > $max_file_size * 1000000) {
        return ['error' => 'Private key certificate is too large. (max ' .
                $max_file_size . 'M)'];
      }
      $tmp = $_FILES['privateKeyCert']['tmp_name'];
      if (!move_uploaded_file($tmp, $path.'/xero_private.key')) {
        return ['error' => 'Private key certificate was not uploaded.'];
      }
    }
    return $this->Diagnostics();
  }
  
}
