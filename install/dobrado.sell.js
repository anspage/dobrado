// @source: /js/source/dobrado.sell.js
// 
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// that code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if(!this.dobrado.sell){dobrado.sell={};}
(function(){'use strict';var sell=null;var purchaseGrid=null;var purchaseGridId='';var allAvailableGrid=null;var allAvailableGridId='';var allAvailableData=[];var currentProduct=null;var currentUser='';var currentWeight='';var leavePageWarning=true;var user=0;var sparkID=null;var sparkAddress=null;var updateQuantity=false;var saving=false;var viewPurchases=true;$(function(){if($('.sell').length===0){return;}
var currentPage=$('#page-select').val();window.addEventListener('beforeunload',function(event){if(sell&&sell.processed&&sell.processed.length!==0&&leavePageWarning){$('.control .info').hide();$('#page-select').val(currentPage).selectmenu('refresh');var message='Please save your orders before leaving the page.';event.returnValue=message;return message;}});if($('.sell-payment-message').html()!==''){$('.sell-payment-message button').button().click(paymentDone);$('.sell-payment-message').dialog({show:true,modal:true,width:400,height:200,position:{my:'top',at:'top+50',of:window},title:'Payment',close:paymentDone,create:dobrado.fixedDialog});}
$('.sell-finished').dialog({show:true,autoOpen:false,modal:true,width:500,height:250,position:{my:'top',at:'top+50',of:window},title:'Payment Method',create:dobrado.fixedDialog});$('#customer-detail-submit').button().click(customerDetail);$('#sell-checkout-dialog').dialog({show:true,autoOpen:false,modal:true,width:550,height:550,position:{my:'top',at:'top+50',of:window},title:'Checkout',create:dobrado.fixedDialog});$('#purchase-form .view-all').button().click(viewAll);$('.sell .save').button().click(save);$('.sell .done').button({disabled:true}).click(finishSale);$('.sell .reset').button().click(resetSaleForUser);$('#purchase-form .remove').button().click(remove);$('#purchase-form .add').button().click(add);$('#purchase-form').keypress(addFromEnter);$('#purchase-name-input').val('').change(function(){setTimeout(function(){showUser();},10);});$('#purchase-product-input').val('').change(function(){setTimeout(function(){showProduct();},10);});$('#purchase-price-input').val('');$('#purchase-quantity-input').val('');$('#purchase-quantity-input').spinner({disabled:true,min:0,spin:setQuantity,change:setQuantity});$('.sell-finished-back').button().click(function(){$('.sell-finished').dialog('close');});$('.sell-finished-done').button().click(newSale);$('#sell-auto-save').prop('checked',true).change(function(){$('.sell .save').toggle();$('.sell .warning').toggle();});$('.sell-describe-auto-save').click(function(){$('.sell-auto-save-description').toggle();});if($('.grid').length!==0){gridSetup();}
if($('#purchase-connect').length===1){$('#purchase-connect').controlgroup();$('#purchase-update-quantity').checkboxradio({icon:false}).click(toggleUpdate);$('#purchase-connect-settings').button({icon:'ui-icon-gear',showLabel:false}).click(connectSettings);}
if(dobrado.localStorage){if(localStorage.sell){sell=JSON.parse(localStorage.sell);if($('.sell-payment-message').length===0){if(sell.processed&&sell.processed.length===0){loadProducts(false);}
else if(sell.data&&sell.data.length!==0){restorePreviousPurchase();}}}}
else{$('.sell .warning').show();}
var productsOnly=sell?true:false;loadProducts(productsOnly);});function decimalString(value){return(value+0.0001).toFixed(2);}
function gridSetup(){$('.grid').each(function(index){if(index===0){purchaseGridId='#'+$(this).attr('id');}
if(index===1){allAvailableGridId='#'+$(this).attr('id');}});var purchaseColumns=[{id:'product',name:'Product',field:'name',width:490,sortable:true}];purchaseColumns.push({id:'quantity',name:'Qty',field:'quantity',width:60,sortable:true,editor:Slick.Editors.Float});purchaseColumns.push({id:'price',name:'Price',field:'price',width:130,sortable:true,formatter:Slick.Formatters.Dollar});if(!dobrado.mobile){purchaseColumns.push({id:'total',name:'Total',field:'total',width:120,sortable:true,formatter:Slick.Formatters.Dollar});}
var purchaseOptions={autoHeight:true,editable:true,forceFitColumns:true};purchaseGrid=dobrado.grid.instance(purchaseGridId,[],purchaseColumns,purchaseOptions);purchaseGrid.setSelectionModel(new Slick.RowSelectionModel());purchaseGrid.onClick.subscribe(function(e,item){showPurchase(item.row);});purchaseGrid.onSelectedRowsChanged.subscribe(function(e,item){if(item.rows.length===1){showPurchase(item.rows[0]);}});purchaseGrid.onCellChange.subscribe(updateAvailablePurchaseData);purchaseGrid.onSort.subscribe(function(e,args){if(!sell.data||!sell.data[user]){return;}
sell.data[user].sort(function(row1,row2){var field=args.sortCol.field;var sign=args.sortAsc?1:-1;var value1=row1[field];var value2=row2[field];if(field!=='name'){value1=parseFloat(value1);value2=parseFloat(value2);}
if(value1===value2){return 0;}
if(value1>value2){return sign;}
else{return sign* -1;}});purchaseGrid.invalidate();});if($('.grid').length>=2){var allAvailableColumns=[{id:'product',name:'Product',field:'name',width:490,sortable:true}];allAvailableColumns.push({id:'quantity',name:'Qty',field:'quantity',width:60,sortable:true,editor:Slick.Editors.Float});allAvailableColumns.push({id:'price',name:'Price',field:'price',width:130,sortable:true,formatter:Slick.Formatters.Units});if(!dobrado.mobile){allAvailableColumns.push({id:'total',name:'Total',field:'total',width:120,sortable:true,formatter:Slick.Formatters.Dollar});}
var allAvailableOptions={autoHeight:true,editable:true,forceFitColumns:true};allAvailableGrid=dobrado.grid.instance(allAvailableGridId,[],allAvailableColumns,allAvailableOptions);allAvailableGrid.setSelectionModel(new Slick.RowSelectionModel());allAvailableGrid.onClick.subscribe(function(e,item){showAllAvailablePurchase(item.row);});allAvailableGrid.onSelectedRowsChanged.subscribe(function(e,item){if(item.rows.length===1){showAllAvailablePurchase(item.rows[0]);}});allAvailableGrid.onCellChange.subscribe(updateAvailablePurchaseData);allAvailableGrid.onSort.subscribe(function(e,args){allAvailableData.sort(function(row1,row2){var field=args.sortCol.field;var sign=args.sortAsc?1:-1;var value1=row1[field];var value2=row2[field];if(field!=='name'){value1=parseFloat(value1);value2=parseFloat(value2);}
if(value1===value2){return 0;}
if(value1>value2){return sign;}
else{return sign* -1;}});allAvailableGrid.invalidate();});}
$('.grid').hide();}
function reloadPage(){$.post('/php/request.php',{request:'sell',action:'payment-done',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'sell reloadPage')){return;}
var payment=JSON.parse(response);location.href=payment.location;});}
function paymentDone(){if($('.sell-payment-message .error').length!==0){if(sell.processed&&sell.processed.length!==0){sell.data[sell.processed.length-1]=[];}
if(dobrado.localStorage){localStorage.sell=JSON.stringify(sell);}
leavePageWarning=false;reloadPage();}
else{saveBeforeLoad(true);}}
function customerDetail(){if($('#customer-detail-name').val()===''){$('#sell-checkout-info').html('Please enter your name.');return false;}
if($('#customer-detail-email').val()===''){$('#sell-checkout-info').html('Please enter your email.');return false;}
if($('#customer-detail-address').val()===''){$('#sell-checkout-info').html('Please enter your address.');return false;}
if($('#customer-detail-postcode').val()===''){$('#sell-checkout-info').html('Please enter your postcode.');return false;}
if($('#customer-detail-city').val()===''){$('#sell-checkout-info').html('Please enter your city.');return false;}
if($('#customer-detail-state').val()===''){$('#sell-checkout-info').html('Please enter your state.');return false;}
if($('#customer-detail-country').val()===''){$('#sell-checkout-info').html('Please enter your country.');return false;}
$('#sell-checkout-info').html('Processing... please wait.');$.post('/php/request.php',{request:'sell',action:'checkout',name:$('#customer-detail-name').val(),email:$('#customer-detail-email').val(),phone:$('#customer-detail-phone').val(),address:$('#customer-detail-address').val(),city:$('#customer-detail-city').val(),state:$('#customer-detail-state').val(),country:$('#customer-detail-country').val(),postcode:$('#customer-detail-postcode').val(),notes:$('#customer-detail-notes').val(),total:$('.order .total').html(),method:$('#purchase-payment-input').val(),url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'sell customerDetail')){return;}
var checkout=JSON.parse(response);$('#sell-checkout-info').html('');$('#sell-customer-details-form').hide();$('#sell-checkout-message').html(checkout.content);$('#cart-payment').button();if($('#sell-checkout-message .error').length===0){leavePageWarning=false;}});return false;}
function loadProducts(productsOnly){dobrado.log('Loading products...','info');$.post('/php/request.php',{request:'sell',action:'list',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'sell loadProducts')){return;}
var data=JSON.parse(response);if(productsOnly){sell.products=data.products;}
else{sell=data;}
$('#purchase-name-input').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:sell.users,select:showUser});updateProducts();if(dobrado.localStorage){localStorage.sell=JSON.stringify(sell);}});}
function restorePreviousPurchase(){var userTotal=0;user=sell.data.length-1;$.each(sell.data[user],function(i,item){userTotal+=parseFloat(item.total);});$('.order .total').html(decimalString(userTotal));$('.sell .done').button('option','disabled',false);$('#purchase-name-input').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:sell.users,select:showUser});updateProducts();viewPurchases=false;viewAll();}
function formatFloat(value){value=parseFloat(value);value=value.toFixed(3);if(value.indexOf('.')!==-1){for(var i=value.length-1;i>0;i--){if(value[i]==='0'){continue;}
if(value[i]==='.'){value=value.substring(0,i);}
else{value=value.substring(0,i+1);}
break;}}
return value;}
function showUser(event,ui){var username=$('#purchase-name-input').val();if(ui){username=ui.item.value;}
if(username!==''&&$.inArray(username,sell.users)===-1){alert('If an account was just created for '+username+' please reload the page.');$('#purchase-name-input').val('');username='';}
if(username!==''&&currentUser===username){return;}
currentUser=username;if(username===''||$.inArray(username,sell.userIndex)!==-1){sell.userIndex[user]=username;if(dobrado.localStorage){localStorage.sell=JSON.stringify(sell);}}
else{var message='Changing username will remove unsaved data, continue?';if(sell.data[user]&&sell.data[user].length!==0&&!confirm(message)){$('#purchase-name-input').val('');return;}
dobrado.log('Loading purchases for user...','info');$.post('/php/request.php',{request:'sell',action:'listUser',username:username,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'sell showUser')){return;}
var quantity=1;var price=sell.buyerGroup[username];sell.userIndex[user]=username;sell.data[user]=JSON.parse(response);if(sell.data[user]){if(dobrado.localStorage){localStorage.sell=JSON.stringify(sell);}
var userTotal=0;$.each(sell.data[user],function(i,item){$.each(sell.products,function(index,product){if(item.name===product.name){if(product.quantityAdjustment&&decimalString(product[price])!==item.price){item.basePrice=decimalString(product.price);item.price=decimalString(product[price]);quantity=parseFloat(item.quantity)*product.quantityAdjustment;item.quantity=formatFloat(quantity);item.total=decimalString(quantity*product[price]);}
return false;}});userTotal+=parseFloat(item.total);});$('.order .total').html(decimalString(userTotal));$('.sell .done').button('option','disabled',false);$('.sell .reset').show();}
viewPurchases=false;viewAll();});}}
function showPurchase(row){var data=sell.data[user][row];if(!data)return;$('#purchase-product-input').val(dobrado.decode(data.name));$('#purchase-quantity-input').val('');var total=decimalString(data.quantity*data.price);$.each(sell.products,function(index,item){if(item.name===data.name){currentProduct=item;if(currentProduct.unit==='variable'){$('#purchase-price-input').val(data.price).attr('readonly',false);$('#purchase-quantity-input').spinner('value','1');$('#purchase-quantity-input').spinner('option','disabled',true);}
else{$('#purchase-price-input').val('$'+total+' @ ($'+data.price+'/'+currentProduct.unit+')');$('#purchase-price-input').attr('readonly',true);$('#purchase-quantity-input').spinner('enable');}
if(currentProduct.grower){$('#purchase-grower-info').text('Grower info: '+
currentProduct.grower);}
else{$('#purchase-grower-info').html('');}
return false;}});}
function showProduct(event,ui){var username=$('#purchase-name-input').val();var price='retail';if(username!==''){price=sell.buyerGroup[username];}
var productFound=false;var product=$('#purchase-product-input').val();if(ui){product=ui.item.value;}
if(currentProduct&&currentProduct.name===product){return;}
$.each(sell.products,function(index,item){if(item.name===product){productFound=true;currentProduct=item;if(currentProduct.unit==='variable'){$('#purchase-quantity-input').val('1').spinner('disable');$('#purchase-price-input').val(decimalString(item[price]));$('#purchase-price-input').attr('readonly',false);}
else{$('#purchase-price-input').val('($'+decimalString(item[price])+'/'+item.unit+')');$('#purchase-price-input').attr('readonly',true);if(currentProduct.unit==='kg'&&currentWeight!==''){$('#purchase-quantity-input').val(currentWeight).spinner('enable');setQuantity();}
else{$('#purchase-quantity-input').val('').spinner('enable');}}
if(currentProduct.grower){$('#purchase-grower-info').text('Grower info: '+
currentProduct.grower);}
else{$('#purchase-grower-info').html('');}
return false;}});if(!productFound){resetForm();}
$('#purchase-quantity-input').focus();}
function updateProducts(){var products=[];$.each(sell.products,function(index,item){products.push(item.name);});$('#purchase-product-input').autocomplete({minLength:1,search:dobrado.fixAutoCompleteMemoryLeak,source:products,select:showProduct});}
function setQuantity(event,ui){if(!currentProduct){return;}
var username=$('#purchase-name-input').val();var price='retail';if(username!==''){price=sell.buyerGroup[username];}
var quantity=0;if(ui&&'value'in ui){quantity=ui.value;}
else{quantity=parseFloat($('#purchase-quantity-input').val());if(!quantity||quantity<0){quantity=0;$('#purchase-quantity-input').val('0');}}
if(currentProduct.unit==='each'&&/\./.test(quantity)){alert('Please note!\n'+'This product is usually purchased in whole quantities.');}
if(currentProduct.unit!=='variable'){var total=decimalString(quantity*currentProduct[price]);$('#purchase-price-input').val('$'+total+' @ ($'+
decimalString(currentProduct[price])+'/'+currentProduct.unit+')');}}
function remove(){var product=$('#purchase-product-input').val();removePurchase(product);return false;}
function removePurchase(product){var total=parseFloat($('.order .total').html());if(sell.data[user]){$.each(sell.data[user],function(i,item){if(item.name===product){total-=item.total;$('.order .total').html(decimalString(total));sell.data[user][i].quantity=0;sell.data[user][i].total=0;return false;}});if(purchaseGrid){purchaseGrid.setData(sell.data[user]);purchaseGrid.updateRowCount();purchaseGrid.render();purchaseGrid.setSelectedRows([]);}
if(dobrado.localStorage){localStorage.sell=JSON.stringify(sell);}}
resetForm();return false;}
function addFromEnter(event){if(event.which===13){event.preventDefault();add();}}
function add(){if(!currentProduct){return false;}
var product=currentProduct.name;if(product!==$('#purchase-product-input').val()){alert('Product not found.');resetForm();return false;}
var accumulative=true;var supplier=currentProduct.user;var priceLevel='retail';var username=$('#purchase-name-input').val();if(username!==''){priceLevel=sell.buyerGroup[username];}
var price=currentProduct[priceLevel];var basePrice=currentProduct.price;if(currentProduct.unit==='variable'){price=parseFloat($('#purchase-price-input').val());if(!price){$('#purchase-price-input').val('');return false;}
basePrice=price;if(priceLevel==='wholesale'&&sell.wholesalePercent){basePrice-=basePrice*sell.wholesalePercent/100;}
else if(priceLevel==='retail'&&sell.retailPercent){basePrice-=basePrice*sell.retailPercent/100;}
accumulative=false;}
var quantity=parseFloat($('#purchase-quantity-input').val());if(!quantity||quantity<0){return false;}
update(product,supplier,price,basePrice,quantity,accumulative);return false;}
function update(product,supplier,price,basePrice,quantity,accumulative){var itemTotal=parseFloat(decimalString(quantity*price));var selectedRow=0;var newItem=true;if(!sell.data[user]){sell.data[user]=[];}
$.each(sell.data[user],function(i,item){if(item.name===product){newItem=false;if(accumulative){itemTotal+=parseFloat(item.total);quantity+=parseFloat(item.quantity);}
sell.data[user][i]={name:product,supplier:supplier,grower:currentProduct.grower,quantity:formatFloat(quantity),price:decimalString(price),basePrice:decimalString(basePrice),total:decimalString(itemTotal)};if(purchaseGrid){purchaseGrid.setData(sell.data[user]);selectedRow=i;}
return false;}});if(newItem){sell.data[user].push({name:product,supplier:supplier,grower:currentProduct.grower,quantity:formatFloat(quantity),price:decimalString(price),basePrice:decimalString(basePrice),total:decimalString(itemTotal)});if(purchaseGrid&&sell.data[user].length===1){$(purchaseGridId).show();purchaseGrid.setData(sell.data[user]);if(!viewPurchases){$(purchaseGridId).hide();}}
selectedRow=sell.data[user].length-1;}
var userTotal=0;$.each(sell.data[user],function(i,item){userTotal+=parseFloat(item.total);});$('.order .total').html(decimalString(userTotal));if(purchaseGrid&&viewPurchases){$(purchaseGridId).show();purchaseGrid.updateRowCount();purchaseGrid.render();purchaseGrid.setSelectedRows([selectedRow]);purchaseGrid.scrollRowIntoView(selectedRow);}
resetForm();if($.inArray(user,sell.processed)===-1){sell.processed.push(user);}
if(dobrado.localStorage){localStorage.sell=JSON.stringify(sell);}
$('.sell .done').button('option','disabled',false);}
function saveBeforeLoad(reload){var html='Warning: Unsaved sales';var method='cash';if(reload){method='check transaction';}
dobrado.log('Saving...','info');$.post('/php/request.php',{request:'sell',data:JSON.stringify(sell.data),userIndex:JSON.stringify(sell.userIndex),newSales:JSON.stringify(sell.newSales),action:'save',method:method,url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'sell saveBeforeLoad')){if(sell.processed.length===1){html+=' (currently <b>1</b> sale requires saving)';}
else if(sell.processed.length>1){html+=' (currently <b>'+sell.processed.length+'</b> sales require saving)';}
html+='.';$('.sell .warning').html(html).show();$('#purchase-payment-input').val('cash');$('.sell .save').show();return;}
resetAfterSave();if(reload){reloadPage();}
else{loadProducts(false);}});return;}
function save(){function saveToServer(){$.post('/php/request.php',{request:'sell',data:JSON.stringify(sell.data),userIndex:JSON.stringify(sell.userIndex),newSales:JSON.stringify(sell.newSales),action:'save',method:method,url:location.href,token:dobrado.token},function(response){saving=false;if(dobrado.checkResponseError(response,'sell save')){return;}
resetAfterSave();});}
var method=$('#purchase-payment-input').val();var username='';if(sell.userIndex[user]){username=sell.userIndex[user];}
if(method!=='cash'&&method!=='eftpos'&&method!=='account'){$('#sell-checkout-message').html('Please enter your contact details:');$('#sell-customer-details-form').show();$('#sell-checkout-dialog').dialog('open');}
else if(method==='account'&&username===''){alert('Payment method \'account\' can only be used '+'for an existing user.');}
else{if(saving){return false;}
saving=true;dobrado.log('Saving...','info');if(viewPurchases){if(purchaseGrid){purchaseGrid.gotoCell(0,0);}}
else{if(allAvailableGrid){allAvailableGrid.gotoCell(0,0);}}
setTimeout(saveToServer,200);}
return false;}
function resetAfterSave(){$('.sell .done').button('option','disabled',true);$('.order .total').html('0.00');$('#purchase-name-input').val('');if(purchaseGrid){purchaseGrid.setData([]);purchaseGrid.updateRowCount();purchaseGrid.render();purchaseGrid.setSelectedRows([]);}
$(purchaseGridId).hide();resetForm();if(!$('#sell-auto-save').is(':checked')){$('#sell-auto-save').prop('checked',true);$('.sell .warning').hide();$('.sell .save').hide();}
user=0;sell.data=[];sell.data[user]=[];sell.processed=[];sell.userIndex=[];sell.newSales=[];if(dobrado.localStorage){localStorage.sell=JSON.stringify(sell);}}
function resetSaleForUser(){var username=$('#purchase-name-input').val();if($.inArray(username,sell.users)===-1){alert('If an account was just created for '+username+' please reload the page.');$('#purchase-name-input').val('');username='';}
else if(!confirm('Please make sure existing order for '+username+' has been finalised before starting a new sale.')){return;}
$('.sell .done').button('option','disabled',true);$('.sell .reset').hide();$('.order .total').html('0.00');if(purchaseGrid){purchaseGrid.setData([]);purchaseGrid.updateRowCount();purchaseGrid.render();purchaseGrid.setSelectedRows([]);}
sell.userIndex[user]=username;sell.newSales[user]=true;sell.data[user]=[];if(dobrado.localStorage){localStorage.sell=JSON.stringify(sell);}
viewPurchases=true;viewAll();}
function finishSale(){function finishSaleAfterUpdate(){if($('#sell-auto-save').is(':checked')){if(parseFloat($('.order .total').html())===0){$('#purchase-payment-input').val('cash');save();}
else{$('.sell-finished .total').html($('.order .total').html());$('#purchase-payment-input').val('');$('.sell .warning').html('Working offline: cash sales only.');$('.sell-finished').dialog('open');}}
else{newSale();}}
if(viewPurchases){if(purchaseGrid){purchaseGrid.gotoCell(0,0);}}
else{if(allAvailableGrid){allAvailableGrid.gotoCell(0,0);}}
setTimeout(finishSaleAfterUpdate,200);return false;}
function newSale(){var html='Working offline: cash sales only';var method=$('#purchase-payment-input').val();if($('#sell-auto-save').is(':checked')){if(method===''){alert('Payment method not selected.');return false;}
$('.sell-finished').dialog('close');save();}
else if(method!=='cash'){alert('Payment method must be cash to work offline.');$('.sell-finished .total').html($('.order .total').html());$('#purchase-payment-input').val('');$('.sell-finished').dialog('open');}
else{$('.sell .done').button('option','disabled',true);$('.sell-finished').dialog('close');$('.order .total').html('0.00');$('#purchase-name-input').val('');if(sell.processed.length===1){html+=' (currently <b>1</b> sale requires saving)';}
else if(sell.processed.length>1){html+=' (currently <b>'+sell.processed.length+'</b> sales require saving)';}
html+='.';$('.sell .warning').html(html);sell.data[++user]=[];viewPurchases=false;viewAll();}
return false;}
function resetForm(){if(viewPurchases){$('#purchase-product-input').focus();}
$('#purchase-product-input').val('');$('#purchase-grower-info').html('');$('#purchase-price-input').val('');$('#purchase-quantity-input').val('');currentProduct=null;}
function viewAll(){var username=$('#purchase-name-input').val();var label='View all';if(viewPurchases){viewPurchases=false;label='View purchases only';$('#purchase-form .view-all').button('option','label',label);viewAllAvailable(username);}
else{viewPurchases=true;$('#purchase-form .view-all').button('option','label',label);$('.purchase-all-users').hide();if(purchaseGrid&&sell.data[user]&&sell.data[user].length!==0){$(purchaseGridId).show();purchaseGrid.setData(sell.data[user]);purchaseGrid.updateRowCount();purchaseGrid.render();purchaseGrid.setSelectedRows([]);}
else{$(purchaseGridId).hide();}
if(allAvailableGrid){allAvailableGrid.gotoCell(0,0);$(allAvailableGridId).hide();}}
resetForm();return false;}
function viewAllAvailable(username){var price='retail';if(username!==''){price=sell.buyerGroup[username];}
var current={};if(sell.data[user]){$.each(sell.data[user],function(index,item){if(!current[item.name]){current[item.name]=[];}
current[item.name].push({quantity:item.quantity});});}
allAvailableData=[];var total=0;$.each(sell.products,function(productIndex,product){if(current[product.name]){$.each(current[product.name],function(currentIndex,currentItem){total=decimalString(currentItem.quantity*product[price]);allAvailableData.push({name:product.name,supplier:product.user,quantity:currentItem.quantity,unit:product.unit,price:decimalString(product[price]),basePrice:decimalString(product.price),total:total});});}
else{allAvailableData.push({name:product.name,supplier:product.user,quantity:0,unit:product.unit,price:decimalString(product[price]),basePrice:decimalString(product.price),total:'0.00'});}});$('#purchase-grower-info').html('');if(purchaseGrid){purchaseGrid.gotoCell(0,0);$(purchaseGridId).hide();}
if(allAvailableGrid){$(allAvailableGridId).show();allAvailableGrid.setData(allAvailableData);allAvailableGrid.updateRowCount();allAvailableGrid.render();}}
function showAllAvailablePurchase(row){if(!allAvailableData){return false;}
var data=allAvailableData[row];$('#purchase-product-input').val(dobrado.decode(data.name));$('#purchase-quantity-input').val('');var total=decimalString(data.quantity*data.price);$.each(sell.products,function(index,item){if(item.name===data.name){currentProduct=item;if(currentProduct.unit==='variable'){$('#purchase-price-input').val(data.price).attr('readonly',false);$('#purchase-quantity-input').spinner('value','1');$('#purchase-quantity-input').spinner('option','disabled',true);}
else{$('#purchase-price-input').val('$'+total+' @ ($'+data.price+'/'+currentProduct.unit+')');$('#purchase-price-input').attr('readonly',true);$('#purchase-quantity-input').spinner('enable');}
if(currentProduct.grower){$('#purchase-grower-info').text('Grower info: '+
currentProduct.grower);}
else{$('#purchase-grower-info').html('');}
return false;}});}
function updateAvailablePurchaseData(e,args){var item=args.item;var price=parseFloat(item.price);var basePrice=parseFloat(item.basePrice);var updateProduct=null;$.each(sell.products,function(index,product){if(product.name===item.name){updateProduct=product;if(product.unit==='variable'){if(item.quantity!==0){item.quantity=1;}}
else if(product.unit==='each'&&/\./.test(item.quantity)){alert('Please note!\n'+'This product is usually purchased in whole quantities.');}
return false;}});var newProduct=currentProduct;currentProduct=updateProduct;update(item.name,item.supplier,price,basePrice,item.quantity,false);currentProduct=newProduct;args.item.total=decimalString(item.quantity*price);if(allAvailableGrid){allAvailableGrid.invalidate();}}
function toggleUpdate(){if($('#purchase-update-quantity').is(':checked')){if(sparkID){updateQuantity=true;checkQuantity();}
else{connectSettings();}}
else{updateQuantity=false;}}
function connectSettings(){var sparkOptions=[];function sparkConnect(){var name=$('#purchase-spark-list').val();sparkID=sparkOptions[name].id;sparkAddress=sparkOptions[name].ip;$('.purchase-connect-settings').dialog('close');if(!$('#purchase-update-quantity').is(':checked')){$('#purchase-update-quantity').prop('checked',true);$('#purchase-update-quantity').checkboxradio('refresh');}
if(!updateQuantity){updateQuantity=true;checkQuantity();}}
dobrado.log('Loading connect settings...','info');$.post('/php/request.php',{request:'sell',action:'connectSettings',url:location.href,token:dobrado.token},function(response){if(dobrado.checkResponseError(response,'sell connectSettings')){return;}
var content='';var connect=JSON.parse(response);if(connect.settings.length===0){content='<p>There are no scales available to connect to.</p><p>'+'Please talk to an administrator about updating your settings.</p>';$('#purchase-update-quantity').prop('checked',false);$('#purchase-update-quantity').checkboxradio('refresh');}
else if(connect.settings.length===1){var spark=connect.settings[0];if(sparkID===spark.id&&sparkAddress===spark.ip){content='<p>You are currently connected to <b>'+spark.name+'</b>';if(!updateQuantity){content+=', click the connect button to receive updates';}
content+='.<br><span class="purchase-spark-text">Address: '+
spark.ip+'<br>ID: </span>'+'<span class="purchase-spark-id">'+spark.id+'</span>';}
else{if(!sparkID){updateQuantity=true;if(!$('#purchase-update-quantity').is(':checked')){$('#purchase-update-quantity').prop('checked',true);$('#purchase-update-quantity').checkboxradio('refresh');}
sparkID=spark.id;sparkAddress=spark.ip;checkQuantity();}
else{sparkID=spark.id;sparkAddress=spark.ip;}
content='<p>You are now connected to <b>'+spark.name+'</b>';if(!updateQuantity){content+=', click the connect button to receive updates';}
content+='.<br><span class="purchase-spark-text">Address: '+
spark.ip+'<br>ID: </span>'+'<span class="purchase-spark-id">'+spark.id+'</span>';}}
else{var name='';var options='';$.each(connect.settings,function(index,item){sparkOptions[item.name]={id:item.id,ip:item.ip};if(sparkID===item.id){name=item.name;options+='<option selected="selected">'+item.name+'</option>';}
else{options+='<option>'+item.name+'</option>';}});if(name){content='<p>You are currently connected to <b>'+name+'</b>';if(!updateQuantity){content+=', click the connect button to receive updates';}
content+='.<br><span class="purchase-spark-text">Address: '+
sparkAddress+'<br>ID: </span>'+'<span class="purchase-spark-id">'+sparkID+'</span></p>'+'<p>You can select different scales to connect to:<br>';}
else{content='<p>Please choose the scales to connect to:<br><br>';}
content+='<label for="purchase-spark-list">Name:</label>'+'<select id="purchase-spark-list">'+options+'</select><br><br>'+'<button id="purchase-spark-connect">connect</button></p>';}
$('.purchase-connect-settings').html(content).dialog({show:true,width:400,position:{my:'top',at:'top+50',of:window},title:'Connect Scales',create:dobrado.fixedDialog});$('#purchase-spark-connect').button().click(sparkConnect);});return false;}
function checkQuantity(){function createCORSRequest(url){var xhr=new XMLHttpRequest();if('withCredentials'in xhr){xhr.open('GET',url,true);}
else if(typeof XDomainRequest!=='undefined'){xhr=new XDomainRequest();xhr.open('GET',url);}
else{xhr=null;}
return xhr;}
if(!updateQuantity||!sparkID||!sparkAddress){return;}
var xhr=createCORSRequest('http://'+sparkAddress);if(!xhr){console.log('CORS not supported.');return;}
xhr.onload=function(){var weight=xhr.responseText.match(/([0-9.]+)kg/)[0];if(currentProduct&&currentProduct.unit==='kg'){$('#purchase-quantity-input').val(weight);setQuantity();}
currentWeight=weight;checkQuantity();};xhr.onerror=function(){$('#purchase-quantity-input').val('');console.log('Error reading from spark.');setTimeout(checkQuantity,1000);};xhr.send();}}());