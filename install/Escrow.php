<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Escrow extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'You don\'t have permission to use escrow.'];
    }
    // mode is used by the Extended module, which calls this function when
    // using ckeditor to compose a description.
    if (isset($_POST['mode']) && $_POST['mode'] === 'box') {
      return ['editor' => true, 'source' => ''];
    }

    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'start') {
      return $this->LookupRecipient();
    }
    if ($us_action === 'submit') {
      return $this->CreatePost();
    }
    if ($us_action === 'reply') {
      return $this->CheckReply();
    }
    return ['error' => 'Unknown action.'];
  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return true;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $result = $this->Nickname();
    $patterns = ['/!name/', '/!url/', '/!photo/'];
    $replacements = [$result['name'], $result['url'], $result['photo']];
    $reply = isset($_GET['reply']) ? $_GET['reply'] : '';

    return $this->Substitute('escrow-content', $patterns, $replacements) .
      '<p>' .
        '<label for="escrow-web-address">Enter a web address to get started:' .
        '</label><input id="escrow-web-address" type="text" maxlength="200">' .
        '<button id="escrow-start">go</button>' .
        '<div class="escrow-start-info"></div>' .
      '</p>' .
      '<form id="escrow-form" class="hidden">' .
        '<div class="escrow-recipient-info form-spacing"></div>' .
        '<div class="escrow-reply-context form-spacing hidden">' . $reply .
        '</div>' .
        '<div class="form-spacing">' .
          '<fieldset><legend>Payment Type:</legend>' .
            '<input type="radio" id="escrow-payment-to" ' .
              'name="escrow-payment-type" value="to" checked="checked">' .
            '<label for="escrow-payment-to">Make Payment To</label>' .
            '<input type="radio" id="escrow-payment-from" ' .
              'name="escrow-payment-type" value="from">' .
            '<label for="escrow-payment-from">Request Payment From</label>' .
          '</fieldset>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="escrow-payment-amount">Payment Amount:</label>' .
          '<input type="text" id="escrow-payment-amount" maxlength="10">' .
        '</div>' .
        '<label for="escrow-payment-description">Description:</label>' .
        '<textarea id="escrow-payment-description" class="dobrado-editable">' .
        '</textarea>' .
        '<button id="escrow-payment-submit">submit</button>' .
        '<div class="escrow-submit-info form-spacing"></div>' .
      '</form>' .
      '<p class="escrow-no-micropub hidden">' .
        '<label for="escrow-post">Copy the HTML content below to your site:' .
        '</label><textarea id="escrow-post"></textarea>' .
      '</p>' .
      '<p class="escrow-transaction"></p>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      $count = count($p);
      if ($fn === 'SettleTransaction' && $count === 2) {
        $us_post = $p[0];
        $us_reply = $p[1];
        return $this->SettleTransaction($us_post, $us_reply);
      }
      if ($fn === 'NewTransaction') {
        return $this->NewTransaction($p);
      }
    }
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Need to call AppendScript here if module uses javascript, ie:
    $this->AppendScript($path, 'dobrado.escrow.js');
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS escrow (' .
      'user VARCHAR(50) NOT NULL,' .
      'feed VARCHAR(200) NOT NULL,' .
      'currency VARCHAR(3),' .
      'PRIMARY KEY(user)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Escrow->Install 1: ' . $mysqli->error);
    }
    $query = 'CREATE TABLE IF NOT EXISTS escrow_transaction (' .
      'user VARCHAR(50) NOT NULL,' .
      'recipient VARCHAR(50) NOT NULL,' .
      'payment ENUM("debit", "credit") NOT NULL,' .
      'amount INT UNSIGNED NOT NULL,' .
      'confirmed TINYINT(1) NOT NULL,' .
      'permalink VARCHAR(200) NOT NULL,' .
      'PRIMARY KEY(user, recipient, permalink(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Escrow->Install 2: ' . $mysqli->error);
    }

    $site_style = ['"",".escrow img.profile","float","right"',
                   '"",".escrow img.profile","max-width","100px"',
                   '"",".escrow img.profile","border-radius","2px"',
                   '"","#escrow-form","padding","10px"',
                   '"","#escrow-form","border","1px solid #aaaaaa"',
                   '"","#escrow-form","background-color","#efefef"',
                   '"","#escrow-form","border-radius","2px"',
                   '"",".escrow-recipient-info .photo","width","40px"',
                   '"",".escrow-recipient-info .photo","border-radius","2px"',
                   '"",".escrow-recipient-info .name","font-size","1.5em"',
                   '"",".escrow-recipient-info .reachable","padding","5px"',
                   '"",".escrow-recipient-info .reachable","text-decoration",' .
                     '"none"',
                   '"",".escrow-recipient-info .reachable","border-radius",' .
                     '"5px"',
                   '"",".escrow-recipient-info .reachable","color","#333333"',
                   '"",".escrow-recipient-info .reachable","float","right"',
                   '"",".escrow-recipient-info .reachable","margin-top","20px"',
                   '"",".escrow-recipient-info .reachable","border",' .
                     '"2px solid #3d9e3d"',
                   '"",".escrow-recipient-info .reachable",' .
                     '"background-color","#ddffcc"',
                   '"",".escrow-recipient-info .not.reachable",' .
                     '"border","2px solid #de2e40"',
                   '"",".escrow-recipient-info .not.reachable",' .
                     '"background-color","#ff8888"',
                   '"","#escrow-form fieldset","width","100%"',
                   '"","#escrow-form fieldset","box-sizing","border-box"',
                   '"","#escrow-payment-from","margin-left","20px"',
                   '"","#escrow-form label","width","10em"',
                   '"","#escrow-payment-description","width","100%"',
                   '"","#escrow-payment-description","height","100px"',
                   '"","#escrow-payment-submit","margin-left","10.3em"'];
    $this->AddSiteStyle($site_style);

    $server_name = $this->user->config->ServerName();
    $us_content = '<a href="!url"><img src="!photo" class="profile"></a>' .
      '<h4 class="name">Hello !name</h4>' .
      '<p>Welcome to <b>' . $server_name . '</b>! You can use this site to ' .
        'record a payment to someone you know, or request a payment from ' .
        'them. All you need is their personal web address. Please note that ' .
        'this site <i>only records the transactions</i> it does not transfer ' .
        'any funds.' .
      '</p>';
    $us_lookup_error = 'Sorry the data at the address provided was the wrong ' .
      'format.<br><b>' . $server_name . '</b> looks for an ' .
      '<a href="https://indieweb.org/h-card">h-card</a> that contains the ' .
      'address you provided, this confirms the identity of the person you ' .
      'want to start a transaction with.';
    $us_lookup_reachable = 'Create a payment record for:<br>' .
      '<a href="!url"><img src="!photo" class="photo"> ' .
        '<span class="name">!name</span></a> ' .
      '<a href="/webmentions" class="reachable">' .
        '<img src="/images/tick.png"> Homepage accepts webmentions</a>';
    $us_lookup_not_reachable = 'Create a payment record for:<br>' .
      '<a href="!url"><img src="!photo" class="photo"> ' .
        '<span class="name">!name</span></a> ' .
      '<a href="/webmentions" class="not reachable">' .
        'Homepage does not accept webmentions</a>';
    $us_payment_to = '<p>Payment of <data class="p-debit" value="!amount">' .
      '$!amount</data> made to <a class="u-category h-card" ' .
        'href="!recipient">!name</a></p>!description';
    $us_payment_from = '<p>Payment of <data class="p-credit" value="!amount">' .
      '$!amount</data> requested from <a class="u-category h-card" ' .
        'href="!recipient">!name</a></p>!description';
    $us_payment_reply_to = '<p>Payment request of <data class="p-debit" ' .
      'value="!amount">$!amount</data> accepted.</p>!description';
    $us_payment_reply_from = '<p>Payment of <data class="p-credit" ' .
      'value="!amount">$!amount</data> accepted.</p>!description';
    $us_no_micropub = $server_name .
      ' uses <a href="https://indieweb.org/micropub">micropub</a> to publish ' .
      'a transaction on your own site. Since a micropub endpoint was not ' .
      'found when you logged in, the content is provided below so that you ' .
      'can copy and publish the post.';
    $us_escrow_post_debit = 'You made a payment of <b>$!amount</b>' .
      '<blockquote>!content</blockquote>' .
      'Original post: <a href="!url" class="u-syndication">!url</a>';
    $us_escrow_post_credit =
      'You requested a payment of <b>$!amount</b>' .
      '<blockquote>!content</blockquote>' .
      'Original post: <a href="!url" class="u-syndication">!url</a>';
    $template = ['"escrow-content", "", ' .
                   '"' . $mysqli->escape_string($us_content) . '"',
                 '"escrow-lookup-error", "", ' .
                   '"' . $mysqli->escape_string($us_lookup_error) . '"',
                 '"escrow-lookup-reachable", "", ' .
                   '"' . $mysqli->escape_string($us_lookup_reachable) . '"',
                 '"escrow-lookup-not-reachable", "", ' .
                   '"' . $mysqli->escape_string($us_lookup_not_reachable) . '"',
                 '"escrow-payment-to", "", ' .
                   '"' . $mysqli->escape_string($us_payment_to) . '"',
                 '"escrow-payment-from", "", ' .
                   '"' . $mysqli->escape_string($us_payment_from) . '"',
                 '"escrow-payment-reply-to", "", ' .
                   '"' . $mysqli->escape_string($us_payment_reply_to) . '"',
                 '"escrow-payment-reply-from", "", ' .
                   '"' . $mysqli->escape_string($us_payment_reply_from) . '"',
                 '"escrow-no-micropub", "", ' .
                   '"' . $mysqli->escape_string($us_no_micropub) . '"',
                 '"escrow-post-debit", "", ' .
                   '"' . $mysqli->escape_string($us_escrow_post_debit) . '"',
                 '"escrow-post-credit", "", ' .
                   '"' . $mysqli->escape_string($us_escrow_post_credit) . '"'];
    $mysqli->close();
    $this->AddTemplate($template);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    if (isset($id)) return;

    $mysqli = connect_db();
    $query = 'DELETE FROM escrow WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Escrow->Remove 1: ' . $mysqli->error);
    }
    $query = 'DELETE FROM escrow_transaction WHERE ' .
      'user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Escrow->Remove 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // This is called when the version of the module is updated,
    // to provide a way to update or modify tables etc..
  }

  public function UpdateScript($path) {
    // Need to call AppendScript here if module uses javascript, ie:
    $this->AppendScript($path, 'dobrado.escrow.js');
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function AddRecipients($recipients, $payment,
                                 $amount, $permalink, $us_url) {
    // Convert amount to cents.
    if (preg_match('/([0-9]*)\.([0-9]*)/', $amount, $match)) {
      $dollars = $match[1];
      $cents = substr($match[2], 0, 2);
      $amount = $dollars;
      if (strlen($cents) === 2) $amount .= $cents;
      else if (strlen($cents) === 1) $amount .= $cents . '0';
      else $amount .= '00';
    }
    else {
      $amount .= '00';
    }
    $server_name = $this->user->config->ServerName();
    $base_url = $this->user->config->Secure() ? 'https://' : 'http://';
    $base_url .= $server_name;
    $path = $this->Url('', $permalink);

    $reader_id = $this->ReaderId();
    if ($reader_id === 0) return;

    $mysqli = connect_db();
    $current_owner = $this->owner;
    foreach ($recipients as $subscribe) {
      if (preg_match('/^https?:\/\/(.+)$/i', $subscribe, $match)) {
        $subscribe = strtolower(trim($match[1], ' /'));
      }
      if (strlen($subscribe) > 50 || strpos($subscribe, '.') === false) {
        $this->Log('Escrow->AddRecipients 1: Could not add ' . $subscribe .
                   ' for ' . $this->owner . '/' . $permalink);
        continue;
      }

      $subscribe = preg_replace('/\//', '_', $subscribe);
      list($feed, $currency, $created) = $this->CheckUser($subscribe);
      $this->owner = $subscribe;
      $reader = new Module($this->user, $this->owner, 'reader');
      $reader->Factory('AddFeed', [$reader_id, $base_url . $path, true]);
      // Also add the recipient's own feed if it was just created.
      if ($created) {
        $reader->Factory('AddFeed',
                         [$reader_id, $base_url . $this->Url('', $feed), true]);
      }
      $query = 'INSERT INTO escrow_transaction VALUES (' .
        '"' . $current_owner . '", "' . $this->owner . '", ' .
        '"' . $payment . '", ' . $amount . ', 0, "' . $permalink . '")';
      if (!$mysqli->query($query)) {
        $this->Log('Escrow->AddRecipients 2: ' . $mysqli->error);
      }
    }
    $this->owner = $current_owner;
    $mysqli->close();

    // Add an initial comment to this permalink so that the post shows up
    // in the feed for the recipients.
    $comment = new Module($this->user, $this->owner, 'comment');
    if ($comment->IsInstalled()) {
      $current_page = $this->user->page;
      $this->user->page = $permalink;
      $id = new_module($this->user, $this->owner, 'comment', $permalink,
                       $comment->Group(), $comment->Placement());
      $comment->Add($id);
      $reply_url = $this->Url('reply=' . urlencode($us_url),
                              $this->Substitute('indieauth-page'), 'admin');
      // This makes sure amount is formatted correctly.
      $amount = $this->CentsToDollars($amount);
      $us_description = '<p>A payment of <b>$' . $amount . '</b> was ';
      $us_description .= $payment === 'debit' ? 'made to you' :
        'requested from you';
      $us_description .= ' and is <a href="' . $reply_url . '">waiting for ' .
        'a reply</a>:</p><a href="' . $us_url . '" class="u-in-reply-to"></a>';
      $us_content = ['author' => $server_name, 'description' => $us_description,
                     'permalink' => $permalink . '#' . $id];
      $comment->SetContent($id, $us_content);
      $this->user->page = $current_page;
    }
  }

  private function CentsToDollars($cents) {
    if ($cents === 0) return '0.00';
    if ($cents < 10) return '0.0' . $cents;
    if ($cents < 100) return '0.' . $cents;
    if (preg_match('/^([0-9]+)([0-9]{2})$/', $cents, $match)) {
      return $match[1] . '.' . $match[2];
    }
  }

  private function CheckUser($domain, $post_currency = '') {
    $mysqli = connect_db();
    $exists = false;
    $query = 'SELECT user FROM users WHERE user = "' . $domain . '"';
    if ($result = $mysqli->query($query)) {
      if ($result->num_rows === 1) $exists = true;
      $result->close();
    }
    else {
      $this->Log('Escrow->CheckUser 1: ' . $mysqli->error);
    }
    if (!$exists) {
      $password = bin2hex(openssl_random_pseudo_bytes(8));
      $password_hash =
        $mysqli->escape_string(password_hash($password, PASSWORD_DEFAULT));
      $system_group = $mysqli->escape_string(substitute('indieauth-group'));
      $query = 'INSERT INTO users (user, system_group, password, confirmed, ' .
        'registration_time) VALUES ("' . $domain . '", ' .
        '"' . $system_group . '", "' . $password_hash . '", 1, ' . time() . ')';
      if (!$mysqli->query($query)) {
        $this->Log('Escrow->CheckUser 2: ' . $mysqli->error);
      }
      $query = 'INSERT INTO group_names VALUES ' .
        '("admin", "' . $system_group . '", "' . $domain . '", 0)';
      if (!$mysqli->query($query)) {
        $this->Log('Escrow->CheckUser 3: ' . $mysqli->error);
      }
      mkdir('../' . $domain . '/public', 0755, true);
    }

    $feed = '';
    $user_currency = '';
    $query = 'SELECT feed, currency FROM escrow WHERE user = "' . $domain . '"';
    if ($result = $mysqli->query($query)) {
      if ($escrow = $result->fetch_assoc()) {
        $feed = $escrow['feed'];
        $user_currency = $escrow['currency'];
      }
    }
    else {
      $this->Log('Escrow->CheckUser 4: ' . $mysqli->error);
    }
    $created = false;
    // The post_currency parameter is provided to create a default currency
    // for the user if it's not already set. (TODO: Need a way to update the
    // user's default currency once set.)
    if ($user_currency === '') $user_currency = $post_currency;
    if ($feed === '') {
      $feed = bin2hex(openssl_random_pseudo_bytes(16));
      $query = 'INSERT INTO escrow VALUES ("' . $domain . '", ' .
        '"' . $feed . '", "' . $user_currency . '")';
      if (!$mysqli->query($query)) {
        $this->Log('Escrow->CheckUser 5: ' . $mysqli->error);
      }
      // Need a real profile to subscribe to the feed being created.
      copy('../php/default.php', '../' . $domain . '/index.php');
      chmod('../' . $domain . '/index.php', 0644);

      $handle = fopen('../' . $domain . '/owner.php', 'w');
      fwrite($handle, '<?php $owner=\'' . $domain . '\';');
      fclose($handle);
      chmod('../' . $domain . '/owner.php', 0644);

      mkdir('../' . $domain . '/rss', 0755, true);
      $handle = fopen('../' . $domain . '/rss/index.php', 'w');
      fwrite($handle, "<?php\n" .
             "include '../../php/functions/db.php';\n" .
             "include '../../php/functions/rss.php';\n" .
             "include '../../php/config.php';\n" .
             "include '../../php/module.php';\n" .
             "include '../../php/user.php';\n" .
             "rss('" . $domain . "');\n");
      fclose($handle);
      chmod('../' . $domain . '/rss/index.php', 0644);

      copy_page('default_blog', 'admin', $feed, $domain);
      copy_page('default', 'admin', 'default', $domain);
      $created = true;
    }
    $mysqli->close();
    return [$feed, $user_currency, $created];
  }

  private function CheckReply() {
    if (!isset($_POST['reply'])) return ['error' => 'Reply address not set.'];

    $url = $_POST['reply'];
    $us_post = parse_hentry($url);
    // Make sure this is not a reply post itself.
    if ($us_post['in-reply-to'] !== '') {
      $reply_url = $this->Url('reply=' . urlencode($us_post['in-reply-to']),
                              $this->Substitute('indieauth-page'), 'admin');
      return ['fail' => '<p><a href="' . $url . '">This post</a> looks like ' .
                          'a reply. You might want to try replying to the ' .
                          '<a href="' . $reply_url . '"> original post</a>.' .
                        '</p>'];
    }
    // Make sure this is a payment post.
    if ($us_post['payment'] === '' || $us_post['amount'] === '') {
      return ['fail' => '<p><a href="' . $url . '">This post</a> does not ' .
                          'contain the correct payment details.</p>'];
    }
    // Need to switch underscores back to path separators to match the url.
    $tag = preg_replace('/_/', '/', $this->user->name);
    // Look for the current user in the person-tags in this post.
    if (strpos($us_post['category'], $tag) === false) {
      return ['fail' => '<p>You are not tagged as a recipient in this ' .
                          '<a href="' . $url . '">payment post</a>.</p>'];
    }

    $mysqli = connect_db();
    $author_url = $mysqli->escape_string($us_post['author-url']);
    $mysqli->close();

    $result = $this->LookupRecipient($author_url);
    // Switch the payment type for the reply, leaving the amount the same.
    $result['payment'] = $us_post['payment'] === 'debit' ? 'credit' : 'debit';
    $result['amount'] = $us_post['amount'];
    return $result;
  }

  private function CreatePost() {
    if (!isset($_POST['type'])) return ['error' => 'Payment type not set.'];
    if (!isset($_POST['recipient'])) return ['error' => 'Recipient not set.'];
    if (!isset($_POST['amount'])) return ['error' => 'Payment amount not set.'];

    $us_type = $_POST['type'];
    if ($us_type !== 'to' && $us_type !== 'from') {
      return ['error' => 'Payment type is incorrect format.'];
    }

    $us_recipient = $_POST['recipient'];
    $us_amount = $_POST['amount'];
    if (preg_match('/([0-9.]+)/', $us_amount, $match)) {
      $us_amount = $match[1];
    }
    $us_name = isset($_POST['name']) ? $_POST['name'] : '';
    $us_reply = isset($_POST['reply']) ? $_POST['reply'] : '';
    if ($us_name === '') $us_name = $us_recipient;
    $scheme = $this->user->config->Secure() ? 'https://' : 'http://';
    $server_name = $this->user->config->ServerName();
    $us_description = isset($_POST['description']) ? $_POST['description'] : '';
    $us_category = $scheme . $server_name;
    $payment_type = 'escrow-payment-';
    if ($us_reply !== '') {
      $payment_type .= 'reply-';
      // The link isn't visible here because the Reader module will display the
      // reply context, the reply url is added as a tag for the permalink page.
      $us_description .= '<a href="' . $us_reply . '" class="u-in-reply-to">' .
        '</a>';
      $us_category .= ',In reply to,' . $us_reply;
    }
    $payment_type .= $us_type;
    $patterns = ['/!recipient/', '/!name/', '/!amount/', '/!description/'];
    $replacements = [$us_recipient, $us_name, $us_amount, $us_description];
    $us_content = ['data' => $this->Substitute($payment_type, $patterns,
                                               $replacements),
                   'category' => $us_category, 'paymentType' => $us_type,
                   'paymentAmount' => $us_amount];
    if (isset($this->user->settings['micropub']['endpoint'])) {
      $post = new Module($this->user, $this->owner, 'post');
      $post->SetContent(0, $us_content);
      return ['success' => true];
    }
    return ['fail' => $this->Substitute('escrow-no-micropub'),
            'content' => $us_content['data'] . '<p>Track via ' .
              '<a href="' . $scheme . $server_name . '">' . $server_name .
              '</a></p>'];
  }

  private function LookupRecipient($address = '') {
    if ($address === '') {
      $mysqli = connect_db();
      $address = isset($_POST['address']) ?
        $mysqli->escape_string(trim($_POST['address'])) : '';
      $mysqli->close();
    }

    $us_result = $this->Nickname($address);
    if ($us_result['url'] === '') {
      discover_endpoint($address, 'webmention');
      // Try nickname lookup again after doing discovery.
      $us_result = $this->Nickname($address);
    }
    if ($us_result['url'] === '') {
      return ['fail' => $this->Substitute('escrow-lookup-error')];
    }

    $patterns = ['/!name/', '/!url/', '/!photo/'];
    $replacements = [$us_result['name'], $us_result['url'],
                     $us_result['photo']];
    $history = $this->TransactionHistory($us_result['url'], $us_result['name']);
    if ($us_result['reachable']) {
      return ['details' => $us_result, 'history' => $history,
              'success' => $this->Substitute('escrow-lookup-reachable',
                                             $patterns, $replacements)];
    }
    return ['details' => $us_result, 'history' => $history,
            'success' => $this->Substitute('escrow-lookup-not-reachable',
                                           $patterns, $replacements)];
  }

  private function NewTransaction($us_post) {
    $payment = $us_post['payment'];
    if ($payment !== 'debit' && $payment !== 'credit') {
      return ['HTTP/1.1 400 Bad Request',
              'Payment type must be debit or credit.'];
    }
    
    $us_reply = NULL;
    // First check if this really is a new transaction, or a reply post.
    if ($us_post['in-reply-to'] !== '') {
      $us_original = parse_hentry($us_post['in-reply-to']);
      // Try processing the original post, and call SettleTransaction with this
      // reply after the original post is created.
      $us_reply = $us_post;
      $us_post = $us_original;
      // Also check if the original post had a payment property.
      $payment = $us_post['payment'];
      if ($payment !== 'debit' && $payment !== 'credit') {
        return ['HTTP/1.1 400 Bad Request',
                'Payment type of original post must be debit or credit.'];
      }
    }

    $mysqli = connect_db();
    $author_url = $mysqli->escape_string($us_post['author-url']);
    $amount = $mysqli->escape_string($us_post['amount']);
    $post_currency = $mysqli->escape_string($us_post['currency']);
    // Find all person-tags in this post.
    $recipients = [];
    foreach (explode(',', $us_post['category']) as $us_tag) {
      $us_result = $this->Nickname($mysqli->escape_string($us_tag));
      if ($us_result['url'] !== '') {
        $recipients[] = $mysqli->escape_string($us_result['url']);
      }
    }
    $mysqli->close();
    
    // The domain and path without the scheme is used here as a username.
    $domain = '';
    if (preg_match('/^https?:\/\/(.+)$/i', $author_url, $match)) {
      $domain = strtolower(trim($match[1], ' /'));
    }
    // The following conditions must be met for this to be a valid transaction.
    if (strlen($domain) > 50 || strpos($domain, '.') === false) {
      return ['HTTP/1.1 400 Bad Request', $domain .
              ' is the wrong format to be used as an account name. ' .
              '(Must contain a TLD and be no more than 50 chars.)'];
    }
    if (count($recipients) === 0) {
      return ['HTTP/1.1 400 Bad Request',
              'No recipients found at: ' . $us_post['url']];
    }
    if ((float)$amount < 0.01) {
      return ['HTTP/1.1 400 Bad Request',
              'Invalid payment amount at: ' . $us_post['url']];
    }

    $base_url = $this->user->config->Secure() ? 'https://' : 'http://';
    $base_url .= $this->user->config->ServerName();
    $current_owner = $this->owner;
    $current_user = $this->user->name;
    $current_page = $this->user->page;
    // Create or update the post for this transaction. Want to be able to use
    // $domain as a regular username, so replace slashes with underscores.
    $domain = preg_replace('/\//', '_', $domain);
    list($this->user->page, $user_currency, $created) =
      $this->CheckUser($domain, $post_currency);
    $this->user->name = $domain;
    $this->owner = $domain;
    $reader = new Module($this->user, $this->owner, 'reader');
    if ($created) {
      if ($reader->IsInstalled()) {
        $reader_id = $this->ReaderId();
        $reader->Factory('AddFeed',
                         [$reader_id, $base_url . $this->Url(), true]);
      }
    }
    // TODO: If post_currency doesn't match user_currency then convert amount.
    $us_url = $us_post['url'];
    $module = new Module($this->user, $this->owner, 'post');
    list($id, $description, $permalink) = $module->Factory('Syndicated',
                                                           $us_url);
    if ($id === 0) {
      $id = new_module($this->user, $this->owner, 'post', $this->user->page,
                       $module->Group(), $module->Placement());
      $module->Add($id);
      $format = 'escrow-post-' . $payment;
      $patterns = ['/!author-name/', '/!author-photo/', '/!author-url/',
                   '/!amount/', '/!content/', '/!url/'];
      $replacements = [$us_post['author-name'], $us_post['author-photo'],
                       $us_post['author-url'], $us_post['amount'],
                       $us_post['content'], $us_url];
      $us_data = $this->Substitute($format, $patterns, $replacements);
      // Note that 'Other Payments' is used as the title here so that it gets
      // used as the title for the comments feed. (The post otherwise doesn't
      // need a title and the in-feed post can be configured to not display it.)
      $result = $module->SetContent($id, ['data' => $us_data,
                                          'dataOnly' => false,
                                          'title' => 'Other Payments',
                                          'category' => $us_post['category'],
                                          'author' => $domain]);
      if ($reader->IsInstalled()) {
        $reader->Factory('UpdateFeed', [$base_url . $this->Url(),
                                        $module->Content($id)]);
      }
      $permalink = $result['permalink'];
      $this->AddRecipients($recipients, $payment, $amount, $permalink, $us_url);
    }
    else {
      $us_original = parse_hentry($us_url, '', $description);
      // TODO: Make sure author matches in original post, otherwise probably
      // can't do much here. Then if there are no confirmed transactions on the
      // permalink should be able to update the post with changes to
      // amount, currency and recipients. If there are confirmed transactions
      // probably should update our copy of the post to say that the original
      // has changed.
      $this->Log('Escrow->NewTransaction: ' . $us_url .
                 ' already syndicated to ' . $permalink . ' with id = ' . $id .
                 '. (Update not yet implemented.)');
    }
    $webmention_status = 'HTTP/1.1 200 OK';
    $webmention_message = 'Thanks! Your payment post is now being tracked.';
    if (isset($us_reply)) {
      list($webmention_status, $webmention_message) =
        $this->SettleTransaction($us_post, $us_reply, false);
    }
    $this->owner = $current_owner;
    $this->user->name = $current_user;
    $this->user->page = $current_page;
    return [$webmention_status, $webmention_message];
  }

  private function Nickname($address = '') {
    $us_name = '';
    $us_photo = '';
    $us_url = '';
    $reachable = false;
    $first_name = false;
    if ($address === '') {
      $address = $this->user->name;
      // When no address is given assume being called from Content() and
      // display the user's first name only.
      $first_name = true;
    }
    else if (preg_match('/^https?:\/\/(.+)$/i', $address, $match)) {
      $address = trim($match[1], ' /');
    }
    $mysqli = connect_db();
    // The empty path segment may have been trimmed above, so try matching both.
    $query = 'SELECT name, url, cache, reachable FROM nickname WHERE url ' .
      'LIKE "http%://' . $address . '" OR url LIKE "http%://' . $address . '/"';
    if ($result = $mysqli->query($query)) {
      if ($nickname = $result->fetch_assoc()) {
        $us_name = $nickname['name'];
        $us_url = $nickname['url'];
        $us_photo = $nickname['cache'];
        $reachable = $nickname['reachable'] === '1';
      }
      $result->close();
    }
    else {
      $this->Log('Escrow->Nickname: ' . $mysqli->error);
    }
    // Also add user details for logged in users so that syndicated posts
    // have nicer formatting.
    if ($first_name) {
      $us_last = '';
      if (preg_match('/^(.+) (.+)$/', $us_name, $match)) {
        $us_name = $match[1];
        $us_last = $match[2];
      }
      $detail = new Module($this->user, $this->owner, 'detail');
      if ($detail->IsInstalled()) {
        $first = $mysqli->escape_string($us_name);
        $last = $mysqli->escape_string($us_last);
        $photo = $mysqli->escape_string($us_photo);
        $detail->Factory('UpdateUser', [$this->user->name, $first, $last,
                                        $photo]);
      }
    }
    $mysqli->close();
    if ($us_name === '') $us_name = $address;
    return ['name' => $us_name, 'url' => $us_url, 'photo' => $us_photo,
            'reachable' => $reachable];
  }

  private function ReaderId() {
    $id = 0;
    $mysqli = connect_db();
    $page = $mysqli->escape_string($this->Substitute('indieauth-page'));
    $query = 'SELECT box_id FROM modules WHERE user = "admin" AND ' .
      'label = "reader" AND page = "' . $page . '"';
    if ($result = $mysqli->query($query)) {
      if ($modules = $result->fetch_assoc()) {
        $id = (int)$modules['box_id'];
      }
      $result->close();
    }
    else {
      $this->Log('Escrow->ReaderId: ' . $mysqli->error);
    }
    $mysqli->close();
    return $id;
  }

  private function SettleTransaction($us_post, $us_reply, $fetch = true) {
    // When this function is called from Post->ProcessReceivedWebmention,
    // the reply is the parsed version available with the post. To verify the
    // recipient need to fetch their original reply post.
    if ($fetch) $us_reply = parse_hentry($us_reply['url']);
    $us_reply_url = $us_reply['url'];

    if ($us_reply['payment'] !== 'debit' && $us_reply['payment'] !== 'credit') {
      return ['HTTP/1.1 400 Bad Request',
              'Payment type of reply must be debit or credit.'];
    }
    if ($us_reply['payment'] === $us_post['payment']) {
      return ['HTTP/1.1 400 Bad Request',
              'Payment type of reply is the same as the original post.'];
    }
    if ($us_reply['currency'] !== '' &&
        $us_reply['currency'] !== $us_post['currency']) {
      return ['HTTP/1.1 400 Bad Request',
              'Currency type of reply is different from the original post.'];
    }
    if ($us_reply['amount'] !== $us_post['amount']) {
      return ['HTTP/1.1 400 Bad Request',
              'Payment amount of reply is different from the original post.'];
    }
    if ($us_reply['in-reply-to'] !== $us_post['url']) {
      return ['HTTP/1.1 400 Bad Request',
              'This post is not in reply to: ' . $us_post['url']];
    }
    // Make sure the reply is from an intended recipient of the transaction.
    if (strpos($us_post['category'], $us_reply['author-url']) === false) {
      return ['HTTP/1.1 400 Bad Request', 'The author of this reply is not ' .
              'a recipient of the transaction.'];
    }
    
    $mysqli = connect_db();
    $user = $mysqli->escape_string($us_post['author-url']);
    if (preg_match('/^https?:\/\/(.+)$/i', $user, $match)) {
      $user = strtolower(preg_replace('/\//', '_', trim($match[1], ' /')));
    }
    $recipient = $mysqli->escape_string($us_reply['author-url']);
    if (preg_match('/^https?:\/\/(.+)$/i', $recipient, $match)) {
      $recipient = strtolower(preg_replace('/\//', '_', trim($match[1], ' /')));
    }
    $current_owner = $this->owner;
    $current_user = $this->user->name;
    $current_page = $this->user->page;
    $this->owner = $user;
    $this->user->name = $user;
    $module = new Module($this->user, $this->owner, 'post');
    // Syndicated requires the current page to be set to the user's feed.
    // (Which is set from NewTransaction but not from the Post module.)
    $feed = '';
    $query = 'SELECT feed FROM escrow WHERE user = "' . $user . '"';
    if ($result = $mysqli->query($query)) {
      if ($escrow = $result->fetch_assoc()) {
        $feed = $escrow['feed'];
      }
    }
    else {
      $this->Log('Escrow->SettleTransaction 1: ' . $mysqli->error);
    }
    $this->user->page = $feed;
    list($id, $description, $permalink) = $module->Factory('Syndicated',
                                                           $us_post['url']);
    $query = 'UPDATE escrow_transaction SET confirmed = 1 WHERE ' .
      'user = "' . $user . '" AND recipient = "' . $recipient . '" AND ' .
      'permalink = "' . $permalink . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Escrow->SettleTransaction 2: ' . $mysqli->error);
    }
    $mysqli->close();

    // The comment module requires the current page to be set to the permalink.
    if ($permalink !== '') {
      $this->user->page = $permalink;
      $comment = new Module($this->user, $this->owner, 'comment');
      if ($comment->IsInstalled()) {
        // Firt remove the initial comment added when the post was created.
        // (The correct id is returned here because it's the only comment
        // without a url at this permalink.)
        $comment->Remove($comment->Factory('MatchUrl', ''));
        include_once 'library/HTMLPurifier.auto.php';
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.DefinitionID', 'dobrado-escrow');
        $config->set('HTML.DefinitionRev', 1);
        if ($def = $config->maybeGetRawHTMLDefinition()) {
          $def->addElement('data', 'Inline', 'Inline', 'Common');
          $def->addAttribute('data', 'value', 'CDATA');
        }
        $purifier = new HTMLPurifier($config);
        $us_url = $purifier->purify($us_reply['url']);
        $us_description = $purifier->purify($us_reply['content']);
        $us_author = $purifier->purify($us_reply['author-name']);
        $us_author_photo = $purifier->purify($us_reply['author-photo']);
        $us_author_url = $purifier->purify($us_reply['author-url']);
        $us_content = ['author' => $us_author,
                       'author_photo' => $us_author_photo,
                       'author_url' => $us_author_url, 'url' => $us_url,
                       'description' => $us_description,
        'permalink' => $permalink . '#' . $id];
        $id = $comment->Factory('MatchUrl', $us_url);
        if ($id !== 0) {
          if ($comment->Factory('Modified', [$id, $us_description])) {
            $comment->SetContent($id, $us_content);
          }
        }
        else if ($us_description !== '') {
          $id = new_module($this->user, $this->owner, 'comment',
                           $this->user->page, $comment->Group(),
                           $comment->Placement());
          $comment->Add($id);
          $comment->SetContent($id, $us_content);
        }
      }
    }
    $this->owner = $current_owner;
    $this->user->name = $current_user;
    $this->user->page = $current_page;
    return ['HTTP/1.1 200 OK',
            'Thanks! Your reply has been accepted as payment confirmation.'];
  }

  private function TransactionHistory($us_url, $us_name) {
    $debit_total = 0;
    $credit_total = 0;
    $confirmed_count = 0;
    $unconfirmed_count = 0;
    $recipient = '';
    $mysqli = connect_db();
    if (preg_match('/^https?:\/\/(.+)$/', $us_url, $match)) {
      $us_recipient = trim($match[1], ' /');
      $us_recipient = preg_replace('/\//', '_', $us_recipient);
      $recipient = $mysqli->escape_string($us_recipient);
    }
    // Find all the transactions started by this user.
    $query = 'SELECT payment, amount, confirmed FROM escrow_transaction ' .
      'WHERE user = "' . $this->user->name . '" AND ' .
      'recipient = "' . $recipient . '"';
    if ($result = $mysqli->query($query)) {
      while ($escrow = $result->fetch_assoc()) {
        if ($escrow['confirmed'] === '1') {
          if ($escrow['payment'] === 'debit') {
            $debit_total += (int)$escrow['amount'];
          }
          else {
            $credit_total += (int)$escrow['amount'];
          }
          $confirmed_count++;
        }
        else {
          $unconfirmed_count++;
        }
      }
      $result->close();
    }
    else {
      $this->Log('Escrow->TransactionHistory 1: ' . $mysqli->error);
    }
    // Then find all the transactions where they are the recipient.
    // (Ignore the case there user and recipient are the same here.)
    if ($this->user->name !== $recipient) {
      $query = 'SELECT payment, amount, confirmed FROM escrow_transaction ' .
        'WHERE user = "' . $recipient . '" AND ' .
        'recipient = "' . $this->user->name . '"';
      if ($result = $mysqli->query($query)) {
        while ($escrow = $result->fetch_assoc()) {
          if ($escrow['confirmed'] === '1') {
            // credit and debit are reversed along with the roles here.
            if ($escrow['payment'] === 'credit') {
              $debit_total += (int)$escrow['amount'];
            }
            else {
              $credit_total += (int)$escrow['amount'];
            }
            $confirmed_count++;
          }
          else {
            $unconfirmed_count++;
          }
        }
        $result->close();
      }
      else {
        $this->Log('Escrow->TransactionHistory 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();

    $content = '';
    if ($debit_total !== 0 || $credit_total !== 0) {
      $debit = $this->CentsToDollars($debit_total);
      $credit = $this->CentsToDollars($credit_total);
      if ($this->user->name === $recipient) {
        $content = 'You have sent a total of <b>$' . $debit . '</b> to ' .
          'yourself, and received a total of <b>$' . $credit . '</b>';
      }
      else {
        $content = 'You have sent a total of <b>$' . $debit . '</b> to ' .
          $us_name . ', they have sent you a total of <b>$' . $credit . '</b>';
      }
    }
    else {
      $content = 'You have a zero balance';
      if ($this->user->name !== $recipient) {
        $content .= ' with ' . $us_name;
      }
    }
    return $content . '.<br>(' . $confirmed_count . ' confirmed, ' .
      $unconfirmed_count . ' unconfirmed transactions.)';
  }

}
