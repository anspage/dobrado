/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2017 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.invite) {
  dobrado.invite = {};
}
(function() {

  'use strict';

  // Group private variables and module member variables here.

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.invite').length === 0) {
      return;
    }

    $('#invite-group-select').change(changeGroup);
    $('#invite-add').button().click(add);
    $('.invite-remove').button({
      icon: 'ui-icon-closethick', showLabel: false }).click(remove);
    $('#invite-accept').button().click(accept);
    $('#invite-decline').button().click(decline);
  });

  function changeGroup() {
    dobrado.log('Changing group.', 'info');
    $.post('/php/request.php', { request: 'invite',
                                 action: 'changeGroup',
                                 group: $('#invite-group-select').val(),
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'invite changeGroup')) {
          return;
        }
        location.reload();
      });
  }

  function add() {
    dobrado.log('Inviting group.', 'info');
    $.post('/php/request.php', { request: 'invite',
                                 action: 'invite',
                                 group: $('#invite-select').val(),
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'invite add')) {
          return;
        }
      });
  }

  function remove() {
    dobrado.log('Removing invite.', 'info');
    var group = $(this).siblings('.invite-remove-group');
    $.post('/php/request.php', { request: 'invite',
                                 action: 'remove',
                                 group: group.html(),
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'invite remove')) {
          return;
        }
        group.parent().remove();
      });
  }

  function accept() {
    dobrado.log('Accepting invite.', 'info');
    $.post('/php/request.php', { request: 'invite',
                                 action: 'accepted',
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'invite accept')) {
          return;
        }
        $('.invite-status').html('You have accepted the invitation.');
      });
  }

  function decline() {
    dobrado.log('Declining invite.', 'info');
    $.post('/php/request.php', { request: 'invite',
                                 action: 'declined',
                                 url: location.href,
                                 token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'invite decline')) {
          return;
        }
        $('.invite-status').html('You have declined the invitation.');
      });
  }

})();
