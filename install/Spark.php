<?php
// Dobrado Content Management System
// Copyright (C) 2016 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Spark extends Base {

  public function Add($id) {
    $this->RequestFeedId($id, 'spark');
  }

  public function Callback() {
    // Note that 'spark' is set when called via api.php.
    if (isset($_POST['spark'])) {
      $this->SaveAddress();
      return;
    }

    $us_action = isset($_POST['action']) ? $_POST['action'] : '';

    if ($us_action === 'removeSpark') {
      return $this->RemoveSpark();
    }
    if ($us_action === 'editName') {
      return $this->EditName();
    }
    if ($us_action === 'removeGroup') {
      return $this->RemoveGroup();
    }
    if ($us_action === 'saveGroup') {
      return $this->SaveGroup();
    }
    if ($us_action === 'removeWiFi') {
      return $this->RemoveWiFi();
    }
    if ($us_action === 'saveWiFi') {
      return $this->SaveWiFi();
    }
    if ($us_action === 'updateSpark') {
      return $this->UpdateSpark();
    }

    // TODO: need a callback to save new locations.
    // set authorised flag to 1 in spark_locate table when saving.
  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $mysqli = connect_db();
    $spark_list = [];
    $query = 'SELECT spark_id, name FROM spark WHERE '.
      'user = "'.$this->owner.'" AND box_id = '.$id;
    if ($result = $mysqli->query($query)) {
      while ($spark = $result->fetch_assoc()) {
        $spark_list[] = ['id' => $spark['spark_id'], 'name' => $spark['name']];
      }
      $result->close();
    }
    else {
      $this->Log('Spark->Content 1: '.$mysqli->error);
    }
    $mysqli->close();

    $content = '<h3>Spark Connections:</h3>';
    for ($i = 0; $i < count($spark_list); $i++) {
      $name = $spark_list[$i]['name'];
      $spark_id = $spark_list[$i]['id'];
      $content .= '<form id="spark-form-'.$i.'" class="spark-details">'.
          '<button class="spark-remove">remove</button>'.
          '<label for="spark-name-'.$i.'">Name:</label>'.
          '<input id="spark-name-'.$i.'" class="spark-name" value="'.$name.'">'.
          '<button class="spark-edit-name">save</button><br>'.
          'ID: <span id="spark-id-'.$i.'" class="spark-id">'.$spark_id.
          '</span><br>'.
          $this->Share($id, $spark_id).
          $this->WiFi($id, $spark_id).
        '</form>';
    }
    $feed_id = $this->RequestFeedId($id);
    $content .= '<a href="#" class="spark-api-info">Show API credentials</a>'.
      '<div class="spark-api-credentials hidden">Feed ID: <b>'.$feed_id.
      '</b> API Key: <b>'.$this->RequestApiKey($feed_id).'</b></div>';
    if ($this->user->canEditSite &&
        $this->Substitute('spark-locate') === 'true') {
      $content .= '<a href="#" class="spark-locations">'.
        'Manage all spark locations</a>';
    }
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    if ($fn === 'ListConnections') {
      return $this->ListConnections($p);
    }
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.purchase.js to the existing dobrado.js file.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.spark.js', false);

    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS spark ('.
      'user VARCHAR(50) NOT NULL,'.
      'box_id INT UNSIGNED NOT NULL,'.
      'spark_id VARCHAR(32) NOT NULL,'.
      'name VARCHAR(100),'.
      'ip VARCHAR(50),'.
      'updated TINYINT(1),'.
      'PRIMARY KEY(user, spark_id)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->Install 1: '.$mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS spark_share ('.
      'user VARCHAR(50) NOT NULL,'.
      'box_id INT UNSIGNED NOT NULL,'.
      'spark_id VARCHAR(32) NOT NULL,'.
      'system_group VARCHAR(50),'.
      'PRIMARY KEY(user, box_id, spark_id, system_group)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->Install 2: '.$mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS spark_wifi ('.
      'user VARCHAR(50) NOT NULL,'.
      'box_id INT UNSIGNED NOT NULL,'.
      'spark_id VARCHAR(32) NOT NULL,'.
      'ssid VARCHAR(64),'.
      'password VARCHAR(64),'.
      'PRIMARY KEY(user, box_id, spark_id, ssid)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->Install 3: '.$mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS spark_locate ('.
      'spark_id VARCHAR(32) NOT NULL,'.
      'ip VARCHAR(50),'.
      'url VARCHAR(100),'.
      'feed_id INT UNSIGNED,'.
      'api_key INT UNSIGNED,'.
      'authorised TINYINT(1),'.
      'PRIMARY KEY(spark_id)'.
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->Install 4: '.$mysqli->error);
    }

    $mysqli->close();
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    $this->AppendScript($path, 'dobrado.spark.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  public function Locate() {
    // locate is set when the spark is first turned on, and is requesting the
    // details of the server it should communicate with (via /php/spark.php).
    $mysqli = connect_db();
    $spark_id = $mysqli->escape_string($_POST['locate']);
    $query = 'SELECT ip, url, feed_id, api_key, authorised FROM spark_locate '.
      'WHERE spark_id = "'.$spark_id.'"';
    if ($result = $mysqli->query($query)) {
      if ($result->num_rows === 0) {
        // If spark_id is not found, add it to the table.
        $query = 'INSERT INTO spark_locate VALUES ("'.$spark_id.'", '.
          '"", "", 0, 0, 0)';
        if (!$mysqli->query($query)) {
          $this->Log('Spark->Locate 1: '.$mysqli->error);
        }
      }
      else if ($spark_locate = $result->fetch_assoc()) {
        if ($spark_locate['authorised'] === '1') {
          // Output the string that is read by the spark, note that a final
          // ampersand is used to denote the end of the data.
          echo 'action=initialisation&ip='.$spark_locate['ip'].
            '&url='.$spark_locate['url'].
            '&id='.$spark_locate['feed_id'].
            '&key='.$spark_locate['api_key'].'&';
        }
      }
      $result->close();
    }
    else {
      $this->Log('Spark->Locate 2: '.$mysqli->error);
    }
    $mysqli->close();
  }

  public function Unauthorised() {
    // unauthorised is set after the spark has tried to communicate with a
    // server it was given an initialisation string for, but the credentials
    // were wrong. It then re-connects with the original server to notify.
    $mysqli = connect_db();
    $spark_id = $mysqli->escape_string($_POST['unauthorised']);
    // First check that an entry exists, so that a notification is only
    // generated if authorised status changes.
    $authorised = '';
    $query = 'SELECT authorised FROM spark_locate WHERE '.
      'spark_id = "'.$spark_id.'"';
    if ($result = $mysqli->query($query)) {
      if ($spark_locate = $result->fetch_assoc()) {
        $authorised = $spark_locate['authorised'];
      }
      $result->close();
    }
    else {
      $this->Log('Spark->Unauthorised 1: '.$mysqli->error);
    }
    if ($authorised === '1') {
      $query = 'UPDATE spark_locate SET authorised = 0 WHERE '.
        'spark_id = "'.$spark_id.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Spark->Unauthorised 2: '.$mysqli->error);
      }
      $this->Notification('spark', 'Spark', $spark_id.' not authorised.',
                          'system');
    }
    $mysqli->close();
  }

  // Private functions below here ////////////////////////////////////////////

  private function EditName() {
    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    // Remove the '#dobrado-' prefix.
    $id = (int)substr($_POST['id'], 9);
    $spark_id = $mysqli->escape_string($_POST['sparkID']);
    $query = 'UPDATE spark SET name = "'.$name.'" WHERE '.
      'user = "'.$this->owner.'" AND box_id = '.$id.' AND '.
      'spark_id = "'.$spark_id.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->EditName: '.$mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function ListConnections($group) {
    $mysqli = connect_db();
    $connections = [];
    $query = 'SELECT spark.spark_id, name, ip FROM spark LEFT JOIN '.
      'spark_share ON spark.user = spark_share.user AND '.
      'spark.box_id = spark_share.box_id AND '.
      'spark.spark_id = spark_share.spark_id WHERE system_group = "'.$group.'"';
    if ($result = $mysqli->query($query)) {
      while ($spark = $result->fetch_assoc()) {
        $connections[] = ['id' => $spark['spark_id'], 'name' => $spark['name'],
                          'ip' => $spark['ip']];
      }
      $result->close();
    }
    else {
      $this->Log('Spark->ListConnections: '.$mysqli->error);
    }
    $mysqli->close();
    return $connections;
  }

  private function Locations() {
    $mysqli = connect_db();
    $locations = [];
    $query = 'SELECT spark_id, ip, url, feed_id, api_key, authorised FROM '.
      'spark_locate';
    if ($result = $mysqli->query($query)) {
      while ($spark_locate = $result->fetch_assoc()) {
        $locations[] = $spark_locate;
      }
      $result->close();
    }
    else {
      $this->Log('Spark->Locations: '.$mysqli->error);
    }
    $mysqli->close();
    return $locations;
  }

  private function RemoveGroup() {
    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    // Remove the '#dobrado-' prefix.
    $id = (int)substr($_POST['id'], 9);
    $spark_id = $mysqli->escape_string($_POST['sparkID']);
    $query = 'DELETE FROM spark_share WHERE user = "'.$this->owner.'" AND '.
      'box_id = '.$id.' AND spark_id = "'.$spark_id.'" AND '.
      'system_group = "'.$name.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->RemoveGroup: '.$mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function RemoveSpark() {
    $mysqli = connect_db();
    // Remove the '#dobrado-' prefix.
    $id = (int)substr($_POST['id'], 9);
    $spark_id = $mysqli->escape_string($_POST['sparkID']);
    $query = 'DELETE FROM spark WHERE user = "'.$this->owner.'" AND '.
      'box_id = '.$id.' AND spark_id = "'.$spark_id.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->RemoveSpark 1: '.$mysqli->error);
    }
    $query = 'DELETE FROM spark_share WHERE user = "'.$this->owner.'" AND '.
      'box_id = '.$id.' AND spark_id = "'.$spark_id.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->RemoveSpark 2: '.$mysqli->error);
    }
    $query = 'DELETE FROM spark_wifi WHERE user = "'.$this->owner.'" AND '.
      'box_id = '.$id.' AND spark_id = "'.$spark_id.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->RemoveSpark 3: '.$mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function RemoveWiFi() {
    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    // Remove the '#dobrado-' prefix.
    $id = (int)substr($_POST['id'], 9);
    $spark_id = $mysqli->escape_string($_POST['sparkID']);
    $query = 'DELETE FROM spark_wifi WHERE user = "'.$this->owner.'" AND '.
      'box_id = '.$id.' AND spark_id = "'.$spark_id.'" AND ssid = "'.$name.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->RemoveWiFi: '.$mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function SaveGroup() {
    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    // Remove the '#dobrado-' prefix.
    $id = (int)substr($_POST['id'], 9);
    $spark_id = $mysqli->escape_string($_POST['sparkID']);
    $query = 'INSERT INTO spark_share VALUES ("'.$this->owner.'", '.
      $id.', "'.$spark_id.'", "'.$name.'")';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->SaveGroup: '.$mysqli->error);
    }
    $mysqli->close();
    $content = '<div class="spark-group">'.
        '<button class="spark-group-remove">remove</button>'.
        '<span class="spark-group-name">'.$name.
      '</span></div>';
    return ['content' => $content];
  }

  private function SaveAddress() {
    $mysqli = connect_db();
    $id = 0;
    $feed_id = (int)$mysqli->escape_string($_POST['id']);
    $api_key = (int)$mysqli->escape_string($_POST['key']);
    $query = 'SELECT box_id FROM api WHERE feed_id = '.$feed_id.
      ' AND api_key = "'.$api_key.'"';
    if ($result = $mysqli->query($query)) {
      if ($api = $result->fetch_assoc()) {
        $id = (int)$api['box_id'];
      }
      $result->close();
    }
    else {
      $this->Log('Spark->SaveAddress 1: '.$mysqli->error);
    }
    // Save the current local address provided by this spark.
    $spark_id = $mysqli->escape_string($_POST['spark']);
    $ip = $mysqli->escape_string($_POST['ip']);
    // Give the spark a name with an incremented suffix if it doesn't exist.
    $name = 'scales';
    $query = 'SELECT spark_id FROM spark WHERE user = "'.$this->owner.'" '.
      'AND box_id = '.$id;
    if ($result = $mysqli->query($query)) {
      if ($result->num_rows > 0) {
        $name .= ' '.($result->num_rows + 1);
      }
      $result->close();
    }
    else {
      $this->Log('Spark->SaveAddress 2: '.$mysqli->error);
    }
    $query = 'INSERT INTO spark VALUES ("'.$this->owner.'", '.$id.', '.
      '"'.$spark_id.'", "'.$name.'", "'.$ip.'", 0) '.
      'ON DUPLICATE KEY UPDATE ip = "'.$ip.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->SaveAddress 3: '.$mysqli->error);
    }
    // If 'updated' flag is set need to echo current wifi credentials.
    $updated = false;
    $query = 'SELECT updated FROM spark WHERE user = "'.$this->owner.'" '.
      'AND box_id = '.$id.' AND spark_id = "'.$spark_id.'"';
    if ($result = $mysqli->query($query)) {
      if ($spark = $result->fetch_assoc()) {
        $updated = $spark['updated'] === '1';
      }
      $result->close();
    }
    else {
      $this->Log('Spark->SaveAddress 4: '.$mysqli->error);
    }
    if ($updated) {
      // First unset the updated flag so wifi credentials aren't sent twice.
      $query = 'UPDATE spark SET updated = 0 WHERE user = "'.$this->owner.'" '.
        'AND box_id = '.$id.' AND spark_id = "'.$spark_id.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Spark->SaveAddress 5: '.$mysqli->error);
      }
      $credentials = 'action=credentials';
      $query = 'SELECT ssid, password FROM spark_wifi WHERE '.
        'user = "'.$this->owner.'" AND box_id = '.$id.' AND '.
        'spark_id = "'.$spark_id.'"';
      if ($result = $mysqli->query($query)) {
        while ($spark_wifi = $result->fetch_assoc()) {
          $credentials .= '&ssid='.urlencode($spark_wifi['ssid']).
            '&password='.urlencode($spark_wifi['password']);
        }
        $result->close();
      }
      else {
        $this->Log('Spark->SaveAddress 6: '.$mysqli->error);
      }
      // Output the string that is read by the spark.
      // An ampersand is added to denote the end of the data.
      echo $credentials.'&';
      // Notify other modules that credentials were sent.
      $this->Notify($id, 'spark', 'spark', '', 0, true);
    }
    $mysqli->close();
  }

  private function SaveWiFi() {
    $mysqli = connect_db();
    $name = $mysqli->escape_string($_POST['name']);
    $password = $mysqli->escape_string($_POST['password']);
    // Remove the '#dobrado-' prefix.
    $id = (int)substr($_POST['id'], 9);
    $spark_id = $mysqli->escape_string($_POST['sparkID']);
    // Can only save 7 access points per spark.
    $total = '';
    $query = 'SELECT COUNT(ssid) AS total FROM spark_wifi WHERE '.
      'user = "'.$this->owner.'" AND box_id = '.$id.' AND '.
      'spark_id = "'.$spark_id.'"';
    if ($result = $mysqli->query($query)) {
      if ($spark_wifi = $result->fetch_assoc()) {
        $total = $spark_wifi['total'];
      }
      $result->close();
    }
    else {
      $this->Log('Spark->SaveWiFi 1: '.$mysqli->error);
    }
    if ($total === '' || $total === '7') {
      $mysqli->close();
      // Return an error when the maximum is reached or if there was a
      // problem reading the total.
      return ['error' => 'Maximum access points reached.'];
    }

    $query = 'INSERT INTO spark_wifi VALUES ("'.$this->owner.'", '.
      $id.', "'.$spark_id.'", "'.$name.'", "'.$password.'") '.
      'ON DUPLICATE KEY UPDATE password = "'.$password.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->SaveWiFi 2: '.$mysqli->error);
    }
    $mysqli->close();
    $content = '<div class="spark-wifi">'.
        '<button class="spark-wifi-remove">remove</button>'.
        '<span class="spark-wifi-name">'.$name.
      '</span></div>';
    return ['content' => $content];
  }

  private function Share($id, $spark_id) {
    $mysqli = connect_db();
    $content = '<p><b>Available to groups:</b><br>';
    $query = 'SELECT system_group FROM spark_share WHERE '.
      'user = "'.$this->owner.'" AND box_id = '.$id.' AND '.
      'spark_id = "'.$spark_id.'"';
    if ($result = $mysqli->query($query)) {
      while ($spark_share = $result->fetch_assoc()) {
        $content .= '<div class="spark-group">'.
            '<button class="spark-group-remove">remove</button>'.
            '<span class="spark-group-name">'.$spark_share['system_group'].
          '</span></div>';
      }
      $result->close();
    }
    else {
      $this->Log('Spark->Share: '.$mysqli->error);
    }
    $mysqli->close();
    $content .= '<div class="spark-group-details hidden">'.
        '<label for="spark-group-new">Group:</label>'.
        '<input id="spark-group-new">'.
        '<button class="spark-group-save">save</button>'.
      '</div>'.
      '<a href="#" class="spark-group-add">add group</a></p>';
    return $content;
  }

  private function UpdateSpark() {
    $mysqli = connect_db();
    // Remove the '#dobrado-' prefix.
    $id = (int)substr($_POST['id'], 9);
    $spark_id = $mysqli->escape_string($_POST['sparkID']);
    $query = 'UPDATE spark SET updated = 1 WHERE user = "'.$this->owner.'" '.
      'AND box_id = '.$id.' AND spark_id = "'.$spark_id.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Spark->UpdateSpark: '.$mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function WiFi($id, $spark_id) {
    $mysqli = connect_db();
    $updated = false;
    // Check if this spark is flagged as requiring a WiFi update.
    $query = 'SELECT updated FROM spark WHERE user = "'.$this->owner.'" '.
      'AND box_id = '.$id.' AND spark_id = "'.$spark_id.'"';
    if ($result = $mysqli->query($query)) {
      if ($spark = $result->fetch_assoc()) {
        $updated = $spark['updated'] === '1';
      }
      $result->close();
    }
    else {
      $this->Log('Spark->WiFi 1: '.$mysqli->error);
    }
    $content = '<p><b>Available on Wireless Access Points:</b>';
    if ($updated) {
      $content .= '<button class="spark-wifi-update">'.
        'updating next restart</button><br>';
    }
    else {
      $content .= '<button class="spark-wifi-update">update spark</button><br>';
    }

    $query = 'SELECT ssid FROM spark_wifi WHERE user = "'.$this->owner.'" '.
      'AND box_id = '.$id.' AND spark_id = "'.$spark_id.'"';
    if ($result = $mysqli->query($query)) {
      while ($spark_share = $result->fetch_assoc()) {
        $content .= '<div class="spark-wifi">'.
            '<button class="spark-wifi-remove">remove</button>'.
            '<span class="spark-wifi-name">'.$spark_share['ssid'].
          '</span></div>';
      }
      $result->close();
    }
    else {
      $this->Log('Spark->WiFi 2: '.$mysqli->error);
    }
    $mysqli->close();

    $content .= '<div class="spark-wifi-details hidden">'.
        'New Wireless Access Point details:<br>'.
        '<label for="spark-wifi-new">Name:</label>'.
        '<input id="spark-wifi-new">'.
        '<label for="spark-wifi-password">Password:</label>'.
        '<input id="spark-wifi-password">'.
        '<button class="spark-wifi-save">save</button>'.
      '</div>'.
      '<a href="#" class="spark-wifi-add">add location</a></p>';
    return $content;
  }

}
