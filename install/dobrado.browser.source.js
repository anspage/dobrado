/*global dobrado: true, CKEDITOR: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2018 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.browser) {
  dobrado.browser = {};
}
(function() {

  'use strict';

  // The newModuleCallback sets this callback value when another module has
  // opened the file browser. It is a numeric value when set by ckeditor,
  // or the name of a module otherwise.
  var callback = false;

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.browser').length === 0) {
      return;
    }

    $('.browser').dialog({
      show: true,
      width: 720,
      height: 500,
      position: { my: 'top', at: 'top+50', of: window },
      title: 'Browser',
      create: dobrado.fixedDialog,
      close: function() { $('.browser').remove(); }
    });
    $('#browser-upload-input').val('').change(upload);
    $('#browser-upload').button({ disabled: true }).click(upload);
    $('#browser-upload-progress').progressbar();
    $('.thumbnail').show().click(highlight);
    $('.thumbnail .select').button({
      icon: 'ui-icon-plus', showLabel: false }).click(select);
    $('.thumbnail .rotate-left').button({
      icon: 'ui-icon-arrowrefresh-1-w',
      showLabel: false }).click(rotateLeft);
    $('.thumbnail .rotate-right').button({
      icon: 'ui-icon-arrowrefresh-1-e',
      showLabel: false }).click(rotateRight);
    $('.thumbnail .remove').button({
      icon: 'ui-icon-trash', showLabel: false }).click(remove);
    var filenames = [];
    $('.browser .filename').each(function() {
      filenames.push($(this).text());
    });
    $('#browser-search').autocomplete({ minLength: 1,
                                        search: dobrado.fixAutoCompleteMemoryLeak,
                                        source: filenames,
                                        response: filter,
                                        close: filterReset });
  });

  function remove() {
    var that = this;
    $.post('/php/request.php',
           { request: 'browser', action: 'remove',
             file: $(this).siblings('.filename').html(),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'browser remove')) {
          return;
        }
        $(that).parent().remove();
      });
  }

  function rotateLeft() {
    var that = this;
    $.post('/php/request.php',
           { request: 'browser', action: 'rotate', direction: 'left',
             file: $(this).siblings('.filename').html(),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'browser rotateLeft')) {
          return;
        }
        var image = JSON.parse(response);
        $(that).siblings('img').attr('src', image.src);
        $(that).siblings('.filename').html(image.filename);
      });
  }

  function rotateRight() {
    var that = this;
    $.post('/php/request.php',
           { request: 'browser', action: 'rotate', direction: 'right',
             file: $(this).siblings('.filename').html(),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'browser rotateRight')) {
          return;
        }
        var image = JSON.parse(response);
        $(that).siblings('img').attr('src', image.src);
        $(that).siblings('.filename').html(image.filename);
      });
  }

  function select() {
    // ckeditor uses numeric callback values.
    if (/^[0-9]+$/.test(callback)) {
      CKEDITOR.tools.callFunction(callback,
                                  $(this).siblings('.filename').html());
    }
    else if (callback && dobrado[callback]) {
      dobrado[callback].select($(this).siblings('.filename').html());
    }
    callback = false;
    $('.browser').dialog('close');
  }

  function highlight() {
    // Hide options for other thumbnails.
    $('.thumbnail .select').hide();
    $('.thumbnail .rotate-left').hide();
    $('.thumbnail .rotate-right').hide();
    $('.thumbnail .remove').hide();
    $('.thumbnail').removeClass('highlight');
    $(this).addClass('highlight');
    $(this).children('.select').show();
    $(this).children('.rotate-left').show();
    $(this).children('.rotate-right').show();
    $(this).children('.remove').show();
  }

  function filter(e, ui) {
    var filenames = [];
    $.each(ui.content, function(i, data) {
      filenames.push(data.value);
    });
    $('.browser .filename').parent().show();
    $('.browser .filename').each(function() {
      if ($.inArray($(this).text(), filenames) == -1) {
        $(this).parent().hide();
      }
    });
  }

  function filterReset(e, ui) {
    if ($('#browser-search').val() === '') {
      $('.browser .filename').parent().show();
    }
  }

  function upload() {
    var formData = new FormData();
    if (!formData) {
      dobrado.log('Your browser doesn\'t support file uploading.', 'error');
      return;
    }
    if ($('#browser-upload-input').val() === '') {
      $('#browser-upload').button({ disabled: true });
      return;
    }

    $('#browser-upload').button({ disabled: false });
    let fileList = $('#browser-upload-input').get(0).files;
    for (let i = 0; i < fileList.length; i++) {
      formData.append('file[]', fileList[i]);
    }
    formData.append('request', 'browser');
    formData.append('action', 'upload');
    formData.append('url', location.href);
    formData.append('token', dobrado.token);
    $.ajax({
      url: '/php/request.php',
      data: formData,
      contentType: false,
      processData: false,
      type: 'POST',
      xhr: function() {
        var xhr = new XMLHttpRequest();
        xhr.upload.addEventListener('progress', function(e) {
          if (e.lengthComputable) {
            var percent = e.loaded / e.total * 100;
            if (percent < 100) {
              $('#browser-upload-progress').show();
              $('#browser-upload-progress').progressbar('value', percent);
            }
          }
        });
        return xhr;
      },
      success: function(response) {
        if (dobrado.checkResponseError(response, 'browser upload')) {
          return;
        }
        var browser = JSON.parse(response);
        $('#browser-search').parent().after(browser.content);
        $('.thumbnail').click(highlight);
        $('.thumbnail .select').button({
          icon: 'ui-icon-plus',
          showLabel: false
        }).click(select);
        $('.thumbnail .rotate-left').button({
          icon: 'ui-icon-arrowrefresh-1-w',
          showLabel: false
        }).click(rotateLeft);
        $('.thumbnail .rotate-right').button({
          icon: 'ui-icon-arrowrefresh-1-e',
          showLabel: false
        }).click(rotateRight);
        $('.thumbnail .remove').button({
          icon: 'ui-icon-trash',
          showLabel: false
        }).click(remove);
        $('#browser-upload-input').val('');
        $('#browser-upload').button({ disabled: true });
        $('#browser-upload-progress').hide();
      }
    });
    return false;
  }

  dobrado.browser.newModuleCallback = function(selector, context) {
    callback = context;
    // ckeditor uses zIndex stacking (with a value of 10010), so have to set
    // zIndex on the browser dialog. Can check if the browser was opened by
    // ckeditor because it uses numeric callback values.
    if (/^[0-9]+$/.test(callback)) {
      // There also appears to be a race condition when opening the browser,
      // this callback needs to run last.
      setTimeout(function() {
        $(selector).parents('.ui-dialog').css('z-index', 11000);
      }, 100);
    }
  };

}());
