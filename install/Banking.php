<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Banking extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canEditPage) {
      return ['error' => 'Permission denied editing bank details.'];
    }
    $mysqli = connect_db();
    $name = $mysqli->escape_string(htmlspecialchars($_POST['name']));
    $number = $mysqli->escape_string(htmlspecialchars($_POST['number']));
    $bsb = $mysqli->escape_string(htmlspecialchars($_POST['bsb']));
    $credit = (int)$_POST['credit'];
    $query = 'UPDATE banking SET name = "' . $name . '", ' .
      'number = "' . $number . '", bsb = "' . $bsb . '", credit = ' . $credit .
      ' WHERE user = "' . $this->user->name . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Banking->Callback: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  public function CanAdd($page) {
    // Can only have one banking module on a page.
    return !$this->AlreadyOnPage('banking', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    if (!$this->user->canEditPage) return '';

    $bsb = '';
    $credit = '';
    $name = '';
    $number = '';
    $reference = '';
    $mysqli = connect_db();
    $query = 'SELECT bsb, credit, name, number, reference ' .
      'FROM banking WHERE user = "' . $this->owner . '"';
    if ($result = $mysqli->query($query)) {
      if ($banking = $result->fetch_assoc()) {
        $bsb = $banking['bsb'];
        $credit = (int)$banking['credit'];
        $name = $banking['name'];
        $number = $banking['number'];
        $reference = $banking['reference'];
      }
      $result->close();
    }
    else {
      $this->Log('Banking->Content: ' . $mysqli->error);
    }
    $mysqli->close();

    $content = '<div class="reference">Please use: <b>' . $reference .
      '</b> as the reference when paying your account.</div>';
    if ($this->Substitute('banking-show-details') !== 'true') {
      return $content;
    }

    $checked = $credit === 0 ? '' : ' checked';
    return $content .
      '<div class="edit"><a href="#">Edit your bank details</a></div>' .
      '<form id="banking-form" class="hidden">' .
        '<div class="info"><b>Please note:</b> You only need to fill in ' .
          'your bank details if you are selling to the group, and would ' .
          'like to be paid to your bank account, rather than receive ' .
          'credit for your sold goods.</div>' .
        '<div class="form-spacing">' .
          '<label for="banking-name-input">Account Name: </label>' .
          '<input id="banking-name-input" name="name" value="' . $name . '" ' .
            'type="text" maxlength="100">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="banking-number-input">Account Number: </label>' .
          '<input id="banking-number-input" name="number" ' .
            'value="' . $number . '" type="text" maxlength="20">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="banking-bsb-input">BSB: </label>' .
          '<input id="banking-bsb-input" name="bsb" value="' . $bsb . '" ' .
            'type="text" maxlength="7">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="banking-credit-input">Receive Credit: </label>' .
          '<input type="checkbox" id="banking-credit-input" name="credit"' .
            $checked . '>' .
        '</div>' .
        '<button class="submit">Submit</button>' .
      '</form>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    // Copy is called when a new user is created (assuming a Banking module
    // has been added to the group's default page). A new user needs a unique
    // banking reference created for them, which is done here.

    // References are created here based on the new username, with the last
    // character being chosen to make sure it's unique. References have a
    // maximum length of six characters here, as that is the limit for some
    // banks. So the only possibility of a duplicate reference is if two
    // usernames use the same first six characters, so that's what is checked
    // for here.
    $reference = $this->user->name;
    $mysqli = connect_db();
    if (strlen($this->user->name) > 6) {
      $start = substr($this->user->name, 0, 6);
      $reference = $start;
      $query = 'SELECT user FROM users WHERE user LIKE "' . $start . '%"';
      if ($result = $mysqli->query($query)) {
        // The first user with this start value get these 6 characters as a
        // reference, subsequent users get an incremental suffix.
        if ($result->num_rows > 1 && $result->num_rows < 10) {
          $reference = substr($this->user->name, 0, 5) . $result->num_rows;
        }
        $result->close();
      }
      else {
        $this->Log('Banking->Copy 1: ' . $mysqli->error);
      }
    }
    // References can also be modified, so need to check if the reference that
    // was just created for the new user is already being used by someone else.
    $count = 1;
    while (true) {
      $query = 'SELECT reference FROM banking WHERE ' .
        'reference = "' . $reference . '"';
      if ($result = $mysqli->query($query)) {
        if ($result->num_rows === 0) {
          $result->close();
          break;
        }
        else {
          $result->close();
          // Need to create a new reference, use the first 4 characters of the
          // old one, add a suffix and check it again.
          $reference = substr($reference, 0, 4) . $count;
          $count++;
        }
      }
      else {
        $this->Log('Banking->Copy 2: ' . $mysqli->error);
        break;
      }
    }
    $default_query =
      isset($_SESSION['new-supplier']) && $_SESSION['new-supplier'] ?
      '0, 0, 0, 0, ' : '1, 1, 1, 0, ';
    $group = $mysqli->escape_string($this->Substitute('banking-default-group'));
    $query = 'INSERT INTO banking VALUES ("' . $this->user->name . '", ' .
      '"' . $reference . '", "", "", "", ' . $default_query .
      '"' . $group . '") ON DUPLICATE KEY UPDATE reference = "' .$reference.'"';
    if (!$mysqli->query($query)) {
      $this->Log('Banking->Copy 3: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    // This is used by functions/new_user.php
    if ($fn === 'Settings') {
      return $this->Settings($p);
    }
    if ($fn === 'BuyerGroup') {
      return $this->BuyerGroup();
    }
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.banking.js to the existing dobrado.js file.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.banking.js', false);
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS banking (' .
      'user VARCHAR(50) NOT NULL,' .
      'reference VARCHAR(20) NOT NULL,' .
      'name VARCHAR(100),' .
      'number VARCHAR(20),' .
      'bsb VARCHAR(7),' .
      'credit TINYINT(1),' .
      'surcharge TINYINT(1),' .
      'deposit TINYINT(1),' .
      'next_week INT(10) UNSIGNED NOT NULL,' .
      'buyer_group ENUM("", "wholesale", "retail"),' .
      'PRIMARY KEY(user)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Banking->Install 1: ' . $mysqli->error);
    }
    // Add an entry for the admin user.
    $query = 'INSERT INTO banking VALUES ("' . $this->user->name . '", ' .
      '"' . $this->user->name . '", "", "", "", 1, 1, 1, 0, "")';
    if (!$mysqli->query($query)) {
      $this->Log('Banking->Install 2: ' . $mysqli->error);
    }
    $mysqli->close();

    $description = ['banking-default-group' => 'Either \'retail\', ' .
                      '\'wholesale\' or empty to set which stock price ' .
                      'should be used for new members.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"",".banking .edit a","color","#222222"',
                   '"",".banking .edit a","font-size","0.8em"',
                   '"",".banking .edit a","text-decoration","none"',
                   '"",".banking .edit a:hover","color","#aaaaaa"',
                   '"","#banking-form .info","margin","20px 0"',
                   '"","#banking-form label","width","8em"',
                   '"","#banking-form .submit","margin-left","8.4em"'];
    $this->AddSiteStyle($site_style);
    return $this->Dependencies(['invite']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    // Return if Remove was called for a specific module,
    // only want to remove bank details when deleting an account.
    if (isset($id)) return;

    $mysqli = connect_db();
    $query = 'DELETE FROM banking WHERE user = "' . $this->owner . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Banking->Remove: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // This is called when the version of the module is updated,
    // to provide a way to update or modify tables etc..

  }

  public function UpdateScript($path) {
    // Append dobrado.banking.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.banking.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  public function AllBuyers($organisation = false,
                            $active_only = true, $invited = true) {
    $user_list = [];
    $buyer_groups = [];
    $invite = new Invite($this->user, $this->owner);
    $joined = $invite->Joined();

    $query = '';
    $active_query = $active_only ? 'active = 1 AND ' : '';
    // Only want to return the members that have joined a multi-group buy when
    // invited is true, otherwise this function couldn't return the actual
    // members of the host group.
    if ($invited && count($joined) !== 0) {
      $group_query = '';
      foreach ($joined as $group) {
        if ($group_query !== '') $group_query .= ' OR ';
        $group_query .= 'users.system_group = "' . $group . '"';
      }
      // Add the host group too.
      $group_query .= ' OR users.system_group = "' . $this->user->group . '"';
      $query = 'SELECT banking.user, buyer_group FROM banking LEFT JOIN ' .
        'users ON banking.user = users.user WHERE ' . $active_query .
        '(' . $group_query . ')';
    }
    else if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      $query = 'SELECT banking.user, buyer_group FROM banking LEFT JOIN ' .
        'users ON banking.user = users.user WHERE ' . $active_query .
        $organiser->GroupQuery();
    }
    else {
      $query = 'SELECT banking.user, buyer_group FROM banking LEFT JOIN ' .
        'users ON banking.user = users.user WHERE ' . $active_query .
        'users.system_group = "' . $this->user->group . '"';
    }
    $mysqli = connect_db();
    if ($result = $mysqli->query($query)) {
      while ($banking = $result->fetch_assoc()) {
        $user = $banking['user'];
        $group = $banking['buyer_group'];
        $user_list[] = $user;
        // If the buyer_group for a user is empty, the default used is 'price'.
        $buyer_groups[$user] = $group === '' ? 'price' : $group;
      }
      $result->close();
    }
    else {
      $this->Log('Banking->AllBuyers: ' . $mysqli->error);
    }
    $mysqli->close();
    return [$user_list, $buyer_groups];
  }

  public function AllSettings($organisation = true) {
    $settings = [];
    $query = '';
    if ($organisation) {
      $organiser = new Organiser($this->user, $this->owner);
      $query = 'SELECT banking.user, reference, name, number, bsb, credit, ' .
        'surcharge, deposit, next_week, buyer_group, active FROM banking ' .
        'LEFT JOIN users ON banking.user = users.user WHERE ' .
        $organiser->GroupQuery();
    }
    else {
      $query = 'SELECT banking.user, reference, name, number, bsb, credit, ' .
        'surcharge, deposit, next_week, buyer_group, active FROM banking ' .
        'LEFT JOIN users ON banking.user = users.user WHERE ' .
        'users.system_group = "' . $this->user->group . '"';
    }
    $mysqli = connect_db();
    if ($result = $mysqli->query($query)) {
      while ($banking = $result->fetch_assoc()) {
        $settings[$banking['user']] =
          ['reference' => $banking['reference'],
           'name' => $banking['name'],
           'number' => $banking['number'],
           'bsb' => $banking['bsb'],
           'credit' => (int)$banking['credit'],
           'surcharge' => (int)$banking['surcharge'],
           'deposit' => (int)$banking['deposit'],
           'nextWeek' => $banking['next_week'],
           'buyerGroup' => $banking['buyer_group'],
           'active' => (int)$banking['active']];
      }
      $result->close();
    }
    else {
      $this->Log('Banking->AllSettings: ' . $mysqli->error);
    }
    $mysqli->close();
    return $settings;
  }

  public function BuyerGroup($user = '') {
    if ($user === '') {
      $user = $this->user->name;
    }
    $buyer_group = '';
    $mysqli = connect_db();
    $query = 'SELECT buyer_group FROM banking WHERE user = "' . $user . '"';
    if ($result = $mysqli->query($query)) {
      if ($banking = $result->fetch_assoc()) {
        $buyer_group = $banking['buyer_group'];
      }
      $result->close();
    }
    else {
      $this->Log('Banking->BuyerGroup: ' . $mysqli->error);
    }
    $mysqli->close();
    return $buyer_group === '' ? 'price' : $buyer_group;
  }

  public function UpdateField($user, $field, $value) {
    if ($field === 'reference' && !$this->UniqueReference($user, $value)) {
      return ['error' => 'Bank reference already exists'];
    }

    $mysqli = connect_db();
    $query = 'UPDATE banking SET ' . $field . ' = "' . $value . '" WHERE ' .
      'user = "' . $user . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Banking->UpdateField: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  public function UpdateUser($user, $reference, $name, $number, $bsb,
                             $credit, $surcharge, $deposit, $buyer_group) {
    if (!$this->UniqueReference($user, $reference)) {
      return ['error' => 'Bank reference already exists'];
    }

    $mysqli = connect_db();
    $query = 'UPDATE banking SET reference = "' . $reference . '", ' .
      'name = "' . $name . '", number = "' . $number . '", ' .
      'bsb = "' . $bsb . '", credit = ' . $credit . ', ' .
      'surcharge = ' . $surcharge . ', deposit = ' . $deposit . ', ' .
      'buyer_group = "' . $buyer_group . '" WHERE user = "' . $user . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Banking->UpdateUser: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  public function SaveNextWeek($us_next_week) {
    $co_op_day = $this->Substitute('co-op-day');
    // If co-op-day is not a single weekday, SaveNextWeek is not used.
    $weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
                 'Friday', 'Saturday'];
    if (!in_array($co_op_day, $weekdays)) return;

    // Use the configured day of the week for the co-op to find the next time
    // the co-op will be held.
    $timestamp = strtotime('next ' . $co_op_day);

    $mysqli = connect_db();
    $user_query = '';
    for ($i = 0; $i < count($us_next_week); $i++) {
      if ($user_query !== '') $user_query .= ' OR ';
      $user_query .= 'user = "' . $mysqli->escape_string($us_next_week[$i]).'"';
    }
    if ($user_query !== '') {
      $query = 'UPDATE banking SET next_week = ' . $timestamp . ' WHERE ' .
        $user_query;
      if (!$mysqli->query($query)) {
        $this->Log('Banking->SaveNextWeek: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function CountNextWeek() {
    $co_op_day = $this->Substitute('co-op-day');
    // If co-op-day is not a single weekday, CountNextWeek is not used.
    $weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
                 'Friday', 'Saturday'];
    if (!in_array($co_op_day, $weekdays)) return [0, time()];

    $timestamp = strtotime('this ' . $co_op_day);
    // Want to make sure today is not set as the value if today is co-op day.
    if ($timestamp < strtotime('24 hours')) {
      $timestamp = strtotime('next ' . $co_op_day);
    }
    $count = 0;
    $mysqli = connect_db();
    $query = 'SELECT banking.user FROM banking LEFT JOIN users ON ' .
      'banking.user = users.user WHERE ' .
      'users.system_group = "' . $this->user->group . '" AND ' .
      'next_week = ' . $timestamp;
    if ($result = $mysqli->query($query)) {
      $count = $result->num_rows;
    }
    else {
      $this->Log('Banking->CountNextWeek: ' . $mysqli->error);
    }
    $mysqli->close();
    return [$count, $timestamp];
  }

  public function Settings($user) {
    $banking = [];
    $mysqli = connect_db();
    $query = 'SELECT reference, next_week FROM banking WHERE ' .
      'user = "' . $user . '"';
    if ($result = $mysqli->query($query)) {
      $banking = $result->fetch_assoc();
      $result->close();
    }
    else {
      $this->Log('Banking->Settings: ' . $mysqli->error);
    }
    $mysqli->close();
    return $banking;
  }

  // Private functions below here ////////////////////////////////////////////

  private function UniqueReference($user, $reference) {
    $count = -1;
    $organiser = new Organiser($this->user, $this->owner);

    $mysqli = connect_db();
    // The banking reference needs to be unique so check if it already exists.
    $query = 'SELECT reference FROM banking LEFT JOIN users ON ' .
      'banking.user = users.user WHERE reference = "' . $reference . '" ' .
      'AND banking.user != "' . $user . '" AND ' . $organiser->GroupQuery();
    if ($result = $mysqli->query($query)) {
      $count = $result->num_rows;
    }
    else {
      $this->Log('Banking->UniqueReference: ' . $mysqli->error);
    }
    $mysqli->close();
    return $count === 0;
  }

}
