/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2019 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.groupwizard) {
  dobrado.groupwizard = {};
}
(function() {

  'use strict';

  // Sections are shown one at a time, so track the current one.
  var section = 0;
  var total = 0;

  $(function() {
    total = $('.groupwizard-content > div').length - 1;
    // Only create a dialog if the groupwizard hasn't been permanently added
    // to the page, in which case it's parent div has class 'middle'.
    if (!$('.groupwizard').parent().hasClass('middle')) {
      $('.groupwizard').dialog({
        show: true,
        width: 700,
        height: 450,
        position: { my: 'top', at: 'top+50', of: window },
        create: dobrado.fixedDialog,
        close: function() { $('.groupwizard').remove(); }
      });
    }
    init();
  });

  function init() {
    if ($('.groupwizard').parent().hasClass('middle')) {
      $('.groupwizard').show();
    }
    else {
      var title = 'Group Settings';
      if ($('.groupwizard-selected-group').length === 1) {
        title += ': Updating ' + $('.groupwizard-selected-group').html();
      }
      $('.groupwizard').dialog('option', 'title', title);
    }
    // Set up navigation between sections.
    $('.groupwizard .previous').button({ disabled: true }).click(previous);
    $('.groupwizard .next').button().click(next);
    $('#groupwizard-new-group').button().click(function() {
      $('.groupwizard-new-group-info').show();
      $('.groupwizard-existing-group-info').hide();
    });
    $('#groupwizard-existing-group').button().click(function() {
      if ($('#groupwizard-existing-group-select').length === 0) {
        // If there's no select then only the default group is available,
        // go to the next section as default settings are already provided.
        next();
      }
      else {
        $('.groupwizard-existing-group-info').show();
        $('.groupwizard-new-group-info').hide();
      }
    });
    $('#groupwizard-create-group').button().click(createGroup);
    $('#groupwizard-update-markup').button().click(updateMarkup);
    $('#groupwizard-existing-group-select').change(changeGroup);
    // Some links in the wizard are used to open the preferences dialog.
    $('.groupwizard-purchase-other-order').click(dobrado.account.option);
    $('.groupwizard-purchase-volunteer').click(dobrado.account.option);
    $('.groupwizard-invoice-notifications').click(dobrado.account.option);
    $('.groupwizard-invite').click(dobrado.account.option);
    // Want to know when the dialog is open to show the requested group.
    $('.account').on('dialogopen', showGroup);
    // Need to also update on focus in case it's already open.
    $('.account').on('dialogfocus', showGroup);
    // Some fields switch between datepickers and selects.
    $('#groupwizard-pre-order-open-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-pre-order-final-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-co-op-day-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-invoice-day-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-invoice-remove-orders-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-invoice-send-orders-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-invoice-group-order-input').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-update-order-cycle').datepicker({
      dateFormat: dobrado.dateFormat });
    $('#groupwizard-timezone').autocomplete({
      minLength: 1,
      search: dobrado.fixAutoCompleteMemoryLeak,
      source: timezones() });
    // Save any changes as they are made.
    $('.groupwizard :input').change(save);
    // Some sections have hidden content and should be displayed depending
    // on current settings.
    if ($('#groupwizard-pre-order').is(':checked')) {
      $('.groupwizard-pre-order-settings').show();
    }
    if ($('#groupwizard-surcharge').is(':checked')) {
      $('.groupwizard-payment-settings').show();
    }
    if ($('#groupwizard-stock-order-available').is(':checked')) {
      $('#groupwizard-stock-order-update').parent().show();
    }
    // Some input methods need to be switched depending on their current value.
    switchInput('pre-order-open');
    switchInput('pre-order-final');
    switchInput('co-op-day');
    switchInput('invoice-day');
    switchInput('invoice-remove-orders');
    switchInput('invoice-send-orders');
    switchInput('invoice-group-order');
  }

  function previous() {
    $('.groupwizard-' + section).hide();
    section--;
    $('.groupwizard-' + section).show();
    if (section === 0) {
      $('.groupwizard .previous').button('option', 'disabled', true);
    }
    else {
      $('.groupwizard .previous').button('option', 'disabled', false);
    }
    $('.groupwizard .next').button('option', 'disabled', false);
  }

  function next() {
    $('.groupwizard-' + section).hide();
    section++;
    $('.groupwizard-' + section).show();
    if (section === total) {
      $('.groupwizard .next').button('option', 'disabled', true);
    }
    else {
      $('.groupwizard .next').button('option', 'disabled', false);
    }
    $('.groupwizard .previous').button('option', 'disabled', false);
  }

  function save() {
    var value = '';
    // Remove the 'groupwizard-' prefix from the label.
    var label = $(this).attr('id').substr(12);
    // Toggle input types when date input checkboxes are changed, and clear
    // the current value to force a change event.
    if (label === 'pre-order-open-date') {
      $('#groupwizard-pre-order-open-input').val('').parent().toggle();
      $('#groupwizard-pre-order-open-select').val('').parent().toggle();
      return;
    }
    if (label === 'pre-order-final-date') {
      $('#groupwizard-pre-order-final-input').val('').parent().toggle();
      $('#groupwizard-pre-order-final-select').val('').parent().toggle();
      return;
    }
    if (label === 'co-op-day-date') {
      $('#groupwizard-co-op-day-input').val('').parent().toggle();
      $('#groupwizard-co-op-day-select').val('').parent().toggle();
      return;
    }
    if (label === 'invoice-day-date') {
      $('#groupwizard-invoice-day-input').val('').parent().toggle();
      $('#groupwizard-invoice-day-select').val('').parent().toggle();
      return;
    }
    if (label === 'invoice-send-orders-date') {
      $('#groupwizard-invoice-send-orders-input').val('').parent().toggle();
      $('#groupwizard-invoice-send-orders-select').val('').parent().toggle();
      return;
    }
    if (label === 'invoice-group-order-date') {
      $('#groupwizard-invoice-group-order-input').val('').parent().toggle();
      $('#groupwizard-invoice-group-order-select').val('').parent().toggle();
      return;
    }
    if (label === 'invoice-remove-orders-date') {
      $('#groupwizard-invoice-remove-orders-input').val('').parent().toggle();
      $('#groupwizard-invoice-remove-orders-select').val('').parent().toggle();
      return;
    }
    if (label === 'new-group-input' || label === 'existing-group-select') {
      return;
    }

    // Some labels have a suffix for different input modes.
    if (label === 'pre-order-open-select' || label === 'pre-order-open-input') {
      label = 'pre-order-open';
    }
    else if (label === 'pre-order-final-select' ||
             label === 'pre-order-final-input') {
      label = 'pre-order-final';
    }
    else if (label === 'co-op-day-select' || label === 'co-op-day-input') {
      label = 'co-op-day';
    }
    else if (label === 'invoice-day-select' || label === 'invoice-day-input') {
      label = 'invoice-day';
    }
    else if (label === 'invoice-send-orders-select' ||
             label === 'invoice-send-orders-input') {
      label = 'invoice-send-orders';
    }
    else if (label === 'invoice-group-order-select' ||
             label === 'invoice-group-order-input') {
      label = 'invoice-group-order';
    }
    else if (label === 'invoice-remove-orders-select' ||
             label === 'invoice-remove-orders-input') {
      label = 'invoice-remove-orders';
    }

    if ($(this).is(':checkbox')) {
      value = $(this).is(':checked') ? 'true' : 'false';
    }
    else {
      value = $(this).val();
    }
    // If not showing both availability settings and updates are performed,
    // warn the user that they should only continue if update is finished.
    if (label === 'stock-order-available' && value === 'false' &&
        $('#groupwizard-stock-order-update').val() !== '') {
      if (!confirm('This setting should only be turned off once the last ' +
                   'update has run, to make sure all stock values match. ' +
                   'Updates will also be turned off now, continue?')) {
        $('#groupwizard-stock-order-available').prop('checked', true);
        return;
      }
    }

    dobrado.log('Saving setting.', 'info');
    $.post('/php/request.php',
           { request: 'groupwizard', action: 'saveSetting', label: label,
             value: value, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'groupwizard saveSetting')) {
          return false;
        }
        // Some sections are toggled when a particular setting changes.
        if (label === 'pre-order') {
          $('.groupwizard-pre-order-settings').toggle();
        }
        if (label === 'surcharge') {
          $('.groupwizard-payment-settings').toggle();
        }
        if (label === 'stock-order-available') {
          $('#groupwizard-stock-order-update').parent().toggle();
          // If not showing both availability settings then don't allow updates.
          if (value === 'false' &&
              $('#groupwizard-stock-order-update').val() !== '') {
            $.post('/php/request.php',
                   { request: 'groupwizard', action: 'saveSetting',
                     label: 'stock-order-update', value: '',
                     url: location.href, token: dobrado.token },
              function(response) {
                if (dobrado.checkResponseError(response, 'saveSetting 2')) {
                  return false;
                }
                $('#groupwizard-stock-order-update').val('');
              });
          }
        }
        $('#groupwizard-info').html('All changes saved.');
    });
  }

  function switchInput(label) {
    label = '#groupwizard-' + label;
    if ($(label + '-input').val() !== '') {
      $(label + '-date').prop('checked', true);
      $(label + '-input').parent().show();
      $(label + '-select').parent().hide();
    }
  }

  function showGroup() {
    // Activate the preferences tab.
    $('#account-tabs').tabs('option', 'active', 1);
    // It's a bit hard to know which link was clicked so use the section
    // variable as a guide.
    if (section === 1) {
      $('#group-input').val('admin/invite-notifications');
    }
    if (section === 3) {
      $('#group-input').val('admin/purchase-other-order');
    }
    else if (section === 4) {
      $('#group-input').val('admin/purchase-volunteer');
    }
    else if (section === 6) {
      var group = $('#groupwizard-current-group').val();
      if (group) {
        $('#group-input').val('admin/invoice-notifications-' + group);
      }
      else {
        $('#group-input').val('admin/invoice-notifications');
      }
    }
    $('#account-groups .show-members').click();
  }

  function createGroup() {
    dobrado.log('Creating new group.', 'info');
    $.post('/php/request.php',
           { request: 'groupwizard', action: 'createGroup',
             group: $('#groupwizard-new-group-input').val(),
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'groupwizard createGroup')) {
          return false;
        }
        var groupwizard = JSON.parse(response);
        $('.groupwizard-content').html(groupwizard.content);
        init();
        // Move to the next section once a group has been created.
        next();
      });
  }

  function changeGroup() {
    var group = $(this).val();
    if (group === '') return;

    dobrado.log('Changing group.', 'info');
    $.post('/php/request.php',
           { request: 'groupwizard', action: 'changeGroup',
             group: group, url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'groupwizard changeGroup')) {
          return false;
        }
        var groupwizard = JSON.parse(response);
        $('.groupwizard-content').html(groupwizard.content);
        init();
        // Move to the next section once the group has changed.
        next();
      });
  }

  function updateMarkup() {
    dobrado.log('Updating markup.', 'info');
    $.post('/php/request.php',
           { request: 'groupwizard', action: 'updateMarkup',
             url: location.href, token: dobrado.token },
      function(response) {
        if (dobrado.checkResponseError(response, 'groupwizard updateMarkup')) {
          return false;
        }
        var groupwizard = JSON.parse(response);
        $('#groupwizard-update-markup-info').html(groupwizard.content);
      });
  }

  function timezones() {
    return ['Africa/Abidjan','Africa/Accra','Africa/Addis_Ababa','Africa/Algiers','Africa/Asmara','Africa/Bamako','Africa/Bangui','Africa/Banjul','Africa/Bissau','Africa/Blantyre','Africa/Brazzaville','Africa/Bujumbura','Africa/Cairo','Africa/Casablanca','Africa/Ceuta','Africa/Conakry','Africa/Dakar','Africa/Dar_es_Salaam','Africa/Djibouti','Africa/Douala','Africa/El_Aaiun','Africa/Freetown','Africa/Gaborone','Africa/Harare','Africa/Johannesburg','Africa/Juba','Africa/Kampala','Africa/Khartoum','Africa/Kigali','Africa/Kinshasa','Africa/Lagos','Africa/Libreville','Africa/Lome','Africa/Luanda','Africa/Lubumbashi','Africa/Lusaka','Africa/Malabo','Africa/Maputo','Africa/Maseru','Africa/Mbabane','Africa/Mogadishu','Africa/Monrovia','Africa/Nairobi','Africa/Ndjamena','Africa/Niamey','Africa/Nouakchott','Africa/Ouagadougou','Africa/Porto-Novo','Africa/Sao_Tome','Africa/Tripoli','Africa/Tunis','Africa/Windhoek','America/Adak','America/Anchorage','America/Anguilla','America/Antigua','America/Araguaina','America/Argentina/Buenos_Aires','America/Argentina/Catamarca','America/Argentina/Cordoba','America/Argentina/Jujuy','America/Argentina/La_Rioja','America/Argentina/Mendoza','America/Argentina/Rio_Gallegos','America/Argentina/Salta','America/Argentina/San_Juan','America/Argentina/San_Luis','America/Argentina/Tucuman','America/Argentina/Ushuaia','America/Aruba','America/Asuncion','America/Atikokan','America/Bahia','America/Bahia_Banderas','America/Barbados','America/Belem','America/Belize','America/Blanc-Sablon','America/Boa_Vista','America/Bogota','America/Boise','America/Cambridge_Bay','America/Campo_Grande','America/Cancun','America/Caracas','America/Cayenne','America/Cayman','America/Chicago','America/Chihuahua','America/Costa_Rica','America/Creston','America/Cuiaba','America/Curacao','America/Danmarkshavn','America/Dawson','America/Dawson_Creek','America/Denver','America/Detroit','America/Dominica','America/Edmonton','America/Eirunepe','America/El_Salvador','America/Fortaleza','America/Glace_Bay','America/Godthab','America/Goose_Bay','America/Grand_Turk','America/Grenada','America/Guadeloupe','America/Guatemala','America/Guayaquil','America/Guyana','America/Halifax','America/Havana','America/Hermosillo','America/Indiana/Indianapolis','America/Indiana/Knox','America/Indiana/Marengo','America/Indiana/Petersburg','America/Indiana/Tell_City','America/Indiana/Vevay','America/Indiana/Vincennes','America/Indiana/Winamac','America/Inuvik','America/Iqaluit','America/Jamaica','America/Juneau','America/Kentucky/Louisville','America/Kentucky/Monticello','America/Kralendijk','America/La_Paz','America/Lima','America/Los_Angeles','America/Lower_Princes','America/Maceio','America/Managua','America/Manaus','America/Marigot','America/Martinique','America/Matamoros','America/Mazatlan','America/Menominee','America/Merida','America/Metlakatla','America/Mexico_City','America/Miquelon','America/Moncton','America/Monterrey','America/Montevideo','America/Montserrat','America/Nassau','America/New_York','America/Nipigon','America/Nome','America/Noronha','America/North_Dakota/Beulah','America/North_Dakota/Center','America/North_Dakota/New_Salem','America/Ojinaga','America/Panama','America/Pangnirtung','America/Paramaribo','America/Phoenix','America/Port-au-Prince','America/Port_of_Spain','America/Porto_Velho','America/Puerto_Rico','America/Rainy_River','America/Rankin_Inlet','America/Recife','America/Regina','America/Resolute','America/Rio_Branco','America/Santa_Isabel','America/Santarem','America/Santiago','America/Santo_Domingo','America/Sao_Paulo','America/Scoresbysund','America/Sitka','America/St_Barthelemy','America/St_Johns','America/St_Kitts','America/St_Lucia','America/St_Thomas','America/St_Vincent','America/Swift_Current','America/Tegucigalpa','America/Thule','America/Thunder_Bay','America/Tijuana','America/Toronto','America/Tortola','America/Vancouver','America/Whitehorse','America/Winnipeg','America/Yakutat','America/Yellowknife','Antarctica/Casey','Antarctica/Davis','Antarctica/DumontDUrville','Antarctica/Macquarie','Antarctica/Mawson','Antarctica/McMurdo','Antarctica/Palmer','Antarctica/Rothera','Antarctica/Syowa','Antarctica/Troll','Antarctica/Vostok','Arctic/Longyearbyen','Asia/Aden','Asia/Almaty','Asia/Amman','Asia/Anadyr','Asia/Aqtau','Asia/Aqtobe','Asia/Ashgabat','Asia/Baghdad','Asia/Bahrain','Asia/Baku','Asia/Bangkok','Asia/Beirut','Asia/Bishkek','Asia/Brunei','Asia/Chita','Asia/Choibalsan','Asia/Colombo','Asia/Damascus','Asia/Dhaka','Asia/Dili','Asia/Dubai','Asia/Dushanbe','Asia/Gaza','Asia/Hebron','Asia/Ho_Chi_Minh','Asia/Hong_Kong','Asia/Hovd','Asia/Irkutsk','Asia/Jakarta','Asia/Jayapura','Asia/Jerusalem','Asia/Kabul','Asia/Kamchatka','Asia/Karachi','Asia/Kathmandu','Asia/Khandyga','Asia/Kolkata','Asia/Krasnoyarsk','Asia/Kuala_Lumpur','Asia/Kuching','Asia/Kuwait','Asia/Macau','Asia/Magadan','Asia/Makassar','Asia/Manila','Asia/Muscat','Asia/Nicosia','Asia/Novokuznetsk','Asia/Novosibirsk','Asia/Omsk','Asia/Oral','Asia/Phnom_Penh','Asia/Pontianak','Asia/Pyongyang','Asia/Qatar','Asia/Qyzylorda','Asia/Rangoon','Asia/Riyadh','Asia/Sakhalin','Asia/Samarkand','Asia/Seoul','Asia/Shanghai','Asia/Singapore','Asia/Srednekolymsk','Asia/Taipei','Asia/Tashkent','Asia/Tbilisi','Asia/Tehran','Asia/Thimphu','Asia/Tokyo','Asia/Ulaanbaatar','Asia/Urumqi','Asia/Ust-Nera','Asia/Vientiane','Asia/Vladivostok','Asia/Yakutsk','Asia/Yekaterinburg','Asia/Yerevan','Atlantic/Azores','Atlantic/Bermuda','Atlantic/Canary','Atlantic/Cape_Verde','Atlantic/Faroe','Atlantic/Madeira','Atlantic/Reykjavik','Atlantic/South_Georgia','Atlantic/St_Helena','Atlantic/Stanley','Australia/Adelaide','Australia/Brisbane','Australia/Broken_Hill','Australia/Currie','Australia/Darwin','Australia/Eucla','Australia/Hobart','Australia/Lindeman','Australia/Lord_Howe','Australia/Melbourne','Australia/Perth','Australia/Sydney','Europe/Amsterdam','Europe/Andorra','Europe/Athens','Europe/Belgrade','Europe/Berlin','Europe/Bratislava','Europe/Brussels','Europe/Bucharest','Europe/Budapest','Europe/Busingen','Europe/Chisinau','Europe/Copenhagen','Europe/Dublin','Europe/Gibraltar','Europe/Guernsey','Europe/Helsinki','Europe/Isle_of_Man','Europe/Istanbul','Europe/Jersey','Europe/Kaliningrad','Europe/Kiev','Europe/Lisbon','Europe/Ljubljana','Europe/London','Europe/Luxembourg','Europe/Madrid','Europe/Malta','Europe/Mariehamn','Europe/Minsk','Europe/Monaco','Europe/Moscow','Europe/Oslo','Europe/Paris','Europe/Podgorica','Europe/Prague','Europe/Riga','Europe/Rome','Europe/Samara','Europe/San_Marino','Europe/Sarajevo','Europe/Simferopol','Europe/Skopje','Europe/Sofia','Europe/Stockholm','Europe/Tallinn','Europe/Tirane','Europe/Uzhgorod','Europe/Vaduz','Europe/Vatican','Europe/Vienna','Europe/Vilnius','Europe/Volgograd','Europe/Warsaw','Europe/Zagreb','Europe/Zaporozhye','Europe/Zurich','Indian/Antananarivo','Indian/Chagos','Indian/Christmas','Indian/Cocos','Indian/Comoro','Indian/Kerguelen','Indian/Mahe','Indian/Maldives','Indian/Mauritius','Indian/Mayotte','Indian/Reunion','Pacific/Apia','Pacific/Auckland','Pacific/Bougainville','Pacific/Chatham','Pacific/Chuuk','Pacific/Easter','Pacific/Efate','Pacific/Enderbury','Pacific/Fakaofo','Pacific/Fiji','Pacific/Funafuti','Pacific/Galapagos','Pacific/Gambier','Pacific/Guadalcanal','Pacific/Guam','Pacific/Honolulu','Pacific/Johnston','Pacific/Kiritimati','Pacific/Kosrae','Pacific/Kwajalein','Pacific/Majuro','Pacific/Marquesas','Pacific/Midway','Pacific/Nauru','Pacific/Niue','Pacific/Norfolk','Pacific/Noumea','Pacific/Pago_Pago','Pacific/Palau','Pacific/Pitcairn','Pacific/Pohnpei','Pacific/Port_Moresby','Pacific/Rarotonga','Pacific/Saipan','Pacific/Tahiti','Pacific/Tarawa','Pacific/Tongatapu','Pacific/Wake','Pacific/Wallis','UTC'];
  }

}());
