<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Reader extends Base {

  public function Add($id) {
    // Add the initial notify entry, which is set to private as the reader
    // module doesn't produce a feed.
    $this->Notify($id, 'reader', 'feed', $this->user->page, 0);
  }

  public function Callback() {
    $id = isset($_POST['id']) ? (int)substr($_POST['id'], 9) : 0;
    $us_action = isset($_POST['action']) ? $_POST['action'] : 'feed';
    $us_update = isset($_POST['update']) ? $_POST['update'] : 'newer';

    if ($us_action === 'feed') {
      return $this->GetFeed($id, $us_update);
    }
    if ($us_action === 'changeChannel') {
      return $this->ChangeChannel($id);
    }
    if ($us_action === 'listChannels') {
      return $this->ListChannels($id);
    }
    if ($us_action === 'reset') {
      $this->ResetSession();
      if (isset($_SESSION['indieauth-feed']) &&
          $_SESSION['indieauth-feed'] !== '') {
        $result = $this->AddFeed($id, true, $_SESSION['indieauth-feed'], true);
        unset($_SESSION['indieauth-feed']);
        // Want to return the updated feed settings as well as the new content,
        // as long as the feed was added without error.
        if (!isset($result['error'])) return $result;
      }
      return $this->GetFeed($id);
    }
    if ($us_action === 'add') {
      if ($this->user->canViewPage) return $this->AddFeed($id);
      return ['error' => 'Permission denied adding feed'];
    }
    if ($us_action === 'addMultiple') {
      if ($this->user->canViewPage) {
        $result = [];
        $us_feeds = json_decode($_POST['xmlUrl']);
        $count_feeds = count($us_feeds);
        for ($i = 0; $i < $count_feeds; $i++) {
          // Return the feed settings when the last feed has been added.
          $result = $this->AddFeed($id, $i === $count_feeds - 1, $us_feeds[$i]);
        }
        return $result;
      }
      return ['error' => 'Permission denied adding feed'];
    }
    if ($us_action === 'addImport') {
      if ($this->user->canViewPage) {
        return $this->AddFeed($id, $_POST['last'] === 'true');
      }
      return ['error' => 'Permission denied adding feed'];
    }
    if ($us_action === 'remove') {
      if ($this->user->canViewPage) return $this->RemoveFeedForUser($id);
      return ['error' => 'Permission denied removing feed'];
    }
    if ($us_action === 'setChannel') {
      if ($this->user->canViewPage) return $this->SetChannel($id);
      return ['error' => 'Permission denied setting channel'];
    }
    if ($us_action === 'channelDisplay') {
      if ($this->user->canViewPage) return $this->ChannelDisplay($id);
      return ['error' => 'Permission denied updating channel display'];
    }
    if ($us_action === 'removeChannel') {
      if ($this->user->canViewPage) return $this->RemoveChannel($id);
      return ['error' => 'Permission denied removing channel'];
    }
    if ($us_action === 'import') {
      if ($this->user->canViewPage) return $this->ImportFile();
      return ['error' => 'Permission denied importing file'];
    }
    if ($us_action === 'manualAdd') {
      if ($this->user->canViewPage) return $this->ManualAdd($id);
      return ['error' => 'Permission denied adding feed'];
    }
    return ['error' => 'Unknown action'];
  }

  public function CanAdd($page) {
    // Can only have one reader module on a page.
    return !$this->AlreadyOnPage('reader', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $content = '';
    if ($this->user->canViewPage &&
        $this->Substitute('reader-hide-settings') !== 'true') {
      list($us_follow, $show_value, $show_settings) = isset($_GET['follow']) ?
        [$_GET['follow'], true, ''] : ['', false, ' hidden'];
      $us_follow = trim($us_follow, ' /');
      // If the writer module is on the page it can show reader settings and
      // handle the follow webaction, but that still needs the input set in
      // the feed settings form.
      if ($this->AlreadyOnPage('writer') && $this->CanPublish()) {
        $content .= '<div class="reader-settings hidden">' .
          $this->FeedSettings($id, $us_follow, $show_value) . '</div>';
      }
      else {
        $content .= '<button class="reader-edit-settings">feed settings' .
          '</button><div class="reader-settings' . $show_settings . '">' .
            $this->FeedSettings($id, $us_follow, $show_value) . '</div>';
      }
      $content .= '<div class="reader-discovered"></div>';
      if (isset($this->user->settings['reader']['showChannels']) &&
          $this->user->settings['reader']['showChannels'] === 'yes') {
        $content .= '<div class="reader-channel-settings hidden">' .
          $this->ChannelSettings($id) . '</div>';
      }
    }
    $content .= '<div class="reader-show-hidden-wrapper">' .
        '<button id="reader-show-hidden">Display new content</button></div>' .
      '<div class="reader-content"></div>' .
      '<button class="reader-more hidden">Load more items</button>' .
      '<button class="reader-reset hidden">Reset page</button>' .
      '<div class="reader-clear"></div>';
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {
    // Add the initial notify entry for the copied module, which is set to
    // private as the reader module doesn't produce a feed.
    $this->Notify($id, 'reader', 'feed', $new_page, 0);
  }

  public function Cron() {
    // Run every hour, SimplePie's default cache duration is also an hour.
    if (!$this->Run(date('H:00:00'))) return;

    // Fetching all the feeds could take a long time, so remove the time limit.
    set_time_limit(0);
    include_once 'library/Masterminds/HTML5.auto.php';
    include_once 'library/Mf2/Parser.php';
    include_once 'autoloader.php';
    include_once 'idn/idna_convert.class.php';
    $feed = new SimplePie();
    $feed->force_feed(true);
    $feed->enable_order_by_date(false);
    // These settings are set to false because HTMLPurifier handles them.
    $feed->strip_comments(false);
    $feed->strip_htmltags(false);
    $feed->strip_attributes(false);
    $feed->add_attributes(false);
    $scheme = $this->user->config->Secure() ? 'https://' : 'http://';
    $handler = $scheme . $this->user->config->ServerName() . '/php/image.php';
    // The full image handler url is required for Microsub clients.
    $feed->set_image_handler($handler);
    $daily = $this->Run('12am');
    $us_updated = [];
    foreach ($this->FeedList() as $us_xml_url) {
      $feed->set_feed_url($us_xml_url);
      $feed->set_raw_data('');
      $feed->init();
      if ($this->CheckFeed($feed, $us_xml_url, $daily)) {
        $us_updated[] = $us_xml_url;
      }
    }
    $this->NotifyUsers($us_updated);

    $mysqli = connect_db();
    $subscriptions = [];
    // Check hub expiry timestamps every hour.
    $query = 'SELECT hub, self, callback_id, xml_url FROM reader_hub WHERE ' .
      'expiry < ' . time();
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader_hub = $mysqli_result->fetch_assoc()) {
        $subscriptions[] = $reader_hub;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->Cron 1: ' . $mysqli->error);
    }
    foreach ($subscriptions as $feed_list) {
      $this->RenewHub($feed_list['hub'], $feed_list['self'],
                      $feed_list['callback_id'], $feed_list['xml_url']);
    }

    // Remove expired subscribers hourly, renew our subscriptions at midnight.
    $query = 'DELETE FROM reader_notify WHERE timestamp < ' .
      strtotime('-25 hours');
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Cron 2: ' . $mysqli->error);
    }
    if ($daily) {
      $subscriptions = [];
      $prev_register = '';
      $count = 1;
      $query = 'SELECT register, xml_url FROM reader_cloud ORDER BY register';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($reader_cloud = $mysqli_result->fetch_assoc()) {
          $register = $reader_cloud['register'];
          $us_xml_url = $reader_cloud['xml_url'];
          // Group the feeds we're renewing by registration endpoint, as only
          // one request needs to be made. This requires enumerating the feed
          // list.
          if ($register !== $prev_register) {
            $count = 1;
            $prev_register = $register;
            $subscriptions[$register] = 'url1=' . urlencode($us_xml_url);
          }
          else {
            $count++;
            $subscriptions[$register] .=
              '&url' . $count . '=' . urlencode($us_xml_url);
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->Cron 3: ' . $mysqli->error);
      }
      foreach ($subscriptions as $register => $feed_list) {
        $this->RenewCloud($register, $feed_list);
      }

      // Check current disk usage by the SimplePie cache directory. PurgeCache
      // will remove the oldest content until under the allocated limit.
      $cache_limit = (int)$this->Substitute('reader-cache-limit');
      if ($cache_limit !== 0) {
        $handle = popen('/usr/bin/du -sm ' . $feed->cache_location, 'r');
        $size = fgets($handle);
        pclose($handle);
        if (preg_match('/^([0-9]+)/', $size, $matches)) {
          $count = (int)$matches[1] - $cache_limit;
          if ($count > 0) $this->PurgeCache($count, $feed->cache_location);
        }
        else {
          $this->Log('Reader->Cron 4: Could not check cache directory');
        }
      }

      // To create a new items table set this value to a name for the old
      // items (ie previous year/month) make sure it doesn't already exist.
      $label = 'reader-create-items-table';
      $items_table = $mysqli->escape_string($this->Substitute($label));
      if ($items_table !== '') {
        $this->CreateItemsTable($items_table);
        // Only want to run this once so remove the template.
        $query = 'DELETE FROM template WHERE label = "' . $label . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Reader->Cron 5: ' . $mysqli->error);
        }
      }
    }
    $mysqli->close();
  }

  public function Factory($fn, $p = NULL) {
    if (is_array($p)) {
      $count = count($p);
      if ($fn === 'UpdateFeed' && $count >= 2) {
        $us_xml_url = $p[0];
        $data = $p[1];
        $external = $count === 3 ? $p[2] : false;
        return $this->UpdateFeed($us_xml_url, $data, $external);
      }
      if ($fn === 'Register' && $count === 3) {
        $us_xml_url = $p[0];
        $register = $p[1];
        $hub = $p[2];
        return $this->Register($us_xml_url, $register, $hub);
      }
      if ($fn === 'AddFeed' && $count >= 2) {
        $id = $p[0];
        $us_xml_url = $p[1];
        $force = $count === 3 ? $p[2] : false;
        return $this->AddFeed($id, false, $us_xml_url, true, $force);
      }
      if ($fn === 'RegisteredFeed' && $count >= 2) {
        $us_xml_url = $p[0];
        $us_id = $p[1];
        $lease = $count === 3 ? $p[2] : 0;
        return $this->RegisteredFeed($us_xml_url, $us_id, $lease);
      }
      if ($fn === 'CheckFollowPost' && $count === 2) {
        $us_post = $p[0];
        $us_target = $p[1];
        return $this->CheckFollowPost($us_post, $us_target);
      }
      if ($fn === 'RemoveFollowPost' && $count === 2) {
        $us_source = $p[0];
        $us_target = $p[1];
        return $this->RemoveFollowPost($us_source, $us_target);
      }
      return;
    }

    if ($fn === 'Channels') {
      return $this->Channels();
    }
    if ($fn === 'CreateFeedList') {
      return $this->CreateFeedList($p);
    }
    if ($fn === 'Following') {
      return $this->Following($p);
    }
    if ($fn === 'Microsub') {
      return $this->Microsub();
    }
    if ($fn === 'RegisteredFeed') {
      return $this->RegisteredFeed($p);
    }
    if ($fn === 'UpdateFeed') {
      return $this->UpdateFeed($p);
    }
  }

  public function Group() {
    return 'reader-writer';
  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    $this->AppendScript($path, 'dobrado.reader.js');
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS reader (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'xml_url VARCHAR(300) NOT NULL,' .
      'auto TINYINT(1),' .
      'PRIMARY KEY(user, box_id, xml_url(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Install 1: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS reader_feeds (' .
      'xml_url VARCHAR(300) NOT NULL,' .
      'description TEXT,' .
      'html_url VARCHAR(200),' .
      'language VARCHAR(50),' .
      'feed_title VARCHAR(100),' .
      'image_url VARCHAR(200),' .
      'image_title VARCHAR(100),' .
      'image_link VARCHAR(200),' .
      'error_count INT UNSIGNED,' .
      'slow TINYINT(1),' .
      'PRIMARY KEY(xml_url(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Install 2: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS reader_cloud (' .
      'register VARCHAR(200) NOT NULL,' .
      'xml_url VARCHAR(300) NOT NULL,' .
      'PRIMARY KEY(xml_url(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Install 3: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS reader_hub (' .
      'hub VARCHAR(200) NOT NULL,' .
      'self VARCHAR(200) NOT NULL,' .
      'expiry INT(10) UNSIGNED,' .
      'callback_id VARCHAR(32),' .
      'xml_url VARCHAR(300) NOT NULL,' .
      'PRIMARY KEY(xml_url(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Install 4: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS reader_notify (' .
      'register VARCHAR(200) NOT NULL,' .
      'xml_url VARCHAR(300) NOT NULL,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'hub TINYINT(1),' .
      'PRIMARY KEY(register(100), xml_url(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Install 5: ' . $mysqli->error);
    }
      
    $query = 'CREATE TABLE IF NOT EXISTS reader_items_table_names (' .
      'name VARCHAR(200),' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(name)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Install 6: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS reader_queue (' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'xml_url VARCHAR(300) NOT NULL,' .
      'PRIMARY KEY(xml_url(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Install 7: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS reader_channels (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'xml_url VARCHAR(300) NOT NULL,' .
      'html_url VARCHAR(200),' .
      'author VARCHAR(200) NOT NULL,' .
      'channel VARCHAR(100),' .
      'PRIMARY KEY(user, box_id, xml_url(50), author(50))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Install 8: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS reader_channel_info (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'channel_name VARCHAR(100) NOT NULL,' .
      'channel_order INT UNSIGNED,' .
      'display_unread ENUM("true", "false", "count"),' .
      'last_read INT(10) UNSIGNED NOT NULL,' .
      'PRIMARY KEY(user, box_id, channel_name)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Install 9: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS reader_subscribe (' .
      'subscribe_url VARCHAR(300) NOT NULL,' .
      'xml_url VARCHAR(300) NOT NULL,' .
      'PRIMARY KEY(subscribe_url(100), xml_url(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Install 10: ' . $mysqli->error);
    }

    $query = 'CREATE TABLE IF NOT EXISTS reader_follow_posts (' .
      'user VARCHAR(50) NOT NULL,' .
      'box_id INT UNSIGNED NOT NULL,' .
      'permalink VARCHAR(300) NOT NULL,' .
      'xml_url VARCHAR(300) NOT NULL,' .
      'PRIMARY KEY(user, box_id, permalink(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Install 11: ' . $mysqli->error);
    }

    $this->CreateItemsTable();

    $format_with_title = '<h5 class="author">!date !author</h5>' .
      '<h4 class="title">!title</h4>' .
      '<div class="content">!content</div>' .
      '<div class="media">!media</div>' .
      '<div class="options">!category</div>';
    $format_no_title = '<h5 class="author">!date !author</h5>' .
      '<div class="content">!content</div>' .
      '<div class="media">!media</div>' .
      '<div class="options">!category</div>';
    $template = ['"reader-item-count", "", "10"',
                 '"reader-format-with-title", "", ' .
                   '"' . $mysqli->escape_string($format_with_title) . '"',
                 '"reader-format-no-title", "", ' .
                   '"' . $mysqli->escape_string($format_no_title) . '"'];
    $mysqli->close();

    $this->AddTemplate($template);    
    $description = ['reader-item-count' => 'The number of items to return ' .
                      'when more are requested.',
                    'reader-format-with-title' => 'The template for a reader ' .
                      'item with a title, substitutes: !title, !description, ' .
                      '!author, !category, !media and !date.',
                    'reader-format-no-title' => 'The template for a reader ' .
                      'item with no title, substitutes: !description, ' .
                      '!author, !category, !media and !date.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"",".reader .feed-item","padding","5px"',
                   '"",".reader .feed-item","margin","5px"',
                   '"",".reader .feed-item","clear","both"',
                   '"",".reader .feed-text.added","background-color","#ff5500"',
                   '"",".reader .feed-url-wrapper","font-size","0.8em"',
                   '"",".reader .feed-url-wrapper","color","#aaaaaa"',
                   '"",".reader .feed-url a","color","#aaaaaa"',
                   '"",".reader .content .h-card img","width","20px"',
                   '"",".reader .content .h-card img","border-radius","2px"',
                   '"",".reader .author","margin","10px 0"',
                   '"",".reader .group-title img","max-height","50px"',
                   '"",".reader .group-title img","max-width","70px"',
                   '"",".reader .group-title img","float","left"',
                   '"",".reader .group-title img","margin","0 10px 10px 0"',
                   '"",".reader-settings","background-color","#eeeeee"',
                   '"",".reader-settings","border","1px solid #aaaaaa"',
                   '"",".reader-settings","border-radius","2px"',
                   '"",".reader-settings","padding","5px"',
                   '"",".reader-settings","margin","5px"',
                   '"",".reader-feed-list","max-height","250px"',
                   '"",".reader-feed-list","overflow","auto"',
                   '"",".reader-feed-list","clear","both"',
                   '"",".reader-feed-list > div:nth-child(2n)",' .
                     '"background-color","#ffffff"',
                   '"","#reader-options-button","width","150px"',
                   '"","#reader-options-button","float","right"',
                   '"","#reader-options-button","margin-top","4px"',
                   '"",".reader-add-feed","margin-left","5px"',
                   '"","#reader-feed-input-wrapper","clear","none"',
                   '"","#reader-file-input-wrapper","clear","none"',
                   '"","#reader-channel-wrapper","clear","none"',
                   '"",".reader-channel-button","float","right"',
                   '"",".reader-hidden","display","none"',
                   '"","#reader-show-hidden","display","block"',
                   '"","#reader-show-hidden","margin-left","auto"',
                   '"","#reader-show-hidden","margin-right","auto"',
                   '"",".reader-show-hidden-wrapper","display","none"',
                   '"",".reader-show-hidden-wrapper","padding","5px"',
                   '"",".reader-show-hidden-wrapper","margin","5px"',
                   '"",".reader-show-hidden-wrapper","background-color",' .
                     '"#eeeeee"',
                   '"",".reader-show-hidden-wrapper","border",' .
                     '"1px solid #aaaaaa"',
                   '"",".reader-show-hidden-wrapper","border-radius","2px"',
                   '"",".reader-group","padding","5px"',
                   '"",".reader-group","margin","5px"',
                   '"",".reader-group","background-color","#eeeeee"',
                   '"",".reader-group","border","1px solid #aaaaaa"',
                   '"",".reader-group","border-radius","2px"',
                   '"",".reader-item","margin-left","10px"',
                   '"",".reader-item","overflow","hidden"',
                   '"",".reader-item","clear","both"',
                   '"",".reader-item .author img","max-height","50px"',
                   '"",".reader-item .author img","max-width","70px"',
                   '"",".reader-item .author img","float","left"',
                   '"",".reader-item .author img","border-radius","2px"',
                   '"",".reader-item .author img","margin","0 10px 10px 0"',
                   '"",".reader-item a.author-name","font-size","1.2em"',
                   '"",".reader-item a.author-name ","color","#222222"',
                   '"",".reader-item a.author-name:hover","color","#999999"',
                   '"",".reader-item .permalink","float","right"',
                   '"",".reader-item .options","overflow","auto"',
                   '"",".reader-item .options","margin-top","5px"',
                   '"",".reader-item .media","clear","both"',
                   '"",".reader-reset","float","right"',
                   '"",".reader-clear","clear","both"',
                   '"",".reader h4","margin","10px 0"',
                   '"",".reader a","color","#666666"',
                   '"",".reader a:hover","color","#999999"',
                   '"",".reader-actions","display","flex"',
                   '"",".reader-actions","clear","both"',
                   '"",".reader-actions a","margin","5px"',
                   '"",".reader-actions a.selected","font-weight","bold"',
                   '"",".reader-actions > indie-action","flex-grow","1"',
                   '"",".reader-actions > indie-action","text-align","center"',
                   '"",".reader-discovered label","float","none"',
                   '"",".reader-discovered button","margin-top","20px"',
                   '"",".reader-discovered-add","float","right"',
                   '"",".reader-discovered-feed-url","color","#aaaaaa"',
                   '"",".reader-discovered-feed-url","font-size","0.8em"',
                   '"","#reader-channel-title","font-weight","bold"',
                   '"","#reader-channel-title","font-size","1.2em"',
                   '"",".reader-channel-settings label","width","7em"',
                   '"","#reader-channel-submit","margin-left","5px"',
                   '"","#reader-channel-display","margin-top","5px"',
                   '"","label[for=reader-channel-order]","width","7em"',
                   '"","#reader-channel-order","width","50px"',
                   '"","label[for=reader-channel-update]","width","7em"',
                   '"","#reader-channel-display-submit","margin-left","8em"',
                   '"","#reader-channel-display-remove","float","right"',
                   '"","#reader-subscribe-list .form-spacing","line-height",' .
                     '"2em"',
                   '"",".reader-subscribe-add","float","right"',
                   '"",".reader-reposted-by","font-size","0.8em"',
                   '"",".reader-reposted-by","margin-left","5px"',
                   '"",".reader-reposted-by","white-space","nowrap"',
                   '"",".h-card .ui-icon.ui-icon-person","display",' .
                     '"inline-block"',
                   '"","a.tooltip-photo","float","left"',
                   '"","a.tooltip-photo","margin","5px"',
                   '"","a.tooltip-photo img","max-width","80px"',
                   '"","a.tooltip-photo img","max-height","80px"',
                   '"","a.tooltip-photo img","border-radius","2px"',
                   '"","a.tooltip-name","line-height","2em"',
                   '"","a.tooltip-name","font-size","1.2em"',
                   '"","a.tooltip-name","text-decoration","none"',
                   '"","a.tooltip-url","font-size","0.8em"',
                   '"","a.tooltip-url","text-decoration","none"',
                   '"","a.tooltip-url","color","#aaaaaa"',
                   '"","a.tooltip-url","white-space","nowrap"',
                   '"",".tooltip-channel-info","clear","both"',
                   '"",".tooltip-channel-info","font-size","0.9em"',
                   '"",".tooltip-channel-add","width","150px"',
                   '"",".tooltip-channel-add","margin","5px"',
                   '"",".ui-tooltip .ui-selectmenu-button","width","6em"',
                   '"","li.reader-unread-true","font-weight","bold"',
                   '"","#page-select-menu .ui-state-active","font-weight",' .
                     '"inherit"',
                   '"","#reader-action-wrapper","margin-bottom","30px"',
                   '"","#reader-action-wrapper","clear","both"',
                   '"","#reader-action-cancel","margin-top","2px"',
                   '"","#reader-action-submit","margin-top","2px"',
                   '"","#reader-action-submit","float","right"',
                   '"",".reader-web-actions","display","flex"',
                   '"",".reader-web-actions > indie-action","flex-grow","1"',
                   '"",".reader-web-actions > indie-action","text-align",' .
                     '"center"'];
    $this->AddSiteStyle($site_style);
    $this->AddSettingTypes(['"reader","defaultChannel","","text","Enter the ' .
                              'name of the channel you would like to see ' .
                              'when you first log in."',
                            '"reader","showChannels","yes,no","radio","If ' .
                              'you would prefer to not use channels you can ' .
                              'set that here."']);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {
    $mysqli = connect_db();
    if (isset($id)) {
      $this->RemoveNotify($id);
      // Check if anyone else subscribes to feeds in the list, otherwise they
      // will be removed.
      $query = 'SELECT xml_url FROM reader WHERE ' .
        'user = "' . $this->owner . '" AND box_id = ' . $id;
      if ($mysqli_result = $mysqli->query($query)) {
        while ($reader = $mysqli_result->fetch_assoc()) {
          $this->CheckSubscriptions($reader['xml_url']);
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->Remove 1: ' . $mysqli->error);
      }
      $query = 'DELETE FROM reader WHERE user = "' . $this->owner . '" ' .
        'AND box_id = ' . $id;
      if (!$mysqli->query($query)) {
        $this->Log('Reader->Remove 2: ' . $mysqli->error);
      }
    }
    else {
      // Check if anyone else subscribes to feeds in the list, otherwise they
      // will be removed.
      $query = 'SELECT xml_url FROM reader WHERE user = "' . $this->owner . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($reader = $mysqli_result->fetch_assoc()) {
          $this->CheckSubscriptions($reader['xml_url']);
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->Remove 3: ' . $mysqli->error);
      }
      $query = 'DELETE FROM reader WHERE user = "' . $this->owner . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->Remove 4: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // Note that the Reader module can't call Update automatically... this
    // is because the old version has already been included when install_module
    // is called by the Autoupdate module, so autoload doesn't get a chance to
    // include the file that has just been downloaded. To get around this
    // create two updates: the first adds the changes required to this function,
    // the second update can remove them and will trigger the first version.
    // Make sure the two updates are published at least an hour apart!
  }

  public function UpdateScript($path) {
    // Append dobrado.reader.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.reader.js');
  }

  // Public functions that aren't part of interface here /////////////////////

  public function CreateFeedList($type) {
    $feed_list = '';
    $owner = $this->SwitchOwner();

    $mysqli = connect_db();
    $query = 'SELECT reader.xml_url, description, html_url, language, ' .
      'feed_title, image_url, image_title, image_link FROM reader LEFT JOIN ' .
      'reader_feeds ON reader.xml_url = reader_feeds.xml_url WHERE ' .
      'user = "' . $owner . '" AND box_id = ' . $this->Id() .
      ' ORDER BY xml_url';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader = $mysqli_result->fetch_assoc()) {
        $xml_url = $reader['xml_url'];
        $html_url = $reader['html_url'];
        $description = $reader['description'];
        $title = $reader['feed_title'];
        if ($title === '') $title = $html_url;
        if ($title === '') $title = $xml_url;

        if ($type === 'opml') {
          $language = $reader['language'];
          $feed_list .= '<outline text="' . htmlspecialchars($title) . '" ' .
            'type="rss" xmlUrl="' . htmlspecialchars($xml_url) . '"';
          if ($html_url !== '') {
            $feed_list .= ' htmlUrl="' . htmlspecialchars($html_url) . '"';
          }
          if ($description !== '') {
            $feed_list .= ' description="' . htmlspecialchars($description).'"';
          }
          if ($language !== '') {
            $feed_list .= ' language="' . htmlspecialchars($language) . '"';
          }
          $feed_list .= '/>' . "\n";
        }
        else if ($type === 'mf2') {
          $image_url = $reader['image_url'];
          // Nickname cache may have a photo for this person if not found in
          // feed settings.
          if ($image_url === '') {
            $result = $this->LookupNickname($title, $html_url);
            $image_url = $result[2];
          }
          $feed_list .= '<div class="h-card">';
          if ($image_url !== '') {
            $feed_list .= '<a href="' . $html_url . '">' .
              '<img class="thumb u-photo" src="' . $image_url . '"></a>';
          }
          $feed_list .= '<a class="p-name u-url" href="' . $xml_url . '">' .
            $title . '</a>';
          if ($description !== '') {
            $feed_list .= '<p class="p-note">' . $description . '</p>';
          }
          $feed_list .= '<div class="clear"></div></div>' . "\n";
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->CreateFeedList: ' . $mysqli->error);
    }
    $mysqli->close();
    return $feed_list;
  }

  // Private functions below here ////////////////////////////////////////////

  private function AddFeed($id, $content = true, $us_xml_url = '',
                           $auto = false, $force = false,
                           $microsub = false, $discover_only = false) {
    include_once 'library/HTMLPurifier.auto.php';
    include_once 'library/Masterminds/HTML5.auto.php';
    include_once 'library/Mf2/Parser.php';
    include_once 'autoloader.php';
    include_once 'idn/idna_convert.class.php';
    // When the url provided is found via feed discovery, force is set to true
    // so that the microformats on the page can be used even though there are
    // other feed links on the page.
    if (isset($_POST['force'])) $force = $_POST['force'] === 'true';
    if ($us_xml_url === '') $us_xml_url = $_POST['xmlUrl'];
    // If xml_url doesn't have a scheme, add one. Otherwise it can't be
    // removed below because it's matched against the href property returned
    // by javascript, which adds the scheme and also a trailing slash.
    if (stripos($us_xml_url, 'http') !== 0) {
      $us_xml_url = 'http://' . $us_xml_url;
    }
    // Make sure the given value is the actual feed by passing it to SimplePie
    // to do feed discovery.
    $feed = new SimplePie();
    $feed->enable_order_by_date(false);
    // These settings are set to false because HTMLPurifier handles them.
    $feed->strip_comments(false);
    $feed->strip_htmltags(false);
    $feed->strip_attributes(false);
    $feed->add_attributes(false);
    $scheme = $this->user->config->Secure() ? 'https://' : 'http://';
    $server_name = $this->user->config->ServerName();
    // The full image handler url is required for Microsub clients.
    $feed->set_image_handler($scheme . $server_name . '/php/image.php');
    $feed->set_feed_url($us_xml_url);
    $feed->force_feed($force);
    // get_all_discoverd_feeds doesn't work if caching is used.
    $feed->set_cache_duration(0);
    $feed->set_autodiscovery_cache_duration(0);
    if (!$feed->init()) {
      $us_xml_url = trim($us_xml_url, ' /');
      $this->RemoveFeed($us_xml_url);
      if ($error = $feed->error()) {
        $this->Log('Reader->AddFeed 1: Error fetching ' . $us_xml_url . ': ' .
                   $error);
      }
      return ['error' => 'Feed not found from: ' . $us_xml_url];
    }

    $raw_data = $feed->get_raw_data();
    // The auto parameter is set to true for indieauth users so that they are
    // automatically subscribed to their own feeds, without showing options.
    // Also don't do feed discovery when a fragment identifier has been added
    // to the url as this represents a specific feed on the page.
    if (!$auto && strpos($us_xml_url, '#') === false) {
      // If microformats were found on the page then there could be more than
      // one h-feed, so show all of them during discovery.
      $feed_author = '';
      $h_feed_list = [];
      $mf = $raw_data ? Mf2\parse($raw_data) : Mf2\fetch($us_xml_url);
      if (isset($mf['items'])) {
        foreach ($mf['items'] as $mf_item) {
          if (!isset($mf_item['type'])) continue;

          if (in_array('h-feed', $mf_item['type'])) {
            if (isset($mf_item['properties']['name'][0])) {
              $h_feed_list[] = $mf_item['properties']['name'][0];
            }
            else if (isset($mf_item['properties']['uid'][0])) {
              $h_feed_list[] = $mf_item['properties']['uid'][0];
            }
            else if (isset($mf_item['id'])) {
              $h_feed_list[] = $mf_item['id'];
            }
            else if (!in_array('', $h_feed_list)) {
              // Assume an h-feed without a name is the main feed on the page,
              // and push it onto the front of the list.
              array_unshift($h_feed_list, '');
            }
          }
          // Also look for h-feed on children.
          if (!isset($mf_item['children'][0]['type'])) continue;

          foreach ($mf_item['children'] as $child) {
            if (in_array('h-feed', $child['type'])) {
              // The parent of this h-feed may be an h-card, so use the name as
              // the feed author if a name isn't given for the h-feed.
              if (in_array('h-card', $mf_item['type'])) {
                $feed_author = $mf_item['properties']['name'][0];
              }
              if (isset($child['properties']['name'][0])) {
                $h_feed_list[] = $child['properties']['name'][0];
              }
              else if (isset($child['properties']['uid'][0])) {
                $h_feed_list[] = $child['properties']['uid'][0];
              }
              else if (isset($child['id'])) {
                $h_feed_list[] = $child['id'];
              }
              else if (!in_array('', $h_feed_list)) {
                array_unshift($h_feed_list, '');
              }
            }
          }
        }
      }
      // get_all_discovered_feeds() returns an array of SimplePie_File objects.
      $all_feeds = $feed->get_all_discovered_feeds();
      if ($discover_only || count($all_feeds) > 1 || count($h_feed_list) > 1) {
        return $this->DiscoveredFeeds($us_xml_url, $feed_author, $h_feed_list,
                                      $all_feeds, $microsub);
      }
    }

    // SimplePie will update the feed_url when the url given was
    // previously used for autodiscovery, or if there were redirects.
    $us_xml_url = $feed->subscribe_url();

    $mysqli = connect_db();
    $owner = $this->SwitchOwner();
    $us_xml_url = trim($us_xml_url, ' /');
    $xml_url = $mysqli->escape_string($us_xml_url);
    $query = 'SELECT xml_url FROM reader WHERE user = "' . $owner . '" AND ' .
      'box_id = ' . $id . ' AND xml_url = "' . $xml_url . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows === 1) {
        $mysqli_result->close();
        $mysqli->close();
        return ['error' => 'Already subscribed to: ' . $us_xml_url];
      }
    }
    else {
      $this->Log('Reader->AddFeed 2: ' . $mysqli->error);
    }

    // Need to check if this user is manually subscribing to a feed that was
    // previously subscribed to automatically.
    $current_auto = '';
    $query = 'SELECT auto FROM reader WHERE user = "' . $owner . '" AND ' .
      'box_id = ' . $id . ' AND xml_url = "' . $xml_url . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($reader = $mysqli_result->fetch_assoc()) {
        $current_auto = $reader['auto'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->AddFeed 3: ' . $mysqli->error);
    }
    // Not subscribed, so use whatever was passed in.
    if ($current_auto === '') {
      $auto_query = $auto ? '1' : '0';
      $query = 'INSERT INTO reader VALUES ("' . $owner . '", ' . $id . ', ' .
        '"' . $xml_url . '", ' . $auto_query . ')';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->AddFeed 4: ' . $mysqli->error);
      }
    }
    else if ($current_auto === '1' && !$auto) {
      $query = 'UPDATE reader SET auto = 0 WHERE user = "' . $owner . '" AND ' .
        'box_id = ' . $id . ' AND xml_url = "' . $xml_url . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->AddFeed 5: ' . $mysqli->error);
      }
    }
    // Update page_updates table for feed list subscribers.
    $query = 'INSERT INTO page_updates VALUES ("' . $owner . '", ' .
      '"' . $this->user->page . '", "feed", ' . time() . ') ' .
      'ON DUPLICATE KEY UPDATE timestamp = ' . time();
    if (!$mysqli->query($query)) {
      $this->Log('Reader->AddFeed 6: ' . $mysqli->error);
    }

    // Check if this feed is already in reader_feeds.
    $feed_exists = false;
    $query = 'SELECT xml_url FROM reader_feeds WHERE xml_url = "' .$xml_url.'"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows === 1) {
        $feed_exists = true;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->AddFeed 7: ' . $mysqli->error);
    }
    if (!$feed_exists) {
      $us_title = $feed->get_title();
      $us_description = $feed->get_description();
      $us_html_url = trim($feed->get_link(), ' /');
      $us_language = $feed->get_language();
      $us_image_url = $feed->get_image_url();
      $us_image_title = $feed->get_image_title();
      $us_image_link = $feed->get_image_link();
      $title = isset($us_title) ? $mysqli->escape_string($us_title) : '';
      $description = isset($us_description) ?
        $mysqli->escape_string($us_description) : '';
      $html_url = isset($us_html_url) ?
        $mysqli->escape_string($us_html_url) : '';
      $language = isset($us_language) ?
        $mysqli->escape_string($us_language) : '';
      $image_url = isset($us_image_url) ?
        $mysqli->escape_string($us_image_url) : '';
      $image_title = isset($us_image_title) ?
        $mysqli->escape_string($us_image_title) : '';
      $image_link = isset($us_image_link) ?
        $mysqli->escape_string($us_image_link) : '';
      // These feeds change the html_url link so it's hard to identify their
      // item groups, and we want to use the group info to know whether an
      // item should be automatically syndicated to the silos they came from.
      if (strpos($xml_url, 'https://facebook-atom.appspot.com') === 0) {
        $html_url = 'https://facebook-atom.appspot.com';
        $image_url = $scheme . $server_name . '/images/facebook.png';
      }
      else if (strpos($xml_url, 'https://twitter-atom.appspot.com') === 0) {
        $html_url = 'https://twitter-atom.appspot.com';
        $image_url = $scheme . $server_name . '/images/twitter.png';
      }
      $query = 'INSERT INTO reader_feeds VALUES ("' . $xml_url . '", ' .
        '"' . $description . '", "' . $html_url . '", "' . $language . '", ' .
        '"' . $title . '", "' . $image_url . '", "' . $image_title . '", ' .
        '"' . $image_link . '", 0, 0)';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->AddFeed 8: ' . $mysqli->error);
      }
      // Leave call to SaveCloud until after the insert, as it triggers a call
      // to RegisteredFeed which will update reader_feeds for this entry.
      if ($us_cloud = $feed->get_channel_tags('', 'cloud')) {
        $this->SaveCloud($us_cloud, $us_xml_url);
      }
      else if ($us_hub = $feed->get_link(0, 'hub')) {
        $us_self = $feed->get_link(0, 'self');
        $this->SaveHub($us_hub, $us_self, $us_xml_url);
      }
      // If there are no feed items then this might be a list of feeds in
      // microformats, which expects an h-feed of h-cards.
      if ($feed->get_item_quantity() === 0 ||
          $feed->get_type() & SIMPLEPIE_TYPE_OPML_20) {
        // Once a feed has been cached SimplePie will no longer return raw data,
        // but it's required when processing a feed list so fetch it again.
        if (!$raw_data) $raw_data = file_get_contents($us_xml_url);
        if ($raw_data) {
          // This can take a long time depending on the feed list, so remove
          // the time limit.
          set_time_limit(0);
          foreach ($this->ProcessFeedList($raw_data) as $us_add_url) {
            $add_url = $mysqli->escape_string($us_add_url);
            $query = 'INSERT INTO reader_subscribe VALUES ' .
              '("' . $xml_url . '", "' . $add_url . '") ' .
              'ON DUPLICATE KEY UPDATE xml_url = xml_url';
            if (!$mysqli->query($query)) {
              $this->Log('Reader->AddFeed 9: ' . $mysqli->error);
            }
            $this->AddFeed($id, false, $us_add_url, true, false, $microsub);
          }
        }
      }
      else {
        $config = HTMLPurifier_Config::createDefault();
        // Allow iframes from youtube and vimeo.
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp',
                     '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|' .
                     'player\.vimeo\.com/video/)%');
        $config->set('CSS.ForbiddenProperties', ['white-space']);
        $config->set('HTML.DefinitionID', 'dobrado-reader');
        $config->set('HTML.DefinitionRev', 2);
        if ($def = $config->maybeGetRawHTMLDefinition()) {
          // These attributes are both for lightbox markup.
          $def->addAttribute('a', 'data-lightbox', 'Text');
          $def->addAttribute('a', 'data-title', 'Text');
        }
        $purifier = new HTMLPurifier($config);
        // Check for a special feed that is used by the Autoupdate module.
        // The actual subscription may have a fragment identifier so just match
        // the start of the url.
        $autoupdate_all = $this->Substitute('autoupdate-all');
        $autoupdate_new = $this->Substitute('autoupdate-new');
        $autoupdate_all_match = $autoupdate_all !== '' &&
          strpos($xml_url, $autoupdate_all) === 0;
        $autoupdate_new_match = $autoupdate_new !== '' &&
          strpos($xml_url, $autoupdate_new) === 0;
        $autoupdate_url = $autoupdate_all_match || $autoupdate_new_match;
        foreach ($feed->get_items() as $item) {
          $this->SaveItem($item, $xml_url, $purifier, $autoupdate_url);
        }
        if ($autoupdate_all_match) {
          // Once the feed of all modules has been saved, switch the autoupdates
          // feed to new updates only.
          $this->Log('Reader->AddFeed 10: Switching autoupdate feed from ' .
                     $autoupdate_all . ' to ' . $autoupdate_new);
          $this->RemoveFeedForUser($id, $us_xml_url, false);
          if ($autoupdate_new !== '') {
            $this->AddFeed($id, false, $autoupdate_new, true, true);
          }
        }
      }
    }
    $mysqli->close();
    if (!$content) return ['add' => $us_xml_url];

    // Return the feed with the new entry, the session variables used to
    // track the reader's position need to be reset too.
    if (!$microsub) $this->ResetSession();
    $result = $this->GetFeed($id);
    $result['settings'] = $this->FeedSettings($id, $xml_url);
    return $result;
  }

  private function ChangeChannel($id) {
    $found = false;
    $us_channel = $_POST['channel'];
    if ($us_channel === 'all') {
      $_SESSION['reader-channel'] = $us_channel;
      return ['channel' => true];
    }

    $mysqli = connect_db();
    $owner = $this->SwitchOwner();
    $channel = $mysqli->escape_string($us_channel);
    $query = 'SELECT DISTINCT channel FROM reader_channels WHERE ' .
      'user = "' . $owner . '" AND box_id = ' . $id .
      ' AND channel = "' . $channel . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows === 1) {
        $_SESSION['reader-channel'] = $us_channel;
        $found = true;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->ChangeChannel: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['channel' => $found];
  }

  private function ChannelDisplay($id) {
    $us_update = $_POST['update'];
    if ($us_update === 'all' || $us_update === 'global') {
      return ['error' => '\'' . $us_update . '\' is a reserved channel.'];
    }

    $mysqli = connect_db();
    $update = $mysqli->escape_string($us_update);
    $name = $mysqli->escape_string($_POST['name']);
    $unread = $mysqli->escape_string($_POST['unread']);
    $order = (int)$_POST['order'];
    $current_order = 0;

    $owner = $this->SwitchOwner();
    if ($update !== '' && $update !== $name) {
      // If the updated channel name already exists then don't continue.
      $query = 'SELECT channel_name FROM reader_channel_info WHERE ' .
        'user = "' . $owner . '" AND box_id = ' . $id . ' AND ' .
        'channel_name = "' . $update . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($mysqli_result->num_rows !== 0) {
          $mysqli->close();
          return ['error' => 'A channel with that name already exists.'];
        }
      }
      else {
        $this->Log('Reader->ChannelDisplay 1: ' . $mysqli->error);
      }
    }

    $query = 'SELECT channel_order FROM reader_channel_info WHERE ' .
      'user = "' . $owner . '" AND box_id = ' . $id . ' AND ' .
      'channel_name = "' . $name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($reader_channel_info = $mysqli_result->fetch_assoc()) {
        $current_order = (int)$reader_channel_info['channel_order'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->ChannelDisplay 2: ' . $mysqli->error);
    }
    // First update the order for channels that have been displaced by this
    // channel having it's order changed.
    if ($order !== $current_order) {
      if ($order < $current_order) {
        $query = 'UPDATE reader_channel_info SET ' .
          'channel_order = channel_order + 1 WHERE user = "' . $owner . '" ' .
          'AND box_id = ' . $id . ' AND channel_order < ' . $current_order .
          ' AND channel_order >= ' . $order ;
        if (!$mysqli->query($query)) {
          $this->Log('Reader->ChannelDisplay 3: ' . $mysqli->error);
        }
      }
      else if ($order > $current_order) {
        $query = 'UPDATE reader_channel_info SET ' .
          'channel_order = channel_order - 1 WHERE user = "' . $owner . '" ' .
          'AND box_id = ' . $id . ' AND channel_order > ' . $current_order .
          ' AND channel_order <= ' . $order;
        if (!$mysqli->query($query)) {
          $this->Log('Reader->ChannelDisplay 4: ' . $mysqli->error);
        }
      }
    }
    // If update is not set then not updating the channel name here.
    if ($update === '') $update = $name;
    $query = 'UPDATE reader_channel_info SET ' .
      'channel_name = "' . $update . '", channel_order = ' . $order .
      ', display_unread = "' . $unread . '" WHERE user = "' . $owner . '" ' .
      'AND box_id = ' . $id . ' AND channel_name = "' . $name . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->ChannelDisplay 5: ' . $mysqli->error);
    }
    $mysqli->close();
    return $this->ListChannels($id, true);
  }

  private function ChannelInfo($owner, $id) {
    $channel_info = [];
    $mysqli = connect_db();
    $query = 'SELECT channel_name, display_unread, last_read FROM ' .
      'reader_channel_info WHERE user = "' . $owner . '" AND box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader_channel_info = $mysqli_result->fetch_assoc()) {
        $us_channel = $reader_channel_info['channel_name'];
        $display_unread = $reader_channel_info['display_unread'];
        $last_read = (int)$reader_channel_info['last_read'];
        if ($display_unread !== 'false') {
          $count = $this->ChannelUnreadCount($id, $us_channel, $last_read);
          $channel_info[$us_channel] = $display_unread === 'count' ?
            $count : $count !== 0;
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->ChannelInfo: ' . $mysqli->error);
    }
    $mysqli->close();
    return $channel_info;
  }

  private function ChannelLastRead($id, $timestamp, $us_channel) {
    if ($us_channel === '') {
      $us_channel = isset($_SESSION['reader-channel']) ?
        $_SESSION['reader-channel'] : '';
    }
    if ($us_channel === '' || $us_channel === 'all') return;

    $mysqli = connect_db();
    $owner = $this->SwitchOwner();
    $channel = $mysqli->escape_string($us_channel);
    $query = 'UPDATE reader_channel_info SET last_read = ' . $timestamp .
      ' WHERE user = "' . $owner . '" AND box_id = ' . $id . ' AND ' .
      'channel_name =  "' . $channel . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->ChannelLastRead: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function ChannelQuery($id, $items_table = '',
                                $us_channel = '', $microsub = false) {
    if ($us_channel === '') {
      $us_channel = isset($_SESSION['reader-channel']) ?
        $_SESSION['reader-channel'] : '';
    }
    if ($us_channel === '' || $us_channel === 'all') return '';

    $feed_list = [];
    $author_list = [];
    $mysqli = connect_db();
    $owner = $this->SwitchOwner();
    // First need to create a list of entries where an author is set,
    // so that they're not included in a channel where author is not set.
    $query = 'SELECT xml_url, author FROM reader_channels WHERE ' .
      'user = "' . $owner . '" AND box_id = ' . $id . ' AND author != ""';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader_channels = $mysqli_result->fetch_assoc()) {
        $xml_url = $mysqli->escape_string($reader_channels['xml_url']);
        $author = $mysqli->escape_string($reader_channels['author']);
        if (isset($author_list[$xml_url])) {
          $author_list[$xml_url][] = $author;
        }
        else {
          $author_list[$xml_url] = [$author];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->ChannelQuery 1: ' . $mysqli->error);
    }

    if ($items_table === '') $items_table = 'reader_items';
    $channel_query = '';
    $query = 'SELECT xml_url, author FROM reader_channels WHERE ' .
      'user = "' . $owner . '" AND box_id = ' . $id . ' AND ' .
      'channel = "' . $mysqli->escape_string($us_channel) . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader_channels = $mysqli_result->fetch_assoc()) {
        $us_xml_url = $reader_channels['xml_url'];
        $xml_url = $mysqli->escape_string($us_xml_url);
        $us_author = $reader_channels['author'];
        $author = $mysqli->escape_string($us_author);
        if ($channel_query !== '') $channel_query .= ' OR ';
        // The feed can be added to a channel in which case author is not
        // set, otherwise a specific author can be added to a channel.
        if ($author === '') {
          if ($microsub) {
            // Can only provide the feed when returning a feed list for
            // Microsub, it has to be left to clients to check for authors to
            // exclude if they want to support multi-author feeds.
            $feed_list[] = ['type' => 'feed', 'url' => $us_xml_url];
          }
          else if (isset($author_list[$xml_url])) {
            // When the author is not set still need to remove authors from the
            // query that are set so they don't show up in both channels.
            $channel_query .=
              '(' . $items_table . '.xml_url = "' . $xml_url . '"';
            foreach ($author_list[$xml_url] as $author) {
              $channel_query .=
                ' AND ' . $items_table . '.author NOT LIKE "%' . $author . '%"';
            }
            $channel_query .= ')';
          }
          else {
            $channel_query .= $items_table . '.xml_url = "' . $xml_url . '"';
          }
        }
        else if ($microsub) {
          $feed_list[] = ['type' => 'feed', 'url' => $us_xml_url,
                          'author' => $us_author];
        }
        else {
          $channel_query .=
            '(' . $items_table . '.xml_url = "' . $xml_url . '" AND ' .
            $items_table . '.author LIKE "%' . $author . '%")';
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->ChannelQuery 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return $microsub ? ['items' => $feed_list] : $channel_query;
  }

  private function Channels($id = 0, $microsub = false) {
    $all_feeds = false;
    $channel_data = [];
    $channel_list = [];
    $channel_info = [];

    $mysqli = connect_db();
    // This function is called from the Control module, which doesn't have
    // the Reader module box_id so look it up for the current page. Can add
    // some extra options used by page-select input here too.
    if ($id === 0) {
      $id = $this->Id();
      $all_feeds = true;
    }

    $owner = $this->SwitchOwner();
    $query = 'SELECT channel_name FROM reader_channel_info WHERE ' .
      'user = "' . $owner . '" AND box_id = ' . $id . ' ORDER BY channel_order';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader_channel_info = $mysqli_result->fetch_assoc()) {
        $channel_list[] = $reader_channel_info['channel_name'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->Channels 1: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($microsub) {
      $channel_info = $this->ChannelInfo($owner, $id);
      $channel_data[] = ['uid' => 'notifications', 'name' => 'Notifications',
                         'unread' => false];
      foreach ($channel_list as $channel) {
        if (isset($channel_info[$channel])) {
          $channel_data[] = ['uid' => $channel, 'name' => $channel,
                             'unread' => $channel_info[$channel]];
        }
        else {
          $channel_data[] = ['uid' => $channel, 'name' => $channel,
                             'unread' => false];
        }
      }
      // Add a default channel if none were found.
      if (count($channel_data) === 1) {
        $channel_data[] = ['uid' => 'all', 'name' => 'all', 'unread' => false];
      }
      return ['channels' => $channel_data];
    }

    // Also look up channel info when generating a channel list for the
    // Control module.
    if ($all_feeds) $channel_info = $this->ChannelInfo($owner, $id);
    // Display the currently selected channel in the session, otherwise look for
    // a default setting if not set, otherwise use the first channel listed.
    $current_channel = '';
    $default_channel =
      isset($this->user->settings['reader']['defaultChannel']) ?
      $this->user->settings['reader']['defaultChannel'] : '';
    if (isset($_SESSION['reader-channel'])) {
      $current_channel = $_SESSION['reader-channel'];
    }
    else if ($default_channel !== '') {
      $current_channel = $default_channel;
      $_SESSION['reader-channel'] = $current_channel;
    }
    else if (isset($channel_list[0])) {
      $current_channel = $channel_list[0];
      $_SESSION['reader-channel'] = $current_channel;
    }
    $option_list = '';
    foreach ($channel_list as $channel) {
      $data = 'false';
      $unread = '';
      if (isset($channel_info[$channel])) {
        if ($channel_info[$channel] === true) {
          $data = 'true';
        }
        else if ($channel_info[$channel] > 0) {
          $data = 'true';
          $unread = ' (' . $channel_info[$channel] . ')';
        }
      }
      $selected = $channel === $current_channel ? ' selected="selected"' : '';
      $option_list .= '<option' . $selected. ' data-class="' . $data . '" ' .
        'value="' . $channel . '">' . $channel . $unread . '</option>';
    }
    if ($all_feeds && $option_list !== '') {
      $selected = $current_channel === 'all' ? ' selected="selected"' : '';
      $option_list .= '<option value="all"' . $selected . '>all feeds</option>';
      if ($this->user->group !== $this->Substitute('indieauth-group')) {
        $option_list .= '<option value="">change page...</option>';
      }
    }
    return $option_list;
  }

  private function ChannelSettings($id) {
    $options = $this->Channels($id);
    $hide_channel_options = $options === '' ? ' hidden' : '';
    // Channel options will be displayed once a channel has been added.
    return '<div id="reader-channel-info"></div>' .
      '<div class="form-spacing' . $hide_channel_options . '">' .
        '<label for="reader-channel-select">Set channel:</label>' .
        '<select id="reader-channel-select">' . $options .
        '</select>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="reader-channel-add">Add channel:</label>' .
        '<input id="reader-channel-add" type="text">' .
        '<button id="reader-channel-submit">submit</button>' .
      '</div>' .
      '<div id="reader-subscribe-list"></div>';
  }

  private function ChannelUnreadCount($id, $us_channel, $last_read) {
    $total_unread = 0;
    foreach ($this->ItemsTableNames() as $items_table) {
      $unread = $this->ChannelUnreadCountFromTable($id, $us_channel,
                                                   $last_read, $items_table);
      if ($unread === 0) return $total_unread;
      else $total_unread += $unread;
    }
    return $total_unread;
  }

  private function ChannelUnreadCountFromTable($id, $us_channel,
                                               $last_read, $items_table) {
    $xml_url_query = $this->ChannelQuery($id, $items_table, $us_channel);
    if ($xml_url_query === '') return 0;

    $unread = 0;
    $mysqli = connect_db();
    $query = 'SELECT COUNT(*) FROM ' . $items_table . ' WHERE ' .
      'timestamp > ' . $last_read . ' AND (' . $xml_url_query . ')';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($reader_items = $mysqli_result->fetch_row()) {
        $unread = (int)$reader_items[0];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->ChannelUnreadCountFromTable: ' . $mysqli->error);
    }
    $mysqli->close();
    return $unread;
  }

  private function CheckFeed($feed, $us_xml_url, $daily = false) {
    if ($feed->error()) {
      $this->RemoveFeed($us_xml_url);
      return false;
    }

    // get_raw_data returns false when the cache is still valid, so don't need
    // to check if anything has changed in this case.
    $raw_data = $feed->get_raw_data();
    if (!$raw_data) return false;

    $mysqli = connect_db();
    $xml_url = $mysqli->escape_string($us_xml_url);
    $updated = false;
    if ($feed->get_item_quantity() === 0 ||
        $feed->get_type() & SIMPLEPIE_TYPE_OPML_20) {
      $us_feed_list = $this->ProcessFeedList($raw_data);
      $updated = $this->CheckFeedList($us_feed_list, $us_xml_url);
    }
    else {
      include_once 'library/HTMLPurifier.auto.php';
      $config = HTMLPurifier_Config::createDefault();
      // Allow iframes from youtube and vimeo.
      $config->set('HTML.SafeIframe', true);
      $config->set('URI.SafeIframeRegexp',
                   '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|' .
                   'player\.vimeo\.com/video/)%');
      $config->set('CSS.ForbiddenProperties', ['white-space']);
      $config->set('HTML.DefinitionID', 'dobrado-reader');
      $config->set('HTML.DefinitionRev', 2);
      if ($def = $config->maybeGetRawHTMLDefinition()) {
        // These attributes are both for lightbox markup.
        $def->addAttribute('a', 'data-lightbox', 'Text');
        $def->addAttribute('a', 'data-title', 'Text');
      }
      $purifier = new HTMLPurifier($config);

      // TODO: check if $feed->subscribe_url() !== $us_xml_url, which means the
      // feed location has been updated. Need a function to update all
      // references to the location in the database.

      // Now go through all items to check which have updated.
      // Also check for a special feed that is used by the Autoupdate module.
      $autoupdate_new = $this->Substitute('autoupdate-new');
      $autoupdate_url = $autoupdate_new !== '' &&
        strpos($xml_url, $autoupdate_new) === 0;
      foreach ($feed->get_items() as $item) {
        if ($this->SaveItem($item, $xml_url, $purifier, $autoupdate_url)) {
          $updated = true;
        }
      }
    }

    // Update feed attributes once a day, in case anything has changed.
    if ($daily) {
      $us_title = $feed->get_title();
      $us_description = $feed->get_description();
      $us_html_url = trim($feed->get_link(), ' /');
      $us_language = $feed->get_language();
      $us_image_url = $feed->get_image_url();
      $us_image_title = $feed->get_image_title();
      $us_image_link = $feed->get_image_link();
      $title = isset($us_title) ? $mysqli->escape_string($us_title) : '';
      $description = isset($us_description) ?
        $mysqli->escape_string($us_description) : '';
      $html_url = isset($us_html_url) ?
        $mysqli->escape_string($us_html_url) : '';
      $language = isset($us_language) ?
        $mysqli->escape_string($us_language) : '';
      $image_url = isset($us_image_url) ?
        $mysqli->escape_string($us_image_url) : '';
      $image_title = isset($us_image_title) ?
        $mysqli->escape_string($us_image_title) : '';
      $image_link = isset($us_image_link) ?
        $mysqli->escape_string($us_image_link) : '';
      if (strpos($xml_url, 'https://facebook-atom.appspot.com') === 0) {
        $html_url = 'https://facebook-atom.appspot.com';
        $scheme = $this->user->config->Secure() ? 'https://' : 'http://';
        $image_url = $scheme . $this->user->config->ServerName() .
          '/images/facebook.png';
      }
      else if (strpos($xml_url, 'https://twitter-atom.appspot.com') === 0) {
        $html_url = 'https://twitter-atom.appspot.com';
        $scheme = $this->user->config->Secure() ? 'https://' : 'http://';
        $image_url = $scheme . $this->user->config->ServerName() .
          '/images/twitter.png';
      }
      if ($us_cloud = $feed->get_channel_tags('', 'cloud')) {
        $this->SaveCloud($us_cloud, $us_xml_url);
      }
      else if ($us_hub = $feed->get_link(0, 'hub')) {
        $us_self = $feed->get_link(0, 'self');
        $this->SaveHub($us_hub, $us_self, $us_xml_url);
      }
      $query = 'UPDATE reader_feeds SET feed_title = "' . $title . '", ' .
        'description = "' . $description . '", html_url = "' . $html_url .'", '.
        'language = "' . $language . '", image_url = "' . $image_url . '", ' .
        'image_title = "' . $image_title . '", ' .
        'image_link = "' . $image_link . '", error_count = 0 WHERE ' .
        'xml_url = "' . $xml_url . '"';
      if ($mysqli->query($query)) {
        // If none of the feeds items have been updated, still classify this
        // feed as updated if any feed level attributes have changed.
        if (!$updated && $mysqli->affected_rows !== 0) {
          $updated = true;
        }
      }
      else {
        $this->Log('Reader->CheckFeed 1: ' . $mysqli->error);
      }
    }
    if ($updated) {
      // When a feed is updated reset the slow flag regardless of support for
      // last modified flags (this gives it another chance if previously set).
      $query = 'UPDATE reader_feeds SET slow = 0 WHERE xml_url="' .$xml_url.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->CheckFeed 2: ' . $mysqli->error);
      }
    }
    else if ($feed->check_modified) {
      // If this feed was processed but nothing changed, set this feed to slow
      // if SimplePie sent any last modified flags with the request.
      $query = 'UPDATE reader_feeds SET slow = 1 WHERE xml_url="' .$xml_url.'"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->CheckFeed 3: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    return $updated;
  }

  private function CheckFeedList($us_feed_list, $us_xml_url) {
    $us_remove_feed_list = [];
    $us_current_feed_list = [];

    $mysqli = connect_db();
    $xml_url = $mysqli->escape_string($us_xml_url);
    $query = 'SELECT xml_url FROM reader_subscribe WHERE ' .
      'subscribe_url = "' . $xml_url . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader_subscribe = $mysqli_result->fetch_assoc()) {
        if (in_array($reader_subscribe['xml_url'], $us_feed_list)) {
          $us_current_feed_list[] = $reader_subscribe['xml_url'];
        }
        else {
          $us_remove_feed_list[] = $reader_subscribe['xml_url'];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Rader->CheckFeedList 1: ' . $mysqli->error);
    }
    // When the feed list changes need to update all subscribed users.
    $subscribed_list = [];
    $query = 'SELECT user, box_id FROM reader WHERE ' .
      'xml_url = "' . $xml_url . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader = $mysqli_result->fetch_assoc()) {
        $subscribed_list[] = $reader;
      }
    }
    else {
      $this->Log('Reader->CheckFeedList 2: ' . $mysqli->error);
    }

    $updated = false;
    $current_owner = $this->owner;
    foreach ($us_feed_list as $us_add_feed) {
      if (!in_array($us_add_feed, $us_current_feed_list)) {
        $updated = true;
        $query = 'INSERT INTO reader_subscribe VALUES ("' . $xml_url . '", ' .
          '"' . $mysqli->escape_string($us_add_feed) . '")';
        if (!$mysqli->query($query)) {
          $this->Log('Reader->CheckFeedList 3: ' . $mysqli->error);
        }
        foreach ($subscribed_list as $subscribed) {
          $this->owner = $subscribed['user'];
          $id = (int)$subscribed['box_id'];
          $this->AddFeed($id, false, $us_add_feed, true);
          // Also set the channel for the feed, using the channel the user has
          // set for the subscribe url as the default.
          $us_channel = '';
          $query = 'SELECT channel FROM reader_channels WHERE ' .
            'user = "' . $this->owner . '" AND id = ' . $id . ' AND ' .
            'xml_url = "' . $xml_url . '"';
          if ($mysqli_result = $mysqli->query($query)) {
            if ($reader_channels = $mysqli_result->fetch_assoc()) {
              $us_channel = $reader_channels['channel'];
            }
            $mysqli_result->close();
          }
          else {
            $this->Log('Reader->CheckFeedList 4: ' . $mysqli->error);
          }
          if ($us_channel !== '') {
            $this->SetChannel($id, $us_add_feed, $us_channel);
          }
        }
      }
    }
    if (count($us_remove_feed_list) > 0) {
      // The url stored in reader_subscribe may not be the feed url,
      // SimplePie uses discovery when processing a feed list.
      include_once 'library/Masterminds/HTML5.auto.php';
      include_once 'library/Mf2/Parser.php';
      include_once 'autoloader.php';
      include_once 'idn/idna_convert.class.php';
      $feed = new SimplePie();
      foreach ($us_remove_feed_list as $us_remove_feed) {
        $feed->set_feed_url($us_remove_feed);
        if (!$feed->init()) continue;

        $us_remove_feed = trim($feed->subscribe_url(), ' /');
        $query = 'DELETE FROM reader_subscribe WHERE ' .
          'subscribe_url = "' . $xml_url . '" AND ' .
          'xml_url = "' . $mysqli->escape_string($us_remove_feed) . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Reader->CheckFeedList 4: ' . $mysqli->error);
        }
        foreach ($subscribed_list as $subscribed) {
          $this->owner = $subscribed['user'];
          $id = (int)$subscribed['box_id'];
          $this->RemoveFeedForUser($id, $us_remove_feed, false, true);
        }
      }
    }
    $mysqli->close();
    $this->owner = $current_owner;
    return $updated || count($us_remove_feed_list) !== 0;
  }

  private function CheckFollowPost($us_post, $us_target) {
    list($this->user->page, $this->owner) = page_owner($us_target);
    $id = $this->Id();
    if ($id === 0) {
      return ['HTTP/1.1 400 Bad Request',
              'Sorry this account is not set up to handle follow posts.'];
    }

    // This function is called from Post->ProcessReceivedWebmention when a
    // webmention has been received from a page that contains a follow-of the
    // target provided here. This is used to signify that we should in turn
    // subscribe to their feed, so use the author's url to find the feed.
    $us_author = $us_post['author-url'];
    if ($us_author === '') {
      return ['HTTP/1.1 400 Bad Request',
              'Thanks for the follow! Sorry but a feed could not be found ' .
                'to follow by looking at the author of your post.'];
    }

    $us_source = $us_post['url'];
    $us_result = $this->AddFeed($id, false, $us_author, true);
    if (isset($us_result['error'])) {
      // If the error is that a feed is already subscribed to for this author,
      // then allow updating the reader_follow_posts table so that they can
      // use this post to remove their feed.
      $regex = '/^Already subscribed to: (.+)$/';
      if (preg_match($regex, $us_result['error'], $match)) {
        $us_xml_url = $match[1];
        $this->SaveFollowPost($id, $us_source, $us_xml_url);
      }
      return ['HTTP/1.1 400 Bad Request',
              'Thanks for the follow! ' . $us_result['error']];
    }
    if (!isset($us_result['add'])) {
      return ['HTTP/1.1 500 Internal Server Error',
              'Error checking follow post.'];
    }

    // Need to store the post url to check for unfollows, which are done by
    // sending a webmention from that url and having it return a status code
    // of 410.
    $us_xml_url = $us_result['add'];
    $this->SaveFollowPost($id, $us_source, $us_xml_url);
    // Create a notification for this user when a subscription is added.
    $description = 'Subscribed to <a href="' . $us_xml_url . '">' .
      $us_xml_url . '</a>';
    $content = ['author' => 'Reader', 'description' => $description,
                'category' => 'system', 'permalink' => '', 'feed' => '',
                'public' => 0];
    $notification = new Notification($this->user, $this->owner);
    $notification_id = new_module($this->user, $this->owner, 'notification',
                                  '', $notification->Group(), 'outside');
    $notification->SetContent($notification_id, $content);
    return ['HTTP/1.1 200 OK',
            'Thanks for the follow! Now subscribed to: ' . $us_xml_url];
  }

  private function CheckSubscriptions($us_xml_url) {
    $mysqli = connect_db();
    $xml_url = $mysqli->escape_string($us_xml_url);
    $query = 'SELECT xml_url FROM reader WHERE xml_url = "' . $xml_url . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      // Remove the feed when there is only one subscription, as it's about to
      // be removed too.
      if ($mysqli_result->num_rows === 1) {
        $this->RemoveFromTables($xml_url);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->CheckSubscriptions: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function CreateChannel($id, $us_name, $us_channel = '') {
    if ($us_channel === '') $us_channel = $us_name;
    if ($us_channel === 'all' || $us_channel === 'global') {
      return ['error' => '\'' . $us_channel . '\' is a reserved channel.'];
    }

    $owner = $this->SwitchOwner();
    $max_order = 0;
    $mysqli = connect_db();
    $query = 'SELECT MAX(channel_order) AS channel_order FROM ' .
      'reader_channel_info WHERE user = "' . $owner . '" AND box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($reader_channel_info = $mysqli_result->fetch_assoc()) {
        $max_order = (int)$reader_channel_info['channel_order'] + 1;
      }
    }
    else {
      $this->Log('Reader->CreateChannel 1: ' . $mysqli->error);
    }
    $channel = $mysqli->escape_string($us_channel);
    $name = $mysqli->escape_string($us_name);
    // Update the channel name on duplicate key.
    $query = 'INSERT INTO reader_channel_info VALUES ("' . $owner . '", ' .
      $id . ', "' . $channel . '", ' . $max_order . ', "true", ' . time() .
      ') ON DUPLICATE KEY UPDATE channel_name = "' . $name . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->CreateChannel 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['uid' => $us_channel, 'name' => $us_name];
  }

  private function CreateItemsTable($items_table = '') {
    $mysqli = connect_db();
    if ($items_table !== '') {
      $query = 'RENAME TABLE reader_items TO ' . $items_table;
      if (!$mysqli->query($query)) {
        $this->Log('Reader->CreateItemsTable 1: ' . $mysqli->error);
      }
    }

    $query = 'CREATE TABLE IF NOT EXISTS reader_items (' .
      'title TEXT,' .
      'content TEXT,' .
      'author VARCHAR(200),' .
      'category TEXT,' .
      'enclosure TEXT, ' .
      'permalink VARCHAR(300),' .
      'guid VARCHAR(200) NOT NULL,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'xml_url VARCHAR(300) NOT NULL,' .
      'PRIMARY KEY(guid(100), xml_url(100))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->CreateItemsTable 2: ' . $mysqli->error);
    }

    if ($items_table !== '') {
      // Now that a new table has been created and the old one renamed, update
      // the timestamps in the reader_items_table_names table so that the
      // reader_items table is used for new items and the old table is checked
      // after that.
      $query = 'UPDATE reader_items_table_names SET ' .
        'name = "' . $items_table .'" WHERE name = "reader_items"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->CreateItemsTable 3: ' . $mysqli->error);
      }
    }
    $query = 'INSERT INTO reader_items_table_names VALUES ("reader_items", ' .
      time() . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->CreateItemsTable 4: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function DiscoveredFeeds($xml_url, $author, $h_feed_list,
                                   $all_feeds, $microsub = false) {
    $results = [];
    $subscribed = [];
    if ($microsub) {
      // This is called via Microsub search, which also wants to know about
      // any feeds that match the given url.
      $domain = $xml_url;
      if (preg_match('/\/\/([^\/]+)/', $xml_url, $match)) $domain = $match[1];
      $mysqli = connect_db();
      $query = 'SELECT xml_url, description, feed_title, image_url FROM ' .
        'reader_feeds WHERE xml_url LIKE "%' . $domain . '%" OR ' .
        'html_url LIKE "%' . $domain . '%"';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($reader_feeds = $mysqli_result->fetch_assoc()) {
          $results[] = ['type' => 'feed', 'url' => $reader_feeds['xml_url'],
                        'name' => $reader_feeds['feed_title'],
                        'photo' => $reader_feeds['image_url'],
                        'description' => $reader_feeds['description']];
          $subscribed[] = $reader_feeds['xml_url'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->DiscoveredFeeds: ' . $mysqli->error);
      }
      $mysqli->close();
    }

    $discovered = '<p>Multiple feeds were found on this page.<br>' .
      'Please select the ones you would like to subscribe to:</p>';
    $i = 0;
    $j = 0;
    for ($i; $i < count($h_feed_list); $i++) {
      // Select the first feed by default, can also skip the first entry in
      // $all_feeds because that is the page this h-feed was found on.
      $checked = '';
      if ($i === 0) {
        $j = 1;
        $checked = ' checked="checked"';
      }
      // The id is just to link the label to the checkbox.
      $discovered_id = 'reader-discovered-feed-' . $i;
      $discovered_url = $xml_url;
      $short_url = $xml_url;
      $name = $h_feed_list[$i];
      // Use the author's name if provided and the current feed doesn't have
      // an explicit name. Otherwise give it a generic label.
      if ($name === '') {
        $discovered_title = $author === '' ? 'Main Feed' : $author;
      }
      else {
        $discovered_title = $name;
        $discovered_url .= '#' . urlencode($name);
        $short_url .= '#' . $name;
      }
      $short_url = strlen($short_url) <= 50 ? $short_url :
        substr($short_url, 0, 47) . '...';
      $discovered_link = '<a href="' . $discovered_url . '">' . $short_url .
        '</a>';
      if ($microsub && !in_array($discovered_url, $subscribed)) {
        $results[] = ['type' => 'feed', 'url' => $discovered_url,
                      'name' => $discovered_title];
      }
      else {
        $display = $discovered_title === '' ? $discovered_link :
          $discovered_title . ' <span class="reader-discovered-feed-url">[' .
          $discovered_link . ']</span>';
        $discovered .= '<div class="form-spacing">' .
            '<input type="checkbox" id="' . $discovered_id. '" ' .
              'value="' . $discovered_url . '"' . $checked . '>' .
            '<label for="' . $discovered_id . '">' . $display . '</label>' .
          '</div>';
      }
    }
    for ($j; $j < count($all_feeds); $j++) {
      // Select the first feed by default (if not checked by an h-feed).
      $checked = $j === 0 ? ' checked="checked"' : '';
      $discovered_id = 'reader-discovered-feed-' . ($i + $j);
      $discovered_url = $all_feeds[$j]->url;
      $short_url = strlen($discovered_url) <= 50 ?
        $discovered_url : substr($discovered_url, 0, 47) . '...';
      $discovered_link = '<a href="' . $discovered_url . '">' . $short_url .
        '</a>';
      $discovered_title = $this->FeedTitle($all_feeds[$j]->body);
      if ($microsub && !in_array($discovered_url, $subscribed)) {
        $results[] = ['type' => 'feed', 'url' => $discovered_url,
                      'name' => $discovered_title];
      }
      else {
        $display = $discovered_title === '' ? $discovered_link :
          $discovered_title . ' <span class="reader-discovered-feed-url">[' .
          $discovered_link . ']</span>';
        $discovered .= '<div class="form-spacing">' .
            '<input type="checkbox" id="' . $discovered_id . '" ' .
              'value="' . $discovered_url . '"' . $checked . '>' .
            '<label for="' . $discovered_id . '">' . $display . '</label>' .
          '</div>';
      }
    }
    if ($microsub) return ['results' => $results];

    $discovered .= '<button class="reader-discovered-cancel">cancel' .
      '</button><button class="reader-discovered-add">add</button>';
    return ['discovered' => $discovered];
  }

  private function FeedList($id = NULL) {
    $us_list = [];
    $mysqli = connect_db();
    $query = '';
    if (isset($id)) {
      $owner = $this->SwitchOwner();
      $query = 'SELECT xml_url FROM reader WHERE user = "' . $owner . '" ' .
        'AND box_id = ' . $id;
    }
    else {
      // Feeds that don't support last modified checks are set as "slow" and
      // only checked once a day.
      $slow_query = $this->Run('12am') ? '' : ' WHERE slow = 0';
      $query = 'SELECT xml_url FROM reader_feeds' . $slow_query;
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader = $mysqli_result->fetch_assoc()) {
        $us_list[] = $reader['xml_url'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->FeedList: ' . $mysqli->error);
    }
    $mysqli->close();
    return $us_list;
  }

  private function FeedSettings($id, $us_xml_url = '', $show_value = false) {
    $show_channels = isset($this->user->settings['reader']['showChannels']) ?
      $this->user->settings['reader']['showChannels'] === 'yes' : false;

    $value = $show_value ? ' value="' . $us_xml_url . '"' : '';
    $content = '<select id="reader-options">' .
      '<option value="add">All Feeds</option>';
    if ($show_channels) {
      $content .= '<option value="channels">Channel Feeds</option>';
    }
    $content .= '<option value="import">Import Feeds</option></select>' .
      '<div id="reader-feed-input-wrapper" class="form-spacing">' .
        '<label for="reader-feed-input">Add feed:</label>' .
        '<input id="reader-feed-input" type="text" maxlength="300"' .$value.'>'.
        '<button class="reader-add-feed">add</button>' .
      '</div>' .
      '<div id="reader-file-import-wrapper" class="form-spacing hidden">' .
        '<label for="reader-file-import">Select a file:</label>' .
        '<input id="reader-file-import" type="file">' .
      '</div>';
    if ($show_channels) {
      $options = $this->Channels($id);
      if ($options === '') {
        $content .= '<div id="reader-channel-wrapper" class="hidden">' .
          'Add a channel by clicking a channel button next to a feed.</div>';
      }
      else {
        $content .= '<div id="reader-channel-wrapper" ' .
              'class="form-spacing hidden">' .
            '<label for="reader-channel-show">Show channel:</label>' .
            '<select id="reader-channel-show">' .
              '<option selected="selected" value="">All feeds</option>' .
              $options .
            '</select>' .
            '<fieldset id="reader-channel-display" class="hidden">' .
              '<legend>Channel settings:</legend>' .
              '<div id="reader-channel-unread-wrapper">Display unread: ' .
                '<input type="radio" id="reader-channel-unread-true" ' .
                  'name="reader-channel-unread" value="true">' .
                '<label for="reader-channel-unread-true">true</label>' .
                '<input type="radio" id="reader-channel-unread-false" ' .
                  'name="reader-channel-unread" value="false">' .
                '<label for="reader-channel-unread-false">false</label>' .
                '<input type="radio" id="reader-channel-unread-count" ' .
                  'name="reader-channel-unread" value="count">' .
                '<label for="reader-channel-unread-count">count</label>' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="reader-channel-order">Change order:</label>' .
                '<input type="text" id="reader-channel-order">' .
              '</div>' .
              '<div class="form-spacing">' .
                '<label for="reader-channel-update">Change name:</label>' .
                '<input type="text" id="reader-channel-update">' .
              '</div>' .
              '<button id="reader-channel-display-remove">remove channel' .
              '</button>' .
              '<button id="reader-channel-display-submit">submit</button>' .
            '</fieldset>' .
          '</div>';
      }
    }
    $content .= '<div class="reader-feed-list">';

    $feed_list = [];
    $mysqli = connect_db();
    $owner = $this->SwitchOwner();
    $query = 'SELECT reader.xml_url, feed_title, channel FROM reader '.
      'LEFT JOIN reader_feeds ON reader.xml_url = reader_feeds.xml_url ' .
      'LEFT JOIN reader_channels ON reader.user = reader_channels.user AND ' .
      'reader.box_id = reader_channels.box_id AND ' .
      'reader.xml_url = reader_channels.xml_url WHERE ' .
      'reader.user = "' . $owner . '" AND reader.box_id = ' . $id .
      ' ORDER BY feed_title, author';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader = $mysqli_result->fetch_assoc()) {
        $url = $reader['xml_url'];
        // This is used to avoid showing duplicate feeds in the list because
        // of entries for specific authors in reader_channels table. These are
        // ignored here because entries are ordered by feed_title, author so
        // the channel for the feed without an author is listed first.
        if (in_array($url, $feed_list)) continue;

        $feed_list[] = $url;
        $short_url = strlen($url) <= 50 ? $url : substr($url, 0, 47) . '...';
        $title = $reader['feed_title'];
        $added = $us_xml_url === $url ? ' added' : '';
        $link = '<a class="feed-url" href="' . $url . '">' .
          urldecode($short_url) . '</a>';
        // TODO: If no channel button add a check now button, otherwise add
        // check now button to channel dialog.
        $channel_button = '';
        if ($show_channels) {
          $channel = htmlspecialchars($reader['channel']);
          if ($channel === '') $channel = 'not set';
          $channel_class = preg_replace('/[[:^alnum:]]/', '', $channel);
          $channel_button = '<button class="reader-channel-button ' .
            'reader-channel-' . $channel_class . '">' . $channel . '</button>';
        }
        $content .= '<div class="feed-item">' .
          '<button class="remove-feed-item">remove</button> ' .
          '<span class="feed-text' . $added . '">';
        if ($title === '') {
          $content .= $link . '</span>' . $channel_button . '</div>';
        }
        else {
          $content .= '<span class="feed-title">' . $title . '</span> ' .
            '<span class="feed-url-wrapper">[' . $link . ']</span></span>' .
            $channel_button . '</div>';
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->FeedSettings: ' . $mysqli->error);
    }
    $mysqli->close();
    // Close 'reader-feed-list' div.
    $content .= '</div>';
    return $content;
  }

  private function FeedTitle($content) {
    // First try parsing content for microformats.
    $h_feed = [];
    $mf = Mf2\parse($content);
    foreach ($mf['items'] as $mf_item) {
      if (!isset($mf_item['type'])) continue;

      if (in_array('h-feed', $mf_item['type'])) {
        $h_feed = $mf_item;
        break;
      }
      if (isset($mf_item['children'][0]['type']) &&
          in_array('h-feed', $mf_item['children'][0]['type'])) {
        $h_feed = $mf_item['children'][0];
        break;
      }
    }
    if (isset($h_feed['properties']['name'][0])) {
      return $h_feed['properties']['name'][0];
    }

    // Otherwise try parsing as xml.
    $xml = simplexml_load_string($content);
    if (isset($xml->channel->title)) return $xml->channel->title;

    return '';
  }

  private function Following($target) {
    $following = false;
    $mysqli = connect_db();
    $target = trim($target, ' /');
    $owner = $this->SwitchOwner();
    $query = 'SELECT user FROM reader LEFT JOIN reader_feeds ON ' .
      'reader.xml_url = reader_feeds.xml_url WHERE user = "' . $owner . '" ' .
      'AND (reader.xml_url = "' . $target . '" OR html_url = "' . $target .'")';
    if ($mysqli_result = $mysqli->query($query)) {
      $following = $mysqli_result->num_rows !== 0;
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->Following: ' . $mysqli->error);
    }
    $mysqli->close();
    return $following;
  }

  private function GetContent($id, $query, $microsub, $us_channel,
                              $update_first = false, $update_last = true) {
    $count = 0;
    $group_count = 0;
    $prev_class = '';
    $content = $microsub ? [] : '';
    $paging = [];
    $key = $this->owner . '-' . $this->user->page;
    $config =
      ['actions' => $this->user->canViewPage &&
         $this->AlreadyOnPage('writer') && $this->CanPublish(),
       'hide-titles' => $this->Substitute('reader-hide-titles') === 'true',
       'hide-settings' => $this->Substitute('reader-hide-settings') === 'true',
       'hide-permalinks' =>
         $this->Substitute('reader-hide-permalinks') === 'true'];
    // Store channel info before generating content so that unread status for
    // the current channel is also included.
    $owner = $this->SwitchOwner();
    // Microsub fetches channel info in a separate request.
    $info = $microsub ? '' : $this->ChannelInfo($owner, $id);

    $mysqli = connect_db();
    if ($mysqli_result = $mysqli->query($query)) {
      while ($item = $mysqli_result->fetch_assoc()) {
        if ($microsub) {
          $content[] = $this->GetItem($item, $prev_class, $config, $microsub);
        }
        else {
          list($class, $new_content) =
            $this->GetItem($item, $prev_class, $config, $microsub);
          if ($class === $prev_class) {
            $content .= mb_convert_encoding($new_content, 'UTF-8');
            $group_count++;
          }
          else {
            if ($group_count > 0) {
              // Add a link to show other items when more than one in a group.
              $content .= '<a href="#" class="show-group">Show ' .
                $group_count . ' more</a>';
            }
            // Close the previous group before adding the new item.
            if ($content !== '') $content .= '</div>';
            $content .= '<div class="reader-group">' .
              mb_convert_encoding($new_content, 'UTF-8');
            $prev_class = $class;
            $group_count = 0;
          }
        }
        $timestamp = (int)$item['timestamp'];
        if ($update_first) {
          if ($microsub) {
            $paging['before'] = $timestamp;
          }
          else if (isset($_SESSION['reader-first'])) {
            $_SESSION['reader-first'][$key] = $timestamp;
          }
          else {
            $_SESSION['reader-first'] = [$key => $timestamp];
          }
          $update_first = false;
          // Also set last read for this channel here, since when first is
          // true items are ordered by timestamp descending.
          $this->ChannelLastRead($id, $timestamp, $us_channel);
        }
        if ($update_last) {
          if ($microsub) {
            $paging['after'] = $timestamp;
          }
          else if (isset($_SESSION['reader-last'])) {
            $_SESSION['reader-last'][$key] = $timestamp;
          }
          else {
            $_SESSION['reader-last'] = [$key => $timestamp];
          }
        }
        $count++;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->GetContent: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($microsub) {
      return [['items' => $content, 'paging' => $paging], '', $count];
    }

    if ($group_count > 0) {
      $content .= '<a href="#" class="show-group">Show ' . $group_count.
        ' more</a>';
    }
    // Close the final group of items.
    if ($content !== '') $content .= '</div>';
    return [$content, $info, $count];
  }

  private function GetFeed($id, $us_update = 'newer', $microsub = false,
                           $items_table = 'reader_items', $item_count = 0,
                           $us_channel = '', $first = 0, $last = 0) {
    if ($us_channel === '' && isset($_POST['channel'])) {
      $us_channel = $_POST['channel'];
    }

    $mysqli = connect_db();
    // Create a query for this id, if channels are not used check feed list.
    $xml_url_query = $this->ChannelQuery($id, $items_table, $us_channel);
    if ($xml_url_query === '') {
      foreach ($this->FeedList($id) as $us_xml_url) {
        if ($xml_url_query !== '') $xml_url_query .= ' OR ';
        $xml_url_query .= $items_table . '.xml_url = ' .
          '"' . $mysqli->escape_string($us_xml_url) . '"';
      }
    }
    $mysqli->close();
    if ($xml_url_query === '') return ['content' => '', 'more' => false];

    // The maximum number of items to return.
    if ($item_count === 0) {
      $item_count = (int)$this->Substitute('reader-item-count');
      if ($item_count === 0) $item_count = 10;
    }
    $key = $this->owner . '-' . $this->user->page;
    // The same fields are selected in each of the queries used below.
    $select_query = 'SELECT title, content, author, category, enclosure, ' .
      'permalink, guid, timestamp, ' . $items_table . '.xml_url, feed_title, ' .
      'html_url, image_url, image_title, image_link FROM ' . $items_table .
      ' LEFT JOIN reader_feeds ON ' .
      $items_table . '.xml_url = reader_feeds.xml_url';

    if ($us_update === 'newer') {
      if (!$microsub && isset($_SESSION['reader-first'][$key])) {
        $first = $_SESSION['reader-first'][$key];
      }
      // This will run the first time a page is loaded.
      if ($first === 0) {
        $query = $select_query . ' WHERE ' . $xml_url_query .
          ' ORDER BY timestamp DESC LIMIT ' . $item_count;
        list($content, $info, $count) =
          $this->GetContent($id, $query, $microsub, $us_channel, true);
        if ($count >= $item_count) {
          // There are more items if at least $item_count items were found.
          return ['content' => $content, 'info' => $info, 'more' => true];
        }

        // Otherwise call GetFeed again with an older reader_items table
        // and update the item_count required.
        $item_count -= $count;
        $older_table = $this->ItemsTableNames(false, $items_table);
        if ($older_table === '') {
          return ['content' => $content, 'info' => $info, 'more' => false];
        }

        // If content already found then first is already set, otherwise it's
        // ok to set it from an older items table and try 'newer' again.
        $update = $count === 0 ? 'newer' : 'older';
        // For Microsub need to pass on the value for last that was just set.
        if ($update === 'older' && isset($content['paging']['after'])) {
          $last = $content['paging']['after'];
        }
        $older = $this->GetFeed($id, $update, $microsub, $older_table,
                                $item_count, $us_channel, 0, $last);
        if ($microsub) {
          $content['items'] =
            array_merge($content['items'], $older['content']['items']);
          $content['paging'] = $older['content']['paging'];
        }
        else {
          $content .= $older['content'];
        }
        return ['content' => $content, 'info' => $info,
                'more' => $older['more']];
      }

      // This will run when the reader module is notified of newer content.
      $query = $select_query . ' WHERE timestamp > ' . $first .
        ' AND (' . $xml_url_query . ') ORDER BY timestamp DESC';
      list($content, $info, $count) =
        $this->GetContent($id, $query, $microsub, $us_channel, true, false);
      if ($count >= $item_count) {
        return ['content' => $content, 'info' => $info, 'more' => true];
      }

      $item_count -= $count;
      $older_table = $this->ItemsTableNames(false, $items_table);
      if ($older_table === '') {
        return ['content' => $content, 'info' => $info, 'more' => false];
      }

      $older = $this->GetFeed($id, 'newer', $microsub, $older_table,
                              $item_count, $us_channel);
      if ($microsub) {
        $content['items'] =
          array_merge($content['items'], $older['content']['items']);
        $content['paging'] = $older['content']['paging'];
      }
      else {
        $content .= $older['content'];
      }
      return ['content' => $content, 'info' => $info, 'more' => $older['more']];
    }

    if ($us_update === 'older') {
      if (!$microsub && isset($_SESSION['reader-last'][$key])) {
        $last = $_SESSION['reader-last'][$key];
      }
      // This will run when the user requests older content, which means
      // last should already be set.
      if ($last === 0) {
        return ['error' => 'There was a problem loading more items.'];
      }

      $query = $select_query . ' WHERE timestamp < ' . $last . ' AND ' .
        '(' . $xml_url_query . ') ORDER BY timestamp DESC LIMIT ' . $item_count;
      list($content, $info, $count) =
        $this->GetContent($id, $query, $microsub, $us_channel);
      if ($count >= $item_count) {
        return ['content' => $content, 'info' => $info, 'more' => true];
      }

      $item_count -= $count;
      $older_table = $this->ItemsTableNames(false, $items_table);
      if ($older_table === '') {
        return ['content' => $content, 'info' => $info, 'more' => false];
      }

      $older = $this->GetFeed($id, 'older', $microsub, $older_table,
                              $item_count, $us_channel, 0, $last);
      if ($microsub) {
        $content['items'] =
          array_merge($content['items'], $older['content']['items']);
      }
      else {
        $content .= $older['content'];
      }
      return ['content' => $content, 'info' => $info, 'more' => $older['more']];
    }
  }

  private function GetItem($item, $prev_class, $config, $microsub = false) {
    $permalink = $item['permalink'];
    // Look for a repost url to match the original author.
    $check = $permalink;
    $regex = '/<span class="repost-of"><\/span> <a href="([^"]+)">/';
    if (preg_match($regex, $item['content'], $match)) {
      $check = $match[1];
    }
    $repost_author = '';
    $original_author = $item['author'];
    if (strpos($original_author, ',') !== false) {
      list($original_author, $repost_author) = explode(',', $original_author);
    }
    list($author_name, $author_url, $author_photo) =
      $this->LookupNickname($original_author, $check);
    // If author name or photo wasn't found try using the feed title or image.
    if ($author_name === '') {
      $author_name = $item['feed_title'];
    }
    // If no title use the feed url.
    if ($author_name === '') {
      $author_name = $item['html_url'];
    }
    // And then also simplify how urls are presented.
    if (stripos($author_name, 'http') === 0) {
      $regex = '/^https?:\/\/([^\/]+\/?[^\/]*)/i';
      if (preg_match($regex, $author_name, $match)) {
        $author_name = trim($match[1], ' /');
      }
    }
    if ($author_photo === '') {
      $author_photo = $item['image_url'];
    }
    $category_list = json_decode($item['category'], true);
    $enclosure_list = json_decode($item['enclosure'], true);
    if ($microsub) {
      return ['type' => 'entry',
              'published' => date(DATE_ATOM, $item['timestamp']),
              'url' => $permalink, 'uid' => $item['guid'],
              'author' => ['type' => 'card', 'name' => $author_name,
                           'url' => $author_url, 'photo' => $author_photo],
              'name' => $item['title'],
              'category' => $category_list,
              'photo' => $enclosure_list['photo'],
              'audio' => $enclosure_list['audio'],
              'video' => $enclosure_list['video'],
              'content' => ['html' => $item['content']], '_id' => 'home'];
    }

    $media = '';
    $title = $item['title'];
    $content = content_markup($item['content']);
    if (isset($enclosure_list['photo'][0])) {
      $photo_list = [];
      foreach ($enclosure_list['photo'] as $photo) {
        if (strpos($content, $photo) === false) $photo_list[] = $photo;
      }
      if (count($photo_list) === 0) {
        // All photos were found to be duplicated in content, so assume this
        // is a photo post and display the lightbox. Add a special wrapper
        // around the existing content so those photos can be hidden.
        $content = '<div class="photo-hidden">' . $content . '</div>';
        $photo_list = $enclosure_list['photo'];
      }
      $count = count($photo_list);
      if ($count > 1) {
        // When there's more than one photo show the first two and use a
        // lightbox. Need a permanent, unique name for the image set, but
        // don't have anything unique except for photo urls, so use that.
        $image_set_id = preg_replace('/[[:^alnum:]]/', '', $photo_list[0]);
        $media .= '<p class="photo-list">';
        for ($i = 0; $i < $count; $i++) {
          $hidden = $i <= 1 ? '' : 'class="hidden" ';
          $media .= '<a href="' . $photo_list[$i] . '" ' . $hidden .
            'data-lightbox="image-set-' . $image_set_id . '">' .
            '<img src="' . $photo_list[$i] . '"></a>';
        }
        $media .= '<br><b>' . $count . ' photos</b></p>';
      }
      else if ($count === 1) {
        $media .= '<p><img src="' . $photo_list[0] . '"></p>';
      }
    }
    if (isset($enclosure_list['audio'][0])) {
      // Don't embed media if secure is true but content is over http.
      $secure = $this->user->config->Secure();
      foreach ($enclosure_list['audio'] as $audio) {
        $media .= $secure && strpos($audio, 'http://') === 0 ?
          '<a href="' . $audio . '">' . $audio . '</a>' :
          '<audio controls src="' . $audio . '"></audio>';
      }
    }
    if (isset($enclosure_list['video'][0])) {
      $secure = $this->user->config->Secure();
      foreach ($enclosure_list['video'] as $video) {
        $media .= $secure && strpos($video, 'http://') === 0 ?
          '<a href="' . $video . '">' . $video . '</a>' :
          '<video controls src="' . $video . '"></video>';
      }
    }

    $author = '';
    if ($author_photo !== '') {
      $author .= '<a href="' . $author_url . '" class="author-photo-url">' .
        '<img class="thumb" src="' . $author_photo . '"></a> ';
    }
    $author .= '<a href="' . $author_url . '" class="author-name">' .
      $author_name . '</a>';
    $repost_url = '';
    if ($repost_author !== '') {
      list($repost_name, $repost_url, $repost_photo) =
        $this->LookupNickname($repost_author, $permalink);
      $author .= '<span class="reader-reposted-by">reposted by ' .
        '<a href="' . $repost_url . '">' . $repost_name . '</a></span>';
    }

    $category = '';
    if (is_array($category_list)) {
      for ($i = 0; $i < count($category_list); $i++) {
        if ($category_list[$i] === 'Reposted from') {
          // Skip repost tags which are added by the Post module, the next tag
          // is the repost-of url which is already displayed in the content.
          $i++;
          continue;
        }
        $category .= category_markup($category_list[$i]);
      }
    }
    if ($category !== '') {
      $category = '<span class="reader-tag-label ui-icon ui-icon-tag" ' .
        'title="tags"></span> ' . $category;
    }

    $date = date('j F g:ia', $item['timestamp']);
    if (!$config['hide-permalinks']) {
      $date = '<a href="' . $permalink . '" class="permalink">' . $date .'</a>';
    }
    // If the content is too long, show an excerpt and add a link so that
    // the real content can be expanded. Also sometimes the content isn't
    // overly long it's just full of markup so strip tags first.
    $start = strip_tags($content);
    if (strlen($start) > 600) {
      $content = '<span class="content-excerpt">' . substr($start, 0, 400) .
        '... <a href="#" class="read-more">read more</a></span>' .
        '<span class="real-content hidden">' . $content . '</span>';
    }
    $group_header = '';
    $group_link = $item['html_url'];
    // If the group link domain doesn't match the author url domain add it to
    // the author, as it's possible the group header is not shown. Special case
    // twitter and facebook atom feeds don't need to show via.
    if ($group_link !== 'https://twitter-atom.appspot.com' &&
        $group_link !== 'https://facebook-atom.appspot.com') {
      $group_link_domain = $group_link;
      if (preg_match('/\/\/([^\/]+)/', $group_link_domain, $match)) {
        $group_link_domain = $match[1];
        // Drop subdomains too.
        if (preg_match('/([^\.]+\.[^\.]+)$/', $group_link_domain, $match)) {
          $group_link_domain = $match[1];
        }
      }
      // Use the repost url when available as it should match the group link.
      $author_url_domain = $repost_url === '' ? $author_url : $repost_url;
      if (preg_match('/\/\/([^\/]+)/', $author_url_domain, $match)) {
        $author_url_domain = $match[1];
        if (preg_match('/([^\.]+\.[^\.]+)$/', $author_url_domain, $match)) {
          $author_url_domain = $match[1];
        }
      }
      if ($group_link_domain !== $author_url_domain) {
        $via = $item['feed_title'];
        if ($via === '') $via = $group_link_domain;
        $author .= ' <span class="reader-via-url">via ' .
          '<a href="' . $group_link . '">' . $via . '</a></span>';
      }
    }

    // Allow feed items to be grouped by their url by setting a matching
    // class name per item.
    $class = 'no-url-match';
    if (preg_match('/\/\/(.+)$/', $group_link, $match)) {
      $class = preg_replace('/[[:^alnum:]]/', '-', $match[1]);
    }
    $hidden = '';
    if ($class === $prev_class) {
      $hidden = ' hidden';
    }
    else {
      // When the group has changed add a new group header.
      $group_header = '<h3 class="group-title">';
      $group_image_url = $item['image_url'];
      if ($group_image_url !== '') {
        $group_image = '<img src="' . $group_image_url . '" ' .
          'title="' . $item['image_title'] . '">';
        $group_image_link = $item['image_link'];
        if ($group_image_link !== '') {
          $group_header .= '<a href="' . $group_image_link . '">' .
            $group_image . '</a>';
        }
        else {
          $group_header .= $group_image;
        }
      }
      $group_header .= '<a class="reader-group-link" ' .
        'href="' . $group_link . '">' . $item['feed_title'] . '</a></h3>';
    }
    $reader_actions = '';
    if ($config['actions']) {
      $reader_actions = '<div class="reader-actions">' .
          '<indie-action do="like" with="' . $permalink . '">' .
            '<a href="#" class="like">' .
              '<span class="ui-icon ui-icon-star"></span>like</a>' .
          '</indie-action>' .
          '<indie-action do="repost" with="' . $permalink . '">' .
            '<a href="#" class="share">' .
              '<span class="ui-icon ui-icon-refresh"></span>share</a>' .
          '</indie-action>' .
          '<indie-action do="reply" with="' . $permalink . '">' .
            '<a href="#" class="reply">' .
              '<span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>' .
                'reply</a>' .
          '</indie-action>' .
        '</div>';
    }
    else if ($permalink !== '' && !$config['hide-settings']) {
      // Show indie-actions when actions can't be performed on the current page.
      $reader_actions = '<div class="reader-web-actions">' .
        '<indie-action do="like" with="' . $permalink . '">' .
          '<a href="#" class="action">' .
            '<span class="ui-icon ui-icon-star"></span>like</a>' .
        '</indie-action>' .
        '<indie-action do="repost" with="' . $permalink . '">' .
          '<a href="#" class="action">' .
            '<span class="ui-icon ui-icon-refresh"></span>share</a>' .
        '</indie-action>' .
        '<indie-action do="reply" with="' . $permalink . '">' .
          '<a href="#" class="action">' .
            '<span class="ui-icon ui-icon-arrowreturnthick-1-w"></span>' .
            'reply</a>' .
        '</indie-action>' .
        '<span class="indie-config-info">' .
          'Want to share this? Click to choose a site:</span>' .
        '<a href="#" class="indie-config" title="web action settings">' .
          '<span class="ui-icon ui-icon-gear"></span>settings</a></div>';
    }

    $reader_format = $title === '' || $config['hide-titles'] ?
      'reader-format-no-title' : 'reader-format-with-title';
    $patterns = ['/!title/', '/!content/', '/!author/', '/!category/',
                 '/!date/', '/!media/'];
    $replacements = [$title, $content, $author, $category, $date, $media];
    $reader_item = $group_header .
      '<div class="reader-item ' . $class . $hidden . '">' .
        $this->Substitute($reader_format, $patterns, $replacements) .
        $reader_actions .
      '</div>';
    return [$class, $reader_item];
  }

  private function Id() {
    $id = 0;
    $mysqli = connect_db();
    $query = 'SELECT box_id FROM modules WHERE label = "reader" AND ' .
      'user = "' . $this->owner . '" AND ' .
      'page = "' . $this->user->page . '" AND deleted = 0';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($modules = $mysqli_result->fetch_assoc()) {
        $id = (int)$modules['box_id'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->Id: ' . $mysqli->error);
    }
    $mysqli->close();
    return $id;
  }

  private function ImportFile() {
    $max_file_size = $this->user->config->MaxFileSize();
    // ['upload']['size'] is given in bytes, MaxFileSize is in megabytes.
    if ($_FILES['file']['size'] > $max_file_size * 1000000) {
      return ['error' => 'Upload file is too large. (max '.$max_file_size.'M)'];
    }
    if (is_uploaded_file($_FILES['file']['tmp_name'])) {
      $xml = simplexml_load_file($_FILES['file']['tmp_name']);
      if (isset($xml->body->outline)) {
        $feed_list = [];
        foreach ($xml->body->outline as $outline) {
          // Check for nested outlines.
          if (isset($outline->outline)) {
            foreach ($outline->outline as $feed) {
              if (isset($feed['xmlUrl'])) {
                $entry = ['xmlUrl' => (string)$feed['xmlUrl']];
                if (isset($feed['title'])) {
                  $entry['title'] = htmlspecialchars((string)$feed['title']);
                }
                if (isset($feed['htmlUrl'])) {
                  $entry['htmlUrl'] = (string)$feed['htmlUrl'];
                }
                $feed_list[] = $entry;
              }
            }
          }
          else if (isset($outline['xmlUrl'])) {
            $entry = ['xmlUrl' => (string)$outline['xmlUrl']];
            if (isset($outline['title'])) {
              $entry['title'] = htmlspecialchars((string)$outline['title']);
            }
            if (isset($outline['htmlUrl'])) {
              $entry['htmlUrl'] = (string)$outline['htmlUrl'];
            }
            $feed_list[] = $entry;
          }
        }
        if (count($feed_list) > 0) return $feed_list;
      }
      return ['error' => 'No feeds found in imported file.'];
    }
    else {
      return ['error' => 'File: ' . basename($_FILES['file']['name']) .
                           ' was not uploaded.'];
    }
  }

  private function ItemsTableNames($array = true,
                                   $items_table = '', $timestamp = 0) {
    $items_table_names = [];
    $items_table_times = [];
    $mysqli = connect_db();
    $query = 'SELECT name, timestamp FROM reader_items_table_names ORDER BY ' .
      'timestamp DESC';
    if (!$array && $items_table === '' && $timestamp === 0) {
      $query .= ' LIMIT 1';
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader_items_table_names = $mysqli_result->fetch_assoc()) {
        $items_table_names[] = $reader_items_table_names['name'];
        $items_table_times[] = (int)$reader_items_table_names['timestamp'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->ItemsTableNames: ' . $mysqli->error);
    }
    $mysqli->close();

    // timestamp is provided by SaveItem so that older items are placed in the
    // correct items table.
    if ($timestamp !== 0) {
      $items_table = '';
      for ($i = 0; $i < count($items_table_names); $i++) {
        $items_table = $items_table_names[$i];
        if ($items_table_times[$i] < $timestamp) return $items_table;
      }
      // Use the oldest items table available if it was created after the
      // timestamp for this item.
      return $items_table;
    }
    if ($items_table === '') {
      return $array ? $items_table_names : $items_table_names[0];
    }
    // When items_table is provided, return the next table listed.
    for ($i = 0; $i < count($items_table_names) - 1; $i++) {
      if ($items_table_names[$i] === $items_table) {
        return $items_table_names[$i + 1];
      }
    }
    // This case is checked for by GetFeed to stop looking for more tables.
    return '';
  }

  private function ListChannels($id, $info_only = false) {
    $owner = $this->SwitchOwner();
    // Need to be viewing your own feed to list channels.
    if ($owner !== $this->user->name) return ['done' => true];

    $info = [];
    $mysqli = connect_db();
    $query = 'SELECT channel_name, channel_order, display_unread FROM ' .
      'reader_channel_info WHERE user = "' . $owner . '" AND box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader_channel_info = $mysqli_result->fetch_assoc()) {
        $info[$reader_channel_info['channel_name']] =
          ['order' => $reader_channel_info['channel_order'],
           'unread' => $reader_channel_info['display_unread']];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->ListChannels 1: ' . $mysqli->error);
    }
    // Make sure empty array has the right type for json.
    if (count($info) === 0) $info = (object)[];
    if ($info_only) {
      $mysqli->close();
      return $info;
    }

    $author = [];
    $query = 'SELECT author, html_url, channel FROM reader_channels WHERE ' .
      'user = "' . $owner . '" AND box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader_channels = $mysqli_result->fetch_assoc()) {
        $us_author = $reader_channels['author'];
        $us_feed = $reader_channels['html_url'];
        if ($us_author === '') {
          // Return generic channels too as they will be used as the default
          // if a channel isn't set for the author.
          $author[$us_feed] = $reader_channels['channel'];
        }
        else {
          $author[$us_author . ' ' . $us_feed] = $reader_channels['channel'];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->ListChannels 2: ' . $mysqli->error);
    }
    // Also want to return feed list information here, as the channel dialog
    // is used to display extra information for them.
    $lists = [];
    $reader_subscribe_query = '';
    $query = 'SELECT DISTINCT subscribe_url FROM reader_subscribe';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader_subscribe = $mysqli_result->fetch_assoc()) {
        if ($reader_subscribe_query !== '') {
          $reader_subscribe_query .= ' OR ';
        }
        $reader_subscribe_query .= 'xml_url = "' .
          $mysqli->escape_string($reader_subscribe['subscribe_url']) . '"';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->ListChannels 3: ' . $mysqli->error);
    }
    if ($reader_subscribe_query !== '') {
      // Get the subset of lists that this user is following.
      $reader_query = '';
      $query = 'SELECT xml_url FROM reader WHERE user = "' . $owner . '" AND ' .
        'box_id = ' . $id . ' AND (' . $reader_subscribe_query . ')';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($reader = $mysqli_result->fetch_assoc()) {
          if ($reader_query !== '') {
            $reader_query .= ' OR ';
          }
          $reader_query .= 'subscribe_url = "' .
            $mysqli->escape_string($reader['xml_url']) . '"';
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->ListChannels 4: ' . $mysqli->error);
      }
      if ($reader_query !== '') {
        // Now know which lists to show, but also need to know the current auto
        // settings for the user's feeds. This means fetching them all so that
        // they can be added with the correct list.
        $auto = [];
        $query = 'SELECT xml_url, auto FROM reader WHERE ' .
          'user = "' . $owner . '" AND box_id = ' . $id;
        if ($mysqli_result = $mysqli->query($query)) {
          while ($reader = $mysqli_result->fetch_assoc()) {
            $auto[$reader['xml_url']] = $reader['auto'] === '1';
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Reader->ListChannels 5: ' . $mysqli->error);
        }
        $query = 'SELECT subscribe_url, xml_url FROM reader_subscribe ' .
          ' WHERE ' . $reader_query;
        if ($mysqli_result = $mysqli->query($query)) {
          while ($reader_subscribe = $mysqli_result->fetch_assoc()) {
            $us_subscribe_url = $reader_subscribe['subscribe_url'];
            if (!isset($lists[$us_subscribe_url])) {
              $lists[$us_subscribe_url] = [];
            }
            $us_xml_url = $reader_subscribe['xml_url'];
            $lists[$us_subscribe_url][] = ['feed' => $us_xml_url,
                                           'auto' => $auto[$us_xml_url]];
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Reader->ListChannels 6: ' . $mysqli->error);
        }
      }
    }
    $mysqli->close();

    if (count($author) === 0) $author = (object)[];
    if (count($lists) === 0) $lists = (object)[];
    return ['info' => $info, 'author' => $author, 'lists' => $lists];
  }

  private function ManualAdd($id) {
    $owner = $this->SwitchOwner();

    $mysqli = connect_db();
    $feed = $mysqli->escape_string($_POST['feed']);
    $query = 'UPDATE reader SET auto = 0 WHERE user = "' . $owner . '" AND ' .
      'box_id = ' . $id . ' AND xml_url = "' . $feed . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->ManualAdd: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function Microsub() {
    $id = 0;
    $mysqli = connect_db();
    $query = 'SELECT DISTINCT box_id FROM reader WHERE ' .
      'user = "' . $this->owner . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($reader = $mysqli_result->fetch_assoc()) {
        $id = (int)$reader['box_id'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->Microsub: ' . $mysqli->error);
    }
    $mysqli->close();

    /////////////
    // GET action
    /////////////
    $get_action = isset($_GET['action']) ? $_GET['action'] : '';
    if ($get_action === 'channels') {
      $default_channels = [['uid' => 'notifications', 'name' => 'Notifications',
                            'unread' => false],
                           ['uid' => 'all', 'name' => 'all',
                            'unread' => false]];
      if ($id === 0) return ['channels' => $default_channels];
      return $this->Channels($id, true);
    }

    if ($get_action === 'timeline') {
      if ($id === 0) return ['items' => []];

      $us_channel = isset($_GET['channel']) ? $_GET['channel'] : '';
      $first = isset($_GET['before']) ? (int)$_GET['before'] : 0;
      $last = isset($_GET['after']) ? (int)$_GET['after'] : 0;
      $update = $last === 0 ? 'newer' : 'older';
      $result = $this->GetFeed($id, $update, true, 'reader_items',
                               0, $us_channel, $first, $last);
      if (isset($result['content']['items'])) {
        return ['items' => $result['content']['items'],
                'paging' => $result['content']['paging']];
      }
      return ['items' => []];
    }

    if ($get_action === 'follow') {
      $us_channel = isset($_GET['channel']) ? $_GET['channel'] : '';
      // TODO: this doesn't work because 'all' is not a reserved channel...
      if ($us_channel === '' || $us_channel === 'all') return ['items' => []];
      return $this->ChannelQuery($id, '', $us_channel, true);
    }

    //////////////
    // POST action
    //////////////
    $post_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($post_action === 'search') {
      $us_query = isset($_POST['query']) ? $_POST['query'] : '';
      // Don't allow fragments in search term as AddFeed uses that to identify
      // a specific feed on the page.
      if (preg_match('/^([^#]*)#/', $us_query, $match)) $us_query = $match[1];
      if ($us_query === '') return ['results' => []];
      return $this->AddFeed($id, false, $us_query, false, false, true, true);
    }

    if ($post_action === 'follow') {
      $us_channel = isset($_POST['channel']) ? $_POST['channel'] : '';
      $us_author = isset($_POST['author']) ? $_POST['author'] : '';
      $us_url = isset($_POST['url']) ? $_POST['url'] : '';
      if ($us_channel === '' || $us_channel === 'all' || $us_url === '') {
        return ['items' => []];
      }
      $this->AddFeed($id, false, $us_url, true, true, true);
      $this->SetChannel($id, $us_url, $us_channel, $us_author);
      return $this->ChannelQuery($id, '', $us_channel, true);
    }

    if ($post_action === 'unfollow') {
      $us_channel = isset($_POST['channel']) ? $_POST['channel'] : '';
      $us_url = isset($_POST['url']) ? $_POST['url'] : '';
      if ($us_channel === '' || $us_channel === 'all' || $us_url === '') {
        return ['items' => []];
      }
      $this->RemoveFeedForUser($id, $us_url, false);
      return $this->ChannelQuery($id, '', $us_channel, true);
    }

    if ($post_action === 'channels') {
      $us_name = isset($_POST['name']) ? $_POST['name'] : '';
      $us_channel = isset($_POST['channel']) ? $_POST['channel'] : '';
      $us_method = isset($_POST['method']) ? $_POST['method'] : '';
      if ($us_method === '') {
        $this->CreateChannel($id, $us_channel, $us_name);
      }
      else if ($us_method === 'delete') {
        $this->RemoveChannel($id, $us_channel);
      }
      else if ($us_method === 'order') {
        $us_channels =
          isset($_POST['channels']) && is_array($_POST['channels']) ?
          $_POST['channels'] : [];
        $this->UpdateChannelOrder($id, $us_channels);
      }
    }
  }

  private function NotifyOthers($us_xml_url, $us_data) {
    $scheme = $this->user->config->Secure() ? 'https://' : 'http://';
    $url = $scheme . $this->user->config->ServerName();
    $curl_headers = ['Content-Type: text/html; charset=UTF-8',
                     'Link: <' . $url . $this->Url() . '>; rel="self", ' .
                           '<' . $url . '/php/cloud.php>; rel="hub"'];
    $us_domain = substr($us_xml_url, strpos($us_xml_url, '//') + 2);

    $mysqli = connect_db();
    $domain = $mysqli->escape_string($us_domain);
    $query = 'SELECT register, hub FROM reader_notify WHERE ' .
      'xml_url LIKE "%' . $domain . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($notify = $mysqli_result->fetch_assoc()) {
        $ch = curl_init($notify['register']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        if ($notify['hub'] === '1') {
          curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $us_data);
        }
        else {
          curl_setopt($ch, CURLOPT_POSTFIELDS, 'url=' . urlencode($us_xml_url));
        }
        $this->Log('Reader->NotifyOthers 1: curl ' . $notify['register']);
        curl_exec($ch);
        curl_close($ch);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->NotifyOthers 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function NotifyUsers($us_updated) {
    $mysqli = connect_db();
    $xml_url_query = '';
    if (is_array($us_updated)) {
      $count = count($us_updated);
      if ($count === 0) return;

      for ($i = 0; $i < count($us_updated); $i++) {
        if ($xml_url_query !== '') {
          $xml_url_query .= ' OR ';
        }
        // The scheme provided in the updated url depends on the server
        // configuration, which may not be the same as the url chosen by the
        // user, so check for all entries that match the domain and path.
        $us_domain = substr($us_updated[$i], strpos($us_updated[$i], '//') + 2);
        $domain = $mysqli->escape_string($us_domain);
        $xml_url_query .= 'xml_url LIKE "%' . $domain . '"';
      }
    }
    else {
      $us_domain = substr($us_updated, strpos($us_updated, '//') + 2);
      $domain = $mysqli->escape_string($us_domain);
      $xml_url_query = 'xml_url LIKE "%' . $domain . '"';
    }
    // Clients will update their feeds when the notify timestamp changes, so
    // need to go through the reader table and find matches for xml_url_query.
    $query = 'SELECT reader.user, reader.box_id, page FROM reader LEFT JOIN ' .
      'modules ON reader.user = modules.user AND reader.box_id = ' .
      'modules.box_id WHERE label = "reader" AND (' . $xml_url_query . ')';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader = $mysqli_result->fetch_assoc()) {
        $id = $reader['box_id'];
        $user = $reader['user'];
        $page = $reader['page'];
        // Notify entries for reader are always private as the module doesn't
        // produce a feed itself.
        $this->Notify($id, 'reader', 'feed', $page, 0, true, $user);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->NotifyUsers 1: ' . $mysqli->error);
    }
    $page = $this->Substitute('indieauth-page');
    // Indieauth users also need to be notified, but don't need to find the
    // page the reader module is on because it's pre-configured for them. The
    // difference from the above query is that the admin user owns the module.
    $query = 'SELECT reader.user, reader.box_id FROM reader LEFT JOIN modules '.
      'ON reader.box_id = modules.box_id WHERE modules.user = "admin" AND ' .
      'page = "' . $page . '" AND (' . $xml_url_query . ')';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader = $mysqli_result->fetch_assoc()) {
        $id = $reader['box_id'];
        $user = $reader['user'];
        // Notify entries for reader are always private as the module doesn't
        // produce a feed itself.
        $this->Notify($id, 'reader', 'feed', $page, 0, true, $user);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->NotifyUsers 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function ProcessFeedList($content) {
    $feed_list = [];
    // First try parsing content for microformats, needs to find an h-feed
    // otherwise might end up just matching a single h-card.
    $doc = new DOMDocument();
    @$doc->loadHTML($content);
    $xpath = new DOMXpath($doc);
    $query = '//*[contains(concat(" ", @class, " "), " h-feed ")]';
    $result = $xpath->query($query);
    if ($result->length > 0) {
      $h_feed = [];
      $mf = Mf2\parse($content);
      foreach ($mf['items'] as $mf_item) {
        if (!isset($mf_item['type'])) continue;

        if (in_array('h-feed', $mf_item['type'])) {
          $h_feed = $mf_item;
          break;
        }
        else if (isset($mf_item['children'][0]['type']) &&
                 in_array('h-feed', $mf_item['children'][0]['type'])) {
          $h_feed = $mf_item['children'][0];
          break;
        }
      }
      if (isset($h_feed['children'])) {
        foreach ($h_feed['children'] as $entry) {
          if (in_array('h-card', $entry['type'])) {
            if (isset($entry['properties']['url'][0])) {
              $feed_list[] = trim($entry['properties']['url'][0], ' /');
            }
          }
        }
        // One h-card on an h-feed does not count as a feed list.
        if (count($feed_list) <= 1) return [];
      }
    }

    // Otherwise try parsing as xml.
    if (count($feed_list) === 0) {
      $xml = simplexml_load_string($content);
      if (isset($xml->body->outline)) {
        foreach ($xml->body->outline as $outline) {
          // Check for nested outlines.
          if (isset($outline->outline)) {
            foreach ($outline->outline as $feed) {
              if (isset($feed['xmlUrl'])) {
                $feed_list[] = trim((string)$feed['xmlUrl'], ' /');
              }
            }
          }
          else if (isset($outline['xmlUrl'])) {
            $feed_list[] = trim((string)$outline['xmlUrl'], ' /');
          }
        }
      }
    }
    return $feed_list;
  }

  private function PurgeCache($count, $cache_dir) {
    $total = 0;
    // Set the number of files to check based on $count.
    $head = '100';
    if ($count > 500) $head = '5000';
    else if ($count > 100) $head = '1000';
    // Convert $count to bytes to match strlen result.
    $count *= 1000000;
    $handle = popen('/bin/ls -1ur ' . $cache_dir .
                      ' | /bin/grep spi | /usr/bin/head -n ' . $head, 'r');
    while ($file = fgets($handle)) {
      // Matching image files in the cache using grep (*.spi can't be used as an
      // argument to ls because it creates too many arguments). Also if the loop
      // is broken while there is still data to read, pclose() will report a
      // broken pipe, so pipe through head to limit results and continue reading
      // even though total has been reached.
      if ($total > $count) continue;

      $image = unserialize(file_get_contents($cache_dir . '/' . trim($file)));
      if (isset($image['body'])) {
        $total += strlen($image['body']);
        // Only the body is removed to save space, the headers are kept so that
        // the original file can be fetched by the image handler if required.
        unset($image['body']);
        file_put_contents($cache_dir . '/' . trim($file), serialize($image));
      }
    }
    pclose($handle);
  }

  private function Register($us_xml_url, $us_register, $hub) {
    $mysqli = connect_db();
    $xml_url = $mysqli->escape_string(trim($us_xml_url, ' /'));
    $register = $mysqli->escape_string($us_register);
    $query = 'INSERT INTO reader_notify VALUES ("' . $register . '", ' .
      '"' . $xml_url . '", ' . time() . ', ' . $hub . ') ON DUPLICATE KEY ' .
      'UPDATE timestamp = ' . time();
    if (!$mysqli->query($query)) {
      $this->Log('Reader->Register: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function RegisteredFeed($us_xml_url, $us_id = '', $lease = 0) {
    $us_xml_url = trim($us_xml_url, ' /');
    if ($us_xml_url === '') return false;

    $registered = '';
    $mysqli = connect_db();
    $xml_url = $mysqli->escape_string($us_xml_url);
    $domain = substr($xml_url, strpos($xml_url, '//') + 2);
    $id = $mysqli->escape_string($us_id);
    $query = 'SELECT xml_url FROM reader_hub WHERE callback_id = "' . $id .'" '.
      'AND (xml_url LIKE "%' . $domain . '" OR self LIKE "%' . $domain . '")';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows >= 1) $registered = 'hub';
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->RegisteredFeed 1: ' . $mysqli->error);
    }
    if ($registered === '') {
      // Try again with reader_cloud table.
      $query = 'SELECT xml_url FROM reader_cloud WHERE xml_url ' .
        'LIKE "%' . $domain . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($mysqli_result->num_rows >= 1) $registered = 'cloud';
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->RegisteredFeed 2: ' . $mysqli->error);
      }
    }
    // Also update the lease time for hub notifications, but don't allow
    // leases less than a day (which is the default just set in RenewHub).
    if ($registered === 'hub' && $lease > 86400) {
      // Convert the lease in seconds to an expiry timestamp.
      $query = 'UPDATE reader_hub SET expiry = ' . ($lease + time()) .
        ' WHERE xml_url = "' . $xml_url . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->RegisteredFeed 3: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    return $registered;
  }

  private function RemoveChannel($id, $us_channel = '') {
    if ($us_channel === '' && isset($_POST['name'])) {
      $us_channel = $_POST['name'];
    }
    if ($us_channel === 'all' || $us_channel === 'global') {
      return ['error' => '\'' . $us_channel . '\' is a reserved channel.'];
    }

    $owner = $this->SwitchOwner();
    $mysqli = connect_db();
    $channel = $mysqli->escape_string($us_channel);
    $query = 'DELETE FROM reader_channels WHERE user = "' . $owner . '" ' .
      'AND box_id = ' . $id . ' AND channel = "' . $channel . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->RemoveChannel 1: ' . $mysqli->error);
    }
    $query = 'DELETE FROM reader_channel_info WHERE user = "' . $owner . '" ' .
      'AND box_id = ' . $id . ' AND channel_name = "' . $channel . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->RemoveChannel 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['settings' => $this->FeedSettings($id)];
  }

  private function RemoveDuplicateImages($content) {
    $all_video_list = [];
    $all_image_list = [];
    $duplicate_image_list = [];
    $doc = new DOMDocument();
    @$doc->loadHTML($content);
    $xpath = new DOMXpath($doc);
    foreach ($xpath->query('//img[@src]') as $img) {
      $src = $img->getAttribute('src');
      if (in_array($src, $all_image_list)) {
        $duplicate_image_list[] = $src;
      }
      else {
        $all_image_list[] = $src;
      }
    }
    foreach ($xpath->query('//video[@src]') as $video) {
      $all_video_list[] = $video->getAttribute('src');
    }
    // Remove duplicate images from content.
    foreach ($duplicate_image_list as $remove) {
      $regex = '/<img[^>]+' . preg_quote($remove, '/') . '[^>]+>/';
      $content = preg_replace($regex, '', $content, 1);
    }
    // Remove anchors from content that match videos. These contain an image
    // from the video, which is considered to be a duplicate image.
    foreach ($all_video_list as $remove) {
      $regex = '/<a[^>]+' . preg_quote($remove, '/') . '.+<\/a>/';
      $content = preg_replace($regex, '', $content, 1);
    }
    return $content;
  }

  private function RemoveFeed($us_xml_url, $force = false) {
    $error_count = 0;
    $mysqli = connect_db();
    $title = $us_xml_url;
    $xml_url = $mysqli->escape_string($us_xml_url);
    if (!$force) {
      // First check that the error count is over a threshold, as there could
      // just be connectivity issues.
      $query = 'SELECT feed_title, error_count FROM reader_feeds WHERE ' .
        'xml_url = "' . $xml_url . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($reader_feeds = $mysqli_result->fetch_assoc()) {
          $error_count = (int)$reader_feeds['error_count'] + 1;
          if ($reader_feeds['feed_title'] !== '') {
            $title = $reader_feeds['feed_title'];
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->RemoveFeed 1: ' . $mysqli->error);
      }
      // RemoveFeed is called by Cron if there's an error, which is called every
      // hour, so allow up to a week for the feed to respond if unavailable.
      if ($error_count < 168) {
        $query = 'UPDATE reader_feeds SET error_count = ' . $error_count .
          ' WHERE xml_url = "' . $xml_url . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Reader->RemoveFeed 2: ' . $mysqli->error);
        }
        $mysqli->close();
        return;
      }
    }

    // Notify users who manually subscribed to this feed that it's been removed.
    $query = 'SELECT user FROM reader WHERE auto = 0 AND ' .
      'xml_url = "' . $xml_url . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader = $mysqli_result->fetch_assoc()) {
        $description = 'The feed <a href="' . $us_xml_url . '">' .
          $title . '</a> is no longer available.';
        $content = ['author' => 'Reader', 'description' => $description,
                    'category' => 'system', 'permalink' => '', 'feed' => '',
                    'public' => 0];
        $owner = $reader['user'];
        $notification = new Notification($this->user, $owner);
        $id = new_module($this->user, $owner, 'notification', '',
                         $notification->Group(), 'outside');
        $notification->SetContent($id, $content);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->RemoveFeed 3: ' . $mysqli->error);
    }
    // Then delete the feed for all users.
    $query = 'DELETE FROM reader WHERE xml_url = "' . $xml_url . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->RemoveFeed 4: ' . $mysqli->error);
    }
    $query = 'DELETE FROM reader_channels WHERE xml_url = "' . $xml_url . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->RemoveFeed 5: ' . $mysqli->error);
    }
    // Also check if this is a feed list and remove the listed feeds for
    // those subscribed by using CheckFeedList with an empty array.
    $found = false;
    $query = 'SELECT COUNT(*) FROM reader_subscribe WHERE ' .
      'subscribe_url = "' . $xml_url . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($reader_subscribe = $mysqli_result->fetch_row()) {
        $found = (int)$reader_subscribe[0] > 0;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->RemoveFeed 6: ' . $mysqli->error);
    }
    if ($found) {
      $this->CheckFeedList([], $us_xml_url);
      $query = 'DELETE FROM reader_subscribe WHERE ' .
        'subscribe_url = "' . $xml_url . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->RemoveFeed 7: ' . $mysqli->error);
      }
    }
    else {
      $this->RemoveFromTables($xml_url);
    }
    $mysqli->close();
  }

  private function RemoveFeedForUser($id, $us_xml_url = '',
                                     $get_feed = true, $auto = false) {
    $owner = $this->SwitchOwner();
    // Remove a final slash that may have been added by javascript.
    if ($us_xml_url === '') $us_xml_url = trim($_POST['xmlUrl'], ' /');

    $mysqli = connect_db();
    $xml_url = $mysqli->escape_string($us_xml_url);
    $us_subscribe_url = '';
    $revert = false;
    // If the auto parameter is true and the user is manually subscribed to
    // this feed, don't continue.
    if ($auto) {
      $user_auto = false;
      $query = 'SELECT auto FROM reader WHERE user = "' . $owner . '" AND ' .
        'box_id = ' . $id . ' AND xml_url = "' . $xml_url . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($reader = $mysqli_result->fetch_assoc()) {
          $user_auto = $reader['auto'] === '1';
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->RemoveFeedForUser 1: ' . $mysqli->error);
      }
      if (!$user_auto) {
        $mysqli->close();
        return;
      }
    }
    // Another option is that the user is removing a feed they manually added,
    // but since it's in reader_subscribe it should only revert to auto.
    else {
      $query = 'SELECT subscribe_url FROM reader_subscribe LEFT JOIN reader ' .
        'ON subscribe_url = reader.xml_url WHERE user = "' . $owner . '" ' .
        'AND box_id = ' . $id . ' AND ' .
        'reader_subscribe.xml_url = "' . $xml_url . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($reader_subscribe = $mysqli_result->fetch_assoc()) {
          $us_subscribe_url = $reader_subscribe['subscribe_url'];
          $revert = true;
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->RemoveFeedForUser 2: ' . $mysqli->error);
      }
    }
    if ($revert) {
      $query = 'UPDATE reader SET auto = 1 WHERE user = "' . $owner . '" ' .
        'AND box_id = ' . $id . ' AND xml_url = "' . $xml_url . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->RemoveFeedForUser 3: ' . $mysqli->error);
      }
      // Also try reverting the channel in case it no longer matches the list.
      $channel = '';
      $subscribe_url = $mysqli->escape_string($us_subscribe_url);
      $query = 'SELECT channel FROM reader_channels WHERE ' .
        'user = "' . $owner . '" AND box_id = ' . $id . ' AND ' .
        'xml_url = "' . $subscribe_url . '" AND author = ""';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($reader_channels = $mysqli_result->fetch_assoc()) {
          $channel = $reader_channels['channel'];
        }
      }
      else {
        $this->Log('Reader->RemoveFeedForUser 4: ' . $mysqli->error);
      }
      $query = 'UPDATE reader_channels SET channel = "' . $channel . '" ' .
        'WHERE user = "' . $owner . '" AND box_id = ' . $id . ' AND ' .
        'xml_url = "' . $xml_url . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->RemoveFeedForUser 5: ' . $mysqli->error);
      }
    }
    else {
      $query = 'DELETE FROM reader WHERE user = "' . $owner . '" AND ' .
        'box_id = ' . $id . ' AND xml_url = "' . $xml_url . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->RemoveFeedForUser 6: ' . $mysqli->error);
      }
      $query = 'DELETE FROM reader_channels WHERE user = "' . $owner . '" ' .
        'AND box_id = ' . $id . ' AND xml_url = "' . $xml_url . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->RemoveFeedForUser 7: ' . $mysqli->error);
      }
      // Also check if this is a feed list and remove the listed feeds for
      // the current user.
      $us_feed_list = [];
      $query = 'SELECT xml_url FROM reader_subscribe WHERE ' .
        'subscribe_url = "' . $xml_url . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($reader_subscribe = $mysqli_result->fetch_assoc()) {
          $us_feed_list[] = $reader_subscribe['xml_url'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->RemoveFeedForUser 8: ' . $mysqli->error);
      }
      if (count($us_feed_list) > 0) {
        // The url stored in reader_subscribe may not be the feed url,
        // SimplePie uses discovery when processing a feed list.
        include_once 'library/Masterminds/HTML5.auto.php';
        include_once 'library/Mf2/Parser.php';
        include_once 'autoloader.php';
        include_once 'idn/idna_convert.class.php';
        $feed = new SimplePie();
        foreach ($us_feed_list as $us_remove_feed) {
          $feed->set_feed_url($us_remove_feed);
          if (!$feed->init()) continue;

          $us_remove_feed = trim($feed->subscribe_url(), ' /');
          $this->RemoveFeedForUser($id, $us_remove_feed, false, true);
        }
      }

      // Check if anyone still subscribes to this feed.
      $query = 'SELECT user FROM reader WHERE xml_url = "' . $xml_url . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($mysqli_result->num_rows === 0) {
          $this->RemoveFromTables($xml_url);
          if (count($us_feed_list) > 0) {
            $query = 'DELETE FROM reader_subscribe WHERE ' .
              'subscribe_url = "' . $xml_url . '"';
            if (!$mysqli->query($query)) {
              $this->Log('Reader->RemoveFeedForUser 9: ' . $mysqli->error);
            }
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->RemoveFeedForUser 10: ' . $mysqli->error);
      }
      // Update page_updates table for feed list subscribers.
      $query = 'INSERT INTO page_updates VALUES ("' . $owner . '", ' .
        '"' . $this->user->page . '", "feed", ' . time() . ') ' .
        'ON DUPLICATE KEY UPDATE timestamp = ' . time();
      if (!$mysqli->query($query)) {
        $this->Log('Reader->RemoveFeedForUser 11: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    if (!$get_feed) return;

    // Return the feed with the given entry removed.
    $this->ResetSession();
    $result = $this->GetFeed($id);
    $result['settings'] = $this->FeedSettings($id);
    return $result;
  }

  private function RemoveFollowPost($us_source, $us_target) {
    list($this->user->page, $this->owner) = page_owner($us_target);
    $id = $this->Id();
    if ($id === 0) return false;

    $mysqli = connect_db();
    $us_xml_url = '';
    $source = $mysqli->escape_string($us_source);
    $query = 'SELECT xml_url FROM reader_follow_posts WHERE ' .
      'user = "' . $this->owner . '" AND box_id = ' . $id . ' AND ' .
      'permalink = "' . $source . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($reader_follow_posts = $mysqli_result->fetch_assoc()) {
        $us_xml_url = $reader_follow_posts['xml_url'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->RemoveFollowPost 1: ' . $mysqli->error);
    }
    if ($us_xml_url === '') {
      $mysqli->close();
      return false;
    }

    $query = 'DELETE FROM reader_follow_posts WHERE ' .
      'user = "' . $this->owner . '" AND box_id = ' . $id . ' AND ' .
      'permalink = "' . $source . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->RemoveFollowPost 2: ' . $mysqli->error);
    }
    $mysqli->close();

    $this->RemoveFeedForUser($id, $us_xml_url, false);
    // Create a notification for this user when a subscription is removed.
    $description = 'Unsubscribed from <a href="' . $us_xml_url . '">' .
      $us_xml_url . '</a>';
    $content = ['author' => 'Reader', 'description' => $description,
                'category' => 'system', 'permalink' => '', 'feed' => '',
                'public' => 0];
    $notification = new Notification($this->user, $this->owner);
    $notification_id = new_module($this->user, $this->owner, 'notification',
                                  '', $notification->Group(), 'outside');
    $notification->SetContent($notification_id, $content);
    return true;
  }

  private function RemoveFromTables($xml_url) {
    $mysqli = connect_db();
    $query = 'DELETE FROM reader_feeds WHERE xml_url = "' . $xml_url . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->RemoveFromTables 1: ' . $mysqli->error);
    }
    $query = 'DELETE FROM reader_cloud WHERE xml_url = "' . $xml_url . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->RemoveFromTables 2: ' . $mysqli->error);
    }
    $query = 'DELETE FROM reader_hub WHERE xml_url = "' . $xml_url . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->RemoveFromTables 3: ' . $mysqli->error);
    }
    foreach ($this->ItemsTableNames() as $items_table) {
      $query = 'DELETE FROM ' . $items_table .
        ' WHERE xml_url = "' . $xml_url . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->RemoveFromTables 4: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  private function RenewCloud($us_register, $feed_list) {
    // It would be nice to be able to specify port 443 if only config->Secure()
    // is true, but most endpoints don't understand it means https. So can only
    // use it if the feed also specifies port 443, which means it's been saved
    // as https in the url to register with.
    $port = $this->user->config->Secure() &&
      strpos($us_register, 'https') === 0 ? '443' : '80';
    $path = urlencode('/php/update.php');
    $post_fields = 'notifyProcedure=&protocol=http-post&port=' . $port .
      '&domain=' . $this->user->config->ServerName() . '&path=' . $path .
      '&' . $feed_list;
    $ch = curl_init($us_register);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    $this->Log('Reader->RenewCloud 1: curl ' . $us_register);
    curl_exec($ch);
    if (preg_match('/4[0-9][0-9]/', curl_getinfo($ch, CURLINFO_HTTP_CODE))) {
      $mysqli = connect_db();
      $register = $mysqli->escape_string($us_register);
      $query = 'DELETE FROM reader_cloud WHERE register = "' . $register . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->RenewCloud 2: ' . $mysqli->error);
      }
      $mysqli->close();
    }
    curl_close($ch);
  }

  private function RenewHub($us_hub, $us_self, $callback_id, $us_xml_url) {
    // If curl succeeds here but the remote server never calls back, then this
    // hub will continue to be renewed ever hour. To prevent that from happening
    // set the expiry time to 24 hours from now before requesting the lease.
    $mysqli = connect_db();
    $hub = $mysqli->escape_string($us_hub);
    $self = $mysqli->escape_string($us_self);
    $query = 'UPDATE reader_hub SET expiry = ' . strtotime('24 hours') .
      ' WHERE hub = "' . $hub . '" AND self = "' . $self . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->RenewHub 1: ' . $mysqli->error);
    }
    $scheme = $this->user->config->Secure() ? 'https://' : 'http://';
    $callback = urlencode($scheme . $this->user->config->ServerName() .
                          '/php/update.php?url=' . $us_self . '&id=' .
                          $callback_id);
    $post_fields = 'hub.mode=subscribe&hub.topic=' . urlencode($us_self) .
      '&hub.callback=' . $callback;
    $ch = curl_init($us_hub);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_ENCODING, '');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    $this->Log('Reader->RenewHub 2: curl ' . $us_hub);
    $body = curl_exec($ch);
    if (preg_match('/4[0-9][0-9]/', curl_getinfo($ch, CURLINFO_HTTP_CODE))) {
      $this->Log('Reader->RenewHub 3: ' . $us_hub . ' request for ' . $us_self .
                 ' responded with: ' . $body);
      $query = 'DELETE FROM reader_hub WHERE hub = "' . $hub . '" AND ' .
        'self = "' . $self . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->RenewHub 4: ' . $mysqli->error);
      }
      // Note that the subscribed feed url can be different from self because
      // fragments can be used to specify a unique feed on the page.
      $this->RemoveFeed(trim($us_xml_url, ' /'), true);
    }
    curl_close($ch);
    $mysqli->close();
  }

  private function ResetSession() {
    $key = $this->owner . '-' . $this->user->page;
    if (isset($_SESSION['reader-first'])) {
      $_SESSION['reader-first'][$key] = 0;
    }
    else {
      $_SESSION['reader-first'] = [$key => 0];
    }
    if (isset($_SESSION['reader-last'])) {
      $_SESSION['reader-last'][$key] = 0;
    }
    else {
      $_SESSION['reader-last'] = [$key => 0];
    }
  }

  private function SaveCloud($us_cloud, $us_xml_url) {
    // If xml_url is on the same domain as the currently configured ServerName,
    // do nothing as UpdateFeed does notifications in this case.
    if (preg_match('/\/\/([^\/]+)/', $us_xml_url, $match)) {
      $domain = $match[1];
      if ($domain === $this->user->config->ServerName()) return;
    }
    // Also don't try registering from localhost.
    if ($this->user->config->ServerName() === 'localhost') return;

    if ($us_attributes = $us_cloud[0]['attribs']['']) {
      // Only supporting cloud notifications that specify 'http-post' as the
      // protocol here.
      if ($us_attributes['protocol'] !== 'http-post' ||
          $us_attributes['domain'] === '') return;

      $us_register = '';
      $mysqli = connect_db();
      $xml_url = $mysqli->escape_string($us_xml_url);
      $domain = $mysqli->escape_string($us_attributes['domain']);
      $port = $mysqli->escape_string($us_attributes['port']);
      // Use ports 80 and 443 to specify whether http or https should be
      // used to register, otherwise explicitly add the port number.
      if ($port === '80') {
        $us_register = 'http://' . $domain;
      }
      else if ($port === '443') {
        $us_register = 'https://' . $domain;
      }
      else {
        $us_register = 'http://' . $domain . ':' . $port;
      }
      $us_register .= $us_attributes['path'];
      $register = $mysqli->escape_string($us_register);
      // Check if there's an existing address to register for this feed, only
      // want to register again if it's changed.
      $us_existing = '';
      $query = 'SELECT register FROM reader_cloud WHERE ' .
        'xml_url = "' . $xml_url . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($reader_cloud = $mysqli_result->fetch_assoc()) {
          $us_existing = $reader_cloud['register'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->SaveCloud 1: ' . $mysqli->error);
      }
      if ($us_existing !== $us_register) {
        $query = 'INSERT INTO reader_cloud VALUES ("' . $register . '", ' .
          '"' . $xml_url . '") ON DUPLICATE KEY UPDATE ' .
          'register = "' . $register . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Reader->SaveCloud 2: ' . $mysqli->error);
        }
        $this->RenewCloud($us_register, 'url1=' . urlencode($us_xml_url));
      }
      $mysqli->close();
    }
  }

  private function SaveFollowPost($id, $us_source, $us_xml_url) {
    $mysqli = connect_db();
    $source = $mysqli->escape_string($us_source);
    $xml_url = $mysqli->escape_string($us_xml_url);
    $query = 'INSERT INTO reader_follow_posts VALUES ' .
      '("' . $this->owner . '", ' . $id . ', "' . $source . '", ' .
      '"' . $xml_url . '") ON DUPLICATE KEY UPDATE xml_url = "' . $xml_url .'"';
    if (!$mysqli->query($query)) {
      $this->Log('Reader->SaveFollowPost: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function SaveHub($us_hub, $us_self, $us_xml_url) {
    // If xml_url is on the same domain as the currently configured ServerName,
    // do nothing as UpdateFeed does notifications in this case.
    if (preg_match('/\/\/([^\/]+)/', $us_xml_url, $match)) {
      $domain = $match[1];
      if ($domain === $this->user->config->ServerName()) return;
    }
    // Also don't try registering from localhost.
    if ($this->user->config->ServerName() === 'localhost') return;

    $mysqli = connect_db();
    $us_existing = '';
    $xml_url = $mysqli->escape_string($us_xml_url);
    // Check if there's an existing address to register for this feed, only
    // want to register again if it's changed.
    $query = 'SELECT hub FROM reader_hub WHERE xml_url = "' . $xml_url . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($reader_hub = $mysqli_result->fetch_assoc()) {
        $us_existing = $reader_hub['hub'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->SaveHub 1: ' . $mysqli->error);
    }
    if ($us_existing !== $us_hub) {
      if ($us_self === '') {
        // If self is not provided try using the feed url but make sure there's
        // no fragment identifier.
        $us_self = $us_xml_url;
        if ($fragment = strpos($us_self, '#')) {
          $us_self = substr($us_self, 0, $fragment);
        }
      }
      $hub = $mysqli->escape_string($us_hub);
      $self = $mysqli->escape_string($us_self);
      $callback_id = bin2hex(openssl_random_pseudo_bytes(8));
      $query = 'INSERT INTO reader_hub VALUES ("' . $hub . '", ' .
        '"' . $self . '", ' . strtotime('24 hours') . ', ' .
        '"' . $callback_id . '", "' . $xml_url . '") ' .
        'ON DUPLICATE KEY UPDATE hub = "' . $hub . '", ' .
        'self = "' . $self . '", callback_id = "' . $callback_id . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->SaveHub 2: ' . $mysqli->error);
      }
      $this->RenewHub($us_hub, $us_self, $callback_id, $us_xml_url);
    }
    $mysqli->close();
  }

  private function SaveItem($item, $xml_url, $purifier, $autoupdate_url) {
    $us_permalink = $item->get_permalink();
    // If the item doesn't have a permalink then skip it.
    if (!isset($us_permalink)) return false;

    $updated = false;
    $timestamp = 0;
    $us_category_list = [];
    $us_content = $item->get_description();
    $us_content = isset($us_content) ?
      $this->RemoveDuplicateImages($us_content) : '';
    $us_guid = $item->get_id();

    $us_title = '';
    // Twitter feed is a special case, there are no titles on Twitter but there
    // is usually a modified version of the content in the item's title, so
    // ignore it here and leave title empty. Twitter also adds content about
    // not supporting video that we don't want to display.
    if (strpos($xml_url, 'https://twitter-atom.appspot.com') === 0) {
      $remove_content = 'Your browser does not support the video tag.';
      if (strpos($us_content, $remove_content) !== false) {
        $us_content = preg_replace('/' . preg_quote($remove_content) . '/', '',
                                   $us_content);
      }
    }
    else {
      $us_title = $item->get_title();
      $us_title = isset($us_title) ? strip_tags($us_title) : '';
    }
    // Note that SimplePie Parser now handles p-name e-content being on the
    // same tag in microformats and doesn't set the title in this case.
    if ($us_title !== '') {
      if ($us_content === '') {
        // Use title as content when no content provided, it looks better.
        $us_content = $us_title;
        $us_title = '';
      }
      else if (strlen($us_title) > 140) {
        // Otherwise assume that a long title is probably the same as the
        // content and show a shortened version of the title.
        $us_title = substr($us_title, 0, 80) . '...';
      }
    }

    $mysqli = connect_db();
    $permalink = $mysqli->escape_string($us_permalink);
    $title = $mysqli->escape_string($us_title);
    $content = $mysqli->escape_string($purifier->purify($us_content));
    $guid = isset($us_guid) ? $mysqli->escape_string($us_guid) : '';
    // guid's must be less than 100 characters as they are used as a primary
    // key in the reader_items table, so use md5 to keep them unique.
    if (strlen($guid) > 100) $guid = md5($guid);
    $query = '';
    // If both title and content are empty, delete the item from the feed.
    // Allow checking by permalink too in this case because we let SimplePie
    // create new guids so ours won't match anymore.
    if ($title === '' && $content === '') {
      $items_table = $this->ItemsTableNames(false);
      $items_query = $items_table . ' WHERE (guid = "' . $guid . '" OR ' .
        'permalink = "' . $permalink . '") AND xml_url = "' . $xml_url . '"';
      // Get the category list so that the Autoupdate module can remove the
      // update, otherwise the next update with the same version number won't
      // be applied.
      if ($autoupdate_url) {
        $query = 'SELECT category FROM ' . $items_query;
        if ($mysqli_result = $mysqli->query($query)) {
          if ($reader_items = $mysqli_result->fetch_assoc()) {
            $us_category_list = json_decode($reader_items['category']);
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Reader->SaveItem 1: ' . $mysqli->error);
        }
      }
      $query = 'DELETE FROM ' . $items_query;
    }
    else {
      $us_author = '';
      if ($item_author = $item->get_author()) {
        $us_author = $item_author->get_email();
        if (!isset($us_author)) {
          $us_author = $item_author->get_name();
        }
        if (!isset($us_author)) {
          $us_author = $item_author->get_link();
        }
      }
      // html isn't escaped for author and categories to support h-card.
      $us_author = $purifier->purify(htmlspecialchars_decode($us_author));
      $us_author_name = '';
      $us_old_name = '';
      // Special case for items generated by facebook-atom.appspot.com, the
      // author is always the first link in the content. Pass it to parse_hcard
      // so that it gets stored in the nickname cache.
      if (strpos($xml_url, 'https://facebook-atom.appspot.com') === 0) {
        $regex = '/<a\s+href="([^"]+)">([^<]+)<\/a>/';
        if (preg_match($regex, $us_content, $match)) {
          $us_author_url = trim($match[1]);
          $us_author_name = trim($match[2]);
          $h_card = ['type' => ['h-card'],
                     'properties' => ['url' => [$us_author_url],
                                      'name' => [$us_author_name]]];
          list($us_author_name, $us_author_photo,
               $us_author_url, $us_old_name) = parse_hcard($h_card, true);
          // Also remove the author from content.
          $us_content = preg_replace($regex, '', $us_content, 1);
          $content = $mysqli->escape_string($purifier->purify($us_content));
        }
      }
      else {
        // Try parsing author as microformats which will store it in the
        // nickname cache.
        $mf = Mf2\parse($us_author);
        foreach ($mf['items'] as $mf_item) {
          list($us_author_name, $us_author_photo,
               $us_author_url, $us_old_name) = parse_hcard($mf_item, true);
          if ($us_author_name !== '') break;
        }
      }
      // If name not found above use the original content for author.
      if ($us_author_name === '') $us_author_name = $us_author;
      $author = $mysqli->escape_string($us_author_name);
      if ($us_old_name !== '' && $us_old_name !== $us_author_name) {
        $old_name = $mysqli->escape_string($us_old_name);
        foreach ($this->ItemsTableNames() as $items_table) {
          $query = 'UPDATE ' . $items_table .
            ' SET author = "' . $author . '" WHERE ' .
            'author = "' . $old_name . '" AND xml_url = "' . $xml_url . '"';
          if (!$mysqli->query($query)) {
            $this->Log('Reader->SaveItem 1: ' . $mysqli->error);
          }
        }
        $query = 'UPDATE reader_channels SET author = "' . $author . '" ' .
          'WHERE author = "' . $old_name . '" AND xml_url = "' . $xml_url . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Reader->SaveItem 2: ' . $mysqli->error);
        }
      }

      if ($all_categories = $item->get_categories()) {
        foreach ($all_categories as $category) {
          if (!$category) continue;

          // Allow html in categories but purify.
          $us_label =
            $purifier->purify(htmlspecialchars_decode($category->get_label()));
          $us_category_list[] = $us_label;
        }
      }
      $category = $mysqli->escape_string(json_encode($us_category_list));
      // Store enclosures grouped together as photo, audio, video.
      $photo = [];
      $audio = [];
      $video = [];
      if ($all_enclosures = $item->get_enclosures()) {
        foreach ($all_enclosures as $enclosure) {
          if (!$link = $enclosure->get_link()) continue;

          $type = $enclosure->get_type();
          if (strpos($type, 'image/') === 0) $photo[] = $link;
          else if (strpos($type, 'audio/') === 0) $audio[] = $link;
          else if (strpos($type, 'video/') === 0) $video[] = $link;
        }
      }
      $media = ['photo' => $photo, 'audio' => $audio, 'video' => $video];
      $enclosure = $mysqli->escape_string(json_encode($media));
      // If this item doesn't have a date or the date is in the future, set
      // it to the current time to avoid having it pinned at the top of the
      // reader. Also if an item has an old timestamp but hasn't been saved
      // yet, give it the current time so it's not buried. Don't want to see
      // all items when subscribing to a new feed, so don't change the
      // timestamp when older than 48 hours.
      $date = $item->get_date();
      if (isset($date)) $timestamp = (int)strtotime($date);
      if ($timestamp === 0 || $timestamp > time() ||
          ($timestamp < strtotime('-2 hours') &&
           $timestamp > strtotime('-48 hours'))) {
        $timestamp = time();
      }
      // If using multiple items tables then need to add items with timestamps
      // older than the current table to their respective older tables. Passing
      // the item's timestamp in here means the correct table name will be
      // returned for this item.
      $items_table = $this->ItemsTableNames(false, '', $timestamp);
      $query = 'INSERT INTO ' . $items_table . ' VALUES ("' . $title . '", ' .
        '"' . $content . '", "' . $author . '", "' . $category . '", ' .
        '"' . $enclosure . '", "' . $permalink . '", "' . $guid . '", ' .
        $timestamp . ', "' . $xml_url . '") ON DUPLICATE KEY UPDATE ' .
        'title = "' . $title . '", content = "' . $content . '", ' .
        'author = "' . $author . '", category = "' . $category . '", ' .
        'enclosure = "' . $enclosure . '", permalink = "' . $permalink . '"';
    }
    if ($mysqli->query($query)) {
      $updated = $mysqli->affected_rows !== 0;
    }
    else {
      $this->Log('Reader->SaveItem 3: ' . $mysqli->error);
    }
    // SimplePie creates it's own guid from the data if one wasn't explicitly
    // set, so old items with the same permalink need to be removed.
    foreach ($this->ItemsTableNames() as $items_table) {
      $query = 'DELETE FROM ' . $items_table .
        ' WHERE permalink = "' . $permalink . '" ' .
        ' AND xml_url = "' . $xml_url . '" AND guid != "' . $guid . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->SaveItem 4: ' . $mysqli->error);
      }
    }
    $mysqli->close();

    if ($updated && $autoupdate_url) {
      $autoupdate = new Module($this->user, $this->owner, 'autoupdate');
      if ($autoupdate->IsInstalled()) {
        $autoupdate->Factory('SaveItem', [$us_title, $us_content,
                                          $us_category_list, $timestamp]);
      }
    }
    return $updated;
  }

  private function SetChannel($id, $us_feed = '',
                              $us_channel = '', $us_author = '') {
    if ($us_feed === '') $us_feed = $_POST['feed'];
    if ($us_channel === '') $us_channel = $_POST['channel'];
    if ($us_channel === 'not set') $us_channel = '';
    else $us_channel = htmlspecialchars($us_channel);
    if ($us_author === '' && isset($_POST['author'])) {
      $us_author = $_POST['author'];
    }
    if ($us_channel === 'all' || $us_channel === 'global') {
      return ['error' => '\'' . $us_channel . '\' is a reserved channel.'];
    }

    $html_url = '';
    $xml_url = '';
    $mysqli = connect_db();
    $feed = $mysqli->escape_string($us_feed);
    $author = $mysqli->escape_string($us_author);
    $channel = $mysqli->escape_string($us_channel);
    $owner = $this->SwitchOwner();
    // If author is set check if the channel being set is the same as for the
    // generic channel for this feed and remove the more specific entry. Also
    // when author is provided, SetChannel is called with html_url as the feed
    // so look up the xml_url too (and the reverse for the generic case.)
    $delete_author = false;
    if ($author === '') {
      $xml_url = $feed;
      $query = 'SELECT html_url FROM reader_feeds WHERE ' .
        'xml_url = "' . $xml_url . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($reader_feeds = $mysqli_result->fetch_assoc()) {
          $html_url = $mysqli->escape_string($reader_feeds['html_url']);
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->SetChannel 1: ' . $mysqli->error);
      }
    }
    else {
      $html_url = $feed;
      $query = 'SELECT channel, xml_url FROM reader_channels WHERE ' .
        'user = "' . $owner . '" AND box_id = ' . $id . ' AND ' .
        'html_url = "' . $html_url . '" AND author = ""';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($reader_channels = $mysqli_result->fetch_assoc()) {
          $delete_author = $reader_channels['channel'] === $channel;
          $xml_url = $mysqli->escape_string($reader_channels['xml_url']);
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Reader->SetChannel 2: ' . $mysqli->error);
      }
    }
    if ($delete_author) {
      $query = 'DELETE FROM reader_channels WHERE user = "' . $owner . '" ' .
        'AND box_id = ' . $id . ' AND html_url = "' . $html_url . '" AND ' .
        'author = "' . $author . '"';
    }
    else {
      $query = 'INSERT INTO reader_channels VALUES ("' . $owner . '", ' .
        $id . ', "' . $xml_url . '", "' . $html_url . '", "' . $author . '", ' .
        '"' . $channel . '") ON DUPLICATE KEY UPDATE ' .
        'channel = "' . $channel . '"';
    }
    if (!$mysqli->query($query)) {
      $this->Log('Reader->SetChannel 3: ' . $mysqli->error);
    }
    // Also check if this is a feed list and add the listed feeds to the
    // channel, but only those that have been added automatically. The
    // feed settings need to be updated in this case.
    $feed_settings = '';
    $us_set_channel_list = [];
    $query = 'SELECT reader_feeds.xml_url, reader_feeds.html_url FROM ' .
      'reader_feeds LEFT JOIN reader ON ' .
      'reader_feeds.xml_url = reader.xml_url LEFT JOIN reader_subscribe ON ' .
      'reader_feeds.xml_url = reader_subscribe.xml_url WHERE ' .
      'user = "' . $owner . '" AND box_id = ' . $id . ' AND auto = 1 AND ' .
      'subscribe_url = "' . $xml_url . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader_feeds = $mysqli_result->fetch_assoc()) {
        $us_set_channel_list[] = ['xml_url' => $reader_feeds['xml_url'],
                                  'html_url' => $reader_feeds['html_url']];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->SetChannel 4: ' . $mysqli->error);
    }
    if (count($us_set_channel_list) !== 0) {
      foreach ($us_set_channel_list as $us_set_channel) {
        $xml_url = $mysqli->escape_string($us_set_channel['xml_url']);
        $html_url = $mysqli->escape_string($us_set_channel['html_url']);
        $query = 'INSERT INTO reader_channels VALUES ("' . $owner . '", ' .
          $id . ', "' . $xml_url . '", "' . $html_url . '", "", ' .
          '"' . $channel . '") ON DUPLICATE KEY UPDATE ' .
          'channel = "' . $channel . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Reader->SetChannel 5: ' . $mysqli->error);
        }
      }
      $feed_settings = $this->FeedSettings($id);
    }
    $mysqli->close();

    $this->CreateChannel($id, $us_channel);
    return ['name' => $us_channel, 'feed' => $html_url,
            'settings' => $feed_settings];
  }

  private function SwitchOwner() {
    // This is a special case for when a user has logged in using indieauth.
    // They are given access to the reader module on a specific admin page
    // and can interact with the reader module as if they were the owner.
    if ($this->owner === 'admin' &&
        $this->user->page === $this->Substitute('indieauth-page') &&
        $this->user->group === $this->Substitute('indieauth-group')) {
      return $this->user->name;
    }
    return $this->owner;
  }

  private function UpdateChannelOrder($id, $us_channels) {
    if (count($us_channels) === 0) return;

    $channel_order = [];
    $mysqli = connect_db();
    $owner = $this->SwitchOwner();
    $query = 'SELECT channel_name FROM reader_channel_info WHERE ' .
      'user = "' . $owner . '" AND box_id = ' . $id . ' ORDER BY channel_order';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($reader_channel_info = $mysqli_result->fetch_assoc()) {
        $channel_order[] = $reader_channel_info['channel_name'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Reader->UpdateChannelOrder 1: ' . $mysqli->error);
    }
    // Use the channel order to create a new array containing the indexes of
    // just the channels that need to be swapped.
    $swap_order = [];
    foreach ($channel_order as $index => $name) {
      if (in_array($name, $us_channels)) $swap_order[] = $index;
    }
    for ($i = 0; $i < count($swap_order); $i++) {
      if (!isset($us_channels[$i])) continue;

      $name = $mysqli->escape_string($us_channels[$i]);
      if ($name === 'all' || $name === 'global') continue;

      $query = 'UPDATE reader_channel_info SET ' .
        'channel_order = ' . $swap_order[$i] .
        ' WHERE user = "' . $owner . '" AND box_id = ' . $id .
        ' AND channel_name = "' . $name . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Reader->UpdateChannelOrder 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  private function UpdateFeed($us_xml_url, $data = '', $external = false) {
    $us_xml_url = trim($us_xml_url, ' /');
    if ($external) {
      $mysqli = connect_db();
      $xml_url = $mysqli->escape_string($us_xml_url);
      if ($data === '') {
        $recent = false;
        $query = 'SELECT timestamp from reader_queue WHERE ' .
          'xml_url = "' . $xml_url . '" AND ' .
          'timestamp > ' . strtotime('-5 minutes');
        if ($mysqli_result = $mysqli->query($query)) {
          $recent = $mysqli_result->num_rows === 1;
          $mysqli_result->close();
        }
        else {
          $this->Log('Reader->UpdateFeed 1: ' . $mysqli->error);
        }
        if ($recent) {
          // When this feed has been recently updated, it's not checked again
          // but the slow flag is updated so that it's at least checked by cron.
          $query = 'UPDATE reader_feeds SET slow = 0 WHERE ' .
            'xml_url = "' . $xml_url . '"';
          if (!$mysqli->query($query)) {
            $this->Log('Reader->UpdateFeed 2: ' . $mysqli->error);
          }
          $mysqli->close();
          return;
        }
      }
      else {
        // When data is provided set the slow flag as this feed doesn't need to
        // be checked as often.
        $query = 'UPDATE reader_feeds SET slow = 1 WHERE ' .
          'xml_url = "' . $xml_url . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Reader->UpdateFeed 3: ' . $mysqli->error);
        }
      }
      // Only external feeds need to be added to the queue.
      $query = 'INSERT INTO reader_queue VALUES (' . time() . ', ' .
        '"' . $xml_url . '") ON DUPLICATE KEY UPDATE timestamp = ' . time();
      if (!$mysqli->query($query)) {
        $this->Log('Reader->UpdateFeed 4: ' . $mysqli->error);
      }
      // Clean up the queue.
      $query = 'DELETE FROM reader_queue WHERE timestamp < ' .
        strtotime('-5 minutes');
      if (!$mysqli->query($query)) {
        $this->Log('Reader->UpdateFeed 5: ' . $mysqli->error);
      }
      $mysqli->close();
    }

    include_once 'library/Masterminds/HTML5.auto.php';
    include_once 'library/Mf2/Parser.php';
    include_once 'autoloader.php';
    include_once 'idn/idna_convert.class.php';
    $feed = new SimplePie();
    $feed->set_feed_url($us_xml_url);
    $feed->enable_order_by_date(false);
    // These settings are set to false because HTMLPurifier handles them.
    $feed->strip_comments(false);
    $feed->strip_htmltags(false);
    $feed->strip_attributes(false);
    $feed->add_attributes(false);
    $scheme = $this->user->config->Secure() ? 'https://' : 'http://';
    $handler = $scheme . $this->user->config->ServerName() . '/php/image.php';
    // The full image handler url is required for Microsub clients.
    $feed->set_image_handler($handler);
    $feed->force_feed(true);
    $feed->set_cache_duration(0);
    $feed->set_raw_data($data);
    $feed->init();
    if ($this->CheckFeed($feed, $us_xml_url)) {
      $this->NotifyUsers($us_xml_url);
      $this->NotifyOthers($us_xml_url, $feed->get_raw_data());
    }
  }

}
