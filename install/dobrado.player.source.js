/*global dobrado: true */
// @licstart The following is the entire license notice
// for the JavaScript code in this page.
// 
// Copyright (C) 2018 Malcolm Blaney
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// As additional permission under GNU AGPL version 3 section 7, you
// may distribute non-source (e.g., minimized or compacted) forms of
// this code without the copy of the GNU GPL normally required by
// section 4, provided you include this license notice and a URL
// through which recipients can access the Corresponding Source.
// 
// @licend The above is the entire license notice
// for the JavaScript code in this page.

if (!this.dobrado.player) {
  dobrado.player = {};
}
(function() {

  'use strict';

  // Group private variables and module member variables here.

  $(function() {
    // Don't run if the module isn't on the page.
    if ($('.player audio').length === 0) {
      return;
    }

    var count = 0;
    window.onfocus = function() {
      if (count < 2) {
        $('.player audio').get(0).play();
      }
    };
    window.onblur = function() {
      $('.player audio').get(0).pause();
    };
    // Only play audio through 3 times and then stop.
    $('.player audio').bind('ended', function() {
      if (count < 2) {
        this.play();
        count++;
      }
    });
    $('.player audio').get(0).play();
  });

}());
