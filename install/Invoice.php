<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Invoice extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'You don\'t have permission to view invoices.'];
    }

    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'search') {
      return $this->Search();
    }
    if ($us_action === 'submit') {
      return $this->Submit();
    }
    if ($us_action === 'remove') {
      return $this->RemoveInvoice();
    }
  }

  public function CanAdd($page) {
    // Need admin privileges to add the invoice module.
    if (!$this->user->canEditSite) return false;
    // Can only have one invoice module on a page.
    return !$this->AlreadyOnPage('invoice', $page);
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $purchase_id_input = '';
    if ($this->Substitute('invoice-show-purchase-id') === 'true') {
      $purchase_id_input = '<div class="form-spacing">' .
          '<label for="invoice-purchase-id-input">Purchase Id:</label>' .
          '<input id="invoice-purchase-id-input" type="text" maxlength="20">' .
        '</div>';
    }
    return '<form id="invoice-form" autocomplete="off">' .
        '<button class="default-action hidden">default</button>' .
        '<div class="form-spacing">' .
          '<label for="invoice-username-input">Username:</label>' .
          '<input id="invoice-username-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="invoice-fullname-input">Full Name:</label>' .
          '<input id="invoice-fullname-input" type="text" maxlength="100">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="invoice-email-input">Email:</label>' .
          '<input id="invoice-email-input" type="text" maxlength="100">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="invoice-date-input">Date:</label>' .
          '<input id="invoice-date-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="invoice-number-input">Invoice Number:</label>' .
          '<input id="invoice-number-input" type="text" maxlength="20">' .
        '</div>' .
        $purchase_id_input .
        '<hr>' .
        '<a class="toggle-search-options" href="#">' .
          'Search between start and end dates</a><br>' .
        '<div class="search-options hidden">' .
          '<div class="form-spacing">' .
            '<label for="invoice-date-options">Options:</label>' .
            '<select id="invoice-date-options">' .
              '<option value="0">quick search...</option>' .
              '<option value="30">past month</option>' .
              '<option value="182">past 6 months</option>' .
              '<option value="365">past 12 months</option>' .
              '<option value="current">current financial year</option>' .
              '<option value="previous">previous financial year</option>' .
            '</select>' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="invoice-start-input">Start:</label>' .
            '<input id="invoice-start-input" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="invoice-end-input">End:</label>' .
            '<input id="invoice-end-input" type="text" maxlength="50">' .
          '</div>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input id="invoice-export-data" type="checkbox"> ' .
          '<label for="invoice-export-data">Download search results</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input id="invoice-email-self" type="checkbox"> ' .
          '<label for="invoice-email-self">Send invoice to yourself</label>' .
        '</div>' .
        '<div class="form-spacing">' .
          '<input id="invoice-email-user" type="checkbox"> ' .
          '<label for="invoice-email-user">Send invoice to the user</label>' .
        '</div>' .
        '<button class="submit">send</button>' .
        '<button class="search">search</button>' .
        '<button class="remove">remove</button>' .
        '<button class="clear">clear</button>' .
        '<span class="info"></span>' .
      '</form>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {
    if (!$this->Run(date('H:00:00'))) return;

    // Sending mail could take a long time, so remove the time limit.
    set_time_limit(0);

    // Returns a list of groups where 'invoice-day' matches today's date.
    foreach ($this->RunGroups('invoice-day') as $run_group) {
      $group = $run_group['group'];
      $timezone = $run_group['timezone'];
      $this->user = new User($this->owner, $group, $timezone);
      // Check if a surcharge is being added for this group, otherwise can
      // just skip straight to getting the current purchase data.
      $surcharge = $this->Substitute('surcharge') === 'true';
      $all_data = $surcharge ? $this->AddSurcharge() : $this->Data(false);
      $total = count($all_data);
      if ($total === 0) continue;

      // Allow groups to not send invoices when an account has been paid.
      $skip_zero = $this->Substitute('invoice-skip-zero-balance') === 'true';
      $invoice_balance = (float)$this->Substitute('invoice-balance');
      $sent = 0;
      foreach ($all_data as $data) {
        if ($skip_zero) {
          $balance = (float)$data['balance'];
          if ($balance < $invoice_balance + 0.1 &&
              $balance > $invoice_balance - 0.1) {
            continue;
          }
        }
        $result = $this->SendInvoice($data);
        if (!isset($result['done'])) {
          if ($result['error'] !== '') {
            // Add a notification for unsent invoices.
            $description = 'Cron: ' . $result['error'];
            if ($group !== '') {
              $description .= ' for ' . ucfirst($group);
            }
            $this->Notification('invoice', 'Invoice', $description, 'system',
                                $group);
          }
        }
        else {
          $sent++;
        }
      }
      if ($sent === 1) {
        $description = 'Cron: sent <b>1</b> invoice';
        if ($group !== '') {
          $description .= ' for ' . ucfirst($group);
        }
        $this->Notification('invoice', 'Invoice', $description, 'system',
                            $group);
      }
      else if ($sent > 1) {
        $description = 'Cron: sent <b>' . $sent . '</b> invoices';
        if ($group !== '') {
          $description .= ' for ' . ucfirst($group);
        }
        $this->Notification('invoice', 'Invoice', $description, 'system',
                            $group);
      }
    }

    // Returns a list of groups where 'remove-orders' matches today's date.
    foreach ($this->RunGroups('invoice-remove-orders') as $run_group) {
      $group = $run_group['group'];
      $timezone = $run_group['timezone'];
      $this->user = new User($this->owner, $group, $timezone);
      $purchase = new Purchase($this->user, $this->owner);
      $purchase->RemoveAllPurchases();
      $description = 'Cron: removed group order';
      if ($group !== '') $description .= ' for ' . ucfirst($group);
      $this->Notification('invoice', 'Invoice', $description, 'system', $group);
    }

    // Return a list of groups where 'invoice-group-order' matches today's date.
    // This is when the group orders are sent out, individual orders can be
    // sent out on a different day if required, see 'invoice-send-orders'.
    $all_groups = $this->RunGroups('invoice-group-order');
    foreach ($all_groups as $run_group) {
      $group = $run_group['group'];
      $timezone = $run_group['timezone'];
      $this->user = new User($this->owner, $group, $timezone);
      // If there's more than one group probably only want supplier files
      // sent for the organisation.
      $create_files =
        $this->Substitute('invoice-group-attach-files') === 'true' ?
        true : false;
      $result = $this->NextOrder($create_files);
      if ($result['done'] === true) {
        $description = 'Cron: sent group order';
        if ($group !== '') {
          $description .= ' for ' . ucfirst($group);
        }
        $this->Notification('invoice', 'Invoice', $description, 'system',
                            $group);
      }
      else {
        // Add a notification for unsent orders.
        $description = 'Cron: ' . $result['error'];
        if ($group !== '') {
          $description .= ' for ' . ucfirst($group);
        }
        $this->Notification('invoice', 'Invoice', $description, 'system',
                            $group);
      }
    }
    // Get a trimmed down list of system_groups that represent the parent
    // organisations that need to be sent orders here.
    $organiser = new Organiser($this->user, $this->owner);
    foreach ($organiser->MatchParents($all_groups) as $run_group) {
      $group = $run_group['group'];
      $timezone = $run_group['timezone'];
      $this->user = new User($this->owner, $group, $timezone);
      // Note that a new organiser module is required once the group is set.
      $group_organiser = new Organiser($this->user, $this->owner);
      // Ignore a group in the organisation such as 'non-purchasing'.
      $ignore_group = $this->Substitute('invoice-ignore-group');
      $send_groups = $group_organiser->Siblings($ignore_group);
      // Only send orders for the organisation if more than one group.
      if (count($send_groups) > 1) {
        $result = $this->NextOrder(true, true, $send_groups);
        $organisation = $group_organiser->Parent();
        if ($organisation === false) $organisation = '';
        if ($result['done'] === true) {
          $description = 'Cron: sent organisation order';
          if ($organisation !== '') {
            $description .= ' for ' . $organisation;
          }
          $this->Notification('invoice', 'Invoice', $description, 'system',
                              $organisation);
        }
        else {
          // Add a notification for unsent orders.
          $description = 'Cron: ' . $result['error'];
          if ($organisation !== '') {
            $description .= ' for ' . $organisation;
          }
          $this->Notification('invoice', 'Invoice', $description, 'system',
                              $organisation);
        }
      }
    }

    // Send the orders to individual members.
    foreach ($this->RunGroups('invoice-send-orders') as $run_group) {
      $group = $run_group['group'];
      $timezone = $run_group['timezone'];
      $this->user = new User($this->owner, $group, $timezone);
      $this->SendOrders();
    }

    // Check the number of active users in each group. 'invoice-active-users'
    // should be set to a relative date such as 'first day of this month' so
    // that dates 'first day of last month' and 'last day of last month' make
    // sense in ActiveUsers.
    foreach ($this->RunGroups('invoice-active-users') as $run_group) {
      $group = $run_group['group'];
      $timezone = $run_group['timezone'];
      $this->user = new User($this->owner, $group, $timezone);
      $description = 'Cron: <b>' . $this->ActiveUsers() . '</b> active ' .
        'users for ' . ucfirst($group);
      $this->Notification('invoice', 'Invoice', $description, 'system', $group);
    }
  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.invoice.js to the existing dobrado.js file.
    // Note that the module is only available when logged in.
    $this->AppendScript($path, 'dobrado.invoice.js', false);
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS invoice (' .
      'user VARCHAR(50) NOT NULL,' .
      'timestamp INT(10) UNSIGNED NOT NULL,' .
      'invoice_number INT UNSIGNED NOT NULL,' .
      'purchase_id INT UNSIGNED,' .
      'PRIMARY KEY(user, timestamp)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Invoice->Install: ' . $mysqli->error);
    }

    $start_no_purchase =
      $mysqli->escape_string('<div style="text-align: center;">' .
                               '<b>Account Statement</b></div>' . "\n" .
                             '<div style="text-align: right;">' .
                               'Date: !date</div>' . "\n" .
                             '<p>From: Business name<br>' . "\n" .
                               '(Contact details)</p>' . "\n" .
                             '<p>To: !first !last</p>' . "\n" .
                             '<p>You don\'t have any purchases this week, ' .
                               'below is an update of your account.</p>' .
                             "\n");
    $start_purchase =
      $mysqli->escape_string('<div style="text-align: center;">' .
                               '<b>Invoice</b></div>' . "\n" .
                             '<div style="text-align: right;">' .
                               'Date: !date<br>' .
                               'Invoice Number: !number</div>' . "\n" .
                             '<p>From: Business name<br>' .
                               '(Contact details)</p>' . "\n" .
                             '<p>To: !first !last</p>' . "\n" .
                             '<p>Your purchases for this week:</p>' . "\n");
    $tax_included =
      $mysqli->escape_string('<p>Total GST included in this invoice: ' .
                             '$!total</p>');
    $user_info =
      $mysqli->escape_string('<p>You can log in at !server to view your ' .
                               'transactions, using your username <b>!user' .
                               '</b></p>' . "\n" . '<p>!volunteer</p>' . "\n" .
                             '<p>The bank details for business are:<br>' .
                               "\n" . '(bank details)<br>' . "\n" .
                               'Please use your reference: <b>!reference</b> ' .
                               'when paying.</p>' . "\n");
    $current_balance =
      $mysqli->escape_string('<p>Your balance is currently <b>$!balance</b> ' .
                               'owing.</p>' . "\n");
    $old_balance =
      $mysqli->escape_string('<p>Your balance is <b>$!balance</b> owing ' .
                               'after processing was last done on !date.' .
                               '<br>' . "\n" . '(You can ignore this if you ' .
                               'have made a payment since then).</p>' . "\n");
    $no_orders = $mysqli->escape_string('<p>There are no orders for next ' .
                                          'week.</p>' . "\n");
    $orders = $mysqli->escape_string('<p>Here are the orders for next ' .
                                       'week:</p>' . "\n");
    $mysqli->close();

    $template = ['"invoice-day-count","","1"',
                 '"invoice-order-day-count","","8"',
                 '"invoice-start-no-purchase","","' . $start_no_purchase . '"',
                 '"invoice-start-purchase","","' . $start_purchase . '"',
                 '"invoice-after-purchase","","<p>The following is an update ' .
                   'of your account, and is not part of your invoice.</p>"',
                 '"invoice-tax-included","","' . $tax_included . '"',
                 '"invoice-user-info","","' . $user_info . '"',
                 '"invoice-current-balance","","' . $current_balance . '"',
                 '"invoice-old-balance","","' . $old_balance . '"',
                 '"invoice-sign-off","","Thanks, see you next week!"',
                 '"invoice-date-format","","j F Y"',
                 '"invoice-subject","","Invoice for !date"',
                 '"invoice-no-purchase-subject","",' .
                   '"Account statement for !date"',
                 '"invoice-sender","","noreply@!host"',
                 '"invoice-order-subject","","Order for next week"',
                 '"invoice-order-start","","Here is your order for this week:"',
                 '"invoice-group-email","","(group email)"',
                 '"invoice-group-subject","","Orders for next week"',
                 '"invoice-intro-no-orders","","' . $no_orders . '"',
                 '"invoice-intro-orders","","' . $orders . '"',
                 '"invoice-group-attach-files","","true"',
                 '"invoice-group-format","","user"',
                 '"invoice-organisation-format","","vertical"',
                 '"invoice-organisation-sender","","noreply@!host"',
                 '"invoice-organisation-email","","(organisation email)"',
                 '"invoice-organisation-subject","",' .
                   '"Orders for next week"',
                 '"invoice-membership-reminder","","<b>Reminder:</b> ' .
                   'Your membership is due."',
                 '"invoice-user-count", "", "There were !count members who ' .
                   'purchased this month."',
                 '"invoice-surcharge-description", "", "Surcharge"'];
    $this->AddTemplate($template);
    $description = ['invoice-day' => 'The day invoices are sent out.',
                    'invoice-day-count' => 'The number of days to look back ' .
                      'for purchases to include in the invoice.',
                    'invoice-group-attach-files' => 'The string \'true\' or ' .
                      '\'false\' to attach order lists to group stock emails. '.
                      'Useful if there is no parent organisation for the ' .
                      'group receiving the attachments.',
                    'invoice-send-orders' => 'The day of the week to send ' .
                      'orders to individual members. (Separate from ' .
                      'pre-order-final so that orders can be sent closer to ' .
                      'co-op-day as a reminder).',
                    'invoice-send-orders-time' => 'The time of day to send ' .
                      'orders to individual members.',
                    'invoice-ignore-group' => 'The name of a group to ignore ' .
                      'when counting the number of real buyers groups in an ' .
                      'organisation, such as \'non-purchasing\'.',
                    'invoice-start-no-purchase' => 'The start of the invoice ' .
                      'for when no purchases have been made. Substitutes: ' .
                      '!date, !first and !last.',
                    'invoice-start-purchase' => 'The start of the invoice ' .
                      'for when purchases are made. Substitutes: !number ' .
                      '(invoice number), !date, !first, !last.',
                    'invoice-after-purchase' => 'Only included when purchases '.
                      'are made, allows custom text to say that this is the ' .
                      'formal end of the invoice.',
                    'invoice-user-info' => 'Substitutes personal information: '.
                      '!user (username), !volunteer (volunteer message), ' .
                      '!reference (bank reference) and also !server. This is ' .
                      'where bank details should go.',
                    'invoice-balance' => 'The amount used to consider an ' .
                      'account as \'in balance\'.',
                    'invoice-current-balance' => 'Allows !balance to be ' .
                      'substituted without qualifying when transaction ' .
                      'processing was last done.',
                    'invoice-old-balance' => 'Substitutes !balance, and also ' .
                      '!date (the date the transactions were last processed) ' .
                      'so that users can work out if their balance is ' .
                      'accurate.',
                    'invoice-sign-off' => 'A final massage at the end of the ' .
                      'invoice.',
                    'invoice-date-format' => 'A formatting string that will ' .
                      'be used to display the date. Options that can be used ' .
                      'together include: j (day), F (month), Y (year), ' .
                      'W (week)',
                    'invoice-date-offset' => 'The number of days to offset ' .
                      'the current date by when formatting the invoice date',
                    'invoice-subject' => 'A subject line for the invoice ' .
                      'email, can substitute !date or !number, where the ' .
                      'date format is set in invoice-date-format',
                    'invoice-no-purchase-subject' => 'A subject line for the ' .
                      'account statement email when there are no purchases, ' .
                      'can substitute !date where the format is set in ' .
                      'invoice-date-format',
                    'invoice-sender' => 'The \'from\' address of invoice ' .
                      'email, must be on the domain.',
                    'invoice-sender-name' => 'A descriptive name prepended ' .
                      'to the \'from\' address of the invoice email.',
                    'invoice-cc' => 'The cc address to use when an email is ' .
                      'sent from invoice-sender',
                    'invoice-bcc' => 'The bcc address to use when an email ' .
                      'is sent from invoice-sender',
                    'invoice-order-subject' => 'The subject of the individual '.
                       'orders email',
                    'invoice-order-start' => 'The start of the individual ' .
                      'orders email',
                    'invoice-order-cc' => 'The cc address to use when ' .
                      'sending individual order emails',
                    'invoice-order-bcc' => 'The bcc address to use when ' .
                      'sending individual order emails',
                    'invoice-group-subject' => 'A subject of the orders email '.
                      'for the group.',
                    'invoice-remove-orders' => 'The day of the week the ' .
                      'orders are removed.',
                    'invoice-group-email' => 'The \'to\' address for the ' .
                      'orders email for the group.',
                    'invoice-group-cc' => 'A cc address for the orders email ' .
                      'for the group.',
                    'invoice-group-bcc' => 'A bcc address for the orders ' .
                      'email for the group.',
                    'invoice-intro-no-orders' => 'Intro to orders email when ' .
                      'there are no orders recorded.',
                    'invoice-intro-orders' => 'Intro to orders email when ' .
                      'there are orders recorded.',
                    'invoice-attendance-hello' => 'A custom message at the ' .
                      'start of the attendance email.',
                    'invoice-attendance-email' => 'The \'to\' address for the '.
                      'attendance email.',
                    'invoice-attendance-cc' => 'The cc address to use when an '.
                      'email is sent from invoice-attendance-email',
                    'invoice-attendance-bcc' => 'The bcc address to use when ' .
                      'an email is sent from invoice-attendance-email',
                    'invoice-attendance-subject' => 'The subject line for the '.
                      'attendance email.',
                    'invoice-order-rounding' => 'A value between zero and ' .
                      'one, used to decide if the number of packs should be ' .
                      'rounded up or down. If empty the number of packs will ' .
                      'be left as a fraction.',
                    'invoice-organisation-sender' => 'The \'from\' address ' .
                      'of the orders email for the organisation, must be on ' .
                      'the domain.',
                    'invoice-organisation-sender-name' => 'A descriptive ' .
                      'name for the \'from\' address for the organisation ' .
                      'orders email.',
                    'invoice-organisation-email' => 'The \'to\' address of ' .
                      'the orders email for the organisation.',
                    'invoice-organisation-subject' => 'The subject line of ' .
                      'the orders email for the organisation.',
                    'invoice-group-format' => 'The string \'user\' or ' .
                      '\'product\' for choosing the display format. The ' .
                      'group email will be ordered by this category.',
                    'invoice-organisation-format' => 'The string ' .
                      '\'vertical\' or \'horizontal\' for choosing the ' .
                      'display format. Horizontal is useful for more than ' .
                      'one group, as it displays the group names in their ' .
                      'own columns. Vertical only displays a row for a group ' .
                      'if there is an order for that item by the group.',
                    'invoice-extra-column' => 'The name of an extra column ' .
                      'in the supplier spreadsheet, in case they want to ' .
                      'provide feedback on the order.',
                    'invoice-show-grower' => 'Show grower information ' .
                      'rather than the supplier name in orders and invoices.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"","#invoice-form label","width","14em"',
                   '"","#invoice-form label[for=invoice-export-data]",' .
                     '"float","none"',
                   '"","#invoice-form label[for=invoice-email-self]",' .
                     '"float","none"',
                   '"","#invoice-form label[for=invoice-email-user]",' .
                     '"float","none"',
                   '"","#invoice-date-options-button","width","10em"',
                   '"","#invoice-form .submit","float","right"',
                   '"","#invoice-form .search","float","right"',
                   '"","#invoice-form .search","margin-right","10px"',
                   '"","#invoice-form .clear","margin-left","10px"',
                   '"","#invoice-form .info","float","right"',
                   '"","#invoice-form .info","margin","10px"',
                   '"","#invoice-form .info","font-weight","bold"'];
    $this->AddSiteStyle($site_style);
    $dependencies = ['banking', 'detail', 'invite', 'payment', 'purchase',
                     'roster', 'stock'];
    return $this->Dependencies($dependencies);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Append dobrado.invoice.js to the existing dobrado.js file.
    $this->AppendScript($path, 'dobrado.invoice.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  public function NextOrder($create_files, $organisation = false,
                            $group_list = [],
                            $send_email = true, $start = 0, $end = 0) {
    if ($start === 0 && $end  === 0) {
      // Note that the day set for invoice-group-order can't be used here,
      // as that might result in a timestamp for the following week.
      $lead_time = (int)$this->Substitute('invoice-group-lead-time');
      $start = $lead_time === 0 ? time() : strtotime($lead_time . ' days');
      // Make sure the start timestamp is from the beginning of the day.
      $start = strtotime(date('F j Y 00:00:00', $start));
      $order_days = (int)$this->Substitute('invoice-order-day-count');
      $end = strtotime(($lead_time + $order_days) . ' days');
      // Make sure the end timestamp is at the end of the day.
      $end = strtotime(date('F j Y 23:59:59', $end));
    }
    $stock = new Stock($this->user, $this->owner);
    $products = $stock->AvailableProducts(false, false);
    $purchase = new Purchase($this->user, $this->owner);
    // Before getting the data for the orders, check if they should be
    // rounded to whole box sizes.
    if ($organisation) {
      if ($this->Substitute('invoice-organisation-rounding') === 'true') {
        $purchase->AdjustOrders($start, $end, true);
        // AdjustOrders does quite a few database updates, so need to sleep
        // for a few seconds before the call to AllData as there appears to
        // be a race condition.
        sleep(5);
      }
    }
    else if ($this->Substitute('invoice-group-rounding') === 'true') {
      $purchase->AdjustOrders($start, $end);
      sleep(5);
    }
    // If the current group is the host for a multi-group buy, call AllData as
    // an 'organisation' and create group_list from those that have joined.
    $invite = new Invite($this->user, $this->owner);
    if ($invite->Host()) {
      $group_list = $invite->Joined();
      $all_data = $purchase->AllData($start, $end, false, true);
      $result = $this->ProcessNextOrder($all_data, $products,
                                        $group_list, $send_email, $start);
      // For individual group emails, need to get AllData again, this time as a
      // 'group' so that individual members are listed. Then group the data by
      // the member's system groups so they can be passed to ProcessNextOrder.
      $group_data = [];
      $group_members = $this->GroupMembers($group_list);
      $all_data = $purchase->AllData($start, $end, false, false);
      foreach ($all_data as $user => $order) {
        foreach ($group_list as $group_name) {
          if (in_array($user, $group_members[$group_name])) {
            if (!isset($group_data[$group_name])) {
              $group_data[$group_name] = [];
            }
            $group_data[$group_name][$user] = $order;
            break;
          }
        }
      }
      foreach ($group_list as $group_name) {
        $data = isset($group_data[$group_name]) ? $group_data[$group_name] : [];
        $group_result = $this->ProcessNextOrder($data, $products, [],
                                                $send_email, $start, false,
                                                true, $group_name);
        if (isset($group_result['error'])) {
          if (isset($result['error'])) {
            $result['error'] .= '<br>' . $group_result['error'];
          }
          else {
            $result['error'] = $group_result['error'];
          }
        }
      }
      return $result;
    }

    $all_data = $purchase->AllData($start, $end, false, $organisation);
    return $this->ProcessNextOrder($all_data, $products, $group_list,
                                   $send_email, $start, $organisation,
                                   $create_files);
  }

  public function ShowOrder($group, $format) {
    $path = $this->user->config->PrivatePath() . '/' .
      $this->user->config->ServerName() . '/group/' . $group;
    if (!is_dir($path)) return ['content' => 'No order available.'];

    $info = '';
    $date = '';
    $saved = NULL;
    if ($handle = opendir($path)) {
      while (($file = readdir($handle)) !== false) {
        if (is_file($path . '/' . $file) &&
            preg_match('/^([0-9]{4}-[0-9]{2}-[0-9]{2})\.txt/', $file, $match)) {
          $date = $match[1];
          $info = 'Found order from: <b>' . $date . '</b>.';
          $saved = unserialize(file_get_contents($path . '/' . $file));
          break;
        }
      }
      closedir($handle);
    }
    $orders = isset($saved['orders']) ? $saved['orders'] : [];
    $products = isset($saved['products']) ? $saved['products'] : [];
    $groups = isset($saved['groups']) ? $saved['groups'] : [];
    $timestamp = isset($saved['timestamp']) ? $saved['timestamp'] : time();
    if (count($orders) === 0) {
      return ['content' => 'No order available.'];
    }

    $message = '';
    $files = [];
    if (in_array($format, ['horizontal', 'vertical'])) {
      list($message, $data) =
        $this->FormatOrganisationOrders($orders, $products, $groups, $format);
      $files = $this->WriteData($data, $groups, $timestamp);
    }
    else if (in_array($format, ['user', 'product', 'packing'])) {
      list($message, $data) =
        $this->FormatGroupOrders($orders, $products, $format);
      $files = $this->WriteData($data, [], $timestamp);
    }
    // A new path is generated for this user, which is used with private.php.
    $path = $this->user->config->PrivatePath() . '/' .
      $this->user->config->ServerName() . '/user/' . $this->user->name;
    if (!is_dir($path)) mkdir($path, 0755, true);
    $order = '';
    if ($message !== '') {
      $message = '<html><head><title>' . $date . "</title></head>\n" .
        "<body>\n" . $message . "\n</body></html>";
      $order = $path . '/' . $group . '-order.html';
      if (file_exists($order)) unlink($order);
      if ($handle = fopen($order, 'w')) {
        fwrite($handle, $message);
        fclose($handle);
      }
      else {
        $this->Log('Invoice->ShowOrder: Error opening file: ' . $order);
        $order = '';
      }
    }
    $filenames = [];
    foreach ($files as $name => $content) {
      if (file_exists($path . '/' . $name)) unlink($path . '/' . $name);
      if ($handle = fopen($path . '/' . $name, 'w')) {
        fwrite($handle, $content);
        fclose($handle);
        $filenames[] = $name;
      }
      else {
        $this->Log('Invoice->ShowOrder: Error opening file: ' .
                   $path . '/' . $name);
      }
    }
    return ['content' => $info, 'name' => $group . '-order.html',
            'files' => $filenames];
  }

  // Private functions below here ////////////////////////////////////////////

  private function ActiveUsers() {
    $count = 0;
    $organiser = new Organiser($this->user, $this->owner);

    $mysqli = connect_db();
    $query = 'SELECT DISTINCT invoice.user FROM invoice LEFT JOIN users ON ' .
      'invoice.user = users.user WHERE ' . $organiser->GroupQuery() .
      ' AND timestamp >= ' . strtotime('first day of last month, 00:00') .
      ' AND timestamp <= ' . strtotime('last day of last month, 23:59');
    if ($mysqli_result = $mysqli->query($query)) {
      $count = $mysqli_result->num_rows;
      $mysqli_result->close();
    }
    else {
      $this->Log('Invoice->ActiveUsers 1: ' . $mysqli->error);
    }
    // Also store the count in a template so that it's accessible by the
    // Invoice module which displays it descriptively.
    $query = 'INSERT INTO template VALUES ("invoice-active-count", ' .
      '"' . $this->user->group . '", ' . $count . ') ON DUPLICATE KEY UPDATE ' .
      'content = ' . $count;
    if (!$mysqli->query($query)) {
      $this->Log('Invoice->ActiveUsers 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return $count;
  }

  private function Data($organisation, $list_users = '') {
    $start = strtotime('00:00:00');
    $end = $start + 86399;
    $invoice_day_count = (int)$this->Substitute('invoice-day-count');
    if ($invoice_day_count > 1) {
      if ($this->Substitute('invoice-look-forward') === 'true') {
        $end = strtotime($invoice_day_count . ' days');
      }
      else {
        $start = strtotime('-' . $invoice_day_count . ' days');
      }
    }
    $banking = new Banking($this->user, $this->owner);
    $all_settings = $banking->AllSettings();
    $payment = new Payment($this->user, $this->owner);
    $purchase = new Purchase($this->user, $this->owner);
    // TODO: Check if tax should be shown for the group and call AllTaxable.
    $purchase_totals = $purchase->AllTotals($start, $end, $organisation);
    $purchase_ids = $purchase->AllIds($start, $end, $organisation);
    $sold_totals = $purchase->AllSold($start, $end);
    // Check if any surcharges have already been calculated.
    $all_surcharge = $purchase->AllSurcharge($start, $end);
    // Get a list of payment totals for all users, so that they can be
    // compared to purchase totals to find outstanding debts.
    $payment_totals = $payment->AllTotals($organisation);
    // Look up the configured value for when an account is in balance.
    // Note that this function works with the negative of the balance and then
    // negates it at the end, so do the same with the configured value.
    $invoice_balance = (float)$this->Substitute('invoice-balance') * -1;
    // Note that AllOutstanding returns how much a user owes, so negate
    // it to show a balance.
    $all_outstanding = $purchase->AllOutstanding($payment_totals, $end,
                                                 $organisation);
    $result = [];
    foreach ($all_outstanding as $user => $balance) {
      $purchase_value = 0;
      if (isset($purchase_totals[$user])) {
        $purchase_value = $purchase_totals[$user];
      }
      $id = 0;
      if (isset($purchase_ids[$user])) {
        $id = $purchase_ids[$user];
      }
      $sold = 0;
      if (isset($sold_totals[$user])) {
        $sold = $sold_totals[$user];
      }
      $surcharge = 0;
      if (isset($all_surcharge[$user])) {
        $surcharge = $all_surcharge[$user];
      }
      $credit = 0;
      if (isset($all_settings[$user])) {
        $credit = $all_settings[$user]['credit'];
      }
      $active = 0;
      if (isset($all_settings[$user])) {
        $active = $all_settings[$user]['active'];
      }
      // Don't list inactive users unless all users were requested.
      if ($list_users !== 'all' && $active === 0) continue;
      // Don't add the user if their account is in balance and they haven't
      // purchased or sold anything. (If list_users is set this condition
      // will be ignored so that all or active users can be displayed.)
      if ($list_users === '' && $balance < $invoice_balance + 0.1 &&
          $balance > $invoice_balance - 0.1 && $sold > -0.01 &&
          $sold < 0.01 && $purchase_value > -0.01 && $purchase_value < 0.01) {
        continue;
      }
      $result[] = ['name' => $user, 'id' => $id,
                   'purchases' => price_string($purchase_value),
                   'surcharge' => price_string($surcharge),
                   'sold' => price_string($sold),
                   'balance' => price_string($balance * -1),
                   'credit' => $credit, 'active' => $active];
    }
    return $result;
  }

  private function AddSurcharge() {
    // Add up the total surcharge for the group to add to a graph.
    $total_surcharge = 0;
    $banking = new Banking($this->user, $this->owner);
    $all_settings = $banking->AllSettings();

    $purchase = new Purchase($this->user, $this->owner);
    $payment = new Payment($this->user, $this->owner);
    $stock = new Stock($this->user, $this->owner);
    // The surcharge supplier can be overriden so that the group hosting a
    // multi-group purchase can receive the surcharge.
    $surcharge_supplier = $this->Substitute('surcharge-supplier-override');
    if ($surcharge_supplier === '') {
      $surcharge_supplier = $stock->SurchargeSupplier();
    }

    foreach ($this->Data(false) as $data) {
      $user = $data['name'];
      if ($all_settings[$user]['surcharge']) {
        $purchase_value = (float)$data['purchases'];
        if ($purchase_value > -0.01 && $purchase_value < 0.01) continue;

        $surcharge = $data['surcharge'];
        if ($surcharge < 0.01) {
          $surcharge = $payment->Surcharge($purchase_value);
          if ($surcharge > 0) {
            $total_surcharge += $surcharge;
            $purchase->AddPurchase($user, time(), 'surcharge',
                                   $surcharge_supplier, 1, $surcharge,
                                   $surcharge, $data['id'], $this->user->name);
          }
        }
      }
    }
    if ($total_surcharge > 0) {
      $this->AddGraphData($total_surcharge);
    }
    return $this->Data(false);
  }

  private function Number($user, $start, $end, $purchase_id) {
    $number = 0;
    $mysqli = connect_db();
    if ($purchase_id !== 0) {
      $query = 'SELECT invoice_number FROM invoice WHERE ' .
        'user = "' . $user . '" AND purchase_id = ' . $purchase_id;
      if ($mysqli_result = $mysqli->query($query)) {
        if ($invoice = $mysqli_result->fetch_assoc()) {
          $number = (int)$invoice['invoice_number'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Invoice->Number 1: ' . $mysqli->error);
      }
    }
    if ($number === 0) {
      $query = 'SELECT invoice_number FROM invoice WHERE ' .
        'user = "' . $user . '" AND timestamp >= ' . $start . ' AND ' .
        'timestamp <= ' . $end;
      if ($mysqli_result = $mysqli->query($query)) {
        if ($invoice = $mysqli_result->fetch_assoc()) {
          $number = (int)$invoice['invoice_number'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Invoice->Number 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    return $number;
  }

  private function RemoveInvoice() {
    $us_all_data = json_decode($_POST['data'], true);
    foreach ($us_all_data as $us_data) {
      $this->RemoveInvoiceFromDate($us_data['date'], $us_data['name'],
                                   $us_data['purchaseId'], $us_data['number']);
    }
    return ['done' => true];
  }

  private function RemoveInvoiceFromDate($us_date, $us_name,
                                         $us_purchase_id, $us_number) {
    $mysqli = connect_db();
    $username = $mysqli->escape_string($us_name);
    $mysqli->close();

    $invoice_date = date('F j Y', (int)$us_date / 1000);
    $start = strtotime($invoice_date . ' 00:00:00');
    $end = $start + 86399;
    $purchase_id = (int)$us_purchase_id;
    $invoice_number = (int)$us_number;
    // Add the number of days to look forward from the end, or subtract the
    // number of days to look back from the start. When it's set to 1, only
    // purchases on the date provided are used.
    $invoice_day_count = (int)$this->Substitute('invoice-day-count');
    if ($invoice_day_count > 1) {
      if ($this->Substitute('invoice-look-forward') === 'true') {
        $end += $invoice_day_count * 86400;
      }
      else {
        $start -= $invoice_day_count * 86400;
      }
    }
    $purchase = new Purchase($this->user, $this->owner);
    $search_data = $purchase->Search($username, 0, '', '', $purchase_id, '', '',
                                     $start, $end);
    foreach ($search_data as $data) {
      // Note that search dates are returned in milliseconds.
      $purchase->RemovePurchase($username, $data['date'] / 1000, $data['name'],
                                $data['supplier'], $data['quantity']);
    }
    if ($invoice_number !== 0) {
      $mysqli = connect_db();
      $query = 'DELETE FROM invoice WHERE user = "' . $username . '" AND ' .
        'invoice_number = ' . $invoice_number;
      if (!$mysqli->query($query)) {
        $this->Log('Invoice->RemoveInvoiceFromDate: ' . $mysqli->error);
      }
      $mysqli->close();
    }
  }

  private function Search() {
    $mysqli = connect_db();
    $username = $mysqli->escape_string($_POST['username']);
    $invoice_date = $mysqli->escape_string($_POST['invoiceDate']);
    $start_date = $mysqli->escape_string($_POST['startDate']);
    $end_date = $mysqli->escape_string($_POST['endDate']);
    $mysqli->close();

    $purchase = new Purchase($this->user, $this->owner);
    $result = ['data' => []];
    $start = 0;
    $end = 0;

    if ($invoice_date !== '') {
      $start = strtotime($invoice_date . ' 00:00:00');
      $end = strtotime($invoice_date . ' 23:59:59');
      $all_surcharge = $purchase->AllSurcharge($start, $end, true, true);
      $purchase_totals = $purchase->AllTotals($start, $end, true, false, true);
      foreach ($purchase_totals as $data) {
        $user = $data['user'];
        if ($username !== '' && $username !== $user) continue;
        if (abs($data['total']) < 0.01) continue;

        $id = $data['id'];
        $surcharge = 0;
        if (isset($all_surcharge[$user][$id])) {
          $surcharge = $all_surcharge[$user][$id];
        }
        $result['data'][] =
          ['date' => $data['timestamp'] * 1000, 'name' => $user,
           'purchases' => price_string($data['total']),
           'surcharge' => price_string($surcharge), 'purchaseId' => $id,
           'number' => $this->Number($user, $start, $end, $id)];
      }
    }
    else if ($start_date !== '' && $end_date !== '') {
      $start = strtotime($start_date . ' 00:00:00');
      $end = strtotime($end_date . ' 23:59:59');
      // Create purchase totals per day.
      $start_day = $start;
      $end_day = $start + 86399;
      while ($end_day < $end) {
        $all_surcharge =
          $purchase->AllSurcharge($start_day, $end_day, true, true);
        $purchase_totals =
          $purchase->AllTotals($start_day, $end_day, true, false, true);
        foreach ($purchase_totals as $data) {
          $user = $data['user'];
          if ($username !== '' && $username !== $user) continue;
          if (abs($data['total']) < 0.01) continue;

          $id = $data['id'];
          $surcharge = 0;
          if (isset($all_surcharge[$user][$id])) {
            $surcharge = $all_surcharge[$user][$id];
          }
          $result['data'][] =
            ['date' => $data['timestamp'] * 1000, 'name' => $user,
             'purchases' => price_string($data['total']),
             'surcharge' => price_string($surcharge), 'purchaseId' => $id,
             'number' => $this->Number($user, $start_day, $end_day, $id)];
        }
        $start_day += 86400;
        $end_day += 86400;
      }
    }
    else if ($username === '') {
      $timestamp = time();
      $invoice_number = (int)$_POST['invoiceNumber'];
      if ($invoice_number === 0) {
        // Search is called on page load with no search terms set, so return
        // default data which is the purchases matching the invoice settings for
        // the group. Also only provide user details on page load.
        $detail = new Detail($this->user, $this->owner);
        $result['userDetails'] = $detail->AllUsers(true);
      }
      else {
        // If an invoice number is provided lookup the timestamp and use that
        // to find start and end dates for purchases.
        $mysqli = connect_db();
        $query = 'SELECT timestamp, purchase_id FROM invoice WHERE ' .
          'invoice_number = ' . $invoice_number;
        if ($mysqli_result = $mysqli->query($query)) {
          if ($invoice = $mysqli_result->fetch_assoc()) {
            $timestamp = (int)$invoice['timestamp'];
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Invoice->Search: ' . $mysqli->error);
        }
        $mysqli->close();
        if ($timestamp === 0) return ['error' => 'Invoice number not found.'];
      }
      $invoice_date = date('F j Y', $timestamp);
      $start = strtotime($invoice_date . '00:00:00');
      $end = $start + 86399;
      $invoice_day_count = (int)$this->Substitute('invoice-day-count');
      if ($invoice_day_count > 1) {
        if ($this->Substitute('invoice-look-forward') === 'true') {
          $end += $invoice_day_count * 86400;
        }
        else {
          $start -= $invoice_day_count * 86400;
        }
      }
      $all_surcharge = $purchase->AllSurcharge($start, $end, true, true);
      $purchase_totals = $purchase->AllTotals($start, $end, true, false, true);
      foreach ($purchase_totals as $data) {
        if (abs($data['total']) < 0.01) continue;

        $user = $data['user'];
        $id = $data['id'];
        $number = $this->Number($user, $start, $end, $id);
        if ($invoice_number !== 0 && $invoice_number !== $number) continue;

        $surcharge = 0;
        if (isset($all_surcharge[$user][$id])) {
          $surcharge = $all_surcharge[$user][$id];
        }
        $result['data'][] =
          ['date' => $data['timestamp'] * 1000, 'name' => $user,
           'purchases' => price_string($data['total']),
           'surcharge' => price_string($surcharge), 'purchaseId' => $id,
           'number' => $number];
      }
    }
    else {
      $result['error'] = 'Please provide search dates.';
    }
    if ($_POST['exportData'] === '1' && count($result['data']) > 0) {
      $filename = 'invoice-' . date('Y-m-d') . '.csv';
      $this->CreateCSV($filename, $result['data']);
      $result['filename'] = $filename;
    }
    return $result;
  }

  private function SendInvoice($data) {
    if (!is_array($data)) return ['error' => 'SendInvoice: data not set'];

    $user = $data['name'];
    $purchase_id = (int)$data['id'];
    $purchase_value = (float)$data['purchases'];
    $surcharge = (float)$data['surcharge'];
    $sold = (float)$data['sold'];
    $balance = (float)$data['balance'];
    $credit = (int)$data['credit'];

    $mysqli = connect_db();
    $invoice_date = time();
    $invoice_number = 0;
    // Only create a new invoice number if there are purchases for this user.
    if ($purchase_value > 0) {
      $organiser = new Organiser($this->user, $this->owner);
      $query = 'SELECT MAX(invoice_number) AS invoice_number FROM invoice ' .
        'LEFT JOIN users ON invoice.user = users.user WHERE ' .
        $organiser->GroupQuery();
      if ($mysqli_result = $mysqli->query($query)) {
        if ($invoice = $mysqli_result->fetch_assoc()) {
          $invoice_number = (int)$invoice['invoice_number'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Invoice->SendInvoice 1: ' . $mysqli->error);
      }
      $invoice_number++;
      $query = 'INSERT INTO invoice VALUES ("' . $user . '", ' .
        $invoice_date . ', ' . $invoice_number . ', ' . $purchase_id . ')';
      if (!$mysqli->query($query)) {
        $this->Log('Invoice->SendInvoice 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();

    // Get the users details.
    $email = '';
    $first = '';
    $last = '';
    $detail = new Detail($this->user, $this->owner);
    $user_details = $detail->User($user);
    if (is_array($user_details)) {
      $email = $user_details['email'];
      $first = $user_details['first'];
      $last = $user_details['last'];
      // Make sure the invoice is addressed to someone.
      if ($first === '' && $last === '') {
        $first = $user;
      }
    }
    // Don't create a notification if an email address wasn't found.
    if ($email === '') return ['error' => ''];

    $invoice_day_count = (int)$this->Substitute('invoice-day-count');
    if ($invoice_day_count === 0) $invoice_day_count = 1;
    $start = 0;
    $end = 0;
    if ($this->Substitute('invoice-look-forward') === 'true') {
      // Go back a few minutes in case a surcharge was just added.
      $start = strtotime('-5 minutes');
      $end = strtotime($invoice_day_count . ' days');
    }
    else if ($invoice_day_count === 1) {
      // Special case if only one day is specified, set the start time to the
      // start of today.
      $start = strtotime('00:00:00');
      $end = strtotime('23:59:59');
    }
    else {
      $start = strtotime('-' . $invoice_day_count . ' days');
      $end = strtotime('23:59:59');
    }
    $message = $this->FormatInvoice($user, $first, $last, $invoice_number,
                                    $start, $end, $purchase_id, $purchase_value,
                                    $surcharge, $sold, $balance, $credit,
                                    $invoice_date);
    $subject = '';
    $date = time();
    $date_format = $this->Substitute('invoice-date-format');
    $date_offset = $this->Substitute('invoice-date-offset');
    if ($date_offset !== '') $date += (int)$date_offset * 86400;
    if ($invoice_number === 0) {
      $subject = $this->Substitute('invoice-no-purchase-subject', '/!date/',
                                   date($date_format, $date));
    }
    else {
      $subject = $this->Substitute('invoice-subject', ['/!date/', '/!number/'],
                                   [date($date_format, $date),
                                    $invoice_number]);
    }
    $sender = $this->Substitute('invoice-sender', '/!host/',
                                $this->user->config->ServerName());
    $sender_name = $this->Substitute('invoice-sender-name');
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $cc = $this->Substitute('invoice-cc');
    $bcc = $this->Substitute('invoice-bcc');
    $headers = "MIME-Version: 1.0\r\n" .
      "Content-Type: text/html; charset=utf-8\r\n" .
      'From: ' . $sender_name . "\r\n";
    if ($cc !== '') {
      $headers .= 'Cc: ' . $cc . "\r\n";
    }
    if (dobrado_mail($email, $subject, $message, $headers,
                     $bcc, $sender, $user, 'invoice', 'sendInvoice')) {
      return ['done' => true];
    }
    return ['error' => 'SendInvoice: Email to ' . $user .
            ' (' . $email . ') not accepted for delivery'];
  }

  private function SendOrders() {
    $stock = new Stock($this->user, $this->owner);
    $products = $stock->AvailableProducts(false, false);

    $roster = new Roster($this->user, $this->owner);
    $purchase = new Purchase($this->user, $this->owner);
    $detail = new Detail($this->user, $this->owner);
    $all_users = $detail->AllUsers(false, true);
    // Also want to show descriptive names for supplier accounts.
    $all_suppliers = $detail->AllSuppliers();

    $lead_time = (int)$this->Substitute('invoice-order-lead-time');
    $start = $lead_time === 0 ? time() : strtotime($lead_time . ' days');
    $order_days = (int)$this->Substitute('invoice-order-day-count');
    $end = strtotime(($lead_time + $order_days) . ' days');
    $show_grower = $this->Substitute('invoice-show-grower') === 'true';
    $subject = $this->Substitute('invoice-order-subject');
    $sender = $this->Substitute('invoice-sender', '/!host/',
                                $this->user->config->ServerName());
    $sender_name = $this->Substitute('invoice-sender-name');
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $cc = $this->Substitute('invoice-order-cc');
    $bcc = $this->Substitute('invoice-order-bcc');
    $headers = "MIME-Version: 1.0\r\n" .
      "Content-Type: text/html; charset=utf-8\r\n" .
      'From: ' . $sender_name . "\r\n";
    if ($cc !== '') {
      $headers .= 'Cc: ' . $cc . "\r\n";
    }

    $sent = 0;
    foreach($all_users as $user => $details) {
      $all_data = $purchase->Data($start, $end, false, $user);
      if (count($all_data) === 0) continue;

      $email = $details['email'];
      if ($email === '') continue;

      $first = $details['first'];
      if ($first === '') $first = $user;
      $message = '<html><head><title>' . $subject . "</title>\n" .
        '<style>body { font-family: Arial, sans-serif; } ' .
          'table { border: 0; border-collapse: collapse; } ' .
          'th { background-color: #f0f0f0; padding: 2px; } ' .
          'td { padding: 5px; }</style>' . "\n" .
        "</head>\n" .
        "<body>\n" .
        '<p>Hello ' . $first . ",</p>\n" .
        '<p>' . $roster->Description($user) . "</p>\n" .
        '<p>' . $this->Substitute('invoice-order-start') . "</p>\n" .
        "<table>\n" .
        '<tr><th>Product</th><th>Supplier</th><th>Quantity</th><th>Price</th>' .
        "</tr>\n";
      foreach ($all_data as $data) {
        $name = $data['name'];
        // Data coming from the Purchase module is not escaped, but names from
        // the Detail module are. Need a better way to know what needs escaping.
        $supplier = htmlspecialchars($data['grower']);
        if (!$show_grower || $supplier === '') {
          $supplier = $data['supplier'];
          if (isset($all_suppliers[$supplier])) {
            if ($all_suppliers[$supplier]['first'] !== '') {
              $supplier = $all_suppliers[$supplier]['first'] . ' ' .
                $all_suppliers[$supplier]['last'];
            }
          }
        }
        $message .= '<tr><td>' . htmlspecialchars($name) . '</td><td>' .
          $supplier . '</td><td>' . $data['quantity'] . '</td><td>$' .
          price_string($data['price']) . '/' . $products[$name]['unit'] .
          '</td></tr>' . "\n";
      }
      $message .= "</table>\n" .
        '</body></html>';
      $message = wordwrap($message);

      if (dobrado_mail($email, $subject, $message, $headers,
                       $bcc, $sender, $user, 'invoice', 'sendOrder')) {
        $sent++;
      }
      else {
        // Add a notification for unsent orders.
        $description = 'SendOrders: Email to ' . $email .
          ' not accepted for delivery';
        if ($this->user->group !== '') {
          $description .= '(' . ucfirst($this->user->group) . ')';
        }
        $this->Notification('invoice', 'Invoice', $description, 'system',
                            $this->user->group);
      }
    }
    if ($sent === 1) {
      $description = 'SendOrders: Sent <b>1</b> order';
      if ($this->user->group !== '') {
        $description .= ' for ' . ucfirst($this->user->group);
      }
      $this->Notification('invoice', 'Invoice', $description, 'system',
                          $this->user->group);
    }
    else if ($sent > 1) {
      $description = 'SendOrders: Sent <b>' . $sent . '</b> orders';
      if ($this->user->group !== '') {
        $description .= ' for ' . ucfirst($this->user->group);
      }
      $this->Notification('invoice', 'Invoice', $description, 'system',
                          $this->user->group);
    }
  }

  private function GroupMembers($group_list) {
    $group_members = [];
    $group_query = '';
    foreach ($group_list as $group) {
      if ($group_query !== '') $group_query .= ' OR ';
      $group_query .= 'system_group = "' . $group . '"';
    }
    if ($group_query === '') return [];

    $mysqli = connect_db();
    $query = 'SELECT user, system_group FROM users WHERE ' . $group_query;
    if ($mysqli_result = $mysqli->query($query)) {
      while ($users = $mysqli_result->fetch_assoc()) {
        $group = $users['system_group'];
        if (!isset($group_members[$group])) {
          $group_members[$group] = [];
        }
        $group_members[$group][] = $users['user'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Invoice->GroupMembers: ' . $mysqli->error);
    }
    $mysqli->close();
    return $group_members;
  }

  private function ProcessNextOrder($orders, $products, $group_list,
                                    $send_email, $start, $organisation = true,
                                    $create_files = true, $invite_group = '') {
    // StoreData is called here to simplify the NextOrder function.
    $store_data = ['orders' => $orders, 'products' => $products,
                   'groups' => $group_list, 'timestamp' => $start];
    $this->StoreData($store_data, $invite_group);
    if (!$send_email) return ['done' => true];

    $files = [];
    $email = '';
    $subject = '';
    $sender = '';
    $sender_name = '';
    $cc = '';
    $bcc = '';
    if ($organisation) {
      $email = $this->Substitute('invoice-organisation-email');
      $subject = $this->Substitute('invoice-organisation-subject');
      $sender = $this->Substitute('invoice-organisation-sender', '/!host/',
                                  $this->user->config->ServerName());
      $sender_name = $this->Substitute('invoice-organisation-sender-name');
      if ($sender_name === '') {
        $sender_name = $sender;
      }
      else {
        $sender_name .= ' <' . $sender . '>';
      }
      $cc = $this->Substitute('invoice-organisation-cc');
      $bcc = $this->Substitute('invoice-organisation-bcc');
    }
    else {
      $email = $this->Substitute('invoice-group-email');
      $subject = $this->Substitute('invoice-group-subject');
      $sender = $this->Substitute('invoice-sender', '/!host/',
                                  $this->user->config->ServerName());
      $sender_name = $this->Substitute('invoice-sender-name');
      if ($sender_name === '') {
        $sender_name = $sender;
      }
      else {
        $sender_name .= ' <' . $sender . '>';
      }
      // Invite group orders are also sent to the host group, but a copy is
      // sent to each participating group listing their own members.
      if ($invite_group !== '') {
        $cc = $this->Substitute('invoice-group-email', '', '', $invite_group);
      }
      else {
        $cc = $this->Substitute('invoice-group-cc');
      }
      $bcc = $this->Substitute('invoice-group-bcc');
    }

    $message = '<html><head><title>' . $subject . "</title></head>\n" .
      "<body>\n";
    if (count($orders) === 0) {
      $message .= $this->Substitute('invoice-intro-no-orders');
    }
    else if ($organisation) {
      list($order_text, $data) =
        $this->FormatOrganisationOrders($orders, $products, $group_list);
      $message .= $order_text;
      $files = $this->WriteData($data, $group_list);
    }
    else {
      list($order_text, $data) = $this->FormatGroupOrders($orders, $products);
      $message .= $order_text;
      if ($create_files) $files = $this->WriteData($data);
    }
    $message .= '</body></html>';
    $message = wordwrap($message);

    $headers = "MIME-Version: 1.0\r\n" .
      'From: ' . $sender_name . "\r\n";
    if ($cc !== '' && $cc !== $email) {
      $headers .= 'Cc: ' . $cc . "\r\n";
    }
    if ($bcc !== '') {
      $headers .= 'Bcc: ' . $bcc . "\r\n";
    }

    // The files array is only used in 'organisation' mode for supplier data.
    if (count($files) !== 0) {
      $uid = uniqid();
      $headers .= 'Content-Type: multipart/mixed; boundary=' . $uid . "\r\n";
      $body = "This is a multi-part message in MIME format.\r\n" .
        '--' . $uid . "\r\n" .
        "Content-Type: text/html; charset=utf-8\r\n\r\n" .
        $message . "\r\n\r\n";
      foreach ($files as $name => $content) {
        $body .= '--' . $uid . "\r\n" .
          'Content-Type: application/octet-stream; name=' . $name . "\r\n" .
          "Content-Transfer-Encoding: base64\r\n" .
          'Content-Disposition: attachment; filename=' . $name . "\r\n\r\n" .
          chunk_split(base64_encode($content)) . "\r\n\r\n";
      }
      $body .= '--' . $uid . '--';
      if (mail($email, $subject, $body, $headers, '-f ' . $sender)) {
        return ['done' => true];
      }
      return ['error' => 'Email to ' . $email . ' not accepted for delivery.'];
    }

    $headers .= "Content-Type: text/html; charset=utf-8\r\n";
    if (mail($email, $subject, $message, $headers, '-f ' . $sender)) {
      return ['done' => true];
    }
    return ['error' => 'Email to ' . $email . ' not accepted for delivery.'];
  }

  private function FormatInvoice($user, $first, $last, $invoice_number,
                                 $start, $end, $purchase_id,
                                 $purchase_value, $surcharge, $sold,
                                 $balance, $credit, $invoice_date) {
    $purchase = new Purchase($this->user, $this->owner);
    $stock = new Stock($this->user, $this->owner);
    $detail = new Detail($this->user, $this->owner);
    $xero = new Module($this->user, $this->owner, 'xero');
    $xero_enabled = $xero->IsInstalled() && $xero->Factory('Enabled');
    $all_suppliers = $detail->AllSuppliers();
    $code = $this->Substitute('invoice-account-code');
    $terms = (int)$this->Substitute('invoice-terms') * 86400;
    $tax_percent = (float)$this->Substitute('stock-tax-percent');
    $tax_multiplier = $tax_percent / (100 + $tax_percent);
    $tax_included = 0;
    $total = 0;
    $line_items = '';
    $message = '<html><head><title>Invoice</title>' . "\n" .
      '<style>body { font-family: Arial, sans-serif; } ' . "\n" .
        'table { border: 0; border-collapse: collapse; } ' . "\n" .
        'th { background-color: #f0f0f0; padding: 2px; } ' . "\n" .
        'td { padding: 5px; }</style>' . "\n" .
      '</head>' . "\n" .
      '<body>' . "\n";
    // The start of the invoice is created via substitutions for the group.
    $message .= $this->FormatInvoiceStart($first, $last, $invoice_number,
                                          $invoice_date);
    $products = $stock->AllProducts(false);
    // Next the table of purchases are displayed.
    if ($purchase_value > 0) {
      $message .= '<table><tr><th>Product</th><th>Supplier</th>' . "\n" .
        '<th>Quantity</th><th>Price</th><th>Total</th></tr>' . "\n";

      $supplier = '';
      $grower = '';
      $show_grower = $this->Substitute('invoice-show-grower') === 'true';
      $all_data = $purchase->Data($start, $end, true, $user, $purchase_id);
      foreach ($all_data as $data) {
        $name = $data['name'];
        $supplier = $data['supplier'];
        $taxable = $products[$supplier][$name]['taxable'];
        $grower = htmlspecialchars($data['grower']);
        if (!$show_grower || $grower === '') {
          $grower = $supplier;
          if (isset($all_suppliers[$supplier])) {
            if ($all_suppliers[$supplier]['first'] !== '') {
              $grower = $all_suppliers[$supplier]['first'] . ' ' .
                $all_suppliers[$supplier]['last'];
            }
          }
        }
        if ($xero_enabled) {
          $line_items .= '<LineItem>' .
              '<Description>' . htmlspecialchars($name) . '</Description>' .
              '<Quantity>' . $data['quantity'] . '</Quantity>' .
              '<UnitAmount>' . $data['price'] . '</UnitAmount>' .
            '<LineAmount>' . $data['total'] . '</LineAmount>';
          if ($taxable === 1) $line_items .= '<TaxType>OUTPUT</TaxType>';
          else $line_items .= '<TaxType>EXEMPTOUTPUT</TaxType>';
          $line_items .= '<AccountCode>' . $code . '</AccountCode></LineItem>';
        }
        $message .= '<tr><td>' . htmlspecialchars($name) . '</td>' .
          '<td>' . $grower . '</td><td>' . $data['quantity'] . '</td>' .
          '<td>$' . $data['price'] . '/' . $products[$supplier][$name]['unit'] .
          '</td><td>$' . $data['total'] . '</td></tr>' . "\n";
        if ($tax_percent > 0 && $taxable === 1) {
          $tax_included += $data['total'] * $tax_multiplier;
        }
        // Create a new total rather than using $purchase_value to make sure
        // there is no rounding.
        $total += $data['total'];
      }
      if ($this->Substitute('surcharge') === 'true') {
        $surcharge_description =
          $this->Substitute('invoice-surcharge-description');
        $message .= '<tr><td></td><td></td><td></td><td>Subtotal</td><td>$' .
            price_string($total) . '</td></tr>' . "\n" .
          '<tr><td></td><td></td><td></td><td>' . $surcharge_description .
          '</td><td>$' . price_string($surcharge) . '</td></tr>' . "\n" .
          '<tr><td></td><td></td><td></td><td><b>Total</b></td><td><b>$' .
            price_string($total + $surcharge) . '</b></td></tr></table>' . "\n";
      }
      else {
        $message .= '<tr><td></td><td></td><td></td><td><b>Total</b></td><td>' .
          '<b>$' . price_string($total) . '</b></td></tr></table>' . "\n";
      }
      if ($tax_included !== 0) {
        $message .= $this->Substitute('invoice-tax-included', '/!total/',
                                      price_string($tax_included));
      }
    }
    if ($xero_enabled && $invoice_number !== 0) {
      $account_name = $user;
      // Prefer descriptive names when sending to xero.
      if ($first !== '') {
        $account_name = $first;
        if ($last !== '') $account_name .= ' ' . $last;
      }
      $prefix = $this->Substitute('invoice-number-prefix');
      $xml = '<Invoices>' .
        '<Invoice>' .
          '<Type>ACCREC</Type>' .
          '<Contact>' .
            '<Name>' . $account_name . '</Name>' .
          '</Contact>' .
          '<Date>' . date('Y-m-d\TH:i:s', $invoice_date) . '</Date>' .
          '<DueDate>' . date('Y-m-d\TH:i:s', $invoice_date + $terms) .
          '</DueDate>' .
          '<InvoiceNumber>' . $prefix . $invoice_number . '</InvoiceNumber>' .
          '<TotalTax>' . price_string($tax_included) .
          '</TotalTax>' .
          '<Total>' . price_string($total + $surcharge) .
          '</Total>' .
          '<LineAmountTypes>Inclusive</LineAmountTypes>' .
          '<LineItems>' . $line_items . '</LineItems>' .
        '</Invoice>' .
        '</Invoices>';
      $xero->Factory('SendInvoice', $xml);
    }
    // Next display user info via substitutions for the group.
    $message .= $this->FormatInvoiceAfterPurchase($user, $invoice_date,
                                                  $invoice_number, $surcharge);
    // Next pre order information is displayed, if in use.
    if ($purchase_value < 0.01 && $this->Substitute('pre-order') === 'true') {
      $message .= '<p>If you would like to order for next week, ' .
        'please log in before the order is placed on ' .
        $this->Substitute('pre-order-final') . '.</p>' . "\n";
    }
    // Next, sold goods are itemised.
    if ($sold > 0) {
      $message .= '<p>Your sold items for this week:</p>' . "\n" .
        '<table><tr><th>Name</th><th>Quantity</th><th>Price</th><th>Total</th>'.
        '</tr>' . "\n";
      $total = 0;
      $sold_data = $purchase->Sold($start, $end, $user, true);
      foreach ($sold_data as $data) {
        $name = $data['name'];
        $total += (float)$data['total'];
        $message .= '<tr><td>' . $name . '</td><td>' . $data['quantity'] .
          '</td><td>$' . $data['price'] . '/' .
          $products[$user][$name]['unit'] . '</td><td>$' . $data['total'] .
          '</td></tr>' . "\n";
      }
      // Only show the title if there's more than one product sold.
      if (count($sold_data) > 1) {
        $message .= '<tr><td></td><td></td><td><b>Total</b></td><td><b>$' .
          price_string($total) . '</b></td></tr>';
      }
      $message .= '</table>' . "\n";
    }
    if ($this->Substitute('display-membership-reminder') === 'true' &&
        $detail->MembershipReminder($user)) {
      $message .= '<p>' . $this->Substitute('invoice-membership-reminder') .
        '</p>';
    }
    if ($this->Substitute('invoice-show-user-count') === 'true') {
      // This allows for a mapping between a group and an account
      // (in a different group) that is used to send the first group invoices.
      $group = $this->Substitute('invoice-group-for-user-' . $user);
      $count = (int)$this->Substitute('invoice-active-count', '', '', $group);
      if ($count > 1) {
        $message .= '<p>' .
          $this->Substitute('invoice-user-count', '/!count/', $count) . '</p>';
      }
    }
    // Lastly format the balance via substitutions for the group.
    $message .= $this->FormatInvoiceAfterSold($balance, $credit);
    $message .= '</body></html>';
    return wordwrap($message);
  }

  private function FormatInvoiceStart($first, $last,
                                      $invoice_number, $invoice_date) {
    // If there are no purchases, an invoice number is not given.
    if ($invoice_number === 0) {
      return $this->Substitute('invoice-start-no-purchase',
                               ['/!date/', '/!first/', '/!last/'],
                               [date('j F Y', $invoice_date), $first, $last]);
    }
    else {
      return $this->Substitute('invoice-start-purchase',
                               ['/!date/', '/!number/', '/!first/', '/!last/'],
                               [date('j F Y', $invoice_date), $invoice_number,
                                $first, $last]);
    }
  }

  private function FormatInvoiceAfterPurchase($user, $date,
                                              $invoice_number, $surcharge) {
    $banking = new Banking($this->user, $this->owner);
    $settings = $banking->Settings($user);
    $reference = $settings['reference'];
    $payment = new Payment($this->user, $this->owner);
    // ListRecent returns formatted payments made in the last week, so only
    // display it if the end date for this invoice is within the last week.
    $recent_payments = $date > strtotime('-7 days') ?
      $payment->ListRecent($user) : '';
    $roster = new Roster($this->user, $this->owner);
    $volunteer = $roster->Description($user);
    $text = '';
    $surcharge_description = '';
    // The invoice number is passed in again here if there were purchases
    // so that different substitutions can be used. The invoice number itself
    // is not used again though.
    if ($invoice_number !== 0) {
      $text .= $this->Substitute('invoice-after-purchase');
      // Only show the surcharge description when there are purchases.
      if ($surcharge > 0 && $this->Substitute('surcharge') === 'true') {
        $surcharge_description = $payment->SurchargeDescription();
      }
    }
    $text .= $this->Substitute('invoice-user-info',
                               ['/!volunteer/', '/!user/', '/!reference/',
                                '/!server/'],
                               [$volunteer, $user, $reference,
                                $this->user->config->ServerName()]);
    return $text . $surcharge_description . "\n" . $recent_payments . "\n";
  }


  private function FormatInvoiceAfterSold($balance, $credit) {
    $text = '';
    // Look up the configured value for when an account is in balance.
    $invoice_balance = (float)$this->Substitute('invoice-balance');
    if ($balance > $invoice_balance + 0.01) {
      if ($credit === 1) {
        $text .= '<p>Your balance is currently <b>$' . price_string($balance) .
          '</b> and you have chosen to keep this as credit in the system.</p>';
      }
      else {
        $text .= '<p>Your balance is currently <b>$' . price_string($balance) .
          '</b>, please expect a payment from our finance team shortly.</p>';
      }
    }
    else if ($balance < $invoice_balance - 0.01) {
      // When the configured value for 'in balance' is greater than zero,
      // can't multiply by -1 to show balance owing.
      if ($invoice_balance <= 0.01) $balance *= -1;
      // Check when payments were last processed is only done if we are
      // expecting a payment for this user.
      $payment = new Payment($this->user, $this->owner);
      $timestamp = $payment->MostRecent();
      // If payment processing has been done in the last 24 hours,
      // don't need to qualify when the user's balance was last accurate.
      // Also if processing has never been done, don't want to show a timestamp.
      if ($timestamp === 0 || time() < $timestamp + 86400) {
        $text .= $this->Substitute('invoice-current-balance', '/!balance/',
                                   price_string($balance));
      }
      else {
        $text .= $this->Substitute('invoice-old-balance',
                                   ['/!balance/', '/!date/'],
                                   [price_string($balance),
                                    date('j F Y', $timestamp)]);
      }
    }
    $text .= $this->Substitute('invoice-sign-off');
    return $text;
  }

  private function FormatGroupOrders($order_list, $products, $format = '') {
    if ($format === '') $format = $this->Substitute('invoice-group-format');
    // This can be used with the packing format to sort the order for each
    // member by category.
    $sort_by_category =
      $this->Substitute('invoice-sort-by-category') === 'true';
    // Also want to save this data to a csv file to send to suppliers.
    $data = [];
    // The message can be re-sorted by product from the data which is given
    // here sorted by user.
    $message_data = [];
    $intro = '';
    $style = '<style>body { font-family: Arial, sans-serif; } ' . "\n" .
      'table { border: 0; border-collapse: collapse; } ' . "\n" .
      'th { background-color: #f0f0f0; padding: 2px; } ' . "\n" .
      'td { padding: 2px; }' . "\n";
    $message = '';
    $packed = time();

    if ($format === 'user') {
      $intro = $this->Substitute('invoice-intro-orders');
      $message .= '<table>' . "\n" .
        '<tr><th>Member</th><th>Product</th><th>Supplier</th><th>Quantity</th>'.
        '<th>Price</th></tr>' . "\n";
    }
    else if ($format === 'product') {
      $intro = $this->Substitute('invoice-intro-orders');
      $message .= '<table>' . "\n" .
        '<tr><th>Product</th><th>Member</th><th>Supplier</th><th>Quantity</th>'.
        '<th>Price</th></tr>' . "\n";
    }
    else if ($format === 'packing') {
      $style .= 'table { margin: 10px; width: 95%; } ' . "\n" .
        'td { border: 1px solid #aaaaaa; }' . "\n" .
        '.divider { height: 10px; margin-top: 20px; ' .
                   'border-top: 2px dashed #aaaaaa; }' . "\n" .
        '.scissors { position: relative; top: -12px; font-size: 1.2em; ' .
                    'background-color: #ffffff; }' . "\n" .
        '@media print { table { page-break-inside: avoid; } ' . "\n";
      if ($this->Substitute('invoice-compact-packing-list') === 'true') {
        $style .= 'th { font-size: 0.8em } ' . "\n" .
          'td { border: 1px solid #aaaaaa; font-size: 0.5em; }' . "\n";
      }
      $style .= "}\n";
      // The packing format shows the date the box was packed, so find the
      // next purchase day.
      $co_op_day = $this->Substitute('co-op-day');
      // If co-op-day contains commas, assume this is multiple weekdays.
      if (strpos($co_op_day, ',') !== false) {
        $multiple_days = $co_op_day;
        $co_op_day = strstr($co_op_day, ',', true);
        $today = date('l');
        if (in_array($today, explode(',', $multiple_days))) {
          $co_op_day = $today;
        }
      }
      $packed = strtotime($co_op_day);
      if ($packed && $packed < strtotime('-24 hours')) {
        if ($next_co_op_day = strtotime('next ' . $co_op_day)) {
          $packed = $next_co_op_day;
        }
      }
      else if ($packed > time() + (86400 * 182)) {
        // If co-op-day isn't set it will default to a year away, so if it's
        // currently more than 6 months away just show today's date.
        $packed = time();
      }
    }
    $style .= '</style>';
    
    foreach ($order_list as $user => $order) {
      if ($format === 'packing') {
        // Create a table for each user in packing mode.
        if ($message !== '') {
          if ($sort_by_category) {
            ksort($message_data);
            $message .= join($message_data);
            $message_data = [];
          }
          $message .= '</table><div class="divider">' .
            '<span class="scissors">&#9986;</span></div>' . "\n";
        }
        if ($sort_by_category) {
          $message .=  "<table>\n" . '<tr><th colspan="3">' .
            '<b>' . $user . '</b> - packed: ' . date('j-F-Y', $packed) .
            '</th><th colspan="2">Entered? &#9633;</th></tr>' . "\n" .
            '<tr><th>Category</th><th>Product</th><th>Quantity</th>' .
            '<th>Weighed</th><th>Collect</th></tr>' . "\n";
        }
        else {
          $message .=  "<table>\n" . '<tr><th colspan="2">' .
            '<b>' . $user . '</b> - packed: ' . date('j-F-Y', $packed) .
            '</th><th colspan="2">Entered? &#9633;</th></tr>' . "\n" .
            '<tr><th>Product</th><th>Quantity</th><th>Weighed</th>' .
            '<th>Collect</th></tr>' . "\n";
        }
      }
      foreach ($order as $name => $details) {
        $supplier = $details['supplier'];
        $quantity = $details['quantity'];
        $unit = isset($products[$name]['unit']) ?
          '/' . $products[$name]['unit'] : '';
        $price = '$' . price_string($details['basePrice']) . $unit;
        if ($format === 'user') {
          $message .= '<tr><td>'  . $user . '</td><td>' . $name . '</td><td>' .
            $supplier . '</td><td>' . $quantity . '</td><td>' . $price .
            '</td></tr>' . "\n";
        }
        else if ($format === 'product') {
          // This line just avoids an 'undefined index' notice...
          if (!isset($message_data[$name])) $message_data[$name] = '';
          // Each section of the order is grouped by product to be sorted.
          $message_data[$name] .= '<tr><td>' . $name . '</td><td>' . $user .
            '</td>' . '<td>' . $supplier . '</td><td>' . $quantity .
            '</td><td>' . $price . '</td>' . '</tr>' . "\n";
        }
        else if ($format === 'packing') {
          $unit = isset($products[$name]['unit']) ?
            $products[$name]['unit'] : '';
          if ($unit !== 'kg' && $unit !== 'g' && $unit !== 'L') {
            $unit = '';
          }
          if ($sort_by_category) {
            $category = isset($products[$name]['category']) ?
              $products[$name]['category'] : '';
            if (!isset($message_data[$category])) $message_data[$category] = '';
            $message_data[$category] .= '<tr><td>' . $category . '</td><td>' .
              $name . '</td><td>' . $quantity . $unit .
                "</td><td></td><td></td></tr>\n";
          }
          else {
            $message .= '<tr><td>' . $name . '</td><td>' . $quantity . $unit .
              "</td><td></td><td></td></tr>\n";
          }
        }
        // Also store each product order in the supplier data.
        if (!isset($data[$supplier])) $data[$supplier] = [];
        if (!isset($data[$supplier][$name])) {
          $size = isset($products[$name]['size']) ?
            $products[$name]['size'] : 'unavailable';
          $data[$supplier][$name] = ['quantity' => $quantity, 'price' => $price,
                                     'size' => $size];
        }
        else {
          $data[$supplier][$name]['quantity'] += $quantity;
        }
      }
    }

    if ($format === 'product') {
      ksort($message_data);
      $message .= join($message_data);
    }
    else if ($format === 'packing' && $sort_by_category) {
      // Sort and add the data for the last member.
      ksort($message_data);
      $message .= join($message_data);
    }
    $message .= '</table>' . "\n";
    return [$style . $intro . $message, $data];
  }

  private function FormatOrganisationOrders($order_list, $products,
                                            $group_list, $format = '') {
    if ($format === '') {
      $format = $this->Substitute('invoice-organisation-format');
    }
    // Also want to save this data to a csv file to send to suppliers.
    $data = [];
    $previous_name = '';
    $supplier = '';
    $price = '';
    $group_quantity = [];
    // Initialise quantities to zero for each group.
    foreach ($group_list as $group_name) {
      $group_quantity[$group_name] = 0;
    }
    $total_quantity = 0;
    $count = 0;

    $style = '<style>body { font-family: Arial, sans-serif; } ' .
      'table { border: 0; border-collapse: collapse; } ' .
      'th { background-color: #f0f0f0; padding: 2px; } ' .
      'td { padding: 2px; }</style>' . "\n";
    $message = $this->Substitute('invoice-intro-orders');
    if ($format === 'vertical') {
      $message .= '<table>' . "\n" .
        '<tr><th>Product</th><th>Co-op</th><th>Supplier</th><th>Quantity</th>' .
        '<th>Price</th></tr>' . "\n";
    }
    else {
      $message .= '<table>' . "\n" .
        '<tr><th>Product</th><th>Supplier</th>';
      foreach ($group_list as $group_name) {
        $message .= '<th>' . ucfirst($group_name) . '</th>';
      }
      $message .= '<th>Total</th><th>Price</th></tr>' . "\n";
    }

    foreach ($order_list as $name => $order) {
      foreach ($order as $group => $details) {
        if ($name !== $previous_name && $previous_name !== '') {
          if ($format === 'vertical') {
            // The purchase data is returned here totaled by group, but if
            // there are purchases here for the current product by more than
            // one group, add a sub-total for the entire quantity.
            if ($count > 1) {
              $message .= '<tr><td>' . $previous_name .
                '</td><td></td><td></td><td><i>Total quantity</i></td><td>' .
                $total_quantity . '</td><td></td></tr>' . "\n";
            }
          }
          else {
            $message .= '<tr><td>' . $previous_name . '</td><td>' . $supplier .
              '</td>';
            foreach ($group_list as $group_name) {
              $message .= '<td>' . $group_quantity[$group_name] . '</td>';
            }
            $message .= '<td>' . $total_quantity . '</td><td>' . $price .
              '</td></tr>' . "\n";
            // Reset all the quantities to zero for the next product.
            foreach ($group_list as $group_name) {
              $group_quantity[$group_name] = 0;
            }
          }
          $total_quantity = 0;
          $count = 0;
        }

        $supplier = $details['supplier'];
        $quantity = $details['quantity'];
        $unit = isset($products[$name]['unit']) ?
          '/' . $products[$name]['unit'] : '';
        $price = '$' . price_string($details['basePrice']) . $unit;
        $previous_name = $name;
        $total_quantity += $quantity;
        $count++;

        if ($format === 'vertical') {
          $message .= '<tr><td>' . $name . '</td><td>' . $group . '</td><td>' .
            $supplier . '</td><td>' . $quantity . '</td><td>' . $price .
            '</td></tr>' . "\n";
        }
        else {
          // When displaying quantities horizontally, just record the current
          // group quantity and add the whole line when the product changes.
          $group_quantity[$group] = $quantity;
        }

        // Also store each product order in the supplier data.
        if (!isset($data[$supplier])) $data[$supplier] = [];
        if ($format === 'vertical') {
          $size = isset($products[$name]['size']) ?
            $products[$name]['size'] : 'unavailable';
          $data[$supplier][] = ['name' => $name, 'group' => $group,
                                'quantity' => $quantity, 'price' => $price,
                                'size' => $size];
        }
        else {
          if (!isset($data[$supplier][$name])) {
            $size = isset($products[$name]['size']) ?
              $products[$name]['size'] : 'unavailable';
            $data[$supplier][$name] = ['price' => $price, 'group' => [],
                                       'size' => $size];
            // Initialise quantities to zero for each group for this item.
            foreach ($group_list as $group_name) {
              $data[$supplier][$name]['group'][$group_name] = 0;
            }
          }
          $data[$supplier][$name]['group'][$group] = $quantity;
        }
      }
    }
    // Finish the last line.
    if ($format === 'vertical') {
      if ($count > 1) {
        $message .= '<tr><td>' . $previous_name . '</td><td></td><td></td>' .
          '<td><i>Total quantity</i></td><td>' . $total_quantity .
          '</td><td></td></tr>' . "\n";
      }
    }
    else {
      $message .= '<tr><td>' . $previous_name . '</td><td>' . $supplier.'</td>';
      foreach ($group_list as $group_name) {
        $message .= '<td>' . $group_quantity[$group_name] . '</td>';
      }
      $message .= '<td>' . $total_quantity . '</td><td>' . $price .
        '</td></tr>' . "\n";
    }
    $message .= '</table>' . "\n";

    return [$style . $message, $data];
  }

  private function RoundPacks($packs, $rounding) {
    // If rounding is the empty string, leave packs as a fraction.
    if ($rounding === '') {
      return number_format($packs, 1, '.', '');
    }
    // Otherwise find the fraction of the last pack to compare rounding to.
    $fraction = $packs - floor($packs);
    if ($fraction < (float)$rounding) return floor($packs);
    return ceil($packs);
  }

  private function WriteData($data, $group_list = [], $timestamp = 0) {
    // Format is either 'horizontal', where group names are written as
    // seperate columns and imply quantity per group (therefore wider
    // formatting), or 'vertical', where groups and quantity are written
    // in their own columns (therefore longer formatting).
    $format = $this->Substitute('invoice-organisation-format');
    // An extra coulmn can be used for suppliers to provide feedback.
    $extra_column_title = $this->Substitute('invoice-extra-column');
    $extra_column = '';
    if ($extra_column_title !== '') {
      $extra_column_title = ',' . $extra_column_title;
      $extra_column = ',';
    }
    // Rounding is a value between zero and one, used to decide if the
    // number of packs should be rounded up or down.
    $rounding = '';
    if (!isset($rounding)) {
      $rounding = $this->Substitute('invoice-order-rounding');
    }

    $files = [];
    foreach ($data as $supplier => $product_list) {
      if (count($product_list) === 0) continue;

      $content = '';
      // If there's more than one group, orders can be displayed with group
      // totals displayed either in their own columns (horizontally) or
      // subtotaled under product groups (vertically).
      if (count($group_list) > 1) {
        if ($format === 'vertical') {
          // First add a title.
          $content = 'Product,Co-op,Quantity,Pack Size,Packs,Price' .
            $extra_column_title . "\n";
          foreach ($product_list as $product) {
            $quantity = $product['quantity'];
            // Format $quantity nicely if possible.
            if ($quantity - floor($quantity) < 0.01) {
              $quantity = floor($quantity);
            }
            $size = $product['size'];
            if ($size < 0.01) {
              $size = '';
              $packs = '';
            }
            else {
              $packs = $this->RoundPacks($quantity / $size, $rounding);
            }
            // Put the product name in double quotes in case it contains commas.
            $content .= '"' . $product['name'] . '",' . $product['group'] .
              ',' . $quantity . ',' . $size . ',' . $packs . ',' .
              $product['price'] . $extra_column . "\n";
          }
        }
        else {
          // First add the title.
          $content = 'Product,';
          foreach ($group_list as $group_name) {
            $content .= ucfirst($group_name) . ',';
          }
          $content .= 'Total,Pack Size,Packs,Price' . $extra_column_title ."\n";
          foreach ($product_list as $name => $details) {
            $content .= '"' . $name . '",';
            $total = 0;
            foreach ($group_list as $group_name) {
              $content .= $details['group'][$group_name] . ',';
              $total += (float)$details['group'][$group_name];
            }
            $size = $details['size'];
            if ($size < 0.01) {
              $size = '';
              $packs = '';
            }
            else {
              $packs = $this->RoundPacks($total / $size, $rounding);
            }
            $content .= $total . ',' . $size . ',' . $packs . ',' .
              $details['price'] . $extra_column . "\n";
          }
        }
      }
      // When there is only one group, there is no format distinction, but the
      // data passed in is not sorted. It's sorted here by first storing each
      // formatted line in an array and then joining the data.
      else {
        $title = 'Product,Quantity,Pack Size,Packs,Price' .
          $extra_column_title . "\n";
        $rows = [];
        foreach ($product_list as $name => $details) {
          $quantity = $details['quantity'];
          // Format $quantity nicely if possible.
          if ($quantity - floor($quantity) < 0.01) {
            $quantity = floor($quantity);
          }
          $size = $details['size'];
          if ($size < 0.01) {
            $size = '';
            $packs = '';
          }
          else {
            $packs = $this->RoundPacks($quantity / $size, $rounding);
          }
          $rows[$name] = '"' . $name . '",' . $quantity . ',' . $size . ',' .
            $packs . ',' . $details['price'] . $extra_column . "\n";
        }
        ksort($rows);
        $content = $title . join($rows);
      }
      if ($timestamp === 0) $timestamp = time();
      $files[$supplier . '-' . date('j-F-Y', $timestamp) . '.csv'] = $content;
    }
    return $files;
  }

  private function AddGraphData($total_surcharge) {
    $graph = new Module($this->user, $this->owner, 'graph');
    if ($graph->IsInstalled()) {
      $graph_label = $this->user->group;
      if ($graph_label !== '') $graph_label .= ' ';
      $graph_label .= 'surcharge per week';
      // Store the time in milliseconds for processing in javascript.
      $graph_data = [$graph_label => $total_surcharge,
                     'time' => time() * 1000];
      $graph->Factory('AddData', [$graph_data, 'invoice']);

      // Request number of users for next week from the Banking module.
      $banking = new Banking($this->user, $this->owner);
      list($count, $next_week) = $banking->CountNextWeek();
      $graph_label = $this->user->group;
      if ($graph_label !== '') $graph_label .= ' ';
      $graph_label .= 'coming next week';
      $graph_data = [$graph_label => $count, 'time' => $next_week * 1000];
      $graph->Factory('AddData', [$graph_data, 'invoice']);
    }
  }

  private function Submit() {
    $invoice_numbers = [];
    $us_all_data = json_decode($_POST['data'], true);
    $us_custom_email = count($us_all_data) === 1 ? $_POST['customEmail'] : '';
    foreach ($us_all_data as $us_data) {
      $result = $this->SendInvoiceFromDate($us_data['date'], $us_data['name'],
                                           $us_data['purchaseId'],
                                           $us_data['number'],
                                           $us_custom_email);
      if (isset($result['error'])) return $result;
      else if (isset($result['number'])) $invoice_numbers[] = $result['number'];
    }
    return $invoice_numbers;
  }

  private function SendInvoiceFromDate($us_date, $us_name, $us_purchase_id,
                                       $us_number, $us_custom_email) {
    $mysqli = connect_db();
    $invoice_date = date('F j Y', (int)$us_date / 1000);
    $start = strtotime($invoice_date . ' 00:00:00');
    $end = $start + 86399;
    $username = $mysqli->escape_string($us_name);
    $purchase_id = (int)$us_purchase_id;
    $invoice_number = (int)$us_number;
    // Get the users details.
    $email = '';
    $first = '';
    $last = '';
    $detail = new Detail($this->user, $this->owner);
    $user_details = $detail->User($username);
    if (is_array($user_details)) {
      $email = $user_details['email'];
      $first = $user_details['first'];
      $last = $user_details['last'];
      // Make sure the invoice is addressed to someone.
      if ($first === '' && $last === '') {
        $first = $username;
      }
    }
    // Get invoice date before modifying start in case it changes.
    $date = $start;
    $date_format = $this->Substitute('invoice-date-format');
    $date_offset = $this->Substitute('invoice-date-offset');
    if ($date_offset !== '') $date += (int)$date_offset * 86400;
    $date = date($date_format, $date);
    // Add the number of days to look forward from the end, or subtract the
    // number of days to look back from the start. When it's set to 1, only
    // purchases on the date provided are used.
    $invoice_day_count = (int)$this->Substitute('invoice-day-count');
    if ($invoice_day_count > 1) {
      if ($this->Substitute('invoice-look-forward') === 'true') {
        $end += $invoice_day_count * 86400;
      }
      else {
        $start -= $invoice_day_count * 86400;
      }
    }
    $purchase = new Purchase($this->user, $this->owner);
    $purchase_value = $purchase->Total($username, $start, $end, $purchase_id);
    $invoice_date = strtotime($invoice_date . ' 12:00');
    // If an invoice number wasn't found earlier, only create a new invoice
    // number now if there are purchases for this user.
    if ($invoice_number === 0 && $purchase_value > 0) {
      $organiser = new Organiser($this->user, $this->owner);
      $query = 'SELECT MAX(invoice_number) AS invoice_number FROM invoice ' .
        'LEFT JOIN users ON invoice.user = users.user WHERE ' .
        $organiser->GroupQuery();
      if ($mysqli_result = $mysqli->query($query)) {
        if ($invoice = $mysqli_result->fetch_assoc()) {
          $invoice_number = (int)$invoice['invoice_number'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Invoice->Submit 2: ' . $mysqli->error);
      }
      $invoice_number++;
      $query = 'INSERT INTO invoice VALUES ("' . $username . '", ' .
        $invoice_date . ', ' . $invoice_number . ', ' . $purchase_id . ')';
      if (!$mysqli->query($query)) {
        $this->Log('Invoice->Submit 3: ' . $mysqli->error);
      }
    }
    $surcharge = $purchase->Surcharge($start, $end, $username, $purchase_id);
    $sold = $purchase->SupplyTotal($username, $start, $end);
    // Don't show balance for old invoices, this is done by using the default
    // balance for the group. This also means credit setting is not required.
    $balance = (float)$this->Substitute('invoice-balance');
    $message = $this->FormatInvoice($username, $first, $last, $invoice_number,
                                    $start, $end, $purchase_id, $purchase_value,
                                    $surcharge, $sold, $balance, 0,
                                    $invoice_date);
    $subject = '';
    if ($invoice_number === 0) {
      $subject = $this->Substitute('invoice-no-purchase-subject', '/!date/',
                                   $date);
    }
    else {
      $subject = $this->Substitute('invoice-subject', ['/!date/', '/!number/'],
                                   [$date, $invoice_number]);
    }
    $sender = $this->Substitute('invoice-sender', '/!host/',
                                $this->user->config->ServerName());
    $sender_name = $this->Substitute('invoice-sender-name');
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $headers = "MIME-Version: 1.0\r\n" .
      "Content-Type: text/html; charset=utf-8\r\n" .
      'From: ' . $sender_name . "\r\n";

    $result = [];
    if ($_POST['emailSelf'] === 'true') {
      $my_email = '';
      $query = 'SELECT email FROM users WHERE ' .
        'user = "' . $this->user->name . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($users = $mysqli_result->fetch_assoc()) {
          $my_email = $users['email'];
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Invoice->Submit 4: ' . $mysqli->error);
      }
      if ($my_email === '') {
        $result['error'] = 'No email address for ' . $this->user->name;
      }
      else if (mail($my_email, $subject, $message, $headers, '-f ' . $sender)) {
        $result['number'] = $invoice_number;
      }
      else {
        $result['error'] = 'Email to ' . $this->user->name .
          ' (' . $my_email . ') not accepted for delivery.';
      }
    }
    if ($_POST['emailUser'] === 'true') {
      if ($us_custom_email !== '') $email = $us_custom_email;
      if ($email === '') {
        $result['error'] = 'No email address for ' . $username;
      }
      else if (dobrado_mail($email, $subject, $message, $headers, '',
                            $sender, $username, 'invoice', 'sendInvoice')) {
        $result['number'] = $invoice_number;
      }
      else {
        $result['error'] = 'Email to ' . $username .
          ' (' . $email . ') not accepted for delivery.';
      }
    }
    $mysqli->close();
    return $result;
  }

  private function StoreData($data, $invite_group) {
    $path = $this->user->config->PrivatePath() . '/' .
      $this->user->config->ServerName() . '/group/' . $this->user->group;
    if (!is_dir($path)) {
      mkdir($path, 0755, true);
    }
    else if ($handle = opendir($path)) {
      // Only the most recent data is kept, so first remove any text files.
      while (($file = readdir($handle)) !== false) {
        if (is_file($path . '/' . $file) &&
            strpos($file, $invite_group . '.txt') !== false) {
          unlink($path . '/' . $file);
        }
      }
      closedir($handle);
    }
    $filename = date('Y-m-d', $data['timestamp']);
    if ($invite_group !== '') {
      $filename .= '-' . $invite_group;
    }
    $filename .= '.txt';
    if ($handle = fopen($path . '/' . $filename, 'w')) {
      fwrite($handle, serialize($data));
      fclose($handle);
    }
    else {
      $this->Log('Invoice->StoreData: Error opening file: ' .
                 $this->user->group . '/' . $filename);
    }
  }

}
