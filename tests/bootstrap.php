<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Only run this script from the command line.
if (php_sapi_name() !== 'cli') exit;

include 'tests/db.php';
include 'tests/helper.php';

include 'php/functions/copy_page.php';
include 'php/functions/new_user.php';
include 'php/functions/new_module.php';
include 'php/functions/permission.php';
include 'php/functions/style.php';
include 'php/functions/write_style.php';
include 'php/functions/create.php';

include 'php/page.php';

if (!is_dir('tests/js')) mkdir('tests/js');
if (!is_writeable('tests/js')) {
  log_db('The \'tests/js\' directory is not writeable.');
}
if (!is_dir('tests/js/source')) mkdir('tests/js/source');
if (!is_writeable('tests/js/source')) {
  log_db('The \'tests/js/source\' directory is not writeable.');
}

helper_drop_tables();

create_tables();

helper_create_test_data();

$all_modules = helper_all_modules();
// Each module's php file must be found in /php/modules for autoload. Also due
// to module dependencies, all php files must be copied before any modules can
// be installed. This is because instance.php already contains the full list
// of installed modules and will attempt to autoload any class requested.
foreach ($all_modules as $label => $module_info) {
  if (!file_exists('php/modules/' . ucfirst($label) . '.php')) {
    copy('install/' . ucfirst($label) . '.php',
         'php/modules/' . ucfirst($label) . '.php');
  }
}

// Files are written to the tests directory so as not to modify the development
// environment.
create_default('', 'tests/', 'php/', $all_modules);
create_site_style();