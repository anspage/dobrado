<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use PHPUnit\Framework\TestCase;

class InvoiceTest extends TestCase {

  private $invoice = NULL;
  private $user = NULL;

  protected function setUp() {
    $this->user = new User();
    $this->invoice = new Invoice($this->user, 'admin');
  }

  public function testCanAdd() {
    $page = '';
    if ($this->user->canEditSite) {
      if ($this->invoice->AlreadyOnPage('invoice', $page)) {
        $this->assertFalse($this->invoice->CanAdd($page));
      }
      else {
        $this->assertTrue($this->invoice->CanAdd($page));
      }
    }
    else {
      $this->assertFalse($this->invoice->CanAdd($page));
    }
  }

  public function testCanEdit() {
    $this->assertFalse($this->invoice->CanEdit(0));
  }

  public function testCanRemove() {
    $this->assertTrue($this->invoice->CanRemove(0));
  }

  public function testCron() {
    $this->assertEmpty($this->invoice->Cron());
  }

  public function testIncludeScript() {
    $this->assertTrue($this->invoice->IncludeScript());
  }

  public function testPlacement() {
    $this->assertEquals($this->invoice->Placement(), 'middle');
  }

}
