<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase {

  private $config = NULL;

  protected function setUp() {
    $this->config = new Config();
  }

  public function testCron() {
    $this->assertEquals($this->config->Cron(), '3600');
  }

  public function testDevelopmentMode() {
    $this->assertTrue($this->config->DevelopmentMode());
  }

  public function testFancyUrl() {
    $this->assertFalse($this->config->FancyUrl());
  }

  public function testGuestAllowed() {
    $this->assertTrue($this->config->GuestAllowed());
  }

  public function testLoginPage() {
    $this->assertEquals($this->config->LoginPage(), 'index');
  }

  public function testMaxFileSize() {
    $this->assertEquals($this->config->MaxFileSize(), '8');
  }

  public function testMaxUpload() {
    $this->assertEquals($this->config->MaxUpload(), '100');
  }

  public function testPermalinkDefault() {
    $this->assertEquals($this->config->PermalinkDefault(), 'default');
  }

  public function testPermalinkOrder() {
    $this->assertEquals($this->config->PermalinkOrder(), '0');
  }

  public function testPrivatePath() {
    $this->assertEquals($this->config->PrivatePath(), '/tmp');
  }

  public function testSecure() {
    $this->assertFalse($this->config->Secure());
  }

  public function testServerName() {
    $this->assertEquals($this->config->ServerName(), 'localhost');
  }

  public function testTheme() {
    $this->assertEquals($this->config->Theme(), 'base-svg');
  }

  public function testTitle() {
    $this->assertEquals($this->config->Title(), 'dobrado');
  }

  public function testTitleIncludesPage() {
    $this->assertTrue($this->config->TitleIncludesPage());
  }

  public function testUnavailable() {
    $this->assertEquals($this->config->Unavailable(), 'unavailable');
  }

}
