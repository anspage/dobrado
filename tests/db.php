<?php
// Dobrado Content Management System
// Copyright (C) 2016 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

error_reporting(E_ALL);

function connect_db() {
  include 'db_config.php';

  $mysqli = new mysqli($db_server, $db_user, $db_password, $db_name);
  if ($mysqli->connect_errno) {
    error_log('Could not connect: '.$mysqli->connect_error);
    exit;
  }
  return $mysqli;
}

function log_db($us_message, $user = '', $visitor = '', $page = '') {
  // Call error_log for the original message.
  error_log($us_message);

  $mysqli = connect_db();
  $query = 'INSERT INTO log VALUES ("'.$user.'", "'.$visitor.'", "'.$page.'", '.
    time().', "'.$mysqli->escape_string($us_message).'")';
  if (!$mysqli->query($query)) {
    error_log('log_db: '.$mysqli->error);
  }
  $mysqli->close();
}
