<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use PHPUnit\Framework\TestCase;

class BankingTest extends TestCase {

  private $banking = NULL;
  private $user = NULL;

  protected function setUp() {
    $this->user = new User();
    $this->banking = new Banking($this->user, 'admin');
  }

  public function testCanAdd() {
    $page = '';
    if ($this->banking->AlreadyOnPage('banking', $page)) {
      $this->assertFalse($this->banking->CanAdd($page));
    }
    else {
      $this->assertTrue($this->banking->CanAdd($page));
    }
  }

  public function testCanEdit() {
    $this->assertFalse($this->banking->CanEdit(0));
  }

  public function testCanRemove() {
    $this->assertTrue($this->banking->CanRemove(0));
  }

  public function testContent() {
    if (!$this->user->canEditPage) {
      $this->assertEquals($this->banking->Content(0), '');
    }
  }

  public function testIncludeScript() {
    $this->assertTrue($this->banking->IncludeScript());
  }

  public function testPlacement() {
    $this->assertEquals($this->banking->Placement(), 'middle');
  }

}
