<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use PHPUnit\Framework\TestCase;

class PurchaseTest extends TestCase {

  private $purchase = NULL;
  private $user = NULL;

  protected function setUp() {
    $this->user = new User();
    $this->purchase = new Purchase($this->user, 'admin');
  }

  public function testCanAdd() {
    $page = '';
    if ($this->user->canEditSite) {
      if ($this->purchase->AlreadyOnPage('purchase', $page)) {
        $this->assertFalse($this->purchase->CanAdd($page));
      }
      else {
        $this->assertTrue($this->purchase->CanAdd($page));
      }
    }
    else {
      $this->assertFalse($this->purchase->CanAdd($page));
    }
  }

  public function testCanEdit() {
    $this->assertFalse($this->purchase->CanEdit(0));
  }

  public function testCanRemove() {
    $this->assertTrue($this->purchase->CanRemove(0));
  }

  public function testIncludeScript() {
    $this->assertTrue($this->purchase->IncludeScript());
  }

  public function testPlacement() {
    $this->assertEquals($this->purchase->Placement(), 'middle');
  }

  /**
   * @dataProvider purchaseProvider
   */
  public function testAddRemovePurchase($username, $timestamp,
                                        $product, $supplier, $quantity) {
    $result = $this->purchase->AddPurchase($username, $timestamp, $product,
                                           $supplier, $quantity,
                                           '1.00', '1.00', 0, '');
    $this->assertEquals($result, ['done' => true]);
    $result = $this->purchase->RemovePurchase($username, $timestamp, $product,
                                              $supplier, $quantity);
    $this->assertEquals($result, ['done' => true]);
  }

  public function testAllSold() {
    // TODO: This is meant to test AllSupplyTotals but need to create a
    // supplier account first.
    $this->assertEmpty($this->purchase->AllSold(0, 0));
    $start = strtotime('-1 week');
    $end = time();
    $this->assertEmpty($this->purchase->AllSold($start, $end));
  }

  public function testAllTaxable() {
    $start = strtotime('-1 week');
    $end = time();
    $this->assertEmpty($this->purchase->AllTaxable($start, $end));
    $this->assertEmpty($this->purchase->AllTaxable($start, $end, true));
    $this->assertEmpty($this->purchase->AllTaxable($start, $end, false, true));
  }

  public function testAllTotals() {
    $start = strtotime('-1 week');
    $end = time();
    $this->assertEmpty($this->purchase->AllTotals($start, $end));
    $this->assertEmpty($this->purchase->AllTotals($start, $end, true));
    $this->assertEmpty($this->purchase->AllTotals($start, $end, true,
                                                  false, true));
    $this->assertEmpty($this->purchase->AllTotals($start, $end, false, true));
    $this->assertEmpty($this->purchase->AllTotals($start, $end, false,
                                                  true, true));
  }

  public function testAllSurcharge() {
    $start = strtotime('-1 week');
    $end = time();
    $this->assertEmpty($this->purchase->AllSurcharge($start, $end));
    $this->assertEmpty($this->purchase->AllSurcharge($start, $end, false));
    $this->assertEmpty($this->purchase->AllSurcharge($start, $end,
                                                     false, true));
  }

  public function testAllOutstanding() {
    $this->assertEmpty($this->purchase->AllOutstanding([], time()));
    $this->assertEmpty($this->purchase->AllOutstanding([], time(), true));
  }

  public function testTotal() {
    $this->assertEquals($this->purchase->Total(), 0);
    $start = strtotime('-1 week');
    $end = time();
    $this->assertEquals($this->purchase->Total('', $start, $end), 0);
  }

  public function testSupplyTotal() {
    $this->assertEquals($this->purchase->SupplyTotal(''), 0);
    $start = strtotime('-1 week');
    $end = time();
    $this->assertEquals($this->purchase->SupplyTotal('', $start, $end), 0);
  }

  public function purchaseProvider() {
    return [['admin', time(), 'product', 'supplier', 1]];
  }
}
