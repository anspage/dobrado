<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/db.php';
include 'functions/microformats.php';
include 'config.php';
include 'module.php';
include 'user.php';

session_start();

// Step 3: Verify the code when it is returned by the redirect_uri, which we've
// previously redirected to below once a csrf token is set.
if (isset($_POST['code'])) {
  $me = '';
  $mysqli = connect_db();
  $code = $mysqli->escape_string($_POST['code']);
  $client_id =
    $mysqli->escape_string(strtolower(trim($_POST['client_id'], ' /')));
  $redirect_uri =
    $mysqli->escape_string(strtolower(trim($_POST['redirect_uri'], ' /')));
  // This query can be used twice, firstly to match an entry using the
  // parameters provided and then to remove it as it should only be used once.
  $auth_codes_query = 'WHERE code = "' . $code . '" AND ' .
    'client_id = "' . $client_id . '" AND ' .
    'redirect_uri = "' .$redirect_uri . '" AND response_type = "id"';
  $query = 'SELECT me FROM auth_codes ' . $auth_codes_query .
    ' AND timestamp > ' . time();
  if ($mysqli_result = $mysqli->query($query)) {
    if ($auth_codes = $mysqli_result->fetch_assoc()) {
      $me = $auth_codes['me'];
    }
    $mysqli_result->close();
  }
  else {
    log_db('auth_endpoint 1: ' . $mysqli->error);
  }
  $query = 'DELETE FROM auth_codes ' . $auth_codes_query;
  if (!$mysqli->query($query)) {
    log_db('auth_endpoint 2: ' . $mysqli->error);
  }
  // Also delete any entries with expired timestamps.
  $query = 'DELETE FROM auth_codes WHERE timestamp < ' . time();
  if (!$mysqli->query($query)) {
    log_db('auth_endpoint 3: ' . $mysqli->error);
  }
  $mysqli->close();

  if ($me === '') {
    header('HTTP/1.1 400 Bad Request');
    echo 'Error: authorization code could not be verified.';
    exit;
  }

  header('Content-Type: application/json');
  echo json_encode(['me' => $me]);
  exit;
}

// Step 2: Verify the csrf token, which is set below once the user confirms
// they want to continue.
if (isset($_POST['token'])) {
  if ($_POST['token'] !== $_SESSION['auth-endpoint'] ||
      !isset($_POST['me']) || $_POST['me'] !== $_SESSION['auth-me'] ||
      !isset($_POST['client_id']) || !isset($_POST['redirect_uri'])) {
    header('HTTP/1.1 400 Bad Request');
    echo 'Error: Could not verify the original request.';
    exit;
  }

  $code = bin2hex(openssl_random_pseudo_bytes(32));
  $expires = time() + 600;
  $mysqli = connect_db();
  $me = $mysqli->escape_string($_POST['me']);
  $client_id = $mysqli->escape_string($_POST['client_id']);
  $redirect_uri = $mysqli->escape_string($_POST['redirect_uri']);
  $scope = $mysqli->escape_string($_POST['scope']);
  $response_type = $mysqli->escape_string($_POST['response_type']);
  $query = 'INSERT INTO auth_codes VALUES ("' . $me . '", "' . $code . '", ' .
    '"' . $client_id . '", "' . $redirect_uri . '", "' . $scope . '", ' .
    '"' . $response_type . '", ' . $expires . ') ON DUPLICATE KEY UPDATE ' .
    'me = "' . $me . '", code = "' . $code . '", scope = "' . $scope . '", ' .
    'response_type = "' . $response_type . '", timestamp = ' . $expires;
  if (!$mysqli->query($query)) {
    log_db('auth_endpoint 4: ' . $mysqli->error);
  }
  $mysqli->close();
  header('Location: ' . $_POST['redirect_uri'] . '?state=' . $_POST['state'] .
         '&code=' . $code);
  exit;
}

// Step 1: Verify the initial request to the endpoint. First check all the
// required parameters are present, then if the user is logged in can present
// them with a form to continue the auth process.
if (!isset($_GET['client_id'])) {
  header('HTTP/1.1 400 Bad Request');
  echo 'Error: \'client_id\' parameter was not provided in the request.';
  exit;
}

if (!isset($_GET['me'])) {
  header('HTTP/1.1 400 Bad Request');
  echo 'Error: \'me\' parameter was not provided in the request.';
  exit;
}
if (!isset($_GET['redirect_uri'])) {
  header('HTTP/1.1 400 Bad Request');
  echo 'Error: \'redirect_uri\' parameter was not provided in the request.';
  exit;
}

$me = '';
$user = new User();
if ($user->loggedIn) {
  // Create our own url for the logged in user and just make sure the 'me'
  // parameter matches the domain. That way authentication can be requested
  // by just providing the domain and the full url is returned.
  $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
    'https://' : 'http://';
  if ($user->config->Secure()) $scheme = 'https://';
  $server_name = $user->config->ServerName();
  $me = $scheme . $server_name;
  if ($user->name !== 'admin') $me .= '/' . $user->name;
  $regex = '/^https?:\/\/' . preg_quote($server_name) . '/';
  if (preg_match($regex, strtolower(trim($_GET['me'], ' /')))) {
    $_SESSION['auth-endpoint'] = bin2hex(openssl_random_pseudo_bytes(8));
    $_SESSION['auth-me'] = $me;
  }
  else {
    header('HTTP/1.1 400 Bad Request');
    echo 'Error: Could not match \'me\' parameter to the current domain.';
    exit;
  }
}
else {
  // TODO: The page displayed by the endpoint could ask the user to log in.
  // dobrado just needs to remember the referer path so that the form below
  // can be generated again once the new session has started.
  header('HTTP/1.1 401 Unauthorised');
  echo 'Error: Please log in first!';
  exit;
}

// The logged in user is asked to confirm that they want to use the resource
// at client_id, when they click Continue auth_endpoint will be re-run with a
// token set in post fields. Display a name and logo for the app if available.
$client_id = strtolower(trim($_GET['client_id'], ' /'));
$app_details = '<a href="' . $client_id . '">' . $client_id . '</a>';
$h_app = parse_happ($client_id);
$h_app_url = strtolower(trim($h_app['url'], ' /'));
// Only display h-app details if they match the client_id.
if ($h_app_url === $client_id) {
  $h_app_name = $h_app['name'] !== '' ? $h_app['name'] : $h_app_url;
  $h_app_logo = $h_app['logo'] !== '' ?
    '<img src="' . $h_app['logo'] . '">' : '';
  $app_details = $h_app_logo . '<a href="' . $h_app_url . '">' .
    $h_app_name . '</a>';
}
// Let the user know if they are going to be redirected to a different site.
$redirect_uri = strtolower(trim($_GET['redirect_uri'], ' /'));
$redirect_info = strpos($redirect_uri, $client_id) === 0 ? '' :
  '<p>You will be redirected to: ' . $redirect_uri . '</p>';
// Default response type is 'id' when not provided.
$response_type = isset($_GET['response_type']) ? $_GET['response_type'] : 'id';
$scope = isset($_GET['scope']) ? $_GET['scope'] : '';
if ($scope === '' && $response_type === 'code') {
  // Set a default scope when response type is code as it can't be empty.
  $scope = 'create';
}

echo '<!DOCTYPE html>' . "\n" .
  '<meta charset="utf-8">' . "\n" .
  '<meta name="viewport" content="width=device-width">' . "\n" .
  '<html><head><title>Authorisation requested</title>' . "\n" .
  '<style> p { line-height: 50px; } img { width: 50px; float: left; ' .
    'margin-right: 10px; } button { margin-left: 60px }' .
  '</style></head>' .
  '<p>' . $app_details . ' wants to log you in as <a href="' . $me . '">' .
    $me . '</a></p>' .
  '<form action="/php/auth_endpoint.php" method="post">' .
    '<input type="hidden" name="me" value="' . $me . '">' .
    '<input type="hidden" name="client_id" value="' . $client_id . '">'.
    '<input type="hidden" name="redirect_uri" value="' . $redirect_uri . '">' .
    '<input type="hidden" name="state" value="' . $_GET['state'] . '">' .
    '<input type="hidden" name="scope" value="' . $scope . '">' .
    '<input type="hidden" name="response_type" value="' . $response_type . '">'.
    '<input type="hidden" name="token" ' .
      'value="' . $_SESSION['auth-endpoint'] . '">' .
    '<button>Continue</button>' .
  '</form>' . $redirect_info .
  '</body></html>';