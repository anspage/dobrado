<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function header_value($headers, $name) {
  foreach ($headers as $key => $value) {
    if (strtolower($key) === strtolower($name)) return $value;
  }
  return '';
}

$us_token = '';
$headers = apache_request_headers();
$authorization = header_value($headers, 'Authorization');
if ($authorization !== '') {
  // Remove the prefix 'Bearer ' from the Authorization header.
  $us_token = substr($authorization, 7);
}
else if (isset($_POST['access_token'])) {
  $us_token = urldecode($_POST['access_token']);
}
if ($us_token === '') {
  header('HTTP/1.1 401 Unauthorised');
  exit;
}

include 'functions/db.php';

$me = '';
$mysqli = connect_db();
$token = $mysqli->escape_string($us_token);
$query = 'SELECT me FROM access_tokens WHERE token = "' . $token . '"';
if ($mysqli_result = $mysqli->query($query)) {
  if ($access_tokens = $mysqli_result->fetch_assoc()) {
    $me = $access_tokens['me'];
  }
  $mysqli_result->close();
}
else {
  log_db('microsub.php 1: ' . $mysqli->error);
}
$mysqli->close();

if (!preg_match('/^https?:\/\/' . $_SERVER['SERVER_NAME'] . '/', $me)) {
  log_db('microsub.php 2: Couldn\'t match ' . $_SERVER['SERVER_NAME'] .
         ' in: ' . $me);
  header('HTTP/1.1 403 Forbidden');
  exit;
}

if (!isset($_GET['action'])) {
  log_db('microsub.php 3: action not set.');
  header('HTTP/1.1 400 Bad Request');
  exit;
}

include 'functions/copy_page.php';
include 'functions/microformats.php';
include 'functions/new_module.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/style.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

// This provides the owner and their home page, because url is from rel=me.
list($page, $owner) = page_owner($me);

$user = new User($owner);
$reader = new Module($user, $owner, 'reader');
if (!$reader->IsInstalled()) {
  header('HTTP/1.1 500 Internal Server Error');
  log_db('microsub.php 4: Reader module is not installed.');
  exit;
}

header('Content-Type: application/json');
echo json_encode($reader->Factory('Microsub'));
