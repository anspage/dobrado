<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

if (!isset($_POST['url'])) {
  echo json_encode(['error' => 'url not provided']);
  exit;
}

include 'functions/db.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

$mysqli = connect_db();
$url = $mysqli->escape_string($_POST['url']);
list($page, $owner) = page_owner($url);

$user = new User();
$user->SetPermission($page, $owner);
if (!$user->canEditPage) {
  echo json_encode(['error' => 'Permission denied editing layout.']);
  $mysqli->close();
  exit;
}

$name = isset($_POST['name']) ? $mysqli->escape_string($_POST['name']) : '';
$difference = isset($_POST['difference']) ? (int)$_POST['difference'] : 0;

if ($difference === 0) {
  echo json_encode(['done' => true]);
  $mysqli->close();
  exit;
}

// TODO: Need a way to specify media queries.

$property = '';
$value = '';
$matches = [];

if ($name === 'left' || $name === 'right') {
  // Check if middle has a margin set.
  $value = page_value($owner, $page, '.middle', 'margin-'.$name);
  // Also check site style if a value wasn't found for the page.
  if ($value === '') {
    $value = site_value('.middle', 'margin-'.$name);
  }
  // If the margin is set to 'auto', use the difference provided to update the
  // width of the middle div, if it currently has a pixel value.
  if ($value === 'auto') {
    $value = page_value($owner, $page, '.middle', 'width');
    if ($value === '') {
      $value = site_value('.middle', 'width');
    }
    preg_match('/^([0-9]+)px$/', $value, $matches);
    if (isset($matches[1])) {
      // Need to subtract the difference here because a decrease to the left
      // or right div means an increase to the middle div (and vice versa).
      $value = (int)$matches[1] - $difference;
      $query = 'INSERT INTO page_style VALUES ("'.$owner.'", "'.$page.'", "", '.
        '".middle", "width", "'.$value.'px") '.
        'ON DUPLICATE KEY UPDATE value = "'.$value.'px"';
      if (!$mysqli->query($query)) {
        log_db('layout_editor 3: '.$mysqli->error, $owner, $user->name, $page);
      }
    }
  }
  else { 
    preg_match('/^([0-9]+)px$/', $value, $matches);
    if (isset($matches[1])) {
      $value = (int)$matches[1] + $difference;
      $query = 'INSERT INTO page_style VALUES ("'.$owner.'", "'.$page.'", "", '.
        '".middle", "margin-'.$name.'", "'.$value.'px") '.
        'ON DUPLICATE KEY UPDATE value = "'.$value.'px"';
      if (!$mysqli->query($query)) {
        log_db('layout_editor 4: '.$mysqli->error, $owner, $user->name, $page);
      }
    }
  }
  // Set property to width to update left or right div.
  $property = 'width';
}
else {
  // Otherwise updating height on header or footer.
  $property = 'height';
}

$value = page_value($owner, $page, '.'.$name, $property);
// Also check site style if a value wasn't found for the page.
if ($value === '') {
  $value = site_value('.'.$name, $property);
}
// Also check min-height if looking for the height property.
if ($value === '' && $property === 'height') {
  $value = site_value('.'.$name, 'min-height');
}
$matches = [];
preg_match('/^([0-9]+)px$/', $value, $matches);
if (isset($matches[1])) {
  $value = (int)$matches[1] + $difference;
  $query = 'INSERT INTO page_style VALUES ("'.$owner.'", "'.$page.'", "", '.
    '".'.$name.'", "'.$property.'", "'.$value.'px") '.
    'ON DUPLICATE KEY UPDATE value = "'.$value.'px"';
  if (!$mysqli->query($query)) {
    log_db('layout_editor 5: '.$mysqli->error, $owner, $user->name, $page);
  }
}
$mysqli->close();

if ($owner === 'admin') write_page_style($owner, '../'.$page.'.css');
else write_page_style($owner, '../'.$owner.'/'.$page.'.css');

// Let the client know the action completed.
echo json_encode(['done' => true]);
