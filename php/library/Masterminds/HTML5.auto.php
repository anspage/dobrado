<?php

include 'HTML5.php';

function html5_autoload($className) {
  $file = 'library/' . str_replace('\\', '/', $className) . '.php';
  if (file_exists($file)) include $file;
}

spl_autoload_register('html5_autoload');