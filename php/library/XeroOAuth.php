<?php
/* Copyright (c) 2013 Xero Limited
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

include 'library/OAuthSimple.php';

class XeroOAuth {
  
  public $config = [];
  private $headers = [];
  private $request_params = [];
  
  public function __construct($config) {
    $this->config = $config;
  }
	
  /**
   * Make an HTTP request using this library.
   * This method doesn't return anything.
   * Instead the response should be inspected directly.
   *
   * @param string $method
   *        	the HTTP method being used. e.g. POST, GET, HEAD etc
   * @param string $url
   *        	the request URL without query string parameters
   * @param array $params
   *        	the request parameters as an array of key=value pairs
   */
  public function request($method, $url, $params = [], $xml = '') {
    $this->headers = ['Accept' => 'application/json'];
    if (isset($params['If-Modified-Since'])) {
      $modDate = 'If-Modified-Since: '.$params['If-Modified-Since'];
      $this->headers['If-Modified-Since'] = $params['If-Modified-Since'];
    }
    if (!isset($params['oauth_signature_method'])) {
      $params['oauth_signature_method'] = 'RSA-SHA1';
    }
    if ($xml !== '') {
      $xml = trim($xml);
      $this->xml = $xml;
    }
    if ($method === 'POST') $params['xml'] = $xml;
    $this->method = strtoupper($method);
    $this->url = $url;
    $oauthObject = new OAuthSimple();
    try {
      $this->sign = $oauthObject->sign(['path' => $url,
                                        'action' => $method,
                                        'parameters' => $params,
                                        'signatures' => $this->config]);
    }
    catch (Exception $e) {
      $errorMessage = 'XeroOAuth::request() '.$e->getMessage();
      $this->response['response'] = $errorMessage;
      $this->response['helper'] = $url;
      return $this->response;
    }

    $curlRequest = $this->curlit();
    if ($this->response['code'] === 401 &&
        isset($this->config['session_handle'])) {
      if ((strpos($this->response['response'],
                  'oauth_problem=token_expired') !== false)) {
        $this->response['helper'] = 'TokenExpired';
      }
      else {
        $this->response['helper'] = 'TokenFatal';
      }
    }
    if ($this->response['code'] == 403) {
      $errorMessage = 'It looks like your Xero Entrust cert issued by Xero '.
        'is either invalid or has expired. See http://developer.xero.com/'.
        'api-overview/http-response-codes/#403 for more';
      // default IIS page isn't informative, a little swap
      $this->response['response'] = $errorMessage;
      $this->response['helper'] = 'SetupIssue';
    }
    if ($this->response['code'] === 0) {
      $errorMessage = 'It looks like your Xero Entrust cert issued by Xero '.
        'is either invalid or has expired. See http://developer.xero.com/'.
        'api-overview/http-response-codes/#403 for more';
      $this->response['response'] = $errorMessage;
      $this->response['helper'] = 'SetupIssue';
    }
    return $this->response;
  }
	
  /**
   * Utility function to create the request URL in the requested format
   *
   * @param string $request
   *        	the API method without extension
   * @return string the concatenation of the host, API version and API method
   */
  public function url($request, $api = 'core') {
    if ($request === 'RequestToken') {
      $this->config['host'] = 'https://api.xero.com/oauth/';
    }
    elseif ($request === 'Authorize') {
      $this->config['host'] = 'https://api.xero.com/oauth/Authorize';
      $request = '';
    }
    elseif ($request === 'AccessToken') {
      $this->config['host'] = 'https://api.xero.com/oauth/';
    }
    else {
      if (isset($api)) {
        if ($api === 'core') {
          $api_stem = 'api.xro';
          $api_version = '2.0';
        }
        if ($api === 'payroll') {
          $api_stem = 'payroll.xro';
          $api_version = '1.0';
        }
        if ($api === 'file') {
          $api_stem = 'files.xro';
          $api_version = '1.0';
        }
      }
      $this->config['host'] = 'https://api.xero.com/'.$api_stem.'/'.
        $api_version.'/';
    }
    return implode([$this->config['host'], $request]);
  }
	
  /*
   * Run some basic checks on our config options etc to make sure all is ok
   */
  public function diagnostics() {
    $public_key = $this->config['rsa_public_key'];
    if (file_exists($public_key)) {
      $cert_content = file_get_contents($public_key);
      $data = openssl_x509_parse($cert_content);
      $validFrom = date ('Y-m-d H:i:s', $data['validFrom_time_t']);
      $validTo = date( 'Y-m-d H:i:s', $data['validTo_time_t']);
      if (time () < $data['validFrom_time_t']) {
        return 'Application cert not valid until '.$validFrom."\n";
      }
      else if (time() > $data['validTo_time_t']) {
        return 'Application cert cert expired on '.$validFrom."\n";
      }
      else {
        $private_key = $this->config['rsa_private_key'];
        if (file_exists($private_key)) {
          $priv_key_content = file_get_contents($private_key);
          if (!openssl_x509_check_private_key($cert_content,
                                              $priv_key_content)) {
            return "Application certificate and key do not match \n";
          }
        }
        else {
          return 'Can\'t read the self-signed cert key. Check your '.
            'rsa_private_key config variable. Private applications require '.
            'a self-signed X509 cert http://developer.xero.com/'.
            "documentation/advanced-docs/public-private-keypair\n";
        }
      }
      return '';
    }
    return 'Can\'t read the self-signed SSL cert. Private applications '.
      'require a self-signed X509 cert http://developer.xero.com/'.
      "documentation/advanced-docs/public-private-keypair\n";
  }

  // Private functions below here ////////////////////////////////////////////
  
  /**
   * Utility function to parse the returned curl headers and store them in the
   * class array variable.
   *
   * @param object $ch curl handle
   * @param string $header the response headers
   * @return the string length of the header
   */
  private function curlHeader($ch, $header) {
    $i = strpos($header, ':');
    if (!empty($i)) {
      $key = str_replace('-', '_', strtolower(substr($header, 0, $i)));
      $value = trim(substr($header, $i + 2));
      $this->response['headers'][$key] = $value;
    }
    return strlen($header);
  }
	
  /**
   * Encodes the string or array passed in a way compatible with OAuth.
   * If an array is passed each array value will will be encoded.
   *
   * @param mixed $data the scalar or array to encode
   * @return $data encoded in a way compatible with OAuth
   */
  private function safe_encode($data) {
    if (is_array($data)) {
      return array_map([$this, 'safe_encode'], $data);
    }
    else if (is_scalar($data)) {
      return str_ireplace(['+', '%7E'], [' ', '~'], rawurlencode($data));
    }
    else {
      return '';
    }
  }
	
  /**
   * Makes a curl request.
   * Takes no parameters as all should have been prepared by the request method
   *
   * @return void response data is stored in the class variable 'response'
   */
  private function curlit() {
    $this->request_params = [];
    $c = curl_init();
    $useragent = isset($this->config['user_agent']) ?
      $this->config['user_agent'] : 'XeroOAuth-PHP';
    curl_setopt_array($c,
      [CURLOPT_USERAGENT => $useragent,
       CURLOPT_CONNECTTIMEOUT => 30,
       CURLOPT_TIMEOUT => 20,
       CURLOPT_RETURNTRANSFER => true,
       CURLOPT_FOLLOWLOCATION => true,
       CURLOPT_MAXREDIRS => 5,
       CURLOPT_URL => $this->sign['signed_url'],
       CURLOPT_VERBOSE => true,
       // process the headers
       CURLOPT_HEADERFUNCTION => [$this, 'curlHeader'],
       CURLOPT_HEADER => FALSE,
       CURLINFO_HEADER_OUT => TRUE]);
	
    if ($this->method === 'GET') {
      $contentLength = 0;
    }
    else if ($this->method === 'POST') {
      curl_setopt($c, CURLOPT_POST, TRUE);
      $post_body = $this->safe_encode ($this->xml);
      curl_setopt ($c, CURLOPT_POSTFIELDS, $post_body);
      $this->request_params['xml'] = $post_body;
      $contentLength = strlen($post_body);
      $this->headers['Content-Type'] = 'application/x-www-form-urlencoded';
    }		
    else if ($this->method === 'PUT') {
      $fh = tmpfile();
      if ($this->format === 'file') {
        $put_body = $this->xml;
      }
      else {
        $put_body = $this->safe_encode($this->xml);
        $this->headers['Content-Type'] = 'application/x-www-form-urlencoded';
      }
      fwrite($fh, $put_body);
      rewind($fh);
      curl_setopt($c, CURLOPT_PUT, true);
      curl_setopt($c, CURLOPT_INFILE, $fh);
      curl_setopt($c, CURLOPT_INFILESIZE, strlen($put_body));
      $contentLength = strlen($put_body);
    }		
    else {
      curl_setopt($c, CURLOPT_CUSTOMREQUEST, $this->method);
    }

    if (empty($this->request_params)) {
      // CURL will set length to -1 when there is no data
      $this->headers['Content-Length'] = $contentLength;
    }
    else {
      foreach ($this->request_params as $k => $v) {
        $ps[] = "{$k}={$v}";
      }
      $this->request_payload = implode('&', $ps);
      curl_setopt($c, CURLOPT_POSTFIELDS, $this->request_payload);
    }
	
    $this->headers['Expect'] = '';

    if (!empty($this->headers)) {
      foreach ($this->headers as $k => $v) {
        $headers[] = trim($k.': '.$v);
      }
      curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
    }

    if (isset($this->config['prevent_request']) &&
        $this->config['prevent_request'] === false) {
      return;
    }
			
    $response = curl_exec($c);
    if ($response === false) {
      $response = 'Curl error: '.curl_error($c);
      $code = 1;
    }
    else {
      $code = curl_getinfo($c, CURLINFO_HTTP_CODE);
    }

    $info = curl_getinfo($c);
    curl_close($c);
    if (isset($fh)) {
      fclose($fh);
    }

    $this->response['code'] = $code;
    $this->response['response'] = $response;
    $this->response['info'] = $info;
    return $code;
  }
	
}
