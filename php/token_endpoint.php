<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function header_value($headers, $name) {
  foreach ($headers as $key => $value) {
    if (strtolower($key) === strtolower($name)) return $value;
  }
  return '';
}

include 'functions/db.php';

// Check if the request is for authorization code verification.
if (isset($_POST['code'])) {
  $us_scope = '';
  $mysqli = connect_db();
  $code = $mysqli->escape_string($_POST['code']);
  $client_id =
    $mysqli->escape_string(strtolower(trim($_POST['client_id'], ' /')));
  $redirect_uri =
    $mysqli->escape_string(strtolower(trim($_POST['redirect_uri'], ' /')));
  $us_me = strtolower(trim($_POST['me'], ' /'));
  $me = $mysqli->escape_string($us_me);
  // This query can be used twice, firstly to match an entry using the
  // parameters provided and then to remove it as it should only be used once.
  $auth_codes_query = 'WHERE me = "' . $me . '" AND code = "' . $code . '" ' .
    'AND client_id = "' . $client_id . '" AND ' .
    'redirect_uri = "' . $redirect_uri . '" AND response_type = "code"';
  $query = 'SELECT scope FROM auth_codes ' . $auth_codes_query .
    ' AND timestamp > ' . time();
  if ($result = $mysqli->query($query)) {
    if ($auth_codes = $result->fetch_assoc()) {
      $us_scope = $auth_codes['scope'];
    }
  }
  else {
    log_db('token_endpoint 1: ' . $mysqli->error);
  }
  $query = 'DELETE FROM auth_codes ' . $auth_codes_query;
  if (!$mysqli->query($query)) {
    log_db('token_endpoint 2: ' . $mysqli->error);
  }
  // Also delete any entries with expired timestamps.
  $query = 'DELETE FROM auth_codes WHERE timestamp < ' . time();
  if (!$mysqli->query($query)) {
    log_db('token_endpoint 3: ' . $mysqli->error);
  }

  if ($us_scope === '') {
    $mysqli->close();
    header('HTTP/1.1 400 Bad Request');
    echo 'Error: authorization code could not be verified.';
    exit;
  }

  $token = bin2hex(openssl_random_pseudo_bytes(32));
  $scope = $mysqli->escape_string($us_scope);
  $query = 'INSERT INTO access_tokens VALUES ("' . $me . '", ' .
    '"' . $token . '" , "' . $client_id . '", "' . $scope . '")';
  if (!$mysqli->query($query)) {
    log_db('token_endpoint 4: ' . $mysqli->error);
  }
  $mysqli->close();
  
  header('Content-Type: application/json');
  echo json_encode(['access_token' => $token, 'token_type' => 'Bearer',
                    'scope' => $us_scope, 'me' => $us_me]);
  exit;
}
else if (isset($_POST['token']) &&
         isset($_POST['action']) && $_POST['action'] === 'revoke') {
  $mysqli = connect_db();
  $token = $mysqli->escape_string($_POST['token']);
  $query = 'DELETE FROM access_tokens WHERE token = "' . $token . '"';
  if (!$mysqli->query($query)) {
    log_db('token_endpoint 5: ' . $mysqli->error);
  }
  $mysqli->close();
  exit;
}

// Otherwise the request is for access token verification.
$headers = apache_request_headers();
$authorization = header_value($headers, 'Authorization');
if ($authorization === '') {
  header('HTTP/1.1 400 Bad Request');
  echo 'Error: Authorization header not found.';
  exit;
}

$me = '';
$client_id = '';
$scope = '';
$mysqli = connect_db();
// Remove the prefix 'Bearer ' from the Authorization header.
$token = $mysqli->escape_string(substr($authorization, 7));
$query = 'SELECT me, client_id, scope FROM access_tokens WHERE ' .
  'token = "' . $token . '"';
if ($result = $mysqli->query($query)) {
  if ($access_tokens = $result->fetch_assoc()) {
    $me = $access_tokens['me'];
    $client_id = $access_tokens['client_id'];
    $scope = $access_tokens['scope'];
  }
  $result->close();
}
else {
  log_db('token_endpoint 6: ' . $mysqli->error);
}
$mysqli->close();

if ($me === '' || $client_id === '' || $scope === '') {
  header('HTTP/1.1 401 Unauthorised');
  echo 'Error: Your access token could not be verified.';
  exit;
}

header('Content-Type: application/json');
echo json_encode(['me' => $me, 'client_id' => $client_id, 'scope' => $scope]);
