<?php
// Dobrado Content Management System
// Copyright (C) 2016 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

if (!isset($_POST['request'])) {
  echo json_encode(['error' => 'request not provided']);
  exit;
}

if (!isset($_POST['url'])) {
  echo json_encode(['error' => 'url not provided']);
  exit;
}

include 'functions/copy_page.php';
include 'functions/db.php';
include 'functions/install_module.php';
include 'functions/microformats.php';
include 'functions/new_module.php';
include 'functions/new_user.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/style.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

$mysqli = connect_db();
$request = $mysqli->escape_string($_POST['request']);
$url = $mysqli->escape_string($_POST['url']);
$mysqli->close();
list($page, $owner) = page_owner($url);

// Other POST variables can be accessed by modules in their Callback function.
$user = new User();
$user->SetPermission($page, $owner);
$module = new Module($user, $owner, $request);
echo json_encode($module->Callback());
