<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/db.php';
include 'functions/install_module.php';
include 'functions/microformats.php';
include 'functions/new_module.php';
include 'config.php';
include 'module.php';
include 'user.php';

$user = new User('admin');
$reader = new Module($user, 'admin', 'reader');
if (!$reader->IsInstalled()) exit;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // RSSCloud explicitly posts the url being updated, for WebSub it's added as
  // a callback query parameter in Reader->RenewHub() along with id which is
  // added to authenticate payloads.
  if (!isset($_REQUEST['url'])) exit;

  $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
  $registered = $reader->Factory('RegisteredFeed', [$_REQUEST['url'], $id]);
  if ($registered !== '') {
    // For external hubs check if update was called with a payload.
    $data = $registered === 'hub' ? file_get_contents('php://input') : '';
    $reader->Factory('UpdateFeed', [$_REQUEST['url'], $data, true]);
  }
}
else if (isset($_GET['url']) && isset($_GET['challenge'])) {
  // Check that we've registered for notifications for this feed and then echo
  // the challenge parameter to verify.
  if ($reader->Factory('RegisteredFeed', $_GET['url']) === 'cloud') {
    echo $_GET['challenge'];
  }
}
else if (isset($_GET['hub_topic']) &&
         isset($_GET['hub_mode']) && $_GET['hub_mode'] === 'subscribe') {
  $id = isset($_GET['id']) ? $_GET['id'] : '';
  $lease = isset($_GET['hub_lease_seconds']) ?
    (int)$_GET['hub_lease_seconds'] : 0;
  if ($reader->Factory('RegisteredFeed',
                       [$_GET['hub_topic'], $id, $lease]) === 'hub') {
    echo $_GET['hub_challenge'];
  }
  else {
    header('HTTP/1.1 404 Not Found');
    echo 'Feed not registered.';
  }
}
