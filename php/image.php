<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

$id = $_GET['i'];
if (!preg_match('/^[[:xdigit:]]+$/', $id)) {
  header('Content-Type: image/png');
  echo file_get_contents('../images/spacer.png');
  exit;
}

include 'autoloader.php';
$simple_pie = new SimplePie();
$scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
  'https://' : 'http://';
$handler = $scheme . $_SERVER['SERVER_NAME'] . '/php/image.php';
$simple_pie->set_image_handler($handler);
$simple_pie->init();
$cache_dir = $simple_pie->cache_location;
$cache = $simple_pie->registry->call('Cache', 'get_handler',
                                     [$cache_dir, $id, 'spi']);
if (!$image = $cache->load()) {
  header('Content-Type: image/png');
  echo file_get_contents('../images/spacer.png');
  exit;
}

$headers = $image['headers'];

$not_modified = false;
if (isset($headers['etag'])) {
  header('ETag: ' . $headers['etag']);
  if (isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
    $not_modified = $_SERVER['HTTP_IF_NONE_MATCH'] === $headers['etag'];
  }
}
if (isset($headers['last-modified'])) {
  header('Last-Modified: ' . $headers['last-modified']);
  if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
    $not_modified =
      $_SERVER['HTTP_IF_MODIFIED_SINCE'] === $headers['last-modified'];
  }
}
if ($not_modified) {
  header('HTTP/1.1 304 Not Modified');
  exit;
}

// It's possible that body may not be set for some images to reduce disk usage,
// in which case the original content-location is saved. Cache the file again
// now that it has been accessed more recently.
$body = false;
if (isset($image['body'])) {
  $body = $image['body'];
}
else if (isset($headers['content-location'])) {
  $body = file_get_contents($simple_pie->sanitize($headers['content-location'],
                                                  SIMPLEPIE_CONSTRUCT_IRI, '',
                                                  true));
}
else {
  unlink($cache_dir . '/' . $id . '.spi');
}
if (!$body) {
  header('Content-Type: image/png');
  echo file_get_contents('../images/spacer.png');
  exit;
}

if (isset($headers['cache-control'])) {
  header('Cache-Control: ' . $headers['cache-control']);
}
if (isset($headers['content-type'])) {
  header('Content-Type: ' . $headers['content-type']);
}
echo $body;