<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function header_value($headers, $name) {
  foreach ($headers as $key => $value) {
    if (strtolower($key) === strtolower($name)) return $value;
  }
  return '';
}

function photo_html($photo_list) {
  if (!is_array($photo_list)) return '';

  $count = count($photo_list);
  if ($count === 0) return '';

  if ($count === 1) {
    $value = '';
    $alt = '';
    if (isset($photo_list[0]['value'])) {
      $value = $photo_list[0]['value'];
      // Also check for alt text.
      if (isset($photo_list[0]['alt'])) {
        $alt = ' alt="' . htmlspecialchars($photo_list[0]['alt']) . '"';
      }
    }
    else {
      $value = $photo_list[0];
    }
    return '<p><img class="u-photo" src="' . $value . '"' . $alt . '></p>';
  }

  // Make sure image-set is unique incase there's more than one shown on
  // the feed page.
  $image_set_id = preg_replace('/[[:^alnum:]]/', '', $photo_list[0]);
  $html = '<p class="photo-list">';
  for ($i = 0; $i < $count; $i++) {
    $hidden = $i <= 1 ? '' : 'class="hidden" ';
    $value = '';
    $alt = '';
    if (isset($photo_list[$i]['value'])) {
      $value = $photo_list[$i]['value'];
      // Also check for alt text.
      if (isset($photo_list[$i]['alt'])) {
        $alt = ' alt="' . htmlspecialchars($photo_list[$i]['alt']) . '"';
      }
    }
    else {
      $value = $photo_list[$i];
    }
    $html .= '<a href="' . $photo_list[$i] . '" ' . $hidden .
      'data-lightbox="' . $image_set_id . '">' .
      '<img class="u-photo" src="' . $value . '"' . $alt . '></a>';
  }
  return $html . '<br><b>' . $count . ' photos</b></p>';
}

$us_token = '';
$headers = apache_request_headers();
$authorization = header_value($headers, 'Authorization');
if ($authorization !== '') {
  // Remove the prefix 'Bearer ' from the Authorization header.
  $us_token = substr($authorization, 7);
}
else if (isset($_POST['access_token'])) {
  $us_token = urldecode($_POST['access_token']);
}
if ($us_token === '') {
  header('HTTP/1.1 401 Unauthorised');
  exit;
}

include 'functions/db.php';

$me = '';
$mysqli = connect_db();
$token = $mysqli->escape_string($us_token);
$query = 'SELECT me FROM access_tokens WHERE token = "' . $token . '"';
if ($result = $mysqli->query($query)) {
  if ($access_tokens = $result->fetch_assoc()) {
    $me = $access_tokens['me'];
  }
  $result->close();
}
else {
  log_db('micropub.php 1: ' . $mysqli->error);
}
$mysqli->close();

if (!preg_match('/^https?:\/\/' . $_SERVER['SERVER_NAME'] . '/', $me)) {
  log_db('micropub.php 2: Couldn\'t match ' . $_SERVER['SERVER_NAME'] .
         ' in: ' . $me);
  header('HTTP/1.1 403 Forbidden');
  exit;
}

include 'functions/copy_page.php';
include 'functions/microformats.php';
include 'functions/new_module.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/style.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

list($page, $owner) = page_owner($me);
$user = new User($owner);
$user->page = $page;

// First check if this is a config request rather than creating a post.
if (isset($_GET['q'])) {
  $query = $_GET['q'];
  header('Content-Type: application/json');
  if ($query === 'config') {
    $media_endpoint = $user->config->Secure() ? 'https://' : 'http://';
    $media_endpoint .= $user->config->ServerName();
    $media_endpoint .= '/php/media_endpoint.php';
    echo json_encode(['media-endpoint' => $media_endpoint,
                      'syndicate-to' => [['uid' => 'twitter',
                                          'name' => 'Twitter']]]);
  }
  else if ($query === 'syndicate-to') {
    echo json_encode(['syndicate-to' => [['uid' => 'twitter',
                                          'name' => 'Twitter']]]);
  }
  else {
    echo json_encode((object)[]);
  }
  exit;
}

$content_type = header_value($headers, 'Content-Type');
$data = $content_type === 'application/json' ?
  json_decode(file_get_contents('php://input'), true) : [];
// TODO: type is not currently used as Post module only supports h-entry.
$type = '';
if ($content_type === 'application/json') {
  if (isset($data['type']) &&
      in_array($data['type'][0], ['h-entry', 'h-card', 'h-event', 'h-cite'])) {
    $type = $data['type'][0];
  }
  else {
    log_db('micropub.php 3: JSON data type not found.');
    header('HTTP/1.1 400 Bad Request');
    exit;
  }
}
else if (isset($_POST['h']) &&
         in_array($_POST['h'], ['entry', 'card', 'event', 'cite'])) {
  $type = 'h-' . $_POST['h'];
}
else {
  log_db('micropub.php 4: POST data type not found.');
  header('HTTP/1.1 400 Bad Request');
  exit;
}

$post = new Module($user, $owner, 'post');
if (!$post->IsInstalled()) {
  header('HTTP/1.1 500 Internal Server Error');
  log_db('micropub.php 5: Post module is not installed.');
  exit;
}

$content = ['data' => ''];
if ($content_type === 'application/json') {
  $use_content = true;
  if (isset($data['properties']['summary'][0])) {
    // Use summary as content if there are any unrecognized properties.
    $known_properties = ['like-of', 'repost-of', 'in-reply-to', 'url',
                         'debit', 'credit', 'author', 'name', 'uid',
                         'category', 'currency', 'published', 'summary',
                         'content', 'photo', 'comment', 'like', 'repost'];
    foreach ($data['properties'] as $name => $property) {
      if (!in_array($name, $known_properties)) {
        $use_content = false;
        break;
      }
    }
    if (!$use_content) {
      if (isset($data['properties']['summary'][0]['html'])) {
        $content['data'] = $data['properties']['summary'][0]['html'];
      }
      else if (is_string($data['properties']['summary'][0])) {
        $content['data'] = htmlspecialchars($data['properties']['summary'][0]);
      }
    }
  }
  if ($use_content && isset($data['properties']['content'][0])) {
    if (isset($data['properties']['content'][0]['html'])) {
      $content['data'] = $data['properties']['content'][0]['html'];
    }
    else {
      $content['data'] = htmlspecialchars($data['properties']['content'][0]);
    }
  }
  if (isset($data['properties']['name'][0])) {
    $content['title'] = $data['properties']['name'][0];
  }
  if (isset($data['properties']['category'])) {
    $content['category'] = implode(',', $data['properties']['category']);
  }
  if (isset($data['properties']['photo'])) {
    $content['data'] .= photo_html($data['properties']['photo']);
  }
  if (isset($data['properties']['in-reply-to'][0])) {
    $content['webactionType'] = 'reply';
    $content['webactionUrl'] = $data['properties']['in-reply-to'][0];
  }
  else if (isset($data['properties']['like-of'][0])) {
    $content['webactionType'] = 'like';
    $content['webactionUrl'] = $data['properties']['like-of'][0];
  }
  else if (isset($data['properties']['repost-of'][0])) {
    $content['webactionType'] = 'share';
    $content['webactionUrl'] = $data['properties']['repost-of'][0];
  }
  else if (isset($data['properties']['debit'][0]) ||
           isset($data['properties']['credit'][0])) {
    $content['webactionType'] = 'payment';
  }
  if (isset($data['properties']['mp-syndicate-to'])) {
    $mp_syndicate_to = $data['properties']['mp-syndicate-to'];
    foreach (explode(',', $mp_syndicate_to) as $syndicate) {
      if (stripos($syndicate, 'twitter') !== false) {
        $content['twitter'] = 1;
      }
    }
  }
}
else {
  if (isset($_POST['content'])) {
    // JSON must be used to specify html content, so escape post content.
    $content['data'] = htmlspecialchars($_POST['content']);
  }
  else if (isset($_POST['summary'])) {
    $content['data'] = htmlspecialchars($_POST['summary']);
  }
  if (isset($_POST['name'])) $content['title'] = $_POST['name'];
  if (isset($_POST['category'])) {
    if (is_array($_POST['category'])) {
      $content['category'] = implode(',', $_POST['category']);
    }
    else {
      $content['category'] = $_POST['category'];
    }
  }
  if (isset($_POST['in-reply-to'])) {
    $content['webactionType'] = 'reply';
    $content['webactionUrl'] = $_POST['in-reply-to'];
  }
  else if (isset($_POST['like-of'])) {
    $content['webactionType'] = 'like';
    $content['webactionUrl'] = $_POST['like-of'];
  }
  else if (isset($_POST['repost-of'])) {
    $content['webactionType'] = 'share';
    $content['webactionUrl'] = $_POST['repost-of'];
  }
  if (isset($_POST['mp-syndicate-to'])) {
    foreach (explode(',', $_POST['mp-syndicate-to']) as $syndicate) {
      if (stripos($syndicate, 'twitter') !== false) {
        $content['twitter'] = 1;
      }
    }
  }
  $photo_list = isset($_POST['photo']) ? $_POST['photo'] : [];
  // If this is multipart/form-data then $_FILES will be set with name="photo".
  if (isset($_FILES['photo'])) {
    $browser = new Module($user, $owner, 'browser');
    if (!$browser->IsInstalled()) {
      header('HTTP/1.1 500 Internal Server Error');
      log_db('micropub.php 6: Browser module is not installed.');
      exit;
    }

    $result = $browser->Factory('Upload', 'photo');
    if (isset($result['error'])) {
      log_db('micropub.php 7: ' . $result['error']);
    }
    else if (is_string($result)) {
      if (!in_array($result, $photo_list)) $photo_list[] = $result;
    }
    else {
      foreach ($result as $location) {
        if (!in_array($location, $photo_list)) $photo_list[] = $location;
      }
    }
  }
  $content['data'] .= photo_html($photo_list);
}
if (count($content) === 0) {
  log_db('micropub.php 8: No content found.');
  header('HTTP/1.1 400 Bad Request');
  exit;
}

// The Writer module provides a designated page for each action.
$writer = new Module($user, $owner, 'writer');
if ($writer->IsInstalled()) {
  $action = isset($content['webactionType']) ?
    $content['webactionType'] : 'post';
  $user->page = $writer->Factory('Designate', $action);
}

$id = new_module($user, $owner, 'post', $user->page, $post->Group(),
                 $post->Placement());
$post->Add($id);
// Write out a new style sheet after the module has been added.
if ($owner === 'admin') write_box_style($owner, '../style.css');
else write_box_style($owner, '../' . $owner . '/style.css');

$content['dataOnly'] = false;
$result = $post->SetContent($id, $content);
// Once SetContent has returned, ContentUpdated is used to send webmentions
// and update feeds, if the feed is published.
if ($result['published'] === 1) {
  $post->Factory('ContentUpdated', [$result['box_id'], $result['content'],
                                    $result['category'], $result['feed'],
                                    $result['permalink']]);
}
header('HTTP/1.1 201 Created');
header('Location: ' . $result['url']);
