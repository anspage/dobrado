<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

if (!isset($_POST['url'])) {
  echo json_encode(['error' => 'url not provided']);
  exit;
}

include 'functions/db.php';
include 'functions/page_owner.php';
include 'functions/permission.php';

include 'config.php';
include 'module.php';
include 'user.php';

$mysqli = connect_db();
$url = $mysqli->escape_string($_POST['url']);
list($page, $owner) = page_owner($url);

$user = new User();
$user->SetPermission($page, $owner);
if (!$user->canEditPage) {
  $mysqli->close();
  exit;
}

// Don't allow guests to publish pages.
if (substr($owner, 0, 5) === 'guest' && strlen($owner) === 20) {
  $error = 'Guest accounts are not allowed to publish pages. '.
    'Please register your account.';
  echo json_encode(['error' => $error]);
  $mysqli->close();
  exit;
}

$published = '';
if (isset($_POST['published'])) {
  $published = (int)$_POST['published'];
}
else {
  // Look for a current value to toggle if none was given.
  $query = 'SELECT published FROM published WHERE user = "'.$owner.'" '.
    'AND page = "'.$page.'"';
  if ($result = $mysqli->query($query)) {
    if ($current_published = $result->fetch_assoc()) {
      $published = $current_published['published'] === '1' ? 0 : 1;
    }
  }
  else {
    log_db('publish 1: '.$mysqli->error);
  }
  $result->close();
}
// If still have no value, toggle the default which is unpublished.
// Note that strict comparison must be used when comparing 0 to empty string.
if ($published === '') $published = 1;

// Alert modules on the page that the published state of the page has changed.
$query = 'SELECT label, box_id FROM modules WHERE user = "'.$owner.'" '.
  'AND page = "'.$page.'" AND deleted = 0';
if ($result = $mysqli->query($query)) {
  while ($module_list = $result->fetch_assoc()) {
    $label = $module_list['label'];
    $id = $module_list['box_id'];
    $module = new Module($user, $owner, $label);
    $module->Publish($id, $published);
  }
  $result->close();
}
else {
  log_db('publish 2: '.$mysqli->error);
}
// Also alert modules when a page is made private, but not per module so that
// more general changes can be made.
if ($published === 0) {
  $query = 'SELECT DISTINCT label FROM modules WHERE user = "'.$owner.'" '.
    'AND page = "'.$page.'" AND deleted = 0';
  if ($result = $mysqli->query($query)) {
    while ($module_list = $result->fetch_assoc()) {
      $label = $module_list['label'];
      $module = new Module($user, $owner, $label);
      $module->Publish(NULL, NULL);
    }
    $result->close();
  }
  else {
    log_db('publish 3: '.$mysqli->error);
  }
}

$query = 'INSERT INTO published VALUES ("'.$owner.'", "'.$page.'", '.
  $published.') ON DUPLICATE KEY UPDATE published = '.$published;
if (!$mysqli->query($query)) {
  log_db('publish 4: '.$mysqli->error);
}

$mysqli->close();

echo json_encode(['published' => $published]);
