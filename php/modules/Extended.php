<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Extended extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $mysqli = connect_db();
    $us_mode = isset($_POST['mode']) ? $_POST['mode'] : '';
    $media = isset($_POST['media']) ?
      $mysqli->escape_string($_POST['media']) : '';
    $action = isset($_POST['action']) ?
      $mysqli->escape_string($_POST['action']) : '';
    $mysqli->close();

    if ($us_mode === 'site') {
      if ($action === 'display') {
        if ($this->user->canEditSite) return $this->DisplaySite($media);
        // Return false for other tabs so that they're disabled.
        return ['custom' => $this->SiteConfig(),
                'content' => $this->SiteContent(), 'style' => false,
                'media' => false, 'history' => false];
      }
      if ($action === 'editConfig') {
        return $this->EditConfig();
      }
      if ($action === 'updateConfig') {
        return $this->UpdateConfig();
      }
      if ($action === 'templates') {
        return $this->Templates();
      }
      if ($action === 'saveTemplate') {
        return $this->SaveTemplate();
      }
      if ($action === 'removeTemplate') {
        return $this->RemoveTemplate();
      }
      if ($action === 'search') {
        return $this->Search();
      }
      if ($action === 'updateContent') {
        return $this->UpdateContent();
      }
      if ($action === 'updateStyle') {
        return $this->UpdateStyle();
      }
      return ['error' => 'Unknown action'];
    }
    if ($us_mode === 'page') {
      if ($this->user->canEditPage) return $this->DisplayPage($media);
      return ['error' => 'You don\'t have permission to edit this page.'];
    }
    if ($us_mode === 'box') {
      $result = [];
      $mysqli = connect_db();
      // Modules must do their own sql safety checks on content.
      $us_content = json_decode($_POST['content'], true);
      $label = $mysqli->escape_string($_POST['label']);
      $module = new Module($this->user, $this->owner, $label);
      if ($this->user->canEditPage) {
        $id = (int)substr($_POST['id'], 9);
        if ($module->CanEdit($id)) $module->SetContent($id, $us_content);
        $result['style'] = style_groups($module->Style($id, $media), $us_mode);
        $result['media'] = $module->Media($id);
        $result['history'] = 'Box History';
      }
      $result['content'] = $module->Callback();
      // Return the content for modules that don't store it, but allow use of
      // the extended editor.
      if (isset($result['content']['source']) &&
          $result['content']['source'] === '') {
        $result['content']['source'] = $us_content['data'];
      }
      $mysqli->close();
      return $result;
    }
  }

  public function CanAdd($page) {
    return false;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return false;
  }

  public function Content($id) {
    $update_all_pages = '';
    if ($this->user->canEditSite) {
      $update_all_pages = '<div class="update-all-pages hidden">' .
          '<label for="extended-update-all-pages">Copy changes to all pages?' .
          '</label>' .
          '<input id="extended-update-all-pages" type="checkbox">' .
        '</div>';
    }
    return '<div id="extended-tabs" class="hidden">' .
        '<ul>' .
          '<li><a href="#extended-custom">Custom</a></li>' .
          '<li><a href="#extended-content">Content</a></li>' .
          '<li><a href="#extended-style">Style</a></li>' .
          '<li><a href="#extended-history">History</a></li>' .
        '</ul>' .
        '<div id="extended-custom"></div>' .
        '<div id="extended-content"></div>' .
        '<div id="extended-style">' .
          $update_all_pages .
          '<div class="form-spacing">' .
            '<label for="extended-media-query">Media query: </label>' .
            '<input id="extended-media-query" type="text">' .
          '</div>' .
          '<div class="style-editor">' .
            '<h3>Add a new style group</h3>' .
            '<div class="form-spacing">' .
              '<label for="extended-new-selector">Selector: </label>' .
              '<input id="extended-new-selector" type="text">' .
            '</div>' .
          '</div>' .
        '</div>' .
        '<div id="extended-history"></div>' .
        '<div id="colorpicker"></div>' .
      '</div>';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.extended.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.extended.js', false);
    // The module is added for the 'admin' user here, for all other
    // users it is added when their account is created.
    $this->AddToAdmin('extended');
    $site_style = ['"",".style-editor .form-spacing","margin-top","0"',
                   '"",".style-rule","margin","0.3em"',
                   '"",".style-rule input[type=text]","height","25px"',
                   '"",".style-rule input[type=text]","width","200px"',
                   '"",".style-property","color","#828282"',
                   '"",".style-property","margin-left","0.6em"',
                   '"",".style-value","color","#828282"',
                   '"",".style-value","margin-left","0.6em"',
                   '"","#published-page","margin","1em"',
                   '"","#unpublished-page","margin","1em"',
                   '"","#colorpicker","position","relative"',
                   '"","#colorpicker","width","195px"',
                   '"","#colorpicker","height","195px"',
                   '"","#colorpicker","z-index","1003"',
                   '"",".extended","padding","0.5em 0em"',
                   '"","#site-config-form label","width","10em"',
                   '"","#site-config-form .submit","margin-left","10.3em"',
                   '"","#site-config-form","background-color","#eeeeee"',
                   '"","#site-config-form","border","1px solid #aaaaaa"',
                   '"","#site-config-form","border-radius","2px"',
                   '"","#site-config-form","padding","5px"',
                   '"","#site-config-form input","width","200px"',
                   '"","#analytics-input","width","200px"',
                   '"","#analytics-input","height","50px"',
                   '"","#site-template-form label","width","7em"',
                   '"","#site-template-form","background-color","#eeeeee"',
                   '"","#site-template-form","border","1px solid #aaaaaa"',
                   '"","#site-template-form","border-radius","2px"',
                   '"","#site-template-form","padding","5px"',
                   '"","#site-template-form","margin-bottom","10px"',
                   '"","#site-template-form .submit","float","right"',
                   '"","#site-template-content-textarea","width","350px"',
                   '"","#site-template-content-textarea","height","80px"',
                   '"","#site-template-description-textarea","width","350px"',
                   '"","#site-template-description-textarea","height","80px"',
                   '"","#site-search-form label","width","7em"',
                   '"","#site-search-form","background-color","#eeeeee"',
                   '"","#site-search-form","border","1px solid #aaaaaa"',
                   '"","#site-search-form","border-radius","2px"',
                   '"","#site-search-form","padding","5px"',
                   '"","#site-search-form","margin-bottom","10px"',
                   '"","#site-search-form .submit","margin-left","7.3em"',
                   '"","#site-search-textarea","width","350px"',
                   '"","#site-search-textarea","height","80px"',
                   '"","#site-update-textarea","width","580px"',
                   '"","#site-update-textarea","height","80px"',
                   '"",".site-search-results","margin-left","10px"',
                   '"",".search-match","margin","5px"',
                   '"",".search-match .content","background-color","#dddddd"',
                   '"",".search-match .content","padding","5px"',
                   '"","#extended-custom-settings label","width","160px"',
                   '"","#extended-custom-settings input[type=text]","width",' .
                     '"250px"',
                   '"","#extended-custom-settings button[type=submit]",' .
                     '"margin-left","165px"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'outside';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Append dobrado.extended.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.extended.js', false);
  }

  // Private functions below here ////////////////////////////////////////////

  private function DisplayPage($media) {
    $mysqli = connect_db();
    $media_queries = [];
    $query = 'SELECT DISTINCT media FROM page_style WHERE ' .
      'user = "' . $this->owner . '" AND media != ""';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($site_style = $mysqli_result->fetch_assoc()) {
        $media_queries[] = $site_style['media'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Extended->DisplayPage 1: ' . $mysqli->error);
    }

    $style = [];
    $query = 'SELECT selector, property, value FROM page_style WHERE ' .
      'user = "' . $this->owner . '" AND name = "' . $this->user->page . '" ' .
      'AND media = "' . $media . '" ORDER BY selector';
    if ($mysqli_result = $mysqli->query($query)) {
      $current = '';
      $rules = [];
      while ($page_style = $mysqli_result->fetch_assoc()) {
        if ($current !== $page_style['selector']) {
          if (count($rules) > 0) $style[$current] = $rules;
          // Reset the current selector and clear the rules array.
          $current = $page_style['selector'];
          $rules = [];
        }
        $rules[$page_style['property']] = $page_style['value'];
      }
      if (count($rules) > 0) $style[$current] = $rules;
      $mysqli_result->close();
    }
    else {
      $this->Log('Extended->DisplayPage 2: ' . $mysqli->error);
    }
    $mysqli->close();

    $publish = $this->Published($this->user->page) === 1 ?
      '<div id="published-page">' .
        'This page is public: <button>make private</button></div>' .
      '<div id="unpublished-page" class="hidden">' .
        'This page is private: <button>make public</button></div>' :
      '<div id="published-page" class="hidden">' .
        'This page is public: <button>make private</button></div>' .
      '<div id="unpublished-page">This page is private: ' .
        '<button>make public</button></div>';
    return ['custom' => $publish, 'content' => $this->ListModules(),
            'style' => style_groups($style, 'page'), 'media' => $media_queries,
            'history' => 'Page History'];
  }

  private function DisplaySite($media) {
    $mysqli = connect_db();
    $media_queries = [];
    $query = 'SELECT DISTINCT media FROM site_style WHERE media != ""';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($site_style = $mysqli_result->fetch_assoc()) {
        $media_queries[] = $site_style['media'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Extended->DisplaySite 1: ' . $mysqli->error);
    }

    $style = [];
    $query = 'SELECT selector, property, value FROM site_style ' .
      'WHERE media = "' . $media . '" ORDER BY selector';
    if ($mysqli_result = $mysqli->query($query)) {
      $current = '';
      $rules = [];
      while ($site_style = $mysqli_result->fetch_assoc()) {
        if ($current !== $site_style['selector']) {
          if (count($rules) > 0) $style[$current] = $rules;
          // Reset the current selector and clear the rules array.
          $current = $site_style['selector'];
          $rules = [];
        }
        $rules[$site_style['property']] = $site_style['value'];
      }
      if (count($rules) > 0) $style[$current] = $rules;
      $mysqli_result->close();
    }
    else {
      $this->Log('Extended->DisplaySite 2: ' . $mysqli->error);
    }
    $mysqli->close();

    $history = 'This tab should contain a history of the content tab ' .
      'changes. Each change should have an expandable discussion area.';
    return ['custom' => $this->SiteConfig(), 'content' => $this->SiteContent(),
            'style' => style_groups($style, 'site'), 'media' => $media_queries,
            'history' => $history];
  }

  private function EditConfig() {
    $us_content = json_decode($_POST['content'], true);
    $columns = '';
    $values = '';
    $update = '';
    // List the settings a user can change if no site permission.
    $user_config = ['login_page', 'permalink_default', 'permalink_order',
                    'timezone', 'title', 'title_includes_page'];
    // Also list the settings that can't be changed in single user mode.
    $single_user_exclude = ['user', 'cron', 'guest_allowed', 'max_upload',
                            'private_path', 'server_name'];
    $single_user = $this->Substitute('account-single-user') === 'true';

    $mysqli = connect_db();
    foreach ($us_content as $key => $value) {
      if ($single_user && in_array($key, $single_user_exclude)) continue;

      if ($this->user->canEditSite || in_array($key, $user_config)) {
        if ($columns !== '') $columns .= ',';
        $columns .= $mysqli->escape_string($key);
        if ($values !== '') $values .= ',';
        $values .= '"' . $mysqli->escape_string(htmlspecialchars($value)) . '"';
        if ($update !== '') $update .= ',';
        $update .= $mysqli->escape_string($key) . ' = "' .
          $mysqli->escape_string(htmlspecialchars($value)) . '"';
      }
    }
    // If no site permission make sure user column is also provided to insert.
    if (!$this->user->canEditSite) {
      $columns .= ', user';
      $values .= ', "' . $this->user->name . '"';
    }
    $query = 'INSERT INTO config (' . $columns . ') ' .
      'VALUES (' . $values . ')  ON DUPLICATE KEY UPDATE ' . $update;
    if (!$mysqli->query($query)) {
      $this->Log('Extended->EditConfig: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function ListModules() {
    $content = '';
    $mysqli = connect_db();
    $query = 'SELECT label, box_id FROM modules WHERE ' .
      'user = "' . $this->owner . '" AND ' .
      '(page = "' . $this->user->page . '" OR page = "") AND ' .
      'deleted = 0 ORDER BY placement, box_order';
    if ($mysqli_result = $mysqli->query($query)) {
      $content = '<div class="content-editor">';
      while ($module_list = $mysqli_result->fetch_assoc()) {
        $label = $module_list['label'];
        $id = (int)$module_list['box_id'];
        $module = new Module($this->user, $this->owner, $label);
        if (!$module->CanRemove($id)) continue;

        $module_content = $module->Content($id);
        if ($module_content === false) continue;

        // Leaving selectors in content will mess with javascript on the page,
        // they are mainly used with div's and form elements.
        $module_content = strip_tags($module_content,
                                     '<p><img><br><h1><h2><h3><h4>');
        if ($module->CanEdit($id) && $module_content === '') {
          $module_content = '<i>(module has no text to display)</i>';
        }
        $content .= '<h3><a href="#">' . $label . '</a></h3><div>' .
          $module_content .
          '<form id="box-move-form-' . $id . '">' .
            '<label id="box-move-label-' . $id . '" ' .
              'for="box-move-input-' . $id . '">Move to:</label>' .
            '<input id="box-move-input-' . $id . '" class="control-input" ' .
              'type="text" name="page" value="page..." maxlength="200">' .
            '<button class="delete">delete</button>' .
          '</form></div>';
      }
      $content .= '</div>';
      $mysqli_result->close();
    }
    else {
      $this->Log('Extended->ListModules: ' . $mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

  private function RemoveTemplate() {
    if (!$this->user->canEditSite) {
      if (!$this->GroupMember('admin', 'admin')) {
        return ['error' => 'You don\'t have permission to remove templates.'];
      }
      // If no site permission make sure the group matches.
      if ($_POST['group'] !== $this->user->group) {
        return ['error' => 'RemoveTemplate: group doesn\'t match.'];
      }
    }

    $mysqli = connect_db();
    $label = $mysqli->escape_string($_POST['label']);
    $group = $mysqli->escape_string($_POST['group']);
    $query = 'DELETE FROM template WHERE label = "' . $label . '" ' .
      'AND system_group = "' . $group . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Extended->RemoveTemplate: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function SaveTemplate() {
    // Don't allow changing the current single user mode setting.
    if ($_POST['label'] === 'account-single-user') {
      return ['error' => 'Cannot change single user mode.'];
    }
    if (!$this->user->canEditSite) {
      if (!$this->GroupMember('admin', 'admin')) {
        return ['error' => 'You don\'t have permission to save templates.'];
      }
      // If no site permission make sure the group matches.
      if ($_POST['group'] !== $this->user->group) {
        return ['error' => 'SaveTemplate: group doesn\'t match.'];
      }
    }

    include 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Attr.AllowedRel', ['me']);
    $config->set('HTML.DefinitionID', 'dobrado-extended');
    $config->set('HTML.DefinitionRev', 1);
    if ($def = $config->maybeGetRawHTMLDefinition()) {
      $def->addElement('article', 'Block', 'Flow', 'Common');
      $def->addElement('data', 'Inline', 'Inline', 'Common');
      $def->addAttribute('data', 'value', 'CDATA');
    }
    $purifier = new HTMLPurifier($config);

    $mysqli = connect_db();
    $label = $mysqli->escape_string($_POST['label']);
    $group = $mysqli->escape_string($_POST['group']);
    $content = $mysqli->escape_string($purifier->purify($_POST['content']));
    $query = 'INSERT INTO template VALUES ("' . $label . '", ' .
      '"' . $group . '", "' . $content . '") ON DUPLICATE KEY UPDATE ' .
      'content = "' . $content . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Extended->SaveTemplate 1: ' . $mysqli->error);
    }
    if ($this->user->canEditSite) {
      $description =
        $mysqli->escape_string(htmlspecialchars($_POST['description']));
      $query = 'INSERT INTO template_description VALUES ' .
        '("' . $label . '", "' . $description . '") ON DUPLICATE KEY UPDATE ' .
        'description = "' . $description . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Extended->SaveTemplate 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function Search() {
    $mysqli = connect_db();
    $search = $mysqli->escape_string($_POST['search']);
    $group = $mysqli->escape_string($_POST['group']);
    $mysqli->close();

    // If no site permission make sure the group matches.
    if ($this->user->canEditSite || ($this->GroupMember('admin', 'admin') &&
                                     $this->user->group === $group)) {
      $simple = new Simple($this->user, $this->owner);
      return $simple->Search($search, $group);
    }

    return ['error' => 'Search: group doesn\'t match.'];
  }

  private function SiteConfig() {
    $mysqli = connect_db();
    $config_values = [];
    $query = '';
    if ($this->user->canEditSite) {
      $query = 'SELECT * FROM config WHERE user = ""';
      if ($mysqli_result = $mysqli->query($query)) {
        $config_values = $mysqli_result->fetch_assoc();
        $mysqli_result->close();
      }
      else {
        $this->Log('Extended->SiteConfig 1: ' . $mysqli->error);
      }
    }
    else {
      // If no site permission user can edit a limited set of config options.
      $query = 'SELECT login_page, permalink_default, permalink_order, ' .
        'timezone, title, title_includes_page FROM config WHERE ' .
        'user = "' . $this->user->name . '" OR user = "" ORDER BY user DESC';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($config = $mysqli_result->fetch_assoc()) {
          foreach ($config as $key => $value) {
            // Only use a default config value if not set for the user.
            if (!isset($config_values[$key])) $config_values[$key] = $value;
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Extended->SiteConfig 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();

    // Hide some config values in single user mode and show others as read-only.
    $single_user = $this->Substitute('account-single-user') === 'true';
    $content = '';
    if ($this->user->canEditSite) {
      $content .= '<b>Rebuild client files:</b> ' .
        '<button class="rebuild-files">rebuild</button><hr>';
    }
    $content .= '<h3>Edit configuration:</h3><form id="site-config-form">';
    foreach ($config_values as $key => $value) {
      if ($key === 'analytics') {
        $content .= '<div class="form-spacing">' .
            '<label for = "' . $key . '-input">' . $key . ':</label>' .
            '<textarea id = "' . $key . '-input" name = "' . $key . '">' .
              $value .
            '</textarea>' .
          '</div>';
        continue;
      }

      $single_user_hidden = ['user', 'guest_allowed', 'private_path'];
      if ($single_user && in_array($key, $single_user_hidden)) continue;

      $single_user_readonly = ['cron', 'max_upload', 'server_name'];
      $readonly = $single_user && in_array($key, $single_user_readonly) ?
        ' readonly="readonly"' : '';
      $content .= '<div class="form-spacing">' .
          '<label for="' . $key . '-input">' . $key . ':</label>' .
          '<input id="' . $key . '-input" name="' . $key . '" ' .
            'value="' . $value . '" type="text"' . $readonly . '>' .
        '</div>';
    }
    $content .= '<button class="submit">submit</button></form>';
    return $content;
  }

  private function SiteContent() {
    $template_group = '';
    $search_group = '';
    $description = '';
    $help = '';

    // Need site permission or membership in admin/admin to edit templates.
    if ($this->user->canEditSite) {
      // Group is an input if user has site permission, otherwise it's shown as
      // a select if there's more than one group in their organisation.
      $template_group = '<div class="form-spacing">' .
          '<label for="site-template-group-input">Group:</label>' .
          '<input id="site-template-group-input" type="text" maxlength="50">' .
        '</div>';
      $search_group = '<div class="form-spacing">' .
          '<label for="site-search-group-input">Group:</label>' .
          '<input id="site-search-group-input" type="text" maxlength="50">' .
        '</div>';
      // Description is shown as a tooltip via the help button if the user
      // doesn't have site permission.
      $description = '<div class="form-spacing">' .
          '<label for="site-template-description-textarea">' .
            'Description:</label>' .
          '<textarea id="site-template-description-textarea"></textarea>' .
        '</div>';
    }
    else if ($this->GroupMember('admin', 'admin')) {
      $template_group = '<input id="site-template-group-input" type="hidden" ' .
        'value = "' . $this->user->group . '">';
      $search_group = '<input id="site-search-group-input" type="hidden" ' .
        'value = "' . $this->user->group . '">';
      // Help for labels is shown in the description textarea in the form if
      // user has site permission, otherwise a help button is shown.
      $help = '<button class="help">help</button>';
    }
    else {
      return false;
    }

    return '<h3>Search:</h3>' .
      '<form id="site-search-form">' .
        '<div class="form-spacing">' .
          '<label for="site-search-textarea">Content:</label>' .
          '<textarea id="site-search-textarea"></textarea>' .
        '</div>' .
        $search_group .
        '<button class="submit">submit</button>' .
        '<div class="site-search-results"></div>' .
        '<div class="site-search-update hidden">' .
          '<div class="site-update-content">' .
            '<textarea id="site-update-textarea"></textarea><br>' .
            '<button class="update">update</button>' .
          '</div>' .
          '<div class="site-update-style">' .
            '<div class="form-spacing">' .
              '<label for="site-update-media">Media:</label>' .
              '<input id="site-update-media" type="text">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="site-update-selector">Selector:</label>' .
              '<input id="site-update-selector" type="text">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="site-update-property">Property:</label>' .
              '<input id="site-update-property" type="text">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="site-update-value">Value:</label>' .
              '<input id="site-update-value" type="text">' .
            '</div>' .
            '<button class="update">update</button>' .
          '</div>' .
        '</div>' .
      '</form>' .
      '<h3>Edit templates:</h3>' .
      '<form id="site-template-form">' .
        '<div class="form-spacing">' .
          '<label for="site-template-label-input">Label:</label>' .
          '<input id="site-template-label-input" type="text" maxlength="200">' .
          $help .
        '</div>' .
        $template_group .
        '<div class="form-spacing">' .
          '<label for="site-template-content-textarea">Content:</label>' .
          '<textarea id="site-template-content-textarea"></textarea>' .
        '</div>' .
        $description .
        '<button class="submit">submit</button>' .
        '<button class="remove">remove</button>' .
      '</form>';
  }

  private function Templates() {
    $object = ['group' => [], 'description' => []];
    if (!$this->user->canEditSite && !$this->GroupMember('admin', 'admin')) {
      return $object;
    }

    $mysqli = connect_db();
    // If the user doesn't have site permission show a limited set of templates.
    $template_query = $this->user->canEditSite ? '' :
      'WHERE system_group = "' . $this->user->group . '" OR system_group = "" ';
    $query = 'SELECT label, system_group, content FROM template ' .
      $template_query . 'ORDER BY system_group DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($template = $mysqli_result->fetch_assoc()) {
        $label = $template['label'];
        $group = $template['system_group'];
        $content = $template['content'];
        if (!isset($object['group'][$group])) {
          $object['group'][$group] = [];
        }
        // Need to simplify the case where a user doesn't have site permission
        // and therefore cannot change the group input. Want to still allow
        // them to see the default values so add them to the user's group array
        // where there isn't already an existing value.
        if ($group === '' && !$this->user->canEditSite) {
          if (!isset($object['group'][$this->user->group])) {
            $object['group'][$this->user->group] = [];
          }
          if (!isset($object['group'][$this->user->group][$label])) {
            $object['group'][$this->user->group][$label] = $content;
          }
        }
        else {
          $object['group'][$group][$label] = $content;
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Extended->Templates 1: ' . $mysqli->error);
    }
    $query = 'SELECT label, description FROM template_description';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($template_description = $mysqli_result->fetch_assoc()) {
        $label = $template_description['label'];
        $object['description'][$label] = $template_description['description'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Extended->Templates 2: ' . $mysqli->error);
    }
    $mysqli->close();
    return $object;
  }

  private function UpdateConfig() {
    if (!$this->user->canEditSite) {
      return ['error' => 'You don\'t have permission to update config.'];
    }

    $config = [];
    $mysqli = connect_db();
    $username = $mysqli->escape_string($_POST['username']);
    $query = 'SELECT * FROM config WHERE user = "' . $username . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      $config = $mysqli_result->fetch_assoc();
      $mysqli_result->close();
    }
    else {
      $this->Log('Extended->UpdateConfig: ' . $mysqli->error);
    }
    $mysqli->close();
    if (count($config) === 0) return ['error' => 'Config not set for user'];
    return $config;
  }

  private function UpdateContent() {
    $mysqli = connect_db();
    $group = $mysqli->escape_string($_POST['group']);
    $old_content = $mysqli->escape_string($_POST['oldContent']);
    $new_content = $mysqli->escape_string($_POST['newContent']);
    $mysqli->close();

    // If no site permission make sure the group matches.
    if ($this->user->canEditSite || ($this->user->group === $group &&
                                     $this->GroupMember('admin', 'admin'))) {
      $simple = new Simple($this->user, $this->owner);
      return $simple->UpdateContent($old_content, $new_content, $group);
    }

    return ['error' => 'UpdateContent: group doesn\'t match.'];
  }

  private function UpdateStyle() {
    $mysqli = connect_db();
    $group = $mysqli->escape_string($_POST['group']);
    $content = $mysqli->escape_string($_POST['content']);
    $media = $mysqli->escape_string($_POST['media']);
    $selector = $mysqli->escape_string($_POST['selector']);
    $property = $mysqli->escape_string($_POST['property']);
    $value = $mysqli->escape_string($_POST['value']);

    // If no site permission make sure the group matches.
    if ($this->user->canEditSite || ($this->GroupMember('admin', 'admin') &&
                                     $this->user->group === $group)) {
      $simple = new Simple($this->user, $this->owner);
      $simple->UpdateStyle($media, $selector, $property, $value, $content,
                           $group);
      // Update the style sheets for users in this group.
      $query = 'SELECT user FROM users WHERE system_group = "' . $group . '" ' .
        'AND user NOT LIKE "buyer\_%"';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($users = $mysqli_result->fetch_assoc()) {
          $name = $users['user'];
          $path = '../' . $name . '/style.css';
          if ($name === 'admin') {
            $path = '../style.css';
          }
          write_box_style($name, $path);
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Extended->UpdateStyle: ' . $mysqli->error);
      }
      $mysqli->close();
      return ['done' => true];
    }

    $mysqli->close();
    return ['error' => 'UpdateStyle: group doesn\'t match.'];
  }

}
