<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Skeleton extends Base {

  public function Add($id) {
    // This is some boilerplate to add per instance style rules when the
    // module is added. Just remove it if not required.
    $selector = '#dobrado-' . $id;
    $box_style = ['"", "' . $selector . '" , "property", "value"'];
    $this->AddBoxStyle($box_style);
  }

  public function Callback() {

  }

  public function CanAdd($page) {
    return true;
  }

  public function CanEdit($id) {
    return true;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    return '';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {

  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Need to call AppendScript here if module uses javascript, ie:
    $this->AppendScript($path, 'dobrado.skeleton.js');

    // You can create css rules for this module here, ie:
    $site_style = ['"@media query", "selector", "property", "value"'];
    $this->AddSiteStyle($site_style);

    // Install can be used to create database tables if required. Please also
    // add this module to the list of all modules in tests/helper.php, and
    // add the paths to the installed files to .gitignore.
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    // This is called when the version of the module is updated,
    // to provide a way to update or modify tables etc..
  }

  public function UpdateScript($path) {
    // Need to call AppendScript here if module uses javascript, ie:
    $this->AppendScript($path, 'dobrado.skeleton.js');
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

}
