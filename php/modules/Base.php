<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

abstract class Base {

  // READ THIS!: If this interface is changed, the Module class (module.php)
  // **********  must also be updated, and *all* modules that implement it.

  // Methods that all modules must provide:
  abstract public function Add($id);

  // Callback is used by request.php as a general request dispatch method.
  abstract public function Callback();                 

  // If the module has id's in it's markup, CanAdd should check if there
  // is already an instance on the specified page.
  abstract public function CanAdd($page);

  // Note that if CanEdit is true, the module should be able to return the
  // editable content of the module when Callback is called with mode=box.
  // This is used by the Extended module which expects a json object returned
  // containing values of: 'editor'(bool), 'source'(content), 'custom'(form).
  abstract public function CanEdit($id);
  abstract public function CanRemove($id);
 
  // Content is called by page.php when the page is initially constructed.
  abstract public function Content($id);
  abstract public function Copy($id, $new_page, $old_owner, $old_id);

  // Cron is called by cron.php, use it in conjunction with Run below.
  abstract public function Cron();

  // If a module is instantiated via the Module class, it will only have access
  // to this interface. Factory is used to provide access to custom methods.
  abstract public function Factory($fn, $p = NULL);

  // Group returns the class name of a div that this module will be placed in,
  // so that it can be grouped with other modules. It is used to set the value
  // in the 'class' column in the module table.
  abstract public function Group();
  abstract public function IncludeScript();

  // Install is used by start.php and installer.php.
  // The $path parameter is the javascript directory without a trailing slash.
  abstract public function Install($path);
  
  // Preferred placement when adding, as specified in the modules table.
  abstract public function Placement();

  // Publish is called by publish.php so that modules can do their own updates.
  abstract public function Publish($id, $update);
  abstract public function Remove($id);
  abstract public function SetContent($id, $content);

  // Both update functions are used by start.php and installer.php.
  abstract public function Update();
  // The $path parameter is the javascript directory without a trailing slash.
  abstract public function UpdateScript($path);

  // Shared implementation below here.
  protected $user = NULL;
  protected $owner = '';

  // Public functions. (Note: must also be added to module.php!!) ////////////

  public function __construct($user, $owner) {
    $this->user = $user;
    $this->owner = $owner;
  }

  public function AlreadyOnPage($label, $page = '') {
    if ($page === '') {
      $page = $this->user->page;
    }
    $already_on_page = true;
    $mysqli = connect_db();
    $query = 'SELECT label FROM modules WHERE label = "' . $label . '"  AND ' .
      'user = "' . $this->owner . '" AND page = "' . $page .'" AND deleted = 0';
    if ($mysqli_result = $mysqli->query($query)) {
      $already_on_page = $mysqli_result->num_rows !== 0;
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->AlreadyOnPage: ' . $mysqli->error);
    }
    $mysqli->close();
    return $already_on_page;
  }

  public function Media($id) {
    $media = [];
    $mysqli = connect_db();
    $query = 'SELECT DISTINCT media FROM box_style WHERE user = ' .
      '"' . $this->owner . '" AND media != ""';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($box_style = $mysqli_result->fetch_assoc()) {
        $media[] = $box_style['media'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->Media: ' . $mysqli->error);
    }
    $mysqli->close();
    return $media;
  }

  public function Style($id, $media = '') {
    $style = [];
    $mysqli = connect_db();
    $query = 'SELECT selector, property, value FROM box_style WHERE user = ' .
      '"' . $this->owner . '" AND (selector LIKE "#dobrado-' . $id . '" OR ' .
      'selector LIKE "#dobrado-' . $id . ' %") AND media = "' . $media . '" ' .
      'ORDER BY selector';
    if ($mysqli_result = $mysqli->query($query)) {
      $current = '';
      $rules = [];
      while ($box_style = $mysqli_result->fetch_assoc()) {
        if ($current !== $box_style['selector']) {
          if (count($rules) > 0) $style[$current] = $rules;
          // Reset the current selector and clear the rules array.
          $current = $box_style['selector'];
          $rules = [];
        }
        $rules[$box_style['property']] = $box_style['value'];
      }
      if (count($rules) > 0) $style[$current] = $rules;
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->Style: ' . $mysqli->error);
    }
    $mysqli->close();
    return $style;
  }

  public function Substitute($label, $patterns = '',
                             $replacements = '', $group = NULL) {
    $content = '';
    if ($group === NULL) $group = $this->user->group;

    $mysqli = connect_db();
    // Look for the content of the specific system_group, but if there isn't
    // one the empty system_group will be returned as the fallback.
    $query = 'SELECT content FROM template WHERE label = "' . $label . '"' .
      ' AND (system_group = "' . $group . '" OR system_group = "") ' .
      'ORDER BY system_group DESC';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($template = $mysqli_result->fetch_assoc()) {
        $content = $template['content'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->Substitute: ' . $mysqli->error);
    }
    $mysqli->close();

    if ($patterns === '' && $replacements === '') {
      return $content;
    }
    // Need to be careful of backreferences in replacements,
    // so escape all dollar signs.
    $replacements = str_replace('$', '\$', $replacements);
    return preg_replace($patterns, $replacements, $content);
  }

  // Protected functions. (Visible to inheriting modules) ////////////////////

  protected function AddBoxStyle($box_style) {
    $this->AddStyle($box_style, 'box_style');
  }

  protected function AddSettingTypes($setting_types) {
    $values = '';
    // Warning: setting_types must already be escaped here.
    foreach ($setting_types as $t) {
      if ($values !== '') $values .= ',';
      $values .= '(' . $t . ')';
    }
    $mysqli = connect_db();
    if (!$mysqli->query('INSERT INTO setting_types VALUES ' . $values)) {
      $this->Log('Base->AddSettingTypes: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  protected function AddSiteStyle($site_style) {
    $this->AddStyle($site_style, 'site_style');
  }

  protected function AddTemplate($template) {
    $values = '';
    // Note: template must already be escaped here.
    foreach ($template as $t) {
      if ($values !== '') $values .= ',';
      $values .= '(' . $t . ')';
    }
    $mysqli = connect_db();
    if (!$mysqli->query('INSERT INTO template VALUES ' . $values)) {
      $this->Log('Base->AddTemplate: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  protected function AddTemplateDescription($description) {
    $values = '';
    // Note: label and description must already be escaped here.
    foreach ($description as $label => $content) {
      if ($values !== '') $values .= ',';
      $values .= '("' . $label . '", "' . $content . '")';
    }
    $mysqli = connect_db();
    if (!$mysqli->query('INSERT INTO template_description VALUES ' . $values)) {
      $this->Log('Base->AddTemplateDescription: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  protected function AddToAdmin($label) {
    // The module is added for the 'admin' user here, for all other
    // users it is added when their account is created.
    $mysqli = connect_db();
    $query = 'INSERT INTO modules (user, page, label, class, box_order, ' .
      'placement, deleted) VALUES ("admin", "", "' . $label . '", "", 0, ' .
      '"outside", 0)';
    if (!$mysqli->query($query)) {
      $this->Log('Base->AddToAdmin 1: ' . $mysqli->error);
    }
    $query = 'INSERT INTO modules_history (user, page, label, class, ' .
      'box_order, placement, action, modified_by, timestamp) VALUES ("admin", '.
      '"", "' . $label . '", "", 0, "outside", "add", "admin", ' . time() . ')';
    if (!$mysqli->query($query)) {
      $this->Log('Base->AddToAdmin 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  protected function AppendScript($path, $file, $public = true) {
    if (file_exists($path . '/' . $file)) {
      $content = "\n// File: " . $file . "\n// Added by: " . $this->user->name .
        "\n// Date: " . date(DATE_COOKIE) . "\n";
      $content .= file_get_contents($path . '/' . $file);
      file_put_contents($path . '/dobrado.js', $content, FILE_APPEND);
      if ($public) {
        file_put_contents($path . '/dobrado.pub.js', $content, FILE_APPEND);
      }
    }
    else {
      $this->Log('Base->AppendScript: File not found: ' . $file);
    }
    if (file_exists($path . '/source/' . $file)) {
      $content = "\n// File: " . $file . "\n// Added by: " . $this->user->name .
        "\n// Date: " . date(DATE_COOKIE) . "\n";
      $content .= file_get_contents($path . '/source/' . $file);
      file_put_contents($path . '/source/dobrado.js', $content, FILE_APPEND);
      if ($public) {
        file_put_contents($path . '/source/dobrado.pub.js', $content,
                          FILE_APPEND);
      }
    }
    else {
      $this->Log('Base->AppendScript: Source file not found: ' . $file);
    }
  }

  protected function CanPublish() {
    if ($this->owner === 'admin' &&
        $this->user->page === $this->Substitute('indieauth-page') &&
        $this->user->group === $this->Substitute('indieauth-group')) {
      return isset($this->user->settings['micropub']['endpoint']);
    }
    // If not an indieauth user, micropub is not used so the availability of
    // an endpoint doesn't matter.
    return true;
  }

  protected function CopyStyle($id, $old_owner, $old_id) {
    $values = '';
    $mysqli = connect_db();
    $query = 'SELECT media, selector, property, value FROM box_style WHERE ' .
      'user = "' . $old_owner . '" AND (selector LIKE ' .
      '"#dobrado-' . $old_id . '" OR selector LIKE "#dobrado-' . $old_id.' %")';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($box_style = $mysqli_result->fetch_assoc()) {
        if ($values !== '') $values .= ',';
        $selector = preg_replace('/' . $old_id . '(.*)/', $id . '\1',
                                 $box_style['selector']);
        $values .= '("' . $this->owner . '","' .
          $mysqli->escape_string($box_style['media']) . '","' .
          $mysqli->escape_string($selector) . '","' .
          $mysqli->escape_string($box_style['property']) . '","' .
          $mysqli->escape_string($box_style['value']) . '")';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->CopyStyle 1: ' . $mysqli->error);
    }
    if ($values !== '') {
      if (!$mysqli->query('INSERT INTO box_style VALUES ' . $values)) {
        $this->Log('Base->CopyStyle 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  protected function CreateCSV($filename, $data, $convert_date = true,
                               $data_label = '', $header = NULL) {
    if (!is_array($data) || !isset($data[0])) return false;

    $content = '';
    // Use the first data row to generate a header row when not provided.
    if (!isset($header)) $header = array_keys($data[0]);
    for ($i = 0; $i < count($header); $i++) {
      if ($content !== '') $content .= ',';
      // The report module has to use the generic key 'data' for one column,
      // allow it to change the name used in the file.
      if ($data_label !== '' && $header[$i] == 'data') {
        $content .= '"' . $data_label . '"';
      }
      else {
        // Quote all values and also remove existing quotes.
        $content .= '"' . preg_replace('/"/', '', $header[$i]) . '"';
      }
    }
    $content .= "\n";
    for ($i = 0; $i < count($data); $i++) {
      $row = '';
      for ($j = 0; $j < count($header); $j++) {
        if ($row !== '') $row .= ',';
        $value = isset($data[$i][$header[$j]]) ? $data[$i][$header[$j]] : '';
        // Convert timestamps (which are assumed to be in milliseconds for js)
        // to human readable dates.
        if ($convert_date && ($header[$j] === 'date' ||
            $header[$j] === $data_label ||
            ($header[$j] === 'data' && $data_label !== ''))) {
          if ($value !== 0 && $value !== '') {
            $value = date('Y-m-d', $value / 1000);
          }
        }
        // Quote all values and also remove existing quotes.
        $row .= '"' . preg_replace('/"/', '', $value) . '"';
      }
      $content .= $row . "\n";
    }
    // Add ServerName to the path in case there are multiple installs on the
    // same server.
    $path = $this->user->config->PrivatePath() . '/' .
      $this->user->config->ServerName() . '/user/' . $this->user->name;
    if (!is_dir($path)) {
      mkdir($path, 0755, true);
    }
    if (file_exists($path . '/' . $filename)) unlink($path . '/' . $filename);
    $handle = fopen($path . '/' . $filename, 'w');
    if (!$handle) {
      $this->Log('Base->CreateCSV: Error opening file: ' . $filename);
      return false;
    }
    fwrite($handle, $content);
    fclose($handle);
    return true;
  }

  protected function Dependencies($dependencies) {
    $install = [];
    foreach ($dependencies as $label) {
      $module = new Module($this->user, $this->owner, $label);
      if (!$module->IsInstalled()) $install[] = $label;
    }
    return $install;
  }

  protected function GroupMember($group, $owner = '') {
    $member = false;
    if ($owner === '') {
      $owner = $this->owner;
    }
    $mysqli = connect_db();
    $query = 'SELECT visitor FROM group_names WHERE user = "' . $owner . '"' .
      ' AND name = "' . $group . '" AND visitor = "' . $this->user->name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows === 1) {
        $member = true;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->GroupMember: ' . $mysqli->error);
    }
    $mysqli->close();
    return $member;
  }

  protected function Log($us_message) {
    // Call error_log for the original message.
    error_log($us_message);
    
    $mysqli = connect_db();
    $query = 'INSERT INTO log VALUES ("' . $this->owner . '", ' .
      '"' . $this->user->name . '", "' . $this->user->page . '", ' .
      time() . ', ' . '"' . $mysqli->escape_string($us_message) . '")';
    if (!$mysqli->query($query)) {
      error_log('Base->Log: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  protected function LookupNickname($us_name, $us_url) {
    $us_photo = '';
    $us_domain = '';
    // The url provided here is the permalink, want to use the domain to
    // make sure we have the right name in case there are collisions.
    if (preg_match('/^https?:\/\/([^\/]+)/i', $us_url, $match)) {
      $us_domain = $match[1];
    }
    $mysqli = connect_db();
    $name = $mysqli->escape_string($us_name);
    $domain = $mysqli->escape_string($us_domain);
    $query = 'SELECT url, cache FROM nickname WHERE name = "' . $name . '" ' .
      'AND url LIKE "%' . $domain . '%"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($nickname = $mysqli_result->fetch_assoc()) {
        $us_url = $nickname['url'];
        $us_photo = $nickname['cache'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->LookupNickname: ' . $mysqli->error);
    }
    $mysqli->close();

    $secure = $this->user->config->Secure();
    $server = $this->user->config->ServerName();
    if ($us_photo === '') {
      // Assume here that there was no match and provide some simplified urls
      // for facebook and twitter feeds knowing they have path segments.
      if (strpos($us_url, 'https://twitter.com') === 0 &&
          preg_match('/^(https:\/\/twitter.com\/[^\/]+)/i', $us_url, $match)) {
        $us_url = $match[1];
        $us_photo = $secure ? 'https://' : 'http://';
        $us_photo .= $server . '/images/twitter.png';
      }
      else if (strpos($us_url, 'https://www.facebook.com') === 0 &&
               preg_match('/^(https:\/\/www.facebook.com\/[^\/]+)/i',
                          $us_url, $match)) {
        $us_url = $match[1];
        $us_photo = $secure ? 'https://' : 'http://';
        $us_photo .= $server . '/images/facebook.png';
      }
    }
    else if ($secure && strpos($us_photo, 'http://' . $server) === 0) {
      // Rewrite scheme on non-secure cache urls in secure mode.
      $us_photo = 'https://' . substr($us_photo, 7);
    }
    return [$us_name, $us_url, $us_photo];
  }

  protected function Notification($label, $us_author, $us_description,
                                  $us_category, $group = '', $permalink = '') {
    $link = '';
    if ($permalink !== '') {
      $link = $this->Url('', $permalink);
    }
    // Create the array required for SetContent. Note that the feed in the
    // content must be empty for the notification to show up on every page
    // for the user. (Also it's ok to pass mysql unsafe content in here.)
    $us_content = ['description' => $us_description, 'category' => $us_category,
                   'permalink' => $link, 'feed' => '', 'public' => 0];
    if (is_string($us_author)) {
      $us_content['author'] = $us_author;
    }
    else {
      if (isset($us_author[0])) $us_content['author'] = $us_author[0];
      if (isset($us_author[1])) $us_content['author_photo'] = $us_author[1];
      if (isset($us_author[2])) $us_content['author_url'] = $us_author[2];
    }

    // Look up who notifications should go to. The name of the group must be
    // of the form: <label>-notifications-<group> where group can be a system
    // group (like is done by the Invoice module) or a page name (like is done
    // by the Post and Comment modules).
    $name = $label . '-notifications';
    if ($group !== '') $name .= '-' . $group;

    $mysqli = connect_db();
    $query = 'SELECT visitor FROM group_names WHERE ' .
      'user = "' . $this->owner . '" AND name = "' . $name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($group_names = $mysqli_result->fetch_assoc()) {
        // A module is created for every member of the group.
        $owner = $group_names['visitor'];
        $notification = new Notification($this->user, $owner);
        // Check for duplicate webmentions but allow empty links that are used
        // internally.
        if ($link === '' || !$notification->ExistingUrl($us_description)) {
          $id = new_module($this->user, $owner, 'notification', '',
                           $notification->Group(), 'outside');
          $notification->SetContent($id, $us_content);
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->Notification: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  protected function Notify($id, $label, $action, $feed, $public = 1,
                            $update_timestamp = false, $user = '') {
    $mysqli = connect_db();
    $update_timestamp_query = '';
    if ($update_timestamp) {
      $update_timestamp_query = ', timestamp = ' . time();
    }
    if ($user === '') {
      $user = $this->owner;
    }
    $query = 'INSERT INTO notify VALUES ("' . $user . '", "' . $feed . '", ' .
      $id . ', "' . $label . '", "' . $action . '", ' . time() . ', ' .
      $public . ') ON DUPLICATE KEY UPDATE public = ' . $public .
      $update_timestamp_query;
    if (!$mysqli->query($query)) {
      $this->Log('Base->Notify 1: ' . $mysqli->error);
    }
    if ($public === 1) {
      $query = 'INSERT INTO page_updates VALUES ("' . $user . '", ' .
        '"' . $feed . '", "' . $action . '", ' . time() . ') ' .
        'ON DUPLICATE KEY UPDATE timestamp = ' . time();
      if (!$mysqli->query($query)) {
        $this->Log('Base->Notify 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  protected function PublicDirectory($us_file = '') {
    // The path to the directory needs to be relative to the calling script.
    // There are two options, a script in the php directory, or index.php
    // (either admin or under a user directory, but relative to the users
    //  public directory they are the same).
    if (strpos($_SERVER['SCRIPT_FILENAME'], 'index.php') === false) {
      if ($this->user->name === 'admin') {
        return '../public/' . $us_file;
      }
      else {
        return '../' . $this->user->name . '/public/' . $us_file;
      }
    }
    else {
      return 'public/' . $us_file;
    }
  }

  protected function Published($page = '', $owner = '') {
    $published_page = 0;
    if ($page === '') {
      $page = $this->user->page;
    }
    if ($owner === '') {
      $owner = $this->owner;
    }
    $mysqli = connect_db();
    $query = 'SELECT published FROM published WHERE user = "' . $owner . '"' .
      ' AND page = "' . $page . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($published = $mysqli_result->fetch_assoc()) {
        $published_page = (int)$published['published'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->Published: ' . $mysqli->error);
    }
    $mysqli->close();
    return $published_page;
  }

  protected function RemoveNotification($label, $page) {
    $mysqli = connect_db();
    // If there's an entry in the user_permission table where visitor is an
    // empty string, everyone has permission to access this page. In that case
    // don't need to do anything here.
    $query = 'SELECT visitor FROM user_permission WHERE visitor = "" AND ' .
      'user = "' . $this->owner . '" AND page = "' . $page . '" AND ' .
      '(edit = 1 OR copy = 1 OR view = 1)';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows === 1) {
        $mysqli_result->close();
        $mysqli->close();
        return;
      }
    }
    else {
      $this->Log('Base->RemoveNotification 1: ' . $mysqli->error);
    }

    // Don't remove the page owner from the list of visitors.
    $visitor_query = 'visitor != "' . $this->owner . '"';
    // Make a list of everyone who still has permission to access the page.
    // First check user_permission table and then group_permission.
    $query = 'SELECT DISTINCT visitor FROM user_permission WHERE ' .
      'user = "' . $this->owner . '" AND page = "' . $page . '" AND ' .
      '(edit = 1 OR copy = 1 OR view = 1)';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($user_permission = $mysqli_result->fetch_assoc()) {
        if ($visitor_query !== '') {
          $visitor_query .= ' OR ';
        }
        $visitor_query .= 'visitor != "' . $user_permission['visitor'] . '"';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->RemoveNotification 2: ' . $mysqli->error);
    }
    $query = 'SELECT DISTINCT visitor FROM group_names INNER JOIN ' .
      'group_permission ON group_names.name = group_permission.name AND ' .
      'group_names.user = group_permission.user WHERE ' .
      'group_names.user = "' . $this->owner . '" AND ' .
      'group_permission.page = "' . $page . '" AND ' .
      '(edit = 1 OR copy = 1 OR view = 1)';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($group_names = $mysqli_result->fetch_assoc()) {
        if ($visitor_query !== '') {
          $visitor_query .= ' OR ';
        }
        $visitor_query .= 'visitor != "' . $group_names['visitor'] . '"';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->RemoveNotification 3: ' . $mysqli->error);
    }
    if ($visitor_query !== '') {
      $query = 'DELETE FROM group_names WHERE user = "' . $this->owner . '" ' .
        'AND name = "' . $label . '-notifications-' . $page . '" AND ' .
        '(' . $visitor_query . ')';
      if (!$mysqli->query($query)) {
        $this->Log('Base->RemoveNotification 4: ' . $mysqli->error);
      }
    }
    $mysqli->close();
  }

  protected function RemoveNotify($id) {
    $mysqli = connect_db();
    $query = 'DELETE FROM notify WHERE user = "' . $this->owner . '"' .
      ' AND box_id = "' . $id . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Base->RemoveNotify: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  protected function RequestApiKey($feed_id) {
    $api_key = '';
    $mysqli = connect_db();
    $query = 'SELECT api_key FROM api WHERE feed_id = ' . $feed_id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($api = $mysqli_result->fetch_assoc()) {
        $api_key = $api['api_key'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->RequestApiKey: ' . $mysqli->error);
    }
    $mysqli->close();
    return $api_key;
  }

  protected function RequestFeedId($id, $label = '') {
    $feed_id = -1;
    $mysqli = connect_db();
    // Check if a feed id already exists for this module.
    $query = 'SELECT feed_id FROM api WHERE user = "' . $this->owner . '" ' .
      'AND box_id = ' . $id;
    if ($mysqli_result = $mysqli->query($query)) {
      if ($api = $mysqli_result->fetch_assoc()) {
        $feed_id = (int)$api['feed_id'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->RequestFeedId 1: ' . $mysqli->error);
    }
    // Otherwise generate a new unique feed id, if a label was provided.
    if ($feed_id === -1 && $label !== '') {
      $feed_id = mt_rand();
      while (true) {
        $query = 'SELECT feed_id FROM api WHERE feed_id = ' . $feed_id;
        if ($mysqli_result = $mysqli->query($query)) {
          if ($mysqli_result->num_rows === 0) {
            $mysqli_result->close();
            break;
          }
          $mysqli_result->close();
          $feed_id = mt_rand();
        }
        else {
          $this->Log('Base->RequestFeedId 2: ' . $mysqli->error);
        }
      }
      $query = 'INSERT INTO api VALUES ("' . $this->owner . '", ' .
        '"' . $id . '", "' . $label . '", ' . $feed_id . ', ' . mt_rand() . ')';
      if (!$mysqli->query($query)) {
        $this->Log('Base->RequestFeedId 3: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    return $feed_id;
  }

  protected function Run($time_description) {
    $run = strtotime($time_description);
    // Config->Cron returns the frequency with which cron.php is run, so
    // allowed to run here when the current time is within half that duration.
    $current = time();
    $allow = $this->user->config->Cron() / 2;
    if (($run >= $current - $allow) && ($run < $current + $allow)) {
      return true;
    }
    return false;
  }

  protected function RunGroups($label, $time = '') {
    $group_time = $time;
    $run_groups = [];
    $mysqli = connect_db();
    $query = 'SELECT DISTINCT system_group FROM users';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($users = $mysqli_result->fetch_assoc()) {
        $group = $users['system_group'];
        $date = $this->Substitute($label, '', '', $group);
        if ($date === '') continue;

        if ($time === '') {
          // When no time is given, assume there is a matching label to the
          // one given (that has a '-time' suffix) for this group.
          $group_time = $this->Substitute($label . '-time', '', '', $group);
          if ($group_time === '') continue;
        }

        // Need to convert the date using the group's timezone, it's also
        // returned so that the account running cron can use it too.
        $timezone = $this->Substitute('timezone', '', '', $group);
        if ($timezone !== '') {
          date_default_timezone_set($timezone);
        }

        $timestamp = strtotime($date . ', ' . $group_time);
        // Give Cron some time to run by checking that the current time
        // is within 15 minutes of the requested time.
        if ($timestamp > strtotime('-15 minutes') &&
            $timestamp < strtotime('15 minutes')) {
          $run_groups[] = ['group' => $group, 'timezone' => $timezone];
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Base->RunGroups: ' . $mysqli->error);
    }
    $mysqli->close();
    return $run_groups;
  }

  protected function Url($part = '', $page = '', $owner = '') {
    $url = '';
    if ($page === '') $page = $this->user->page;
    if ($owner === '') $owner = $this->owner;

    // Can't use fancy url's with Indieauth logins, so ignore config.
    if (strpos($owner, '.') === false && $this->user->config->FancyUrl()) {
      $url = '/' . $page;
      if ($part !== '') $url .= '?' . $part;
    }
    else {
      $url = '/index.php?page=' . $page;
      if ($part !== '') $url .= '&' . $part;
    }
    return $owner === 'admin' ? $url : '/' . $owner . $url;
  }

  // Private functions below here ////////////////////////////////////////////

  private function AddStyle($style, $table) {
    $values = '';
    // Warning: style must already be escaped here.
    foreach ($style as $s) {
      if ($values !== '') $values .= ',';
      $values .= '("' . $this->owner . '",' . $s . ')';
    }
    $mysqli = connect_db();
    if (!$mysqli->query('INSERT INTO ' . $table . ' VALUES ' . $values)) {
      $this->Log('Base->AddStyle: ' . $mysqli->error);
    }
    $mysqli->close();
  }

}
