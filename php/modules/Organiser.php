<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Organiser extends Base {

  public function Add($id) {

  }

  public function Callback() {
    if (!$this->user->canViewPage) {
      return ['error' => 'You don\'t have permission to view organiser.'];
    }
    // mode is used by the Extended module, which calls this function when
    // using ckeditor to compose an email message body.
    if (isset($_POST['mode']) && $_POST['mode'] === 'box') {
      return ['editor' => true, 'source' => ''];
    }

    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'show-organisations') {
      // This is called on load, so return an empty list if user doesn't
      // have permission.
      if ($this->user->canEditSite) {
        return ['list' => $this->ShowOrganisations()];
      }
      return ['list' => []];
    }
    if ($us_action === 'add-member') {
      return $this->user->canEditSite ? $this->AddMember() :
        ['error' => 'Permission denied adding organisation member.'];
    }
    if ($us_action === 'list-users') {
      return ['list' => $this->ShowUsers()];
    }
    if ($us_action === 'send-message') {
      return $this->SendMessage();
    }
    if ($us_action === 'show-members') {
      return $this->ShowMembersAndContacts();
    }
    if ($us_action === 'remove-member') {
      return $this->RemoveMember();
    }
    if ($us_action === 'add-contact') {
      return $this->AddContact();
    }
    if ($us_action === 'remove-contact') {
      return $this->RemoveContact();
    }
    if ($us_action === 'list-group') {
      return $this->ListGroup();
    }
    return ['error' => 'Unknown action.'];
  }

  public function CanAdd($page) {
    // Need admin privileges to add the organiser module.
    if (!$this->user->canEditSite) return false;
    // Can only have one organiser module on a page.
    return !$this->AlreadyOnPage('organiser', $page);
  }

  public function CanEdit($id) {
    // Set to true so that extended editor can be used to compose email.
    return true;
  }

  public function CanRemove($id) {
    return true;
  }

  public function Content($id) {
    $content = '';
    if ($this->user->canEditSite) {
      $content .= '<div class="organisation-editor">' .
        '<div class="form-spacing">' .
          '<label title="loading..." for="organisation-input">' .
            'Organisation:</label>' .
          '<input id="organisation-input" type="text">' .
        '</div>' .
        '<p>Groups:</p>' .
        '<div class="organisation-member-list"></div>' .
        '<form id="organiser-member-form">' .
          '<div class="form-spacing">' .
            '<label for="organiser-member-input">Add group to organisation:' .
            '</label>' .
            '<input id="organiser-member-input" type="text">' .
          '</div>' .
          '<button class="submit">submit</button>' .
        '</form>' .
        '<p>Contacts:<br><i>(Note that mail configuration is not done ' .
          'through this interface.)</i></p>' .
        '<div class="organisation-contact-list"></div>' .
        '<form id="organiser-contact-form">' .
          'Add contact for organisation:<br>' .
          '<div class="form-spacing">' .
            '<label for="organiser-contact-name-input">Name:</label>' .
            '<input id="organiser-contact-name-input" type="text">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="organiser-contact-user-input">User:</label>' .
            '<input id="organiser-contact-user-input" type="text">' .
          '</div>' .
          '<button class="submit">submit</button>' .
        '</form>' .
        '</div>';
    }
    $organisation = $this->Parent();
    $groups = [];
    if ($organisation === false) {
      // If there is no parent then set the value here to the empty string and
      // don't need to look up children.
      $organisation = '';
    }
    else {
      $groups = $this->Children($organisation);
    }
    $options = '<option value="none-selected">select who to send to...' .
      '</option><option value="' . $organisation . '">all ' . $organisation .
      ' members</option>';
    for ($i = 0; $i < count($groups); $i++) {
      $options .= '<option value="' . $groups[$i] . '">all ' . $groups[$i] .
        ' members</option>';
    }
    $options .= '<option value="individual">select individual members</option>';

    $content .= '<form id="organiser-email-form">' .
      '<p>Select the type of email you want to send.</p>' .
      '<div class="form-spacing">' .
        '<label for="organiser-email-type-input">Type:</label>' .
        '<select id="organiser-email-type-input">' . $options . '</select>' .
      '</div>' .
      '<div class="form-spacing hidden recipients">' .
        'Enter a username to add it to the list of recipients.<br>' .
        '<label for="organiser-email-username-input">Username:</label>' .
        '<input id="organiser-email-username-input" type="text">' .
        '<button class="add">add</button>' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="organiser-email-to-input">To:</label>' .
        '<input id="organiser-email-to-input" type="text">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="organiser-email-subject-input">Subject:</label>' .
        '<input id="organiser-email-subject-input" type="text">' .
      '</div>' .
      '<div class="form-spacing">' .
        '<label for="organiser-email-textarea">Message:</label><br>' .
        '<textarea id="organiser-email-textarea" class="dobrado-editable">' .
        '</textarea>' .
      '</div>' .
      '<button class="submit">submit</button><span class="info"></span>' .
      '</form>';
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.organiser.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.organiser.js', false);
    $mysqli = connect_db();
    $query = 'CREATE TABLE IF NOT EXISTS organisation (' .
      'name VARCHAR(50) NOT NULL,' .
      'system_group VARCHAR(50) NOT NULL,' .
      'PRIMARY KEY(system_group)' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Organiser->Install 1: ' . $mysqli->error);
    }

    // Note that 'user' here doesn't have to be in the system, although it
    // can be if users are given email addresses, otherwise it's virtual.
    $query = 'CREATE TABLE IF NOT EXISTS organisation_contact (' .
      'name VARCHAR(50) NOT NULL,' .
      'user VARCHAR(50) NOT NULL,' .
      'contact VARCHAR(200) NOT NULL,' .
      'PRIMARY KEY(name, user, contact(50))' .
      ') ENGINE=MyISAM';
    if (!$mysqli->query($query)) {
      $this->Log('Organiser->Install 2: ' . $mysqli->error);
    }
    $mysqli->close();

    $template = ['"organiser-sender", "", "noreply@!host"',
                 '"organiser-email-body", "", "<html><head><title>Email Title' .
                   '</title></head><body>!message</body></html>"'];
    $this->AddTemplate($template);
    $description = ['organiser-sender' => 'The \'from\' address for ' .
                      'organiser email, must be on the domain.',
                    'organiser-sender-name' => 'The descriptive name used ' .
                      'with the \'from\' address for organiser email.',
                    'organiser-email-body' => 'The email template html ' .
                      'document, substitutes: !message which is the content ' .
                      'provided by the user.'];
    $this->AddTemplateDescription($description);

    $site_style = ['"","#organiser-member-form .submit","margin-left","11em"',
                   '"","#organiser-member-form","background-color","#eeeeee"',
                   '"","#organiser-member-form","border","1px solid #aaaaaa"',
                   '"","#organiser-member-form","border-radius","2px"',
                   '"","#organiser-member-form","padding","5px"',
                   '"",".organisation-member","padding","0.2em"',
                   '"","#organiser-contact-form label","width","4em"',
                   '"","#organiser-contact-form .submit","margin-left","4em"',
                   '"","#organiser-contact-form","background-color","#eeeeee"',
                   '"","#organiser-contact-form","border","1px solid #aaaaaa"',
                   '"","#organiser-contact-form","border-radius","2px"',
                   '"","#organiser-contact-form","padding","5px"',
                   '"",".organisation-contact","padding","0.2em"',
                   '"","#organiser-email-form","background-color","#eeeeee"',
                   '"","#organiser-email-form","border","1px solid #aaaaaa"',
                   '"","#organiser-email-form","border-radius","2px"',
                   '"","#organiser-email-form","padding","5px"',
                   '"","#organiser-email-form label","width","5em"',
                   '"","#organiser-email-form .submit","margin-left","4em"',
                   '"","#organiser-email-textarea","width","500px"',
                   '"","#organiser-email-textarea","height","200px"'];
    $this->AddSiteStyle($site_style);
  }

  public function Placement() {
    return 'middle';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {
    $mysqli = connect_db();
    $query = 'ALTER TABLE organisation_contact DROP PRIMARY KEY';
    if (!$mysqli->query($query)) {
      $this->Log('Organiser->Update 1: ' . $mysqli->error);
    }
    $query = 'ALTER TABLE organisation_contact ADD PRIMARY KEY' .
      '(name, user, contact(50))';
    if (!$mysqli->query($query)) {
      $this->Log('Organiser->Update 2: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  public function UpdateScript($path) {
    // Append dobrado.organiser.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.organiser.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  public function Children($organisation, $ignore = '') {
    $children = [];
    $mysqli = connect_db();
    $ignore_query = '';
    if ($ignore !== '') {
      $ignore_query = 'system_group != "' . $ignore . '" AND ';
    }
    $query = 'SELECT system_group FROM organisation WHERE ' . $ignore_query .
      'name = "' . $organisation . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($organisation = $mysqli_result->fetch_assoc()) {
        $children[] = $organisation['system_group'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Organiser->Children: ' . $mysqli->error);
    }
    $mysqli->close();
    return $children;
  }

  public function Contact($contact, $name) {
    $user = '';
    $mysqli = connect_db();
    $query = 'SELECT user FROM organisation_contact WHERE ' .
      'contact = "' . $contact . '" AND name = "' . $name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($organisation_contact = $mysqli_result->fetch_assoc()) {
        $user = $organisation_contact['user'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Organiser->Contact: ' . $mysqli->error);
    }
    $mysqli->close();
    return $user;
  }

  public function GroupQuery($ignore = '') {
    $group_query = '';
    $groups = $this->Siblings($ignore);
    for ($i = 0; $i < count($groups); $i++) {
      if ($group_query !== '') {
        $group_query .= ' OR ';
      }
      $group_query .= 'users.system_group = "' . $groups[$i] . '"';
    }
    // If there is no data return the user's current group.
    if ($group_query === '') {
      return 'users.system_group = "' . $this->user->group . '"';
    }
    return '(' . $group_query . ')';
  }

  public function MatchParents($groups) {
    $filtered = [];
    $timezone = [];
    $group_query = '';
    for ($i = 0; $i < count($groups); $i++) {
      if ($group_query !== '') $group_query .= ' OR ';
      $name = $groups[$i]['group'];
      $timezone[$name] = $groups[$i]['timezone'];
      $group_query .= 'system_group = "' . $name . '"';
    }
    if ($group_query === '') return $filtered;

    $mysqli = connect_db();
    $query = 'SELECT system_group FROM organisation WHERE ' . $group_query .
      ' GROUP BY name';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($organisation = $mysqli_result->fetch_assoc()) {
        $name = $organisation['system_group'];
        $filtered[] = ['group' => $name, 'timezone' => $timezone[$name]];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Organiser->MatchParents: ' . $mysqli->error);
    }
    $mysqli->close();
    return $filtered;
  }

  public function MatchUser($user, $invite_groups = false) {
    $match = false;
    $mysqli = connect_db();
    $query = 'SELECT user FROM users WHERE user = "' . $user . '" ' .
      'AND ' . $this->GroupQuery();
    if ($mysqli_result = $mysqli->query($query)) {
      $match = $mysqli_result->num_rows === 1;
      $mysqli_result->close();
    }
    else {
      $this->Log('Organiser->MatchUser 1: ' . $mysqli->error);
    }
    // If not a match, check if invite_groups array was provided.
    if (!$match && $invite_groups) {
      $query = 'SELECT system_group FROM users WHERE user = "' . $user . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($users = $mysqli_result->fetch_assoc()) {
          $match = in_array($users['system_group'], $invite_groups);
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Organiser->MatchUser 2: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    return $match;
  }

  public function Parent() {
    $name = false;
    $mysqli = connect_db();
    $query = 'SELECT name FROM organisation WHERE ' .
      'system_group = "' . $this->user->group . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($organisation = $mysqli_result->fetch_assoc()) {
        $name = $organisation['name'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Organiser->Parent: ' . $mysqli->error);
    }
    $mysqli->close();
    return $name;
  }

  public function PurchaseQuery($ignore = '') {
    $group_query = '';
    $groups = $this->Siblings($ignore);
    for ($i = 0; $i < count($groups); $i++) {
      if ($group_query !== '') {
        $group_query .= ' OR ';
      }
      $group_query .= 'purchase_group = "' . $groups[$i] . '"';
    }
    // If there is no data return the user's current group.
    if ($group_query === '') {
      return 'purchase_group = "' . $this->user->group . '"';
    }
    return '(' . $group_query . ')';
  }

  public function ShowOrganisations() {
    $names = [];
    $mysqli = connect_db();
    $query = 'SELECT DISTINCT name FROM organisation';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($organisation = $mysqli_result->fetch_assoc()) {
        $names[] = $organisation['name'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Organiser->ShowOrganisations: ' . $mysqli->error);
    }
    $mysqli->close();
    return $names;
  }

  public function Siblings($ignore = '') {
    $result = [];
    $mysqli = connect_db();
    $ignore_query = '';
    if ($ignore !== '') {
      $ignore_query = 'system_group != "' . $ignore . '" AND ';
    }
    $query = 'SELECT system_group FROM organisation WHERE ' . $ignore_query .
      'name = (SELECT name FROM organisation WHERE ' .
      'system_group = "' . $this->user->group . '")';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($organisation = $mysqli_result->fetch_assoc()) {
        $result[] = $organisation['system_group'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Organiser->Siblings: ' . $mysqli->error);
    }
    $mysqli->close();
    // If no organisation is set for the group just return the current group.
    if (count($result) === 0) {
      $result[] = $this->user->group;
    }
    return $result;
  }

  // Private functions below here ////////////////////////////////////////////

  private function AddContact() {
    if ($this->user->canEditSite) {
      $mysqli = connect_db();
      $organisation =
        $mysqli->escape_string(strtolower($_POST['organisation']));
      $contact = $mysqli->escape_string(strtolower($_POST['contact']));
      // Only want the alias for the user, so ignore any domain name.
      $contact = preg_replace('/^([^@]*).*$/', '\1', $contact);
      $username = $mysqli->escape_string(strtolower($_POST['username']));
      $query = 'INSERT INTO organisation_contact VALUES ' .
        '("' . $organisation . '", "' . $username . '", "' . $contact . '") ' .
        'ON DUPLICATE KEY UPDATE contact = "' . $contact . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Organiser->AddContact: ' . $mysqli->error);
      }
      $mysqli->close();
      return ['contacts' => $this->ShowContacts($organisation),
              'list' => $this->ShowOrganisations()];
    }
    return ['error' => 'Permission denied adding organisation member.'];
  }

  private function AddMember() {
    $mysqli = connect_db();
    $name = '';
    $member = $mysqli->escape_string(strtolower($_POST['member']));
    // Member groups need to be unique across all organisations.
    $query = 'SELECT name FROM organisation WHERE ' .
      'system_group = "' . $member . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($organisation = $mysqli_result->fetch_assoc()) {
        $name = $organisation['name'];
      }
    }
    else {
      $this->Log('Organiser->AddMember 1: ' . $mysqli->error);
    }
    if ($name !== '') {
      $mysqli->close();
      return ['error' => $member . ' is already a member of ' . $name];
    }

    $name = '';
    // Member groups can't have the same name as an organisation either.
    $query = 'SELECT name FROM organisation WHERE name = "' . $member . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($organisation = $mysqli_result->fetch_assoc()) {
        $name = $organisation['name'];
      }
    }
    else {
      $this->Log('Organiser->AddMember 2: ' . $mysqli->error);
    }
    if ($name !== '') {
      $mysqli->close();
      return ['error' => $member . ' is the name of an organisation.'];
    }

    // Can't add invite groups to an organisation. (Note that avoiding
    // adding invite module as a dependency here because organiser is a
    // default module.)
    $invite = new Module($this->user, $this->owner, 'invite');
    if ($invite->IsInstalled() && $invite->Factory('Exists', $member)) {
      $mysqli->close();
      return ['error' => 'Can\'t add an invite group.'];
    }

    $organisation = $mysqli->escape_string(strtolower($_POST['organisation']));
    if ($organisation === $member) {
      $mysqli->close();
      return ['error' => 'Organisation and member group can\'t have the same ' .
                         'name.'];
    }

    $query = 'INSERT INTO organisation VALUES ("' . $organisation . '", ' .
      '"' . $member . '")';
    if (!$mysqli->query($query)) {
      $this->Log('Organiser->AddMember 3: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['members' => $this->ShowMembers($organisation),
            'list' => $this->ShowOrganisations()];
  }

  private function ListGroup() {
    $mysqli = connect_db();
    $group = $mysqli->escape_string(strtolower($_POST['group']));
    // First check if 'group' is actually an organisation.
    $query = 'SELECT DISTINCT name FROM organisation WHERE ' .
      'name = "' . $group . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($mysqli_result->num_rows === 1) {
        // When the organisation is given set group to the empty string
        // so that ShowUsers will use GroupQuery instead.
        $group = '';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Organiser->ListGroup: ' . $mysqli->error);
    }
    $mysqli->close();
    return ['list' => $this->ShowUsers($group, false)];
  }

  private function RemoveContact() {
    if ($this->user->canEditSite) {
      $mysqli = connect_db();
      $organisation =
        $mysqli->escape_string(strtolower($_POST['organisation']));
      $contact = $mysqli->escape_string($_POST['contact']);
      $username = $mysqli->escape_string(strtolower($_POST['username']));
      $query = 'DELETE FROM organisation_contact WHERE ' .
        'name = "' . $organisation . '" AND user = "' . $username . '" AND ' .
        'contact = "' . $contact . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Organiser->RemoveContact: ' . $mysqli->error);
      }
      $mysqli->close();
      return ['done' => true];
    }
    return ['done' => false];
  }

  private function RemoveMember() {
    if ($this->user->canEditSite) {
      $mysqli = connect_db();
      $organisation =
        $mysqli->escape_string(strtolower($_POST['organisation']));
      $member = $mysqli->escape_string(strtolower($_POST['member']));
      $query = 'DELETE FROM organisation WHERE ' .
        'name = "' . $organisation . '" AND system_group = "' . $member . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Organiser->RemoveMember: ' . $mysqli->error);
      }
      $mysqli->close();
      return ['done' => true];
    }
    return ['done' => false];
  }

  private function SendMessage() {
    $us_recipients = $_POST['recipients'];
    $us_subject = $_POST['subject'];
    if ($us_recipients === '') {
      return ['error' => 'Recipients not provided.'];
    }
    if ($us_subject === '') {
      return ['error' => 'Subject not provided.'];
    }

    include 'library/HTMLPurifier.auto.php';
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Attr.EnableID', true);
    $config->set('Attr.IDPrefix', 'anchor-');
    $purifier = new HTMLPurifier($config);
    $us_message = $purifier->purify($_POST['message']);
    if ($us_message === '') {
      return ['error' => 'Message not provided.'];
    }

    $body = wordwrap($this->Substitute('organiser-email-body', '/!message/',
                                       $us_message));
    $sender = $this->Substitute('organiser-sender', '/!host/',
                                $this->user->config->ServerName());
    $sender_name = $this->Substitute('organiser-sender-name');
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $headers  = "MIME-Version: 1.0\r\n" .
      "Content-type: text/html; charset=utf-8\r\n" .
      'From: ' . $sender_name . "\r\n";
    $mysqli = connect_db();
    foreach (explode(',', $us_recipients) as $us_email) {
      $email = $mysqli->escape_string($us_email);
      // The recipients username is provided at the beginning of their email.
      $us_user = trim(substr($us_email, 0, strpos($us_email, '<')));
      $user = $mysqli->escape_string($us_user);
      dobrado_mail($email, $us_subject, $body, $headers, '',
                   $sender, $user, 'organiser', 'message');
    }
    $mysqli->close();
    return ['done' => true];
  }

  private function ShowContacts($organisation) {
    $content = '';
    $server_name = $this->user->config->ServerName();
    $mysqli = connect_db();
    $query = 'SELECT user, contact FROM organisation_contact WHERE ' .
      'name = "' . $organisation . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($organisation_contact = $mysqli_result->fetch_assoc()) {
        $content .= '<div class="organisation-contact">' .
          '<button class="remove-organisation-contact">remove</button> ' .
          '<span class="contact">' . $organisation_contact['contact'] . ': ' .
            $organisation_contact['user'] . '@' . $server_name .
          '</span></div>';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Organiser->ShowContacts: ' . $mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

  private function ShowMembers($organisation) {
    $content = '';
    $children = $this->Children($organisation);
    for ($i = 0; $i < count($children); $i++) {
      $content .= '<div class="organisation-member">' .
        '<button class="remove-organisation-member">remove</button> ' .
        '<span class="member">' . $children[$i] . '</span></div>';
    }
    return $content;
  }

  private function ShowMembersAndContacts() {
    if ($this->user->canEditSite) {
      $mysqli = connect_db();
      $organisation =
        $mysqli->escape_string(strtolower($_POST['organisation']));
      $mysqli->close();
      return ['members' => $this->ShowMembers($organisation),
              'contacts' => $this->ShowContacts($organisation)];
    }
    return ['error' => 'Permission denied showing members.'];
  }

  private function ShowUsers($group = '', $array = true) {
    $user_list = $array ? [] : '';
    $mysqli = connect_db();
    $query = '';
    if ($group !== '') {
      $query = 'SELECT user, email FROM users WHERE active = 1 AND ' .
        'system_group = "' . $group . '"';
    }
    else {
      $query = 'SELECT user, email FROM users WHERE active = 1 AND ' .
        $this->GroupQuery();
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($users = $mysqli_result->fetch_assoc()) {
        $email = $users['email'];
        if ($email === '') continue;

        if ($array) {
          $user_list[] = $users['user'] . ' <' . $email . '>';
        }
        else {
          if ($user_list !== '') {
            $user_list .= ', ';
          }
          $user_list .= $users['user'] . ' <' . $email . '>';
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Organiser->ShowUsers: ' . $mysqli->error);
    }
    $mysqli->close();
    return $user_list;
  }

}
