<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Account extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $us_action = isset($_POST['action']) ? $_POST['action'] : '';
    if ($us_action === 'preferences') {
      return ['content' => $this->Preferences(),
              'groups' => $this->ShowGroups(),
              'users' => $this->ShowUsers()];
    }
    if ($us_action === 'check-username') {
      return $this->CheckUsername();
    }
    if ($us_action === 'add-user') {
      return $this->AddUser();
    }
    if ($us_action === 'add-group-member') {
      return $this->AddGroupMember();
    }
    if ($us_action === 'remove-group-member') {
      return $this->RemoveGroupMember();
    }
    if ($us_action === 'register') {
      // Check if this is a guest account.
      if (preg_match('/^guest[0-9]{10}[a-z0-9]{5}$/', $this->user->name)) {
        return ['content' => $this->RegisterForm('guest')];
      }
      if ($this->GroupMember('admin', 'admin')) {
        return ['content' => $this->RegisterForm('user')];
      }
      return ['error' => 'Registration not available'];
    }
    if ($us_action === 'register-supplier') {
      if ($this->GroupMember('admin', 'admin')) {
        return ['content' => $this->RegisterForm('supplier')];
      }
      return ['error' => 'Register supplier not available'];
    }
    if ($us_action === 'remove-user') {
      return ['content' => '<form id="remove-user-form" autocomplete="off">' .
        'All data associated with the account will be deleted!' .
        '<div class="form-spacing">' .
          '<label for="remove-user-input">Username:</label>' .
          '<input id="remove-user-input" type="text" maxlength="50">' .
        '</div>' .
        '<button class="submit">submit</button></form>'];
    }
    if ($us_action === 'logout') {
      return $this->Logout();
    }
    if ($us_action === 'change-password') {
      return $this->Change('password', $_POST['newPassword'],
                           $_POST['oldPassword']);
    }
    if ($us_action === 'change-email') {
      return $this->Change('email', strtolower($_POST['email']),
                           $_POST['password']);
    }
    if ($us_action === 'remove-user-submit') {
      return $this->RemoveUser();
    }

    $result = [];
    $mysqli = connect_db();
    if ($us_action === 'show-group') {
      $group = $mysqli->escape_string($_POST['group']);
      $result['content'] = $this->ShowGroupMembers($group);
    }
    else if ($us_action === 'user-permission') {
      $visitor = $mysqli->escape_string(htmlspecialchars($_POST['visitor']));
      $page = $mysqli->escape_string($_POST['page']);
      $edit = (int)$_POST['edit'];
      $copy = (int)$_POST['copy'];
      $view = (int)$_POST['view'];
      // Allow admin user to provide an empty page (for site permission).
      $min = $this->user->name === 'admin' ? '0' : '1';
      if (!preg_match('/^[a-z0-9_-]{' . $min . ',200}$/i', $page)) {
        $result['error'] = 'Invalid page name.';
      }
      else {
        // Don't insert a new row if all actions are false.
        if ($edit !== 0 || $copy !== 0 || $view !== 0) {
          $query = 'INSERT INTO user_permission VALUES ' .
            '("' . $this->user->name . '", "' . $page . '", ' .
            '"' . $visitor . '", ' . $edit . ', ' . $copy . ', ' .
            $view . ') ON DUPLICATE KEY UPDATE ' .
            'edit = ' . $edit . ', copy = ' . $copy . ', view = ' . $view;
          if (!$mysqli->query($query)) {
            $this->Log('Account->Callback 4: ' . $mysqli->error);
          }
        }
        $result['content'] = $this->UserPermission();
      }
    }
    else if ($us_action === 'group-permission') {
      $group = $mysqli->escape_string(strtolower($_POST['group']));
      $page = $mysqli->escape_string($_POST['page']);
      $edit = (int)$_POST['edit'];
      $copy = (int)$_POST['copy'];
      $view = (int)$_POST['view'];
      // Allow admin user to provide an empty page (for site permission).
      $min = $this->user->name === 'admin' ? '0' : '1';
      if (!preg_match('/^[a-z0-9_-]{' . $min . ',200}$/i', $page)) {
        $result['error'] = 'Invalid page name.';
      }
      else if (!preg_match('/^[a-z0-9_-]{1,250}$/', $group)) {
        $result['error'] = 'Invalid group name.';
      }
      else {
        // Don't insert a new row if all actions are false.
        if ($edit !== 0 || $copy !== 0 || $view !== 0) {
          $query = 'INSERT INTO group_permission VALUES ' .
            '("' . $this->user->name . '", "' . $page . '", "' . $group . '", '.
            $edit . ', ' . $copy . ', ' . $view . ') ON DUPLICATE KEY UPDATE ' .
            'edit = ' . $edit . ', ' . 'copy = ' . $copy . ', view = ' . $view;
          if (!$mysqli->query($query)) {
            $this->Log('Account->Callback 5: ' . $mysqli->error);
          }
        }
        $result['content'] = $this->GroupPermission();
      }
    }
    else if ($us_action === 'remove-user-permission') {
      $visitor = $mysqli->escape_string($_POST['visitor']);
      $page = $mysqli->escape_string($_POST['page']);
      $query = 'DELETE FROM user_permission WHERE ' .
        'user = "' . $this->user->name . '" AND page = "' . $page . '" AND ' .
        'visitor = "' . $visitor . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Account->Callback 6: ' . $mysqli->error);
      }
      $result['done'] = true;
    }
    else if ($us_action === 'remove-group-permission') {
      $group = $mysqli->escape_string(strtolower($_POST['group']));
      $page = $mysqli->escape_string($_POST['page']);
      $query = 'DELETE FROM group_permission WHERE ' .
        'user = "' . $this->user->name . '" AND page = "' . $page . '" AND ' .
        'name = "' . $group . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Account->Callback 7: ' . $mysqli->error);
      }
      $result['done'] = true;
    }
    else if ($us_action === 'add-supply-group') {
      $new_user = $mysqli->escape_string(strtolower($_POST['newUser']));
      $group = $mysqli->escape_string(strtolower($_POST['group']));
      $stock = new Module($this->user, $this->owner, 'stock');
      if (!preg_match('/^[a-z0-9_-]{1,250}$/', $group)) {
        $result['error'] = 'Invalid group name.';
      }
      else if ($stock->IsInstalled()) {
        $result['content'] = $stock->Factory('AddSupplyGroup',
                                             [$new_user, $group]);
      }
      else {
        $result['error'] = 'Could not add supply group.';
      }
    }
    else if ($us_action === 'remove-supply-group') {
      $new_user = $mysqli->escape_string(strtolower($_POST['newUser']));
      $group = $mysqli->escape_string(strtolower($_POST['group']));
      $stock = new Module($this->user, $this->owner, 'stock');
      if ($stock->IsInstalled()) {
        $stock->Factory('RemoveSupplyGroup', [$new_user, $group]);
        $result['done'] = true;
      }
      else {
        $result['error'] = 'Could not remove supply group.';
      }
    }
    $mysqli->close();
    return $result;
  }

  public function CanAdd($page) {
    return false;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return false;
  }

  public function Content($id) {
    return '';
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {

  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.account.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.account.js', false);
    // The module is added for the 'admin' user here, for all other
    // users it is added when their account is created.
    $this->AddToAdmin('account');
    // Add style rules to the site_style table.
    $site_style = ['"","#account-set-password","margin-left","8.4em"',
                   '"","#change-password-form label","width","11em"',
                   '"","#change-password-form .submit","margin-left","11.3em"',
                   '"","#change-password-form","background-color","#eeeeee"',
                   '"","#change-password-form","border","1px solid #aaaaaa"',
                   '"","#change-password-form","border-radius","2px"',
                   '"","#change-password-form","padding","5px"',
                   '"","#change-email-form label","width","6em"',
                   '"","#change-email-form .submit","margin-left","6.3em"',
                   '"","#change-email-form","background-color","#eeeeee"',
                   '"","#change-email-form","border","1px solid #aaaaaa"',
                   '"","#change-email-form","border-radius","2px"',
                   '"","#change-email-form","padding","5px"',
                   '"","#user-group-form label","width","11em"',
                   '"","#user-group-form .submit","margin-left","9.3em"',
                   '"","#user-group-form","background-color","#eeeeee"',
                   '"","#user-group-form","border","1px solid #aaaaaa"',
                   '"","#user-group-form","border-radius","2px"',
                   '"","#user-group-form","padding","5px"',
                   '"",".group-member","padding","2px"',
                   '"","#user-permission-form label","width","6em"',
                   '"","#user-permission-form .submit","margin-left","6.3em"',
                   '"","#user-permission-form","background-color","#eeeeee"',
                   '"","#user-permission-form","border","1px solid #aaaaaa"',
                   '"","#user-permission-form","border-radius","2px"',
                   '"","#user-permission-form","padding","5px"',
                   '"","#group-permission-form label","width","6em"',
                   '"","#group-permission-form .submit","margin-left","6.3em"',
                   '"","#group-permission-form","background-color","#eeeeee"',
                   '"","#group-permission-form","border","1px solid #aaaaaa"',
                   '"","#group-permission-form","border-radius","2px"',
                   '"","#group-permission-form","padding","5px"',
                   '"","#register-form label","width","8em"',
                   '"","#register-form .submit","margin-left","8.3em"',
                   '"","#register-form .username-info","margin-left","3em"',
                   '"","#register-form .new-user-confirm .info",' .
                     '"margin-left","3em"',
                   '"","#register-form .stock-supply-options",' .
                     '"margin-left","3em"',
                   '"","#register-form .stock-add-supply-group",' .
                     '"margin-left","1em"',
                   '"","#register-form","background-color","#eeeeee"',
                   '"","#register-form","border","1px solid #aaaaaa"',
                   '"","#register-form","border-radius","2px"',
                   '"","#register-form","padding","5px"',
                   '"",".group-page-list","margin-left","10px"',
                   '"",".account .form-spacing > input[type=text]","width",' .
                     '"200px"',
                   '"",".account .form-spacing > input[type=password]",' .
                     '"width","200px"'];
    $this->AddSiteStyle($site_style);
    $this->AddSettingTypes(['"account","forward",' .
      '"forward email,do not forward email","radio","If you have trouble ' .
      'receiving email from ' . $this->user->config->ServerName() . ', ' .
      'you can use this setting to have your email delivered via (address ' .
      'set in mail-forward-bcc).<br><b>Please note that you will need to ' .
      'confirm an email from this address to allow forwarding.</b>"']);
  }

  public function Placement() {
    return 'outside';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Append dobrado.account.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.account.js', false);
  }

  // Private functions below here ////////////////////////////////////////////

  private function AddUser() {
    $result = [];
    $mysqli = connect_db();
    $new_user = $mysqli->escape_string(strtolower(trim($_POST['newUser'])));
    $email = $mysqli->escape_string(strtolower(trim($_POST['email'])));
    $confirmed = (int)$_POST['confirmed'];
    $active = (int)$_POST['active'];
    $supplier = $_POST['supplier'] === 'true';

    if ($this->GroupMember('admin', 'admin')) {
      $us_new_password = trim($_POST['newPassword']);
      // The group input isn't shown in the form if there's no parent
      // organisation, so use the current user's group for the new user.
      $group = $this->user->group;
      if (isset($_POST['group'])) {
        $group = $mysqli->escape_string(strtolower($_POST['group']));
      }
      $user_exists = false;
      $current_group = '';
      $current_email = '';
      $already_confirmed = false;
      $status = false;
      // The supplier flag is used when an account should always be created,
      // so don't check for an existing user when set.
      if (!$supplier) {
        // Check if updating an existing user.
        $query = 'SELECT user, email, system_group, confirmed FROM users ' .
          'WHERE user = "' . $new_user . '"';
        if ($mysqli_result = $mysqli->query($query)) {
          if (($mysqli_result->num_rows === 1) &&
              $users = $mysqli_result->fetch_assoc()) {
            $user_exists = true;
            $current_group = $mysqli->escape_string($users['system_group']);
            $current_email = $mysqli->escape_string($users['email']);
            $already_confirmed = $users['confirmed'] === '1';
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Account->AddUser 1: ' . $mysqli->error);
        }
      }
      $organiser = new Organiser($this->user, $this->owner);
      $siblings = $organiser->Siblings();
      $invite = new Module($this->user, $this->owner, 'invite');
      $created = false;
      if ($invite->IsInstalled()) {
        $created = in_array($current_group, $invite->Factory('Created'));
      }
      // If the current user doesn't have site permission, they can only
      // update existing users if they're in one of the groups in their
      // organisation. Also add a special case for the admin account.
      if ($user_exists && !$this->user->canEditSite &&
          ($new_user === 'admin' ||
           (!in_array($current_group, $siblings) && !$created))) {
        $status = 'Permission denied updating user.';
      }
      // Don't want to change the group for invite group accounts.
      else if ($created) {
        $query = 'UPDATE users SET email = "' . $email . '", ' .
          'active = ' . $active . ' WHERE user = "' . $new_user . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Account->AddUser 2: ' . $mysqli->error);
        }
        $status = true;
      }
      // Also need site permission to change a user to any group, other
      // existing users with permission to add new users can only add them
      // to one of the groups in their organisation.
      else if ($this->user->canEditSite || in_array($group, $siblings)) {
        if ($user_exists) {
          $confirm_query = $confirmed === 1 ? 'confirmed = 1, ' : '';
          if ($already_confirmed || $confirmed === 1) {
            // If the email address for a confirmed account is being set for
            // the first time, send the user a new user email.
            // TODO: Move this to an option in the members module...
            if ($current_email === '' && $email !== '') {
              send_confirmed_user_email($this->user, $new_user, $email,
                                        $us_new_password);
            }
          }
          $password_query = '';
          if ($us_new_password !== '') {
            $password_hash =
              $mysqli->escape_string(password_hash($us_new_password,
                                                   PASSWORD_DEFAULT));
            $password_query = 'password = "' . $password_hash . '", ';
          }
          $query = 'UPDATE users SET email = "' . $email . '", ' .
            $confirm_query . $password_query .
            'system_group = "' . $group . '", active = ' . $active .
            ' WHERE user = "' . $new_user . '"';
          if (!$mysqli->query($query)) {
            $this->Log('Account->AddUser 3: ' . $mysqli->error);
          }
          if ($group !== $current_group) {
            // When changing the group for an unconfirmed (ie new) account,
            // a new default page is copied. This requires first deleting
            // their existing contents, so have a sanity check to at least
            // make sure this isn't the admin account...
            if (!$already_confirmed && $new_user !== 'admin') {
              $this->RemoveContents($new_user);
              $user = new User($new_user, $group);
              copy_default($user, $this->owner);
            }
            else {
              // Just update the default group for the user.
              $query = 'UPDATE group_names SET name = "all-' . $group . '" ' .
                'WHERE user = "admin" AND name = "all-' . $current_group . '" '.
                'AND visitor = "' . $new_user . '"';
              if (!$mysqli->query($query)) {
                $this->Log('Account->AddUser 4: ' . $mysqli->error);
              }
            }
          }
          // Set status to true so that extra details can be updated below.
          $status = true;
        }
        else {
          $user = new User($new_user, $group);
          $status = new_user($user, $this->owner, $active, $email, 1,
                             $us_new_password, $supplier);
          // new_user can update the username for suppliers.
          $new_user = $user->name;
        }
      }
      else {
        $status = 'Group doesn\'t match.';
      }
      // Update extra details if an account was just created or updated.
      if ($status === true) {
        $detail = new Module($this->user, $this->owner, 'detail');
        if ($detail->IsInstalled()) {
          $first = $mysqli->escape_string(htmlspecialchars($_POST['first']));
          $last = isset($_POST['last']) ?
            $mysqli->escape_string(htmlspecialchars($_POST['last'])) : '';
          $phone = isset($_POST['phone']) ?
            $mysqli->escape_string(htmlspecialchars($_POST['phone'])) : '';
          $address = isset($_POST['address']) ?
            $mysqli->escape_string(htmlspecialchars($_POST['address'])) : '';
          $description = isset($_POST['description']) ?
            $mysqli->escape_string(htmlspecialchars($_POST['description'])) :'';
          $display = (int)$_POST['display'];
          $reminder_time = isset($_POST['reminderTime']) ?
            (int)strtotime($_POST['reminderTime']) : 0;
          $reminder_repeat = isset($_POST['reminderRepeat']) ?
            $mysqli->escape_string($_POST['reminderRepeat']) : '';
          $detail->Factory('UpdateUser', [$new_user, $first, $last, $phone,
                                          $address, $description, $display,
                                          true, $reminder_time,
                                          $reminder_repeat, (int)$supplier]);
        }
        $result['name'] = $new_user;
      }
      else if ($status === false) {
        $result['error'] = 'There was a problem updating the account.';
      }
      else {
        $result['error'] = $status;
      }
    }
    // Didn't have permission to add a user, so check if this is a guest
    // account being converted to a real one.
    else if (preg_match('/^guest[0-9]{10}[a-z0-9]{5}$/', $this->user->name)) {
      $user = new User($new_user);
      $status = new_user($user, $this->owner, 1, $email, 0);
      if ($status === true) {
        $detail = new Module($user, $this->owner, 'detail');
        if ($detail->IsInstalled()) {
          $first = $mysqli->escape_string(htmlspecialchars($_POST['first']));
          $last = $mysqli->escape_string(htmlspecialchars($_POST['last']));
          $phone = isset($_POST['phone']) ?
            $mysqli->escape_string(htmlspecialchars($_POST['phone'])) : '';
          $address = isset($_POST['address']) ?
            $mysqli->escape_string(htmlspecialchars($_POST['address'])) : '';
          $description = isset($_POST['description']) ?
            $mysqli->escape_string(htmlspecialchars($_POST['description'])) :'';
          $display = (int)$_POST['display'];
          $detail->Factory('UpdateUser', [$new_user, $first, $last, $phone,
                                          $address, $description, $display,
                                          true]);
        }
        $result['name'] = $new_user;
      }
      else if ($status === false) {
        $result['error'] = 'There was a problem registering the account.';
      }
      else {
        $result['error'] = $status;
      }
    }
    else {
      $result['error'] = 'Could not add user.';
    }
    $mysqli->close();
    return $result;
  }

  private function Change($type, $us_value, $us_password) {
    $valid = false;
    $result = ['content' => '', 'updated' => false];

    if (isset($_SESSION['limited-login'])) {
      $result['content'] = 'For security reasons, please log in again ' .
        'before updating your ' . $type . '.';
      return $result;
    }

    $mysqli = connect_db();
    $query = 'SELECT password FROM users WHERE user = "' .$this->user->name.'"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($users = $mysqli_result->fetch_assoc()) {
        if (password_verify($us_password, $users['password'])) $valid = true;
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Account->Change 1: ' . $mysqli->error);
      $result['content'] = '<i>There was an error changing your ' . $type .
        '.<br>Please try again.</i>';
    }
    if ($valid) {
      if ($type === 'password') {
        $us_value = password_hash($us_value, PASSWORD_DEFAULT);
      }
      $value = $mysqli->escape_string($us_value);
      $query = 'UPDATE users SET ' . $type . ' = "' . $value . '" WHERE ' .
        'user = "' . $this->user->name . '"';
      if (!$mysqli->query($query)) {
        $this->Log('Account->Change 2: ' . $mysqli->error);
        $result['content'] = '<i>There was an error changing your ' . $type .
          '.<br>Please try again.</i>';
      }
      else {
        $result['content'] = '<i>Your ' . $type . ' has been updated.</i>';
        $result['updated'] = true;
      }
    }
    else {
      if ($type === 'password') {
        $result['content'] = '<i>Your old password does not match.</i>';
      }
      else {
        $result['content'] = '<i>Your password does not match.</i>';
      }
    }
    $mysqli->close();
    return $result;
  }

  private function CheckUsername() {
    $result = ['email' => '', 'group' => $this->user->group, 'first' => '',
               'last' => '', 'phone' => '', 'address' => '',
               'description' => '', 'display' => true, 'canUpdate' => false,
               'stock' => '',  'confirmed' => true];
    $mysqli = connect_db();
    $new_user = $mysqli->escape_string(strtolower($_POST['newUser']));
    // Always try to create a supplier account based on the given username,
    // since it doesn't matter what it gets called.
    $supplier = $_POST['supplier'] === 'true';
    $user = new User($new_user);
    list($available, $can_update) =
      username_available($user, $this->owner, $supplier);
    if ($available === true) {
      $result['message'] = '';
      $result['exists'] = false;
    }
    else {
      $result['message'] = $available;
      $result['exists'] = true;
      $email = '';
      $group = '';
      $confirmed = true;
      $active = false;
      $query = 'SELECT email, system_group, confirmed, active FROM users ' .
        'WHERE user = "' . $new_user . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if (($mysqli_result->num_rows === 1) &&
            $users = $mysqli_result->fetch_assoc()) {
          $email = $users['email'];
          $group = $users['system_group'];
          $confirmed = (int)$users['confirmed'] === 1;
          $active = (int)$users['active'] === 1;
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Account->CheckUsername: ' . $mysqli->error);
      }
      // A user with site permission can update all users, but without it need
      // to first check that an existing user is in the same organisation.
      // And also make the admin account a special case.
      $organiser = new Organiser($this->user, $this->owner);
      $siblings = in_array($group, $organiser->Siblings());
      // Avoid adding invite module as a dependency here because account is a
      // default module.
      $invite = new Module($this->user, $this->owner, 'invite');
      $created = false;
      if ($invite->IsInstalled()) {
        $created = in_array($group, $invite->Factory('Created'));
      }
      if ($this->user->canEditSite ||
          ($new_user !== 'admin' && ($siblings || $created))) {
        if ($can_update) {
          $result['message'] .=
            '.<br>(You can update the details for this user.)';
          $result['canUpdate'] = true;
        }
        $result['email'] = $email;
        $result['group'] = $group;
        $result['confirmed'] = $confirmed;
        $result['active'] = $active;
        $detail = new Module($user, $this->owner, 'detail');
        if ($detail->IsInstalled()) {
          $new_user_detail = $detail->Factory('User', $new_user);
          $result['first'] = $new_user_detail['first'];
          $result['last'] = $new_user_detail['last'];
          $result['phone'] = $new_user_detail['phone'];
          $result['address'] = $new_user_detail['address'];
          $result['description'] = $new_user_detail['description'];
          $result['display'] = $new_user_detail['display'];
          $result['reminderTime'] = $new_user_detail['reminderTime'] * 1000;
          $result['reminderRepeat'] = $new_user_detail['reminderRepeat'];
        }
        // If the stock module is installed, optionally allow the new user to
        // pick which groups they want to be a supplier for.
        if ($this->Substitute('stock-group-select') === 'true') {
          $stock = new Module($user, $this->owner, 'stock');
          if ($stock->IsInstalled()) {
            $result['stock'] = $stock->Factory('ShowSupplyGroups');
          }
        }
      }
      else if (strpos($result['message'], 'already exists') !== false) {
        // The username already exists but the current user can't update it.
        $result['message'] .= ' in another group.';
      }
    }
    $mysqli->close();
    return $result;
  }

  private function Logout() {
    $mysqli = connect_db();
    $query = 'DELETE FROM session WHERE user = "' . $this->user->name . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->Logout: ' . $mysqli->error);
    }
    $mysqli->close();

    setcookie('user', $this->user->name, time() - 3600, '/');
    setcookie('token', $this->user->name, time() - 3600, '/');
    session_destroy();
    return ['done' => true];
  }

  private function RemoveUser() {
    $result = [];
    $mysqli = connect_db();
    $remove = $mysqli->escape_string(strtolower($_POST['removeUser']));
    if ($this->user->canEditSite) {
      if ($remove === 'admin') {
        $result['error'] = 'Cannot remove admin account.';
      }
      else if ($remove === $this->user->name) {
        $result['error'] = 'Cannot remove account while logged in.';
      }
      else {
        $user_exists = false;
        $query = 'SELECT user FROM users WHERE user = "' . $remove . '"';
        if ($mysqli_result = $mysqli->query($query)) {
          if ($mysqli_result->num_rows === 1) {
            $user_exists = true;
          }
          $mysqli_result->close();
        }
        else {
          $this->Log('Account->RemoveUser 1: ' . $mysqli->error);
        }
        if ($user_exists) {
          $this->RemoveContents($remove);
          $query = 'DELETE FROM users WHERE user = "' . $remove . '"';
          if (!$mysqli->query($query)) {
            $this->Log('Account->RemoveUser 2: ' . $mysqli->error);
          }
          // The path to the directory needs to be relative to the calling
          // script, which is assumed to always be php/request.php here.
          $path = '../' . $remove;
          $this->RemoveDir($path . '/rss');
          $this->RemoveDir($path . '/public');
          $this->RemoveDir($path);
          $result['done'] = true;
        }
        else {
          $result['error'] = 'Username: ' . $remove . ' not found.';
        }
      }
    }
    else if ($this->GroupMember('admin', 'admin')) {
      $organiser = new Organiser($this->user, $this->owner);
      if ($organiser->MatchUser($remove)) {
        $query = 'UPDATE users SET system_group = "removed" WHERE ' .
          'user = "' . $remove . '"';
        if ($mysqli->query($query)) {
          $result['done'] = true;
        }
        else {
          $this->Log('Account->Callback 2: ' . $mysqli->error);
          $result['error'] = 'Server error.';
        }
        // Also update the default group for the user.
        $query = 'UPDATE group_names SET name = "all-removed" WHERE ' .
          'user = "admin" AND name LIKE "all-%" AND ' .
          'visitor = "' . $remove . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Account->Callback 3: ' . $mysqli->error);
        }
      }
      else {
        $result['error'] = 'User not found.';
      }
    }
    else {
      $result['error'] = 'You don\'t have permission to remove users.';
    }
    $mysqli->close();
    return $result;
  }

  private function ShowGroups() {
    $show_groups = [];
    $mysqli = connect_db();
    // Don't show special notification groups, they are accessed via the
    // 'view all notifications' menu option. 
    $query = 'SELECT DISTINCT name FROM group_names WHERE ' .
      'user = "' . $this->user->name . '" AND ' .
      'name NOT LIKE "post-notifications-%" AND ' .
      'name NOT LIKE "comment-notifications-%" ORDER BY name';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($group_names = $mysqli_result->fetch_assoc()) {
        $show_groups[] = $group_names['name'];
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Account->ShowGroups 1: ' . $mysqli->error);
    }
    // Also show groups belonging to other users where this user is a visitor
    // with permission to edit the group.
    $query = 'SELECT user, name FROM group_names WHERE ' .
      'user != "' . $this->user->name . '" AND ' .
      'visitor = "' . $this->user->name . '" ' .
      'AND name NOT LIKE "post-notifications-%" ' .
      'AND name NOT LIKE "comment-notifications-%" ' .
      'AND edit_group = 1 ORDER BY name';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($group_names = $mysqli_result->fetch_assoc()) {
        $other_group = $group_names['user'] . '/' . $group_names['name'];
        if (!in_array($other_group, $show_groups)) {
          $show_groups[] = $other_group;
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Account->ShowGroups 2: ' . $mysqli->error);
    }
    
    $mysqli->close();
    return $show_groups;
  }

  private function ShowUsers() {
    // Only admin accounts can list users.
    if (!$this->GroupMember('admin', 'admin')) return [];

    $result = [];
    $mysqli = connect_db();
    $query = 'SELECT user FROM users';
    // If no site permission only show members of same organisation.
    if (!$this->user->canEditSite) {
      $organiser = new Organiser($this->user, $this->owner);
      $query .= ' WHERE ' . $organiser->GroupQuery();
    }
    if ($mysqli_result = $mysqli->query($query)) {
      while ($users = $mysqli_result->fetch_assoc()) {
        $result[] = $users['user'];
      }
    }
    else {
      $this->Log('Account->ShowUsers: ' . $mysqli->error);
    }
    $mysqli->close();
    return $result;
  }

  private function ShowGroupMembers($group) {
    $owner = $this->user->name;
    // Check if $group belongs to another user.
    preg_match('/^([a-z0-9_-]+)\/([a-z0-9\s_-]+)$/i', $group, $matches);
    if (count($matches) === 3) {
      $owner = $matches[1];
      $group = trim($matches[2]);
    }
    $group_query = '';
    // If no site permission, only show users that are part of the same
    // organisation.
    if (!$this->user->canEditSite) {
      $organiser = new Organiser($this->user, $this->owner);
      $group_query = $organiser->GroupQuery() . ' AND ';
    }
    $user = '';
    $access = '';
    $content = '';
    $edit_group = false;
    $mysqli = connect_db();
    if ($owner !== $this->user->name) {
      $user = $owner . '/';
      // First check that the current user is a member of the group and has
      // permission to edit the group.
      $query = 'SELECT edit_group FROM group_names WHERE ' .
        'user = "' . $owner . '" AND name = "' . $group . '" AND ' .
        'visitor = "' . $this->user->name . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($group_names = $mysqli_result->fetch_assoc()) {
          $edit_group = (int)$group_names['edit_group'] === 1;
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Account->ShowGroupMembers 1: ' . $mysqli->error);
      }
    }
    else {
      // Don't need to be a member of your own group to edit.
      $edit_group = true;
    }
    if ($edit_group) {
      // Display the current pages this group has access to.
      $query = 'SELECT page, edit, copy, view FROM group_permission WHERE ' .
        'user = "' . $owner . '" AND name = "' . $group . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($permission = $mysqli_result->fetch_assoc()) {
          $page = $permission['page'];
          // If page is empty and the current user is admin, it means the
          // group has permission to edit the site so reflect that here.
          if ($page === '' && $owner === 'admin') {
            $access .= '<i>members have site permission</i><br>';
          }
          if ((int)$permission['edit'] === 1 &&
              (int)$permission['copy'] === 1) {
            $access .= $user . $page . ' <i>(copy and edit)</i><br>';
          }
          else if ((int)$permission['edit'] === 1) {
            $access .= $user . $page . ' <i>(edit)</i><br>';
          }
          else if ((int)$permission['copy'] === 1) {
            $access .= $user . $page . ' <i>(copy)</i><br>';
          }
          else if ((int)$permission['view'] === 1) {
            $access .= $user . $page . '<br>';
          }
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Account->ShowGroupMembers 2: ' . $mysqli->error);
      }
      if ($access !== '') {
        $content .= 'This group has access to the following pages:<br>' .
          '<div class="group-page-list">' . $access . '</div><hr>';
      }
      $query = 'SELECT visitor, edit_group FROM group_names LEFT JOIN users ' .
        'ON group_names.visitor = users.user WHERE ' . $group_query .
        'name = "' . $group . '" AND group_names.user = "' . $owner . '" ' .
        'ORDER BY visitor';
      if ($mysqli_result = $mysqli->query($query)) {
        while ($group_names = $mysqli_result->fetch_assoc()) {
          $can_edit = (int)$group_names['edit_group'] === 1 ?
            ' (can edit group)' : '';
          $content .= '<div class="group-member">' .
              '<button class="remove-group-member">remove</button> ' .
              '<span class="visitor">' . $group_names['visitor'] . '</span>' .
              $can_edit . '</div>';
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Account->ShowGroupMembers 3: ' . $mysqli->error);
      }
    }
    $mysqli->close();
    return $content;
  }

  private function AddGroupMember() {
    if ($this->Substitute('account-single-user') === 'true') {
      return ['error' => 'This site is in single user mode.'];
    }

    $result = [];
    $mysqli = connect_db();
    $visitor = $mysqli->escape_string(htmlspecialchars($_POST['visitor']));
    $group = $mysqli->escape_string(strtolower($_POST['group']));
    $edit = (int)$_POST['edit'];

    $owner = $this->user->name;
    $edit_group = false;
    // Check if group belongs to another user.
    preg_match('/^([a-z0-9_-]+)\/([a-z0-9\s_-]+)$/i', $group, $matches);
    if (count($matches) === 3) {
      $owner = $matches[1];
      $group = trim($matches[2]);
    }
    // The group settings wizard has some shortcuts to a few admin groups,
    // allow the user to edit these.
    $invoice_notifications = 'invoice-notifications';
    if ($this->user->group !== '') {
      $invoice_notifications .= '-' . $this->user->group;
    }
    $wizard_groups = [];
    if ($this->GroupMember('admin', 'admin')) {
      $wizard_groups = ['purchase-other-order', 'purchase-volunteer',
                        $invoice_notifications];
    }
    if ($owner === $this->user->name ||
        ($owner === 'admin' && in_array($group, $wizard_groups))) {
      $edit_group = true;
    }
    else {
      // If owner is not the current user, check permission to edit the group.
      $query = 'SELECT edit_group FROM group_names WHERE ' .
        'user = "' . $owner . '" AND name = "' . $group . '" AND ' .
        'visitor = "' . $this->user->name . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($group_names = $mysqli_result->fetch_assoc()) {
          $edit_group = (int)$group_names['edit_group'] === 1;
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Account->AddGroupMember 1: ' . $mysqli->error);
      }
    }

    if ($edit_group) {
      // If no site permission, make sure the user being added is a member
      // of the same organisation.
      $organiser = new Organiser($this->user, $this->owner);
      if ($this->user->canEditSite || $organiser->MatchUser($visitor)) {
        $query = 'INSERT INTO group_names VALUES ("' . $owner . '", ' .
          '"' . $group . '", "' . $visitor . '", ' . $edit . ') ' .
          'ON DUPLICATE KEY UPDATE edit_group = ' . $edit;
        if (!$mysqli->query($query)) {
          $this->Log('Account->AddGroupMember 2: ' . $mysqli->error);
        }
        $result['content'] = $this->ShowGroupMembers($owner . '/' . $group);
        $result['groups'] = $this->ShowGroups();
      }
      else {
        $result['error'] = 'User not found.';
      }
    }
    else {
      $result['error'] = 'You don\'t have permission to edit the group.';
    }

    $mysqli->close();
    return $result;
  }

  private function RemoveGroupMember() {
    $result = [];
    $mysqli = connect_db();
    $visitor = $mysqli->escape_string(htmlspecialchars($_POST['visitor']));
    $group = $mysqli->escape_string(strtolower($_POST['group']));

    $owner = $this->user->name;
    $edit_group = false;
    // Check if group belongs to another user.
    preg_match('/^([a-z0-9_-]+)\/([a-z0-9\s_-]+)$/i', $group, $matches);
    if (count($matches) === 3) {
      $owner = $matches[1];
      $group = trim($matches[2]);
    }

    if ($owner === $this->user->name) {
      $edit_group = true;
    }
    else {
      // If owner is not the current user, check permission to edit the group.
      $query = 'SELECT edit_group FROM group_names WHERE ' .
        'user = "' . $owner . '" AND name = "' . $group . '" AND ' .
        'visitor = "' . $this->user->name . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($group_names = $mysqli_result->fetch_assoc()) {
          $edit_group = (int)$group_names['edit_group'] === 1;
        }
        $mysqli_result->close();
      }
      else {
        $this->Log('Account->RemoveGroupMember 1: ' . $mysqli->error);
      }
    }

    if ($edit_group) {
      // If no site permission, make sure the user being added is a member
      // of the same organisation.
      $organiser = new Organiser($this->user, $this->owner);
      if ($this->user->canEditSite || $organiser->MatchUser($visitor)) {
        $query = 'DELETE FROM group_names WHERE user = "' . $owner . '" ' .
          'AND name = "' . $group . '" AND visitor = "' . $visitor . '"';
        if (!$mysqli->query($query)) {
          $this->Log('Account->RemoveGroupMember 2: ' . $mysqli->error);
        }
        $result['done'] = true;
      }
      else {
        $result['error'] = 'User not found.';
      }
    }
    else {
      $result['error'] = 'You don\'t have permission to edit the group.';
    }

    $mysqli->close();
    return $result;
  }

  private function GroupPermission() {
    $content = '';
    $mysqli = connect_db();
    $query = 'SELECT page, name, edit, copy, view FROM group_permission ' .
      'WHERE user = "' . $this->user->name . '" ORDER BY name';
    if ($mysqli_result = $mysqli->query($query)) {
      $remove = '<button class="remove-group-permission">remove</button>';
      while ($permission = $mysqli_result->fetch_assoc()) {
        $group = '<span class="group">' . $permission['name'] . '</span>';
        // If page is empty and the current user is admin, it means the
        // group has permission to edit the site so reflect that here.
        if ($permission['page'] === '' && $this->user->name === 'admin') {
          $page = 'the site <span class="page"></span>';
        }
        else {
          $page = 'page <span class="page">' . $permission["page"] . '</span>';
        }
        if ((int)$permission['edit'] === 1 && (int)$permission['copy'] === 1) {
          $content .= '<div class="group-permission">' . $remove . $group .
            ' can copy/edit ' . $page . '</div>';
        }
        else if ((int)$permission['edit'] === 1) {
          $content .= '<div class="group-permission">' . $remove . $group .
            ' can edit ' . $page . '</div>';
        }
        else if ((int)$permission['copy'] === 1) {
          $content .= '<div class="group-permission">' . $remove . $group .
            ' can copy ' . $page . '</div>';
        }
        else if ((int)$permission['view'] === 1) {
          $content .= '<div class="group-permission">' . $remove . $group .
            ' can view ' . $page . '</div>';
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Account->GroupPermission: ' . $mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

  private function UserPermission() {
    $content = '';
    $mysqli = connect_db();
    $query = 'SELECT page, visitor, edit, copy, view FROM user_permission ' .
      'WHERE user = "' . $this->user->name . '" ORDER BY visitor';
    if ($mysqli_result = $mysqli->query($query)) {
      $remove = '<button class="remove-user-permission">remove</button>';
      while ($permission = $mysqli_result->fetch_assoc()) {
        // If visitor is empty, it means everyone has permission for the page.
        // (Keep the span empty here too, so that it can be removed later).
        $user = '<span class="visitor"></span><i>everyone</i>';
        if ($permission['visitor'] != '') {
          $user = '<span class="visitor">' . $permission['visitor'] . '</span>';
        }
        // If page is empty and the current user is admin, it means the
        // user has permission to edit the site so reflect that here.
        if ($permission['page'] === '' && $this->user->name === 'admin') {
          $page = 'the site <span class="page"></span>';
        }
        else {
          $page = 'page <span class="page">' . $permission['page'] . '</span>';
        }

        if ((int)$permission['edit'] === 1 && (int)$permission['copy'] === 1) {
          $content .= '<div class="user-permission">' . $remove . $user .
            ' can copy/edit ' . $page . '</div>';
        }
        else if ((int)$permission['edit'] === 1) {
          $content .= '<div class="user-permission">' . $remove . $user .
            ' can edit ' . $page . '</div>';
        }
        else if ((int)$permission['copy'] === 1) {
          $content .= '<div class="user-permission">' . $remove . $user .
            ' can copy ' . $page . '</div>';
        }
        else if ((int)$permission['view'] === 1) {
          $content .= '<div class="user-permission">' . $remove . $user .
            ' can view ' . $page . '</div>';
        }
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Account->UserPermission: ' . $mysqli->error);
    }
    $mysqli->close();
    return $content;
  }

  private function Preferences() {
    $mysqli = connect_db();
    $email = '';
    $query = 'SELECT email FROM users WHERE user = "' . $this->user->name . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if (($users = $mysqli_result->fetch_assoc()) && $users['email'] !== '') {
        $email = '<i>Your current email is: ' .
          htmlspecialchars($users['email']) . '</i>';
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Account->Preferences: ' . $mysqli->error);
    }
    $mysqli->close();

    $account_settings = '<h4>Change your password:</h4>' .
      '<p id="change-password-form-message"></p>' .
      '<form id="change-password-form" autocomplete="off">' .
        '<div class="form-spacing">' .
          '<label for="change-password-old">Old password:</label>' .
          '<input id="change-password-old" class="control-input" ' .
            'type="password" maxlength="200">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="change-password-new">New password:</label>' .
          '<input id="change-password-new" class="control-input" ' .
            'type="password" maxlength="200">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="change-password-rpt">Repeat new password:</label>' .
          '<input id="change-password-rpt" class="control-input" ' .
            'type="password" maxlength="200">' .
        '</div>' .
        '<button class="submit">submit</button>' .
      '</form>' .
      '<h4>Change your email:</h4>' .
      '<p id="change-email-form-message">' . $email . '</p>' .
      '<form id="change-email-form" autocomplete="off">' .
        '<div class="form-spacing">' .
          '<label for="change-email-input">New email:</label>' .
          '<input id="change-email-input" class="control-input" ' .
            'type="text" maxlength="200">' .
        '</div>' .
        '<div class="form-spacing">' .
          '<label for="change-email-password-input">Password:</label>' .
          '<input id="change-email-password-input" class="control-input" ' .
            'type="password" maxlength="200">' .
        '</div>' .
        '<button class="submit">submit</button>' .
      '</form>';

    if ($this->Substitute('account-single-user') === 'true') {
      return $account_settings;
    }

    return '<div id="account-tabs">' .
      '<ul>' .
        '<li><a href="#account-settings">Settings</a></li>' .
        '<li><a href="#account-groups">Groups</a></li>' .
        '<li><a href="#account-permission">Permission</a></li>' .
      '</ul>' .
      '<div id="account-settings">' . $account_settings . '</div>' .
      '<div id="account-groups">' .
        '<div class="form-spacing">' .
          '<label title="loading..." for="group-input">Group:</label>' .
          '<input id="group-input" type="text">' .
          '<button class="show-members">show members</button>' .
        '</div>' .
        '<div class="group-user-list"></div>' .
        '<form id="user-group-form" autocomplete="off">' .
          '<div class="form-spacing">' .
            '<label for="user-group-input">Add user to group:</label>' .
            '<input id="user-group-input" type="text">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="edit-group-input">User can edit group:</label>' .
            '<input id="edit-group-input" type="checkbox">' .
          '</div>' .
          '<button class="submit">submit</button>' .
        '</form>' .
      '</div>' .
      '<div id="account-permission">' .
        '<h3>User permission:</h3>' .
        '<div class="user-permission-list">' .
          $this->UserPermission() .
        '</div>' .
        '<form id="user-permission-form" autocomplete="off">' .
          'Add user permission:' .
          '<div class="form-spacing">' .
            '<label for="user-permission-input">User:</label>' .
            '<input id="user-permission-input" type="text">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="user-page-input">Page:</label>' .
            '<input id="user-page-input" type="text">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="user-edit-input">Can edit:</label>' .
            '<input id="user-edit-input" type="checkbox">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="user-copy-input">Can copy:</label>' .
            '<input id="user-copy-input" type="checkbox"> ' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="user-view-input">Can view:</label>' .
            '<input id="user-view-input" type="checkbox">' .
          '</div>' .
          '<button class="submit">submit</button>' .
        '</form>' .
        '<h3>Group permission:</h3>' .
        '<div class="group-permission-list">' .
          $this->GroupPermission() .
        '</div>' .
        '<form id="group-permission-form" autocomplete="off">' .
          'Add group permission:' .
          '<div class="form-spacing">' .
            '<label for="group-permission-input">Group:</label>' .
            '<input id="group-permission-input" type="text">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="group-page-input">Page:</label>' .
            '<input id="group-page-input" type="text">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="group-edit-input">Can edit:</label>' .
            '<input id="group-edit-input" type="checkbox">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="group-copy-input">Can copy:</label>' .
            '<input id="group-copy-input" type="checkbox"> ' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="group-view-input">Can view:</label>' .
            '<input id="group-view-input" type="checkbox">' .
          '</div>' .
          '<button class="submit">submit</button>' .
        '</form>' .
      '</div>' .
      '</div>';
  }

  private function RegisterForm($account_type) {
    $group_content = '';
    // Site permission means the new user can be added to any group,
    // otherwise show the list of available groups (if any). Default to the
    // current user's group.
    if ($this->user->canEditSite) {
      $group_content = '<div class="form-spacing">' .
          '<label for="system-group-input">Group:</label>' .
          '<input id="system-group-input" value="' . $this->user->group . '" ' .
            'type="text" maxlength="50">' .
        '</div>';
    }
    else {
      $organiser = new Organiser($this->user, $this->owner);
      $siblings = $organiser->Siblings();
      // Don't show group options if there is only one group, because users
      // will get the the current user's group as a default anyway.
      if (count($siblings) > 1) {
        $options = '';
        for ($i = 0; $i < count($siblings); $i++) {
          $group = $siblings[$i];
          $selected = "";
          // Set the default group for the new user to be the same as the
          // current user.
          if ($group === $this->user->group) {
            $selected = ' selected="selected"';
          }
          $options .= '<option value="' . $group . '"' . $selected . '>' .
            ucfirst($group) . '</option>';
        }
        $group_content = '<div class="form-spacing">' .
            '<label for="system-group-input">Group:</label>' .
            '<select id="system-group-input">' . $options . '</select>' .
          '</div>';
      }
    }

    // Set extra details for the new user if the detail module is installed.
    $detail_content = '';
    if ($account_type === 'user') {
      $detail = new Module($this->user, $this->owner, 'detail');
      if ($detail->IsInstalled()) {
        $detail_content = '<div class="form-spacing">' .
            '<label for="new-details-first-input">First Name:</label>' .
            '<input id="new-details-first-input" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="new-details-last-input">Last Name:</label>' .
            '<input id="new-details-last-input" type="text" maxlength="50">' .
          '</div>';
        $detail_content .= '<div class="form-spacing">' .
            '<label for="new-details-phone-input">Phone Number:</label>' .
            '<input id="new-details-phone-input" type="text" maxlength="50">' .
          '</div>' .
          '<div class="form-spacing">' .
            '<label for="new-details-address-textarea">Address:</label>' .
            '<textarea id="new-details-address-textarea"></textarea>' .
          '</div>';
        $label = $this->Substitute('detail-description-label');
        if ($label !== '') {
          $detail_content .= '<div class="form-spacing">' .
              '<label for="new-details-description-textarea">' .
                $label . ':</label>' .
              '<textarea id="new-details-description-textarea"></textarea>' .
            '</div>';
        }
        if ($this->Substitute('display-profile-checkbox') === 'true') {
          $detail_content .= '<div class="form-spacing">' .
              '<label for="new-details-display-input">Display Profile:</label>'.
              '<input id="new-details-display-input" type="checkbox" ' .
                'checked="checked">' .
            '</div>';
        }
        if ($this->Substitute('display-membership-reminder') === 'true') {
          $detail_content .= 'Add the membership renewal date below.' .
            '<div class="form-spacing">' .
              '<label for="new-details-reminder-time">Renewal Date:</label>' .
              '<input id="new-details-reminder-time" type="text">' .
            '</div>' .
            '<div class="form-spacing">' .
              '<label for="new-details-reminder-repeat">Repeat every:</label>' .
              '<select id="new-details-reminder-repeat">' .
                '<option>6 months</option>' .
                '<option>12 months</option>' .
              '</select>' .
            '</div>';
        }
      }
    }

    $info = '';
    $account_label = 'Username';
    $active_hidden = ' hidden';
    $toggle_password = '';
    $email_hidden = '';
    $stock_content = '';
    if ($account_type === 'user') {
      $active_hidden = '';
      $toggle_password =
        '<button id="account-set-password">set password</button>';
      $email_check = $this->Substitute('account-email-check');
      if ($email_check !== '') {
        $email_check = '<a id="account-email-info" href="">' .
          'What email accounts can I use?</a> ' .
          '<span class="account-email-check hidden">' . $email_check .'</span>';
      }
      $info = '<p>To add a new user, choose a username and provide their ' .
          'email address, and they will be sent an email to log in.</p>'  .
        '<p>You can also modify existing accounts by providing a username. ' .
          'The account details will be loaded into the form when you press ' .
          'the enter or tab key, click the submit button at the bottom of ' .
          'the form to save.</p>' . $email_check;
    }
    else if ($account_type === 'supplier') {
      $account_label = 'Supplier';
      $email_hidden = ' hidden';
      $info = '<p>To add a new supplier, enter a descriptive name for the ' .
          'account.</p>' .
        '<p>(This form will always create a new account. If you would like ' .
          'to modify the details for an existing supplier, please use ' .
          'Manage Accounts, or add the External Supplier option on the ' .
          'accounts page.)</p>';
    }
    else if ($account_type === 'guest') {
      $info = 'To register, choose a username and provide your email address, '.
        'and you will be sent a confirmation link.';
    }
    if ($this->Substitute('stock-group-select') === 'true') {
      $stock_content = '<div class="stock-supply-options"></div>';
    }

    return '<p>' . $info . '</p>' .
      '<form id="register-form" autocomplete="off">' .
        '<div class="form-spacing">' .
          '<label for="new-user-input">' . $account_label . ':</label>' .
          '<input id="new-user-input" type="text" maxlength="50">' .
        '</div>' .
        '<div class="username-info"></div>' .
        '<div class="new-user-confirm hidden">' .
          '<div class="info">This account hasn\'t been confirmed yet. Once ' .
            'you confirm the account they can log in.</div>' .
          '<div class="form-spacing">' .
            '<label for="new-user-confirm-input">Confirm:</label>' .
            '<input id="new-user-confirm-input" type="checkbox">' .
          '</div>' .
        '</div>' .
        $toggle_password .
        '<div class="form-spacing new-password-wrapper hidden">' .
          '<label for="new-password-input">Password:</label>' .
          '<input id="new-password-input" type="password" maxlength="100">' .
          '<br><span class="new-password-info">' .
            '(If you provide a password it will not be emailed to the user.)' .
          '</span>' .
        '</div>' .
        '<div class="form-spacing' . $email_hidden . '">' .
          '<label for="email-input">Email:</label>' .
          '<input id="email-input" type="text" maxlength="100">' .
        '</div>' .
        $group_content .
        '<div class="form-spacing' . $active_hidden . '">' .
          '<label for="active-input">Active:</label>' .
          '<input id="active-input" type="checkbox" checked="checked">' .
        '</div>' .
        $detail_content .
        '<button class="submit">submit</button>' .
        $stock_content .
      '</form>';
  }

  private function RemoveContents($remove) {
    $mysqli = connect_db();
    $query = 'SELECT DISTINCT label FROM modules';
    if ($mysqli_result = $mysqli->query($query)) {
      while ($modules = $mysqli_result->fetch_assoc()) {
        // owner must be set to the removed user when calling Remove.
        $module = new Module($this->user, $remove, $modules['label']);
        $module->Remove(NULL);
      }
      $mysqli_result->close();
    }
    else {
      $this->Log('Account->RemoveContents 1: ' . $mysqli->error);
    }
    $query = 'DELETE FROM modules WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 2: ' . $mysqli->error);
    }
    $query = 'DELETE FROM modules_history WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 3: ' . $mysqli->error);
    }
    $query = 'DELETE FROM box_style WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 4: ' . $mysqli->error);
    }
    $query = 'DELETE FROM page_style WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 5: ' . $mysqli->error);
    }
    $query = 'DELETE FROM user_permission WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 6: ' . $mysqli->error);
    }
    $query = 'DELETE FROM user_permission WHERE visitor = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 7: ' . $mysqli->error);
    }
    $query = 'DELETE FROM group_permission WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 8: ' . $mysqli->error);
    }
    $query = 'DELETE FROM group_names WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 9: ' . $mysqli->error);
    }
    $query = 'DELETE FROM group_names WHERE visitor = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 10: ' . $mysqli->error);
    }
    $query = 'DELETE FROM published WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 11: ' . $mysqli->error);
    }
    $query = 'DELETE FROM api WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 12: ' . $mysqli->error);
    }
    $query = 'DELETE FROM notify WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 13: ' . $mysqli->error);
    }
    $query = 'DELETE FROM page_updates WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 14: ' . $mysqli->error);
    }
    $query = 'DELETE FROM config WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 15: ' . $mysqli->error);
    }
    $query = 'DELETE FROM session WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 16: ' . $mysqli->error);
    }
    $query = 'DELETE FROM settings WHERE user = "' . $remove . '"';
    if (!$mysqli->query($query)) {
      $this->Log('Account->RemoveContents 17: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  private function RemoveDir($dir) {
    // First remove all the files in the directory.
    if ($handle = opendir($dir)) {
      while (($file = readdir($handle)) !== false) {
        if (is_file($dir . '/' . $file)) {
          unlink($dir . '/' . $file);
        }
      }
      closedir($handle);
    }
    // Lastly remove the directory itself.
    rmdir($dir);
  }

}
