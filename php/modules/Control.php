<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Control extends Base {

  public function Add($id) {

  }

  public function Callback() {
    $location = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
      'https://' : 'http://';
    $location .= $_SERVER['SERVER_NAME'].
      $this->Url('', $this->Substitute('indieauth-page'), 'admin');
    return ['location' => $location];
  }

  public function CanAdd($page) {
    return false;
  }

  public function CanEdit($id) {
    return false;
  }

  public function CanRemove($id) {
    return false;
  }

  public function Content($id) {
    // Only display the name 'guest' if this is a guest account.
    $user = preg_match('/^guest[0-9]{10}[a-z0-9]{5}$/', $this->user->name) ?
      'guest' : $this->user->name;

    $admin = $this->GroupMember('admin', 'admin');
    // The menus are added to the end of the control, so that they're not
    // affected by control's css styles. (They are positioned in javascript)
    $account_menu = '<div class="account-menu-wrapper menu-wrapper hidden">' .
        '<div class="arrow-border"></div>' .
        '<div class="arrow"></div>' .
        '<ul class="menu">';
    if ($user === 'guest') {
      $account_menu .= '<li id="register"><div>Register...</div></li>';
    }
    else {
      if ($admin) {
        $account_menu .= '<li id="register"><div>Manage Accounts...</div></li>';
        if ($this->user->canEditSite) {
          $account_menu .=
            '<li id="remove-user"><div>Remove User...</div></li>';
        }
      }
      $account_menu .= '<li id="preferences"><div>Preferences...</div></li>';
    }
    // The Start module is only available to some users, so don't display a
    // menu option if it can't be added to the page.
    $start = new Module($this->user, $this->owner, 'start');
    if ($start->CanAdd($this->user->page)) {
      $account_menu .= '<li id="add-start"><div>Help</div></li>';
    }
    $account_menu .= '<li id="logout"><div>Logout</div></li>' .
        '</ul>' .
      '</div>';

    $message_menu = '<div class="message-count hidden"></div>' .
      '<div class="message-menu-wrapper menu-wrapper hidden">' .
        '<div class="arrow-border"></div>' .
        '<div class="arrow"></div>' .
        '<ul class="menu"><li><div>Messages</div></li></ul>' .
      '</div>';

    $notification_menu = '<div class="notification-count hidden"></div>' .
      '<div class="notification-menu-wrapper menu-wrapper hidden">' .
        '<div class="arrow-border"></div>' .
        '<div class="arrow"></div>' .
        '<ul class="menu"></ul>' .
      '</div>';

    $add_menu = '<div class="add-menu-wrapper menu-wrapper hidden">' .
        '<div class="arrow-border"></div>' .
        '<div class="arrow"></div>' .
        '<ul class="menu">' . $this->AddMenu($admin) . '</ul>' .
      '</div>';

    $copy_menu = '<div class="copy-menu-wrapper menu-wrapper hidden">' .
        '<div class="arrow-border"></div>' .
        '<div class="arrow"></div>' .
        '<form id="control-copy-form" ' .
          'class="ui-widget ui-widget-content ui-corner-all">' .
          '<label for="copy-input">Copy To:</label>' .
          '<input id="copy-input" class="control-input" type="text" ' .
            'name="page" value="new page..." maxlength="200">' .
        '</form>' .
      '</div>';

    $home = $this->user->group === $this->Substitute('indieauth-group') ?
      '<div class="control-button indieauth-home" title="home">home</div>' :
      '<div class="control-button home" title="home">home</div>';
    // Note that class names get repeated on inputs and labels because
    // jquery-ui uses the label information for buttons.
    $content = '<div class="wrapper ui-widget-header">' . $home .
        '<div class="control-button account-button" title="account">' .
          'account</div>' .
        '<div class="control-button message-button" title="messages">' .
          'messages</div>' .
        '<div class="control-button notification-button" ' .
          'title="notifications">notifications</div>' .
        '<input type="checkbox" id="control-tools" ' .
          'class="control-button tools" title="tools">' .
        '<label for="control-tools" class="control-button tools">' .
          '<span class="ui-icon ui-icon-wrench"></span>' .
        '</label>' .
        '<div class="toolbar hidden">' .
          '<div class="control-button site" title="site">site</div>';

    if ($this->user->canEditPage) {
      $content .= '<div class="control-button page" title="page">page</div>' .
        '<div class="control-button add" title="add">add</div>' .
        '<input type="checkbox" id="control-edit" class="control-button edit" '.
          'title="edit">' .
        '<label for="control-edit" class="control-button edit">' .
          '<span class="ui-icon ui-icon-pencil"></span>' .
        '</label>' .
        '<input type="checkbox" id="control-layout" ' .
          'class="control-button layout" title="layout">' .
        '<label for="control-layout" class="control-button layout">' .
          '<span class="ui-icon ui-icon-arrow-4-diag"></span>' .
        '</label>';
    }
    if ($this->user->canCopyPage) {
      $content .= '<div class="control-button copy" title="copy">copy</div>';
    }
    $display_username = '';
    if ($this->Substitute('account-single-user') !== 'true') {
      $display_username = '<div class="username">' . $user . '</div>';
    }
    // Next close the div with class 'toolbar'.
    $content .= '</div>' .
      '<div class="info ui-state-highlight ui-corner-all hidden">' .
        '<span class="ui-icon ui-icon-info"></span>' .
        '<span class="message">info</span>' .
      '</div>' .
      '<div class="error ui-state-error ui-corner-all hidden">' .
        '<span class="ui-icon ui-icon-alert"></span>' .
        '<span class="message">error</span>' .
      '</div>' .
      $this->PageForm() .
      $display_username .
      '</div>' . // This closes the opening div with class 'wrapper'.
      $account_menu . $message_menu . $notification_menu . $add_menu .
      $copy_menu .
      '<div class="notification-dialog hidden">' .
        '<div id="notification-tabs">' .
          '<ul>' .
            '<li><a href="#notification-history">History</a></li>' .
            '<li><a href="#notification-settings">Settings</a></li>' .
          '</ul>' .
          '<div id="notification-history">' .
            '<div class="list"></div>' .
            '<button class="more-notifications hidden">Show more</button>' .
          '</div>' .
          '<div id="notification-settings"></div>' .
        '</div>' .
      '</div>';
    return $content;
  }

  public function Copy($id, $new_page, $old_owner, $old_id) {

  }

  public function Cron() {
    if (!$this->Run($this->Substitute('control-cron'))) return;

    $description = 'Could not create backup file.';
    if ($backup = $this->Backup()) $description = 'Backup created: ' . $backup;
    $this->Notification('control', 'Control', $description, 'system');
  }

  public function Factory($fn, $p = NULL) {
    
  }

  public function Group() {

  }

  public function IncludeScript() {
    return true;
  }

  public function Install($path) {
    // Append dobrado.control.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.control.js', false);
    // The control is added for the 'admin' user here, for all other
    // users it is added when their account is created.
    $this->AddToAdmin('control');

    $mysqli = connect_db();
    $query = 'INSERT INTO group_names VALUES ("' . $this->owner . '", ' .
      '"control-notifications", "' . $this->owner . '", 1)';
    if (!$mysqli->query($query)) {
      $this->Log('Cron->Install: ' . $mysqli->error);
    }
    $mysqli->close();

    $template = ['"control-menu","","simple,more"',
                 '"control-admin-menu","","simple,more"'];
    $this->AddTemplate($template);
    $description = ['control-menu' => 'A comma separated list of module ' .
                      'names to display in the add menu for users who ' .
                      'aren\'t in the admin group.',
                    'control-admin-menu' => 'A comma separated list of ' .
                      'module names to display in the add menu for users who ' .
                      'are in the admin group.',
                    'control-page-options' => 'A comma separated list of ' .
                      'page names used to create a navigation menu.',
                    'control-cron' => 'A relative day and time for when to ' .
                      'run cron.',
                    'backup-path' => 'The file system path for storing ' .
                      'backups.'];
    $this->AddTemplateDescription($description);

    // This is a default media query for mobile devices.
    $media = '@media screen and (max-device-width: 480px)';
    // Add style rules to the site_style table.
    $site_style = ['"",".control","position","sticky"',
                   '"",".control","width","100%"',
                   '"",".control","top","0"',
                   '"",".control","z-index","98"',
                   '"",".control","height","48px"',
                   '"",".control","display","none"',
                   '"",".control .wrapper","height","38px"',
                   '"",".control .wrapper","padding-top","8px"',
                   '"",".control-button","margin-left","7px"',
                   '"",".control-button","float","left"',
                   '"",".control-button","font-size","15px"',
                   '"","label.control-button","margin-top","0"',
                   '"",".arrow","border-style","solid"',
                   '"",".arrow","border-width","10px"',
                   '"",".arrow","height","0px"',
                   '"",".arrow","width","0px"',
                   '"",".arrow","position","absolute"',
                   '"",".arrow","left","10px"',
                   '"",".arrow","top","1px"',
                   '"",".arrow-border","border-style","solid"',
                   '"",".arrow-border","border-width","10px"',
                   '"",".arrow-border","height","0px"',
                   '"",".arrow-border","width","0px"',
                   '"",".arrow-border","margin-left","10px"',
                   '"' . $media . '","#control-layout","display","none"',
                   '"' . $media . '","label[for=control-layout]","display",' .
                     '"none"',
                   '"","#control-page-form","float","right"',
                   '"","#control-page-form","margin","0 7px 2px 2px"',
                   '"","#page-input","height","20px"',
                   '"","#page-input","width","200px"',
                   '"' . $media . '","#page-input","width","100px"',
                   '"","#page-select","width","150px"',
                   '"' . $media . '","#page-select","width","100px"',
                   '"","#reset-page-select","margin-left","2px"',
                   '"","#control-page-form .ui-selectmenu-button","width",' .
                     '"150px"',
                   '"","#control-page-form .ui-selectmenu-button",' .
                     '"padding-top","5px"',
                   '"","#control-page-form .ui-selectmenu-button",' .
                     '"padding-bottom","5px"',
                   '"' . $media . '","#control-page-form ' .
                     '.ui-selectmenu-button","width","100px"',
                   '"' . $media . '","#control-page-form ' .
                     '.ui-selectmenu-button","padding-top","3px"',
                   '"' . $media . '","#control-page-form ' .
                     '.ui-selectmenu-button","padding-bottom","3px"',
                   '"","#control-page-form .ui-selectmenu-icon","top","3px"',
                   '"",".control-input","color","#828282"',
                   '"",".message-count","position","absolute"',
                   '"",".message-count","top","10px"',
                   '"",".message-count","left","230px"',
                   '"' . $media . '",".message-count","left","104px"',
                   '"",".message-count","background-color","#dd0000"',
                   '"",".message-count","border","1px solid #ffffff"',
                   '"",".message-count","border-radius","4px"',
                   '"",".message-count","color","#ffffff"',
                   '"",".message-count","font-size","10px"',
                   '"",".message-count","opacity","0.7"',
                   '"",".message-count","padding","1px 2px"',
                   '"",".message-count","text-align","center"',
                   '"",".notification-count","position","absolute"',
                   '"",".notification-count","top","10px"',
                   '"",".notification-count","left","356px"',
                   '"' . $media . '",".notification-count","left","142px"',
                   '"",".notification-count","background-color","#dd0000"',
                   '"",".notification-count","border","1px solid #ffffff"',
                   '"",".notification-count","border-radius","4px"',
                   '"",".notification-count","color","#ffffff"',
                   '"",".notification-count","font-size","10px"',
                   '"",".notification-count","opacity","0.7"',
                   '"",".notification-count","padding","1px 2px"',
                   '"",".notification-count","text-align","center"',
                   '"",".menu-wrapper","position","absolute"',
                   '"",".menu-wrapper","top","28px"',
                   '"",".menu-wrapper","z-index","1"',
                   '"",".menu-wrapper ul","max-height","400px"',
                   '"",".menu-wrapper ul","overflow-x","hidden"',
                   '"",".menu-wrapper ul","overflow-y","auto"',
                   '"",".menu-wrapper ul","padding","1px"',
                   '"",".account-menu-wrapper","left","109px"',
                   '"' . $media . '",".account-menu-wrapper","left","45px"',
                   '"",".message-menu-wrapper","left","221px"',
                   '"' . $media . '",".message-menu-wrapper","left","85px"',
                   '"",".notification-menu-wrapper","left","352px"',
                   '"",".notification-menu-wrapper","width","350px"',
                   '"' . $media.'",".notification-menu-wrapper","left","126px"',
                   '"", ".notification-menu-wrapper .thumb", "width", "20px"',
                   '"", ".notification-menu-wrapper .thumb", ' .
                     '"border-radius", "2px"',
                   '"", ".notification-menu-wrapper a", ' .
                     '"text-decoration", "none"',
                   '"",".add-menu-wrapper","left","280px"',
                   '"' . $media . '",".add-menu-wrapper","left","125px"',
                   '"",".copy-menu-wrapper","left","545px"',
                   '"' . $media . '",".copy-menu-wrapper","left","247px"',
                   '"","#control-copy-form","width","260px"',
                   '"","#control-copy-form","padding","8px"',
                   '"' . $media . '","#control-copy-form","left","-223px"',
                   '"' . $media .'","#control-copy-form","position","absolute"',
                   '"","label[for=copy-input]","margin-top","3px"',
                   '"","label[for=copy-input]","margin-right","5px"',
                   '"","#copy-input","width","150px"',
                   '"",".control .username","float","right"',
                   '"",".control .username","font-family","Helvetica"',
                   '"",".control .username","margin","6px 10px"',
                   '"' . $media . '",".control .username","display","none"',
                   '"",".control .info","position","absolute"',
                   '"",".control .info","max-width","20em"',
                   '"",".control .info","padding","6px"',
                   '"",".control .info","z-index","1"',
                   '"",".control .info .message","padding-left","0.2em"',
                   '"",".control .info .message","font-family","Helvetica"',
                   '"",".control .error .message","font-family","Helvetica"',
                   '"",".control .error","position","absolute"',
                   '"",".control .error","max-width","20em"',
                   '"",".control .error","padding","6px"',
                   '"",".control .error","z-index","1"',
                   '"",".control .error .message","padding-left","0.2em"'];
    $this->AddSiteStyle($site_style);
    $this->AddSettingTypes(['"control","position","scroll,fixed","radio",' .
        '"The control bar can be fixed to the top of the browser window so ' .
        'that it\'s always visible, or it can scroll with the page."',
      '"control","displayMessageButton","display,hidden","radio",' .
        '"The message button in the control bar is not currently used, so ' .
        'this option can be set to \'hidden\'."',
      '"control","displayToolsButton","display,hidden","radio",' .
        '"The tools button in the control bar provides access to admin and ' .
        'editing tools. Unless you need them this option can be set to ' .
        '\'hidden\'."']);
  }

  public function Placement() {
    return 'outside';
  }

  public function Publish($id, $update) {

  }

  public function Remove($id) {

  }

  public function SetContent($id, $us_content) {

  }

  public function Update() {

  }

  public function UpdateScript($path) {
    // Append dobrado.control.js to the existing dobrado.js file.
    // Note that this module is only available when logged in.
    $this->AppendScript($path, 'dobrado.control.js', false);
  }

  // Public functions that aren't part of interface here /////////////////////

  // Private functions below here ////////////////////////////////////////////

  private function AddMenu($admin) {
    $labels = [];
    $titles = [];
    if ($admin) {
      $labels = explode(',', $this->Substitute('control-admin-menu'));
    }
    else {
      $labels = explode(',', $this->Substitute('control-menu'));
    }
    if ($labels[0] === '') {
      return '<li id="simple">Text Area</li>';
    }

    $mysqli = connect_db();
    $label_query = '';
    for ($i = 0; $i < count($labels); $i++) {
      if ($label_query !== '') {
        $label_query .= ' OR ';
      }
      $label_query .= 'label = "' . $labels[$i] . '"';
    }
    $query = 'SELECT label, title FROM installed_modules WHERE ' . $label_query;
    if ($result = $mysqli->query($query)) {
      while ($installed_modules = $result->fetch_assoc()) {
        if ($installed_modules['title'] === '') {
          $titles[$installed_modules['label']] = $installed_modules['label'];
        }
        else {
          $titles[$installed_modules['label']] = $installed_modules['title'];
        }
      }
      $result->close();
    }
    else {
      $this->Log('Control->AddMenu: ' . $mysqli->error);
    }
    $mysqli->close();

    $menu = '';
    for ($i = 0; $i < count($labels); $i++) {
      if (isset($titles[$labels[$i]])) {
        $menu .= '<li id="' . $labels[$i] . '"><div>' .
          $titles[$labels[$i]] . '</div></li>';
      }
    }
    return $menu;
  }

  private function Backup() {
    // Need to include database settings file here to create backup,
    // for correct path can assume call was from php directory.
    include 'functions/db_config.php';

    $path = $this->Substitute('backup-path');

    // First check if a backup was already created today.
    $date = date('Y-m-d');
    $file = $db_name . '-backup-' . $date . '.sql.gz';
    if (file_exists($path . $file)) return $file;

    exec('mysqldump -u ' . $db_user . ' -h ' . $db_server .
         ' -p' . $db_password . ' ' . $db_name . ' | gzip > ' . $path . $file);
    if (file_exists($path . $file)) return $file;
    return false;
  }

  private function PageForm() {
    // Check if there's a reader module on the page and that the user has
    // selected to show channels, otherwise page options are shown.
    $show_channels = isset($this->user->settings['reader']['showChannels']) ?
      $this->user->settings['reader']['showChannels'] === 'yes' : false;
    if ($show_channels && $this->AlreadyOnPage('reader')) {
      $reader = new Module($this->user, $this->owner, 'reader');
      $options = $reader->Factory('Channels');
      if ($options !== '') {
        $page_form = '<form id="control-page-form" autocomplete="off">' .
          '<select id="page-select">' . $options . '</select>';
        if ($this->user->group !== $this->Substitute('indieauth-group')) {
          $page_form .= '<span id="page-input-wrapper">' .
              '<input id="page-input" class="control-input hidden" ' .
                'type="text" name="page" value="change page..." ' .
                'maxlength="200">' .
              '<button id="reset-page-select" class="hidden">reset</button>' .
            '</span>';
        }
        return $page_form . '</form>';
      }
    }

    // Use a session array to avoid checking permissions on every page load.
    if (!isset($_SESSION['control-page-options'])) {
      $_SESSION['control-page-options'] = [];
      $page_options = explode(',', $this->Substitute('control-page-options'));
      foreach ($page_options as $page) {
        $page = trim($page);
        if ($page === '') continue;

        if (can_view_page('admin/' . $page)) {
          $_SESSION['control-page-options'][] = $page;
        }
      }
    }
    $options = '';
    $selected = false;
    foreach ($_SESSION['control-page-options'] as $page) {
      if ($this->owner === 'admin' && $this->user->page === $page) {
        $options .= '<option value="admin/' . $page . '" ' .
          'selected="selected">' . $page . '</option>';
        $selected = true;
      }
      else {
        $options .= '<option value="admin/' . $page . '">' . $page .
          '</option>';
      }
    }
    if ($options !== '') {
      if (!$selected) {
        // Show the current page at the top of the select for display purposes.
        $options = '<option value="" selected="selected">' . $this->user->page .
          '</option>' . $options;
      }
      // TODO: Indieauth login currently doesn't allow search via page-input.
      if ($this->user->group === $this->Substitute('indieauth-group')) {
        return '<form id="control-page-form" autocomplete="off">' .
          '<select id="page-select">' . $options . '</select></form>';
      }

      // Add an option so that page-input can be displayed instead of select.
      $options .= '<option value="">other...</option>';
      // When input is toggled allow resetting to select menu by adding a reset
      // button with the page input.
      return '<form id="control-page-form" autocomplete="off">' .
        '<select id="page-select">' . $options . '</select>' .
        '<span id="page-input-wrapper">' .
          '<input id="page-input" class="control-input hidden" ' .
            'type="text" name="page" value="change page..." maxlength="200">' .
          '<button id="reset-page-select" class="hidden">reset</button>' .
        '</span>' .
      '</form>';
    }

    // If using indieauth login don't display page-input when there are no
    // options available.
    if ($this->user->group === $this->Substitute('indieauth-group')) return '';
    return '<form id="control-page-form">' .
        '<input id="page-input" class="control-input" ' .
          'type="text" name="page" value="change page..." maxlength="200">' .
      '</form>';
  }

}
