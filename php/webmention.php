<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/copy_page.php';
include 'functions/db.php';
include 'functions/microformats.php';
include 'functions/new_module.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/style.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

$us_source = isset($_POST['source']) ? $_POST['source'] : '';
$us_target = isset($_POST['target']) ? $_POST['target'] : '';

// The source url must be fully qualified.
if (strpos($us_source, 'http') !== 0) {
  header('HTTP/1.1 400 Bad Request');
  echo 'Source URL not found.';
  exit;
}

// The target can be a relative url, so only exit here if not provided.
if ($us_target === '') {
  header('HTTP/1.1 400 Bad Request');
  echo 'Target URL not found.';
  exit;
}

$user = new User();
$post = new Module($user, 'admin', 'post');
if (!$post->IsInstalled()) {
  header('HTTP/1.1 500 Internal Server Error');
  exit;
}

list($header, $content) = $post->Factory('ReceiveWebmention',
                                         [$us_source, $us_target]);
header($header);
echo $content;
