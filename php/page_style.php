<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

foreach (['style', 'media', 'url'] as $name) {
  if (!isset($_POST[$name])) {
    echo json_encode(['error' => $name.' not provided']);
    exit;
  }
}

include 'functions/db.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/update_page_style.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

$mysqli = connect_db();

$url = $mysqli->escape_string($_POST['url']);
list($page, $owner) = page_owner($url);

$user = new User();
$user->SetPermission($page, $owner);
if (!$user->canEditPage) {
  echo json_encode(['error' => 'Permission denied editing page style.']);
  $mysqli->close();
  exit;
}

// The posted style is used sql unsafe here.
$us_style = json_decode($_POST['style'], true);
$media = $mysqli->escape_string($_POST['media']);
$update_all_pages = false;
if ($user->canEditSite && isset($_POST['updateAllPages'])) {
  $update_all_pages = (int)$_POST['updateAllPages'] === 1;
}
$all_pages = $update_all_pages ? all_pages() : [];

foreach ($us_style as $us_selector => $us_rules) {
  if (!is_array($us_rules)) continue;
  $selector = $mysqli->escape_string($us_selector);
  foreach ($us_rules as $us_property => $us_value) {
    $property = $mysqli->escape_string($us_property);
    // Ignore the default 'property' value.
    if ($property === 'property') continue;
    $value = $mysqli->escape_string($us_value);
    // If value is empty, the rule should be removed.
    if ($value === '') {
      $page_query = '';
      if (!$update_all_pages) {
        $page_query = 'user = "'.$owner.'" AND name = "'.$page.'" AND ';
      }
      $query = 'DELETE FROM page_style WHERE '.$page_query.
        'media = "'.$media.'" AND selector = "'.$selector.'" AND '.
        'property = "'.$property.'"';
      if (!$mysqli->query($query)) {
        log_db('page_style 1: '.$mysqli->error, $owner, $user->name, $page);
      }
      continue;
    }
    // If value contains 'javascript' or 'expression', ignore it.
    if (strpos($value, 'javascript') !== false ||
        strpos($value, 'expression') !== false) {
      continue;
    }
    if ($update_all_pages) {
      $all_values = page_style_values($all_pages, $media, $selector,
                                      $property, $value);
      $query = 'INSERT INTO page_style VALUES '.$all_values.
        'ON DUPLICATE KEY UPDATE value = "'.$value.'"';
    }
    else {
      $query = 'INSERT INTO page_style VALUES ("'.$owner.'", "'.$page.'", '.
        '"'.$media.'", "'.$selector.'", "'.$property.'", "'.$value.'") '.
        'ON DUPLICATE KEY UPDATE value = "'.$value.'"';
    }
    if (!$mysqli->query($query)) {
      log_db('page_style 2: '.$mysqli->error, $owner, $user->name, $page);
    }
  }
}

$mysqli->close();

if ($update_all_pages) {
  update_page_style($all_pages);
}
else {
  if ($owner === 'admin') write_page_style($owner, '../'.$page.'.css');
  else write_page_style($owner, '../'.$owner.'/'.$page.'.css');
}

// Let the client know the action completed.
echo json_encode(['done' => true]);
