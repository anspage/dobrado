<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function page_not_modified($username, $page, $action) {
  $timestamp = 0;
  $mysqli = connect_db();
  $query = 'SELECT timestamp FROM page_updates WHERE ' .
    'user = "' . $username . '" AND page = "' . $page . '" AND ' .
    'action = "' . $action . '"';
  if ($mysqli_result = $mysqli->query($query)) {
    if ($page_updates = $mysqli_result->fetch_assoc()) {
      $timestamp = $page_updates['timestamp'];
    }
    $mysqli_result->close();
  }
  else {
    log_db('page_not_modified: ' . $mysqli->error);
  }
  $mysqli->close();

  header('Cache-Control: private, must-revalidate');
  header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', $timestamp));
  // Always report that the page has been modified if the header isn't set.
  if (!isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) return false;
  return $timestamp <= strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']);
}

function rss($username) {
  $mysqli = connect_db();
  $type = 'rss';
  if (isset($_GET['type'])) {
    $type = $mysqli->escape_string($_GET['type']);
  }
  $page = 'index';
  if (isset($_GET['page']) &&
      preg_match('/^[a-z0-9_-]{1,200}$/i', $_GET['page'])) {
    $page = $mysqli->escape_string($_GET['page']);
  }
  $action = 'feed';
  if (isset($_GET['action'])) {
    $action = $mysqli->escape_string($_GET['action']);
  }
  $mysqli->close();

  if (page_not_modified($username, $page, $action)) {
    header('HTTP/1.1 304 Not Modified');
    return;
  }

  if ($type === 'rss') create_rss($username, $page, $action);
  else if ($type === 'opml') create_opml($username, $page);
}

function create_opml($username, $page) {
  // Only show content if the feed page is published or visible to the
  // currently logged in user.
  $published_page = false;
  $mysqli = connect_db();
  $query = 'SELECT published FROM published WHERE ' .
    'user = "' . $username . '" AND page = "' . $page . '"';
  if ($mysqli_result = $mysqli->query($query)) {
    if ($published = $mysqli_result->fetch_assoc()) {
      $published_page = $published['published'] === '1';
    }
    $mysqli_result->close();
  }
  else {
    log_db('create_opml 1: ' . $mysqli->error);
  }
  $mysqli->close();

  if (!$published_page) {
    $user = new User();
    $user->SetPermission($page, $username);
    if (!$user->canViewPage) return;
  }

  $user = new User($username);
  $user->page = $page;
  $reader = new Module($user, $username, 'reader');
  if (!$reader->IsInstalled()) return;

  header('Content-Type: text/x-opml');
  echo '<?xml version="1.0" ?>' . "\n" .
    '<opml version="2.0">' . "\n" .
      '<body>' . "\n" .
        $reader->Factory('CreateFeedList', 'opml') .
      '</body>' . "\n" .
    '</opml>';
}

function create_rss($username, $page, $action) {
  $user = new User($username);
  $fancy_url = $user->config->FancyUrl();
  $title = $user->config->TitleIncludesPage() ?
    $user->config->Title() . ' - ' . $page : $user->config->Title();
  $server = $user->config->ServerName();
  $scheme = $user->config->Secure() ? 'https://' : 'http://';
  $port = $user->config->Secure() ? '443' : '80';
  $link = $scheme . $server;
  if ($user->name !== 'admin') $link .= '/' . $user->name;
  if ($page !== 'index') {
    $link .= $fancy_url ? '/' . $page : '/index.php?page=' . $page;
  }

  header('Content-Type: application/rss+xml');
  echo '<?xml version="1.0" ?>' . "\n" .
    '<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss/">' . "\n" .
      "<channel>\n" .
        '<title>' . htmlspecialchars($title) . "</title>\n" .
        '<link>' . $link . "</link>\n" .
        "<description>Syndicated by Dobrado.</description>\n" .
        '<cloud domain="' . $server . '" port="' . $port . '" ' .
          'path="/php/cloud.php" registerProcedure="" protocol="http-post" />' .
          "\n";

  // The notify table specifies which modules have placed themselves in a feed.
  // The feed list is intentionally left short, so that aggregators subscribing
  // to updates have less to parse (it is up to them to store older posts).
  // 24 hours is used to allow previously syndicated items to be updated. If
  // there are no recent items show the last one to not confuse new subscribers.
  $mysqli = connect_db();
  $start_query = 'SELECT box_id, label FROM notify WHERE ' .
    'user = "' . $user->name . '" AND page = "' . $page . '" AND ' .
    'action = "' . $action . '" AND public = 1';
  $query = $start_query . ' AND timestamp > ' . strtotime('-24 hours') .
    ' ORDER BY timestamp DESC';
  $feed = [];
  if ($mysqli_result = $mysqli->query($query)) {
    while ($notify = $mysqli_result->fetch_assoc()) {
      $feed[] = $notify;
    }
    $mysqli_result->close();
  }
  else {
    log_db('create_rss 1: ' . $mysqli->error);
  }
  if (count($feed) === 0) {
    $query = $start_query . ' ORDER BY timestamp DESC LIMIT 1';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($notify = $mysqli_result->fetch_assoc()) {
        $feed[] = $notify;
      }
      $mysqli_result->close();
    }
    else {
      log_db('create_rss 2: ' . $mysqli->error);
    }
  }
  for ($i = 0; $i < count($feed); $i++) {
    // Modules that want to produce feeds need to match this table structure,
    // and produce one feed item per box_id.
    $query = 'SELECT title, description, author, category, enclosure, ' .
      'permalink, timestamp FROM ' . $feed[$i]['label'] . ' WHERE ' .
      'user = "' . $user->name . '" AND box_id = ' . $feed[$i]['box_id'];
    if ($mysqli_result = $mysqli->query($query)) {
      if ($item = $mysqli_result->fetch_assoc()) {
        $title = $item['title'];
        // Replace encoded spaces in the html.
        $description = preg_replace('/&nbsp;/', ' ', $item['description']);
        // Skip empty items but only when both title and description are empty.
        if ($title === '' && $description === '') continue;

        echo "<item>\n";
        if ($title !== '') {
          echo '<title>' . htmlspecialchars($title) . "</title>\n";
        }
        $url = $item['permalink'];
        if ($url !== '') {
          if (!$fancy_url) {
            $url = 'index.php?page=' . $url;
          }
          $permalink = $scheme . $server . '/';
          $permalink .= $user->name === 'admin' ? $url : $user->name . '/'.$url;
          echo '<link>' . $permalink . "</link>\n";
        }
        if ($description !== '') {
          echo '<description>' . htmlspecialchars($description) .
            "</description>\n";
        }
        if ($item['author'] !== '') {
          echo '<author>' . $item['author'] . "</author>\n";
        }
        if (strpos($item['category'], ',') !== false) {
          foreach (explode(',', $item['category']) as $category) {
            echo '<category>' . htmlspecialchars($category) . "</category>\n";
          }
        }
        else if ($item['category'] !== '') {
          echo '<category>' . htmlspecialchars($item['category']) .
            "</category>\n";
        }
        $enclosure_list = json_decode($item['enclosure'], true);
        if (is_array($enclosure_list)) {
          foreach ($enclosure_list as $enclosure) {
            echo '<media:content url="' . $enclosure . '"/>' . "\n";
          }
        }
        echo '<pubDate>' . gmdate('F j Y g:ia T', $item['timestamp']) .
          "</pubDate>\n</item>\n";
      }
      $mysqli_result->close();
    }
    else {
      log_db('create_rss 3: ' . $mysqli->error);
    }
  }
  $mysqli->close();
  echo "</channel>\n</rss>";
}
