<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

session_start();

function session_expired() {
  if (isset($_SESSION['token']) &&
      isset($_POST['token']) && $_SESSION['token'] === $_POST['token']) {
    return false;
  }

  // May have an expired session because page.php does 'not modified' checks
  // on feed pages. When this happens reload is used to skip the check.
  $_SESSION['reload'] = true;
  // Scripts that include this function should be returning JSON to client.
  echo json_encode(['error' => 'Session expired: reloading page.']);
  return true;
}
