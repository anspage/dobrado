<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function admin_page_exists($page) {
  $exists = 0;
  $mysqli = connect_db();
  $query = 'SELECT DISTINCT page FROM modules WHERE user = "admin" AND ' .
    'page = "' . $page . '" AND deleted = 0';
  if ($mysqli_result = $mysqli->query($query)) {
    if ($mysqli_result->num_rows === 1) {
      $exists = 1;
    }
    $mysqli_result->close();
  }
  else {
    log_db('admin_page_exists: ' . $mysqli->error);
    $exists = -1;
  }
  $mysqli->close();
  return $exists;
}

function username_exists($user) {
  $exists = 0;
  $mysqli = connect_db();
  $query = 'SELECT user FROM users WHERE user = "' . $user . '"';
  if ($mysqli_result = $mysqli->query($query)) {
    if ($mysqli_result->num_rows === 1) {
      $exists = 1;
    }
    $mysqli_result->close();
  }
  else {
    log_db('username_exists: ' . $mysqli->error);
    $exists = -1;
  }
  $mysqli->close();
  return $exists;
}

function username_available($user, $owner, $force = false) {
  if ($user->name === 'guest') {
    return ['The username \'guest\' is not available.', false];
  }

  if (!preg_match('/^[a-z0-9_-]{1,50}$/', $user->name)) {
    $updated = preg_replace('/[^a-z0-9_-]/', '', $user->name);
    if ($force && $updated !== '') {
      $user->name = $updated;
    }
    else {
      $info = '\'' . $user->name . '\' is the wrong format.';
      if ($updated !== '') $info .= ' (try: ' . $updated . ')';
      return [$info, false];
    }
  }

  $exists = username_exists($user->name);
  if ($exists === 1) {
    if ($force) {
      $count = 1;
      while ($exists === 1) {
        $exists = username_exists($user->name . $count);
        $count++;
      }
      $user->name .= $count;
    }
    else {
      return ['The username \'' . $user->name . '\' already exists', true];
    }
  }
  if ($exists === -1) return [false, false];

  // The path to the directory needs to be relative to the calling script.
  // There are two options, a script in the php directory, or index.php.
  // In the case of the latter, need to also check if it's the top level
  // index.php or at the user level (so either admin or non-admin owner). 
  $prefix = '';
  if ($owner !== 'admin' ||
      strpos($_SERVER['SCRIPT_FILENAME'], 'index.php') === false) {
    $prefix = '../';
  }
  if (is_dir($prefix . $user->name)) {
    if ($force) {
      $count = 1;
      while (is_dir($prefix . $user->name . $count)) {
        $count++;
      }
      $user->name .= $count;
    }
    else {
      // When the user doesn't exist, but there's already a directory with that
      // name, then it's not available.
      return ['The username \'' . $user->name . '\' is not available.', false];
    }
  }

  // When FancyUrl is true, admin pages will conflict with username
  // directories, so make sure the admin page doesn't exist for the username.
  if ($user->config->FancyUrl()) {
    $exists = admin_page_exists($user->name);
    if ($exists === 1) {
      if ($force) {
        $count = 1;
        while ($exists === 1) {
          $exists = admin_page_exists($user->name . $count);
          $count++;
        }
        $user->name .= $count;
      }
      else {
        return ['The username \'' . $user->name .'\' is not available.', false];
      }
    }
    if ($exists === -1) return [false, false];
  }
  return [true, true];
}

function new_user($user, $owner, $active = 1, $email = '',
                  $confirmed = 1, $password = '', $force = false) {
  list($available, $can_update) = username_available($user, $owner, $force);
  // available can be boolean false or a string here, so explicitly check true.
  if ($available !== true) return $available;

  // Don't show the password in the email if it's already set.
  $display_password = $password === '';
  // If password is an empty string create a random one.
  if ($password === '') {
    $chars = 'bcdfghjklmnpqrstvwxyz1234567890';
    $length = strlen($chars) - 1;
    for ($i = 0; $i < 8; $i++) {
      $password .= substr($chars, mt_rand(0, $length), 1);
    }
  }

  $mysqli = connect_db();
  $password_hash =
    $mysqli->escape_string(password_hash($password, PASSWORD_DEFAULT));
  $confirm_id = bin2hex(openssl_random_pseudo_bytes(16));
  $query = 'INSERT INTO users VALUES ("' . $user->name . '", ' .
    '"' . $password_hash . '", "' . $email . '", "' . $user->group . '", ' .
    $confirmed . ', "' . $confirm_id . '", ' . time() . ', "", ' . $active .')';
  if (!$mysqli->query($query)) {
    log_db('new_user 1: ' . $mysqli->error, $owner, $user->name);
    $mysqli->close();
    return false;
  }

  // The path to the directory needs to be relative to the calling script.
  // There are two options, a script in the php directory, or index.php.
  // In the case of the latter, need to also check if it's the top level
  // index.php or at the user level (so either admin or non-admin owner).
  $prefix = '';
  if ($owner !== 'admin' ||
      strpos($_SERVER['SCRIPT_FILENAME'], 'index.php') === false) {
    $prefix = '../';
  }
  mkdir($prefix . $user->name . '/rss', 0755, true);
  mkdir($prefix . $user->name . '/public', 0755);

  copy($prefix . 'php/default.php', $prefix . $user->name . '/index.php');
  chmod($prefix . $user->name . '/index.php', 0644);

  $handle = fopen($prefix . $user->name . '/owner.php', 'w');
  fwrite($handle, '<?php $owner=\'' . $user->name . '\';');
  fclose($handle);
  chmod($prefix . $user->name . '/owner.php', 0644);

  $handle = fopen($prefix . $user->name . '/rss/index.php', 'w');
  fwrite($handle, "<?php\n" .
         "include '../../php/functions/db.php';\n" .
         "include '../../php/functions/rss.php';\n" .
         "include '../../php/config.php';\n" .
         "include '../../php/module.php';\n" .
         "include '../../php/user.php';\n" .
         "rss('" . $user->name . "');\n");
  fclose($handle);
  chmod($prefix . $user->name . '/rss/index.php', 0644);

  // Copy default modules for the user. When force is true the new user is a
  // supplier account, so also set a session variable that modules can check
  // so they can do setup specific to supplier accounts.
  $_SESSION['new-supplier'] = $force;
  copy_default($user, $owner);
  // Send the user an email if an address was given.
  if ($email !== '') {
    $server = $user->config->ServerName();
    $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
      'https://' : 'http://';
    $moderator = substitute('new-user-moderator', $user->group);
    $us_moderator_message = '';
    $moderator_subject = '';
    $subject = '';
    // Can't use the detail module settings here because they haven't been
    // added yet by the account module. Might still have access to the same
    // post data though so try using that.
    $us_name = isset($_POST['first']) ? htmlspecialchars($_POST['first']) : '';
    // If not set just use the username.
    if ($us_name === '') {
      $us_name = $user->name;
    }
    $us_message = '<html><head><title>New account</title></head><body>';
    if ($confirmed === 1) {
      $subject = 'New account';
      $password_info = $display_password ? ' and password: ' . $password .
        '<p>You can change your password later from the account menu.</p>' :
        ' and the password entered when your account was created.';
      $us_message .= 'Hello ' . $us_name . '<p>Welcome to <a href="' . $scheme .
        $server . '">' . $server . '</a>!</p><p>Please log in with the ' .
        'username: ' . $user->name . $password_info . '</p>';
      // Allow 'new-user-info' substitution to show the user's bank reference.
      $banking = new Module($user, $user->name, 'banking');
      if ($banking->IsInstalled()) {
        $settings = $banking->Factory('Settings', $user->name);
        $us_message .= $banking->Substitute('new-user-info', '/!reference/',
                                            $settings['reference']);
      }
    }
    else if ($moderator === '') {
      $subject = 'Confirm your account';
      $link = $server . '/php/confirm.php?username=' . $user->name .
        '&confirm_id=' . $confirm_id;
      $us_message .= 'Hello ' . $us_name . '<p>This is an automated email ' .
        'from <a href="' . $scheme . $server . '">' . $server . '</a>.</p>';
      $banking = new Module($user, $user->name, 'banking');
      if ($banking->IsInstalled()) {
        $settings = $banking->Factory('Settings', $user->name);
        $us_message .= $banking->Substitute('new-user-info', '/!reference/',
                                            $settings['reference']);
      }
      $us_message .= '<p>To confirm your account, please go to:<br>' .
        '<a href="' . $scheme . $link . '">' . $link . '</a></p>';
    }
    else {
      // Send an email to the new user informing them that their account must
      // first be confirmed by a moderator.
      $subject = 'New account waiting for approval';
      $us_message .= 'Hello ' . $us_name . '<p>Thank you for your interest ' .
        'in creating an account on <a href="' . $scheme . $server . '">' .
        $server . '</a>!</p><p>Your account is currently waiting to be ' .
        'confirmed, you will receive another email when you are able to log ' .
        'in.</p>';
      $us_detail = '';
      if (isset($_POST['first'])) {
        $us_first = htmlspecialchars($_POST['first']);
        if ($us_first !== '') $us_detail .= 'First Name: ' . $us_first . '<br>';
      }
      if (isset($_POST['last'])) {
        $us_last = htmlspecialchars($_POST['last']);
        if ($us_last !== '') $us_detail .= 'Last Name: ' . $us_last . '<br>';
      }
      if (isset($_POST['phone'])) {
        $us_phone = htmlspecialchars($_POST['phone']);
        if ($us_phone !== '') $us_detail .= 'Phone Number: ' . $us_phone.'<br>';
      }
      if (isset($_POST['description'])) {
        $us_description = htmlspecialchars($_POST['description']);
        if ($us_description !== '') {
          $us_detail .= 'Description: ' . nl2br($us_description, false) .
            '<br>';
        }
      }
      $moderator_subject = 'Confirm new account';
      $link = $server . '/php/confirm.php?username=' . $user->name .
        '&confirm_id=' . $confirm_id . '&moderator=true';
      $us_moderator_message = '<html><head><title>New account</title></head>' .
        '<body>Dear moderator,<p>This is an automated email ' .
        'from <a href="' . $scheme . $server . '">' . $server . '</a></p>' .
        '<p>A new account has been requested and the following details were ' .
        'provided:<br>Username: ' . $user->name . '<br>' .
        'Group: ' . $user->group . '<br>Email: ' . $email . '<br>' .
        $us_detail . '</p><p>To confirm this account, please go to:<br>' .
        '<a href="' . $scheme . $link . '">' . $link . '</a></p></body></html>';
    }
    $us_message .= '</body></html>';
    $us_message = wordwrap($us_message);
    $sender = substitute('new-user-sender', $user->group, '/!host/', $server);
    $sender_name = substitute('new-user-sender-name', $user->group);
    if ($sender_name === '') {
      $sender_name = $sender;
    }
    else {
      $sender_name .= ' <' . $sender . '>';
    }
    $cc = substitute('new-user-sender-cc', $user->group);
    $bcc = substitute('new-user-sender-bcc', $user->group);
    $headers = "MIME-Version: 1.0\r\n" .
      "Content-type: text/html; charset=utf-8\r\n" .
      'From: ' . $sender_name . "\r\n";
    if ($cc !== '') {
      $headers .= 'Cc: ' . $cc . "\r\n";
    }
    if ($bcc !== '') {
      $headers .= 'Bcc: ' . $bcc . "\r\n";
    }
    mail($email, $subject, $us_message, $headers, '-f ' . $sender);
    if ($us_moderator_message !== '') {
      $us_moderator_message = wordwrap($us_moderator_message);
      mail($moderator, $moderator_subject, $us_moderator_message, $headers,
           '-f ' . $sender);
    }
  }

  // Create notifications for the new account, control-notifications is used
  // as the group to query because it already exists.
  $description = 'New user: ' . $user->name;
  if ($user->group !== '') {
    $description .= ' (' . $user->group . ')';
  }
  $content = ['description' => $description, 'author' => 'Account',
              'category' => 'system'];
  // Look up who control notifications should go to.
  $query = 'SELECT visitor FROM group_names WHERE user = "admin" AND ' .
    '(name = "control-notifications" OR name = "control-notifications-' .
    $user->group . '")';
  if ($mysqli_result = $mysqli->query($query)) {
    while ($group_names = $mysqli_result->fetch_assoc()) {
      // A module is created for every member of the group.
      $owner = $group_names['visitor'];
      $notification = new Notification($user, $owner);
      $id = new_module($user, $owner, 'notification', '',
                       $notification->Group(), 'outside');
      $notification->SetContent($id, $content);
    }
    $mysqli_result->close();
  }
  else {
    log_db('new_user 2: ' . $mysqli->error, $owner, $user->name);
  }
  $mysqli->close();
  return true;
}

function copy_default($user, $owner) {
  $prefix = '';
  if ($owner !== 'admin' ||
      strpos($_SERVER['SCRIPT_FILENAME'], 'index.php') === false) {
    $prefix = '../';
  }
  $mysqli = connect_db();

  $us_default = substitute('group-default-page', $user->group);
  $default = $mysqli->escape_string($us_default);
  $query = 'INSERT INTO published (user, page, published) SELECT ' .
    '"' . $user->name . '", "index", published FROM published WHERE ' .
    'user = "admin" AND page = "' . $default . '"';
  if (!$mysqli->query($query)) {
    log_db('copy_default 1: ' . $mysqli->error, $owner, $user->name);
  }
  $query = 'INSERT INTO modules (user, page, label, class, box_order, ' .
    'placement, deleted) SELECT "' . $user->name . '", page, label, class, ' .
    'box_order, placement, deleted FROM modules WHERE user = "admin" AND ' .
    'label != "notification" AND (page = "' . $default . '" OR page = "") ' .
    'AND deleted = 0 ORDER BY box_id';
  if (!$mysqli->query($query)) {
    log_db('copy_default 2: ' . $mysqli->error, $owner, $user->name);
  }
  // Modules from the default page are copied to 'index' for the user, but
  // some modules are also displayed on all pages (which are left empty).
  $query = 'UPDATE modules SET page = "index" WHERE page = "' . $default . '" '.
    'AND user = "' . $user->name . '"';
  if (!$mysqli->query($query)) {
    log_db('copy_default 3: ' . $mysqli->error, $owner, $user->name);
  }
  // Also copy to modules_history to keep box_id's the same.
  $query = 'INSERT INTO modules_history (user, page, label, class, ' .
    'box_order, placement, action, modified_by, timestamp) SELECT ' .
    'user, page, label, class, box_order, placement, "add", "admin", ' .
    time() . ' FROM modules WHERE user = "' . $user->name . '" AND ' .
    'label != "notification" AND (page = "index" OR page = "") ORDER BY box_id';
  if (!$mysqli->query($query)) {
    log_db('copy_default 4: ' . $mysqli->error, $owner, $user->name);
  }
  
  // Note that the current owner and page are still used here because we
  // need the old box_id's.
  $query = 'SELECT box_id, label FROM modules WHERE user = "admin" AND ' .
    '(page = "' . $default . '" OR page = "") AND label != "notification" AND '.
    'deleted = 0 ORDER BY box_id';
  if ($mysqli_result = $mysqli->query($query)) {
    $id = 1;
    while ($module_list = $mysqli_result->fetch_assoc()) {
      $module = new Module($user, $user->name, $module_list['label']);
      $module->Copy($id++, 'index', 'admin', $module_list['box_id']);
    }
    $mysqli_result->close();
  }
  else {
    log_db('copy_default 5: ' . $mysqli->error, $owner, $user->name);
  }
  // Write out a new style sheet after the modules have been copied.
  write_box_style($user->name, $prefix . $user->name . '/style.css');
  // Also copy the page style rules for the new page.
  $query = 'INSERT INTO page_style (user, name, media, selector, property, ' .
    'value) SELECT "' . $user->name . '", "index", media, selector, property, '.
    'value FROM page_style WHERE user = "admin" AND name = "' . $default . '"';
  if (!$mysqli->query($query)) {
    log_db('copy_default 6: ' . $mysqli->error, $owner, $user->name);
  }
  write_page_style($user->name, $prefix . $user->name . '/index.css');
  // Add the user to an admin group containing all members of the group,
  // so that it's easier to set page permissions for the whole group.
  $query = 'INSERT INTO group_names VALUES ("admin", ' .
    '"all-' . $user->group . '", "' . $user->name . '", 0)';
  if (!$mysqli->query($query)) {
    log_db('copy_default 7: ' . $mysqli->error, $owner, $user->name);
  }
  // Set a default timezone for the user.
  $timezone = $mysqli->escape_string(substitute('timezone', $user->group));
  $query = 'INSERT INTO config (user, timezone) VALUES ' .
    '("' . $user->name . '", "' . $timezone . '") ON DUPLICATE KEY UPDATE ' .
    'timezone = "' . $timezone . '"';
  if (!$mysqli->query($query)) {
    log_db('copy_default 8: ' . $mysqli->error, $owner, $user->name);
  }
  $mysqli->close();

  // Add default notifications for a member of this group.
  $notification = new Notification($user, $owner);
  $notification->AddMember();
  // Also if the Settings module is installed add group defaults.
  $settings = new Module($user, $owner, 'settings');
  $settings->Factory('AddMember');
}

function send_confirmed_user_email($user, $new_user, $email, $password) {
  // Don't show the password in the email if it's already set.
  $display_password = $password === '';
  // If password is an empty string need to create a new random one.
  if ($password === '') {
    $chars = 'bcdfghjklmnpqrstvwxyz1234567890';
    $length = strlen($chars) - 1;
    for ($i = 0; $i < 8; $i++) {
      $password .= substr($chars, mt_rand(0, $length), 1);
    }
    $mysqli = connect_db();
    $password_hash =
      $mysqli->escape_string(password_hash($password, PASSWORD_DEFAULT));
    $query = 'UPDATE users SET password = "' . $password_hash . '" WHERE ' .
      'user = "' . $new_user . '"';
    if (!$mysqli->query($query)) {
      log_db('send_confirmed_user_email: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  $server = $user->config->ServerName();
  $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
    'https://' : 'http://';
  $us_name = isset($_POST['first']) ? htmlspecialchars($_POST['first']) : '';
  if ($us_name === '') $us_name = $new_user;
  $us_message = '<html><head><title>New account</title></head><body>';
  $subject = 'New account';
  $password_info = $display_password ? ' and password: ' . $password .
    '<p>You can change your password later from the account menu.</p>' :
    ' and the password entered when your account was created.';
  $us_message .= 'Hello ' . $us_name . '<p>Welcome to <a href="' . $scheme .
    $server . '">' . $server . '</a>!</p><p>Please log in with the username: ' .
    $new_user . $password_info . '</p>';
  // Allow 'new-user-info' substitution to show the user's bank reference.
  $banking = new Module($user, $user->name, 'banking');
  if ($banking->IsInstalled()) {
    $settings = $banking->Factory('Settings', $new_user);
    $us_message .= $banking->Substitute('new-user-info', '/!reference/',
                                        $settings['reference']);
  }
  $us_message .= '</body></html>';
  $us_message = wordwrap($us_message);
  $sender = substitute('new-user-sender', $user->group, '/!host/', $server);
  $sender_name = substitute('new-user-sender-name', $user->group);
  if ($sender_name === '') {
    $sender_name = $sender;
  }
  else {
    $sender_name .= ' <' . $sender . '>';
  }
  $cc = substitute('new-user-sender-cc', $user->group);
  $bcc = substitute('new-user-sender-bcc', $user->group);
  $headers = "MIME-Version: 1.0\r\n" .
    "Content-type: text/html; charset=utf-8\r\n" .
    'From: ' . $sender_name . "\r\n";
  if ($cc !== '') {
    $headers .= 'Cc: ' . $cc . "\r\n";
  }
  dobrado_mail($email, $subject, $us_message, $headers, $bcc,
               $sender, $new_user, 'account', 'newAccountEmail');
}
