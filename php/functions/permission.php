<?php
// Dobrado Content Management System
// Copyright (C) 2018 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function permission($action, $mode, $name = '') {
  if (!in_array($action, ['edit', 'copy', 'view'])) return false;
  if (!isset($_SESSION['user'])) return false;

  $user = $_SESSION['user'];
  $page = '';
  $owner = '';

  // An empty 'page' value is used to signify site permission below,
  // and the owner is always 'admin' in this case.
  if ($mode === 'site') {
    if ($user === 'admin') return true;
    $page = '';
    $owner = 'admin';
  }
  else {
    preg_match('/^([a-z0-9_-]+)\/?([a-z0-9_-]*)$/i', $name, $matches);
    if (count($matches) === 3) {
      // Note that count will always be 3 if there's a match because the second
      // match is allowed to be empty.
      if ($matches[2] === '') {
        $owner = $user;
        $page = $matches[1];
      }
      else {
        $owner = $matches[1];
        $page = $matches[2];
      }
    }
    if ($user === '' || $page === '' || $owner === '') return false;
    if ($mode === 'page' && $user === $owner) return true;
  }

  $permission = false;
  $mysqli = connect_db();
  if ($action === 'view') {
    // Allow edit or copy permissions to also be implicit view permission.
    $action = '(edit = 1 OR copy = 1 OR view = 1)';
  }
  else {
    $action .= ' = 1';
  }
  $query = 'SELECT visitor FROM user_permission WHERE ' .
    'user = "' . $owner . '" AND page = "' . $page . '" AND ' .
    '(visitor = "' . $user . '" OR visitor = "") AND ' . $action;
  if ($result = $mysqli->query($query)) {
    if ($result->num_rows > 0) $permission = true;
    $result->close();
  }
  else {
    log_db('permission 1: ' . $mysqli->error);
  }
  if (!$permission) {
    // Find all groups that have permission to edit/copy the page, for this 
    // user and visitor (it doesn't matter what the group is called).
    $query = 'SELECT visitor FROM group_names LEFT JOIN group_permission ON ' .
      'group_names.name = group_permission.name AND ' .
      'group_names.user = group_permission.user WHERE ' .
      'group_permission.page = "' . $page . '" AND ' .
      'group_names.visitor = "' . $user . '" ' .
      'AND group_names.user = "' . $owner . '" AND ' . $action;
    if ($result = $mysqli->query($query)) {
      if ($result->num_rows > 0) $permission = true;
      $result->close();
    }
    else {
      log_db('permission 2: ' . $mysqli->error);
    }
  }
  $mysqli->close();
  return $permission;
}

function can_edit_site() {
  return permission('edit', 'site');
}

function can_edit_page($name) {
  return permission('edit', 'page', $name);
}

function can_copy_page($name) {
  return permission('copy', 'page', $name);
}

function can_view_page($name) {
  return permission('view', 'page', $name);
}
