<?php
// Dobrado Content Management System
// Copyright (C) 2016 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function page_owner($us_url) {
  // If the regex below doesn't match then the following defaults are used.
  $owner = 'admin';
  $page = 'index';

  // First part of the regex matches an optional username after the server name.
  $regex = '/'.$_SERVER['SERVER_NAME'].'\/?([a-z0-9_-]*)\/?';
  // Next look for 'index.php' in the url, so that the one regex can be used for
  // both fancy and normal urls. Note that it can't be made optional because it
  // will become a match for the username when that isn't matched (ie 'admin').
  if (strpos($us_url, 'index.php') !== false) {
    $regex .= 'index\.php';
  }
  $regex .= '(?:\?page=)?([a-z0-9_-]*)/i';

  $matches = array();
  // Note that $matches is safe to use with mysql even though $us_url is not,
  // due to the regex captures being mysql safe.
  preg_match($regex, $us_url, $matches);
  if (count($matches) === 3 && $matches[1] !== '' && $matches[2] !== '') {
    $owner = $matches[1];
    $page = $matches[2];
  }
  else if (count($matches) >= 2) {
    // When the first match is empty, the owner is the admin user.
    if ($matches[1] === '') {
      $owner = 'admin';
      $page = count($matches) === 3 && $matches[2] !== '' ?
        $matches[2] : 'index';
    }
    else {
      // When the regex returns empty values, or only one value was matched,
      // the value can be either a username or a page name. To find out
      // which, need to check if a username exists for the match.
      $mysqli = connect_db();
      $query = 'SELECT user FROM users WHERE user = "'.$matches[1].'"';
      if ($result = $mysqli->query($query)) {
        if ($result->num_rows === 0) {
          $owner = 'admin';
          $page = $matches[1];
        }
        else {
          $owner = $matches[1];
          $page = count($matches) === 3 && $matches[2] !== '' ?
            $matches[2] : 'index';
        }
        $result->close();
      }
      $mysqli->close();
    }
  }
  return array($page, $owner);
}
