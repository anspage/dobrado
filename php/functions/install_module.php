<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function update_script($user) {
  // If updating a module that includes javascript need to re-write the
  // concatenated javascript files. Start with the two core files, and then
  // each of the installed modules is called to update their own files.
  $date = date(DATE_COOKIE);
  $content = "\n// File: core.pub.js\n// Added by: " . $user->name . "\n" .
    '// Date: ' . $date . "\n";
  $content .= file_get_contents('../js/core.pub.js');
  file_put_contents('../js/dobrado.pub.js', $content);
  $content .= "\n// File: core.js\n// Added by: " . $user->name . "\n" .
    '// Date: ' . $date . "\n";
  $content .= file_get_contents('../js/core.js');
  file_put_contents('../js/dobrado.js', $content);

  // Do the same for the source versions.
  $content = "\n// File: core.pub.js\n// Added by: " . $user->name . "\n" .
    '// Date: ' . $date . "\n";
  $content .= file_get_contents('../js/source/core.pub.js');
  file_put_contents('../js/source/dobrado.pub.js', $content);
  $content .= "\n// File: core.js\n// Added by: " . $user->name . "\n" .
    "// Date: " . $date . "\n";
  $content .= file_get_contents('../js/source/core.js');
  file_put_contents('../js/source/dobrado.js', $content);

  $mysqli = connect_db();
  $query = 'SELECT label FROM installed_modules';
  if ($mysqli_result = $mysqli->query($query)) {
    while ($installed_modules = $mysqli_result->fetch_assoc()) {
      $module = new Module($user, $user->name, $installed_modules['label']);
      $module->UpdateScript('../js');
    }
    $mysqli_result->close();
  }
  else {
    log_db('update_script: ' . $mysqli->error);
  }
  $mysqli->close();
}

function install_module($label, $version, $title, $description,
                        $display, $user, $update_only = false) {
  if ($label === 'base') return ['done' => true];

  $default_modules = ['account', 'control', 'extended', 'login', 'more',
                      'notification', 'organiser', 'simple'];
  if (in_array($label, $default_modules)) {
    if (!file_exists('modules/' . ucfirst($label) . '.php')) {
      return ['error' => 'No php file found for default module:' . $label];
    }
  }
  else {
    $php_source = '../install/' . ucfirst($label) . '.php';
    $php_destination = 'modules/' . ucfirst($label) . '.php';
    $js_name = 'dobrado.' . $label;
    if (!file_exists($php_source)) {
      return ['error' => 'No php file found for module: ' . $label];
    }

    copy($php_source, $php_destination);
    if (file_exists('../install/' . $js_name . '.js')) {
      if (!is_readable('../install/' . $js_name . '.js')) {
        return ['error' => 'Cannot read javascript install file.'];
      }

      copy('../install/' . $js_name . '.js', '../js/' . $js_name . '.js');
    }
    // Look for a matching 'source' file for the javascript.
    if (file_exists('../install/' . $js_name . '.source.js')) {
      if (!is_readable('../install/' . $js_name . '.source.js')) {
        return ['error' => 'Cannot read javascript source file.'];
      }

      copy('../install/' . $js_name . '.source.js',
           '../js/source/' . $js_name . '.js');
    }
  }

  $mysqli = connect_db();
  $update = false;
  $query = 'SELECT version FROM installed_modules WHERE ' .
    'label = "' . $label . '"';
  if ($mysqli_result = $mysqli->query($query)) {
    if ($installed_modules = $mysqli_result->fetch_assoc()) {
      $mysqli_result->close();
      if ($version !== '' && $version === $installed_modules['version']) {
        $mysqli->close();
        return ['error' => $label . ' version: ' . $version . ' up to date.'];
      }

      // If the version number is empty or doesn't match, then update module.
      $query = 'UPDATE installed_modules SET version = "' . $version . '", ' .
        'title = "' . $title . '", description = "' . $description . '", ' .
        'display = "' . $display . '" WHERE label = "' . $label . '"';
      if (!$mysqli->query($query)) {
        log_db('install_module 1: ' . $mysqli->error);
      }
      $update = true;
    }
    else if (!$update_only) {
      $query = 'INSERT INTO installed_modules VALUES ("' . $user->name . '", ' .
        '"' . $label . '", "' . $version . '", "' . $title . '", ' .
        '"' . $description . '", "' . $display . '")';
      if (!$mysqli->query($query)) {
        log_db('install_module 2: ' . $mysqli->error);
      }
    }
  }
  else {
    log_db('install_module 3: ' . $mysqli->error);
  }
  $mysqli->close();

  if ($update_only && !$update) {
    return ['error' => ucfirst($label) . ' not installed.'];
  }

  $write_3rdparty = false;
  // Move css files from the install directory, which are included with modules.
  if ($handle = opendir('../install')) {
    while (($file = readdir($handle)) !== false) {
      if (preg_match('/.*\.css$/', $file)) {
        rename('../install/' . $file, '../css/' . $file);
        $write_3rdparty = true;
      }
    }
    closedir($handle);
  }
  if ($write_3rdparty) {
    $handle = fopen('../3rdparty.css', 'w');
    foreach (scandir('../css') as $file) {
      if (preg_match('/.*\.css$/', $file)) {
        $content = '/* File: ' . $file . "\n" .
          ' * Added by: ' . $user->name . "\n" .
          ' * Date: ' . date(DATE_COOKIE) . "\n" .
          " */\n";
        $content .= file_get_contents('../css/' . $file);
        $content .= "\n";
        fwrite($handle, $content);
      }
    }
    fclose($handle);
  }

  // Don't need a new instance file for updates.
  if (!$update_only) {
    include 'write_instance.php';
    write_instance();
  }
  // module.php must be included after generating the new instance.php. Also if
  // the Module class already exists then it can't be re-declared here, which is
  // ok when updating modules but can't install new ones without a new request.
  if (class_exists('Module')) {
    if (!$update_only) return ['done' => true];
  }
  else {
    include 'module.php';
  }
  $module = new Module($user, $user->name, $label);
  $include_script = $module->IncludeScript();
  // The module's Install method returns the result as an array,
  // which can include information on dependencies.
  $dependencies = false;
  if ($update) {
    $module->Update();
    if ($include_script) update_script($user);
  }
  else {
    $dependencies = $module->Install('../js');
  }
  if ($include_script) {
    // If the module includes a script, update the script version number to
    // force browsers to fetch the new version in page.php.
    $mysqli = connect_db();
    if (!$mysqli->query('UPDATE script_version SET dobrado = dobrado + 1')) {
      log_db('install_module 4: ' . $mysqli->error);
    }
    $mysqli->close();
  }

  // Update site style information once the module is installed.
  include_once 'functions/write_style.php';
  write_site_style();
  if ($dependencies) return $dependencies;
  return ['done' => true];
}