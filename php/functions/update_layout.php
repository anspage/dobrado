<?php
// Dobrado Content Management System
// Copyright (C) 2016 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function update_layout($id, $order, $offset, $page, $owner) {
  $mysqli = connect_db();
  $placement = $mysqli->escape_string($_POST['placement']);
  $old_order = 0;
  $old_placement = '';

  $query = 'SELECT box_order, placement FROM modules WHERE '.
    'user = "'.$owner.'" AND box_id = '.$id.' AND page = "'.$page.'"';
  if ($result = $mysqli->query($query)) {
    if ($modules = $result->fetch_assoc()) {
      $old_placement = $modules['placement'];
      $old_order = (int)$modules['box_order'];
    }
    $result->close();
  }
  else {
    log_db('update_layout 1: '.$mysqli->error);
    return;
  }

  if ($old_placement === $placement) {
    // Find the start and end positions to change the order between.
    $start_position = 0;
    $end_position = 0;
    if ($order > $old_order) {
      $start_position = $old_order;
      $order = $offset;
      $end_position = $offset;
    }
    else {
      $end_position = $old_order;
      $start_position = $order;
    }
    $position = array();
    $query = 'SELECT box_id FROM modules WHERE user = "'.$owner.'"'.
      ' AND page = "'.$page.'" AND placement = "'.$placement.'"'.
      ' AND box_id != '.$id.' AND box_order >= '.$start_position.
      ' AND box_order <= '.$end_position.
      ' AND deleted = 0 ORDER BY box_order';
    if ($result = $mysqli->query($query)) {
      // If start_position is equal to the new position, need to increase it
      // by one to accomodate the new module.
      if ($start_position === $order) $start_position++;
      while ($modules = $result->fetch_assoc()) {
        $position[$modules['box_id']] = $start_position++;
      }
      $result->close();
    }
    else {
      log_db('update_layout 2: '.$mysqli->error);
    }
    foreach ($position as $key => $value) {
      $query = 'UPDATE modules SET box_order = '.$value.' WHERE '.
        'user = "'.$owner.'" AND box_id = '.$key.' AND page = "'.$page.'"';
      if (!$mysqli->query($query)) {
        log_db('update_layout 3: '.$mysqli->error);
      }
    }
  }
  else {
    // When the placement changes, need to update the order of modules
    // greater than the old position in the old group, and greater than
    // or equal to the new position in the new group.
    $position = array();
    $query = 'SELECT box_id FROM modules WHERE user = "'.$owner.'"'.
      ' AND page = "'.$page.'" AND placement = "'.$old_placement.'"'.
      ' AND box_id != '.$id.' AND box_order > '.$old_order.
      ' AND deleted = 0 ORDER BY box_order';
    if ($result = $mysqli->query($query)) {
      while ($modules = $result->fetch_assoc()) {
        $position[$modules['box_id']] = $old_order++;
      }
      $result->close();
    }
    else {
      log_db('update_layout 4: '.$mysqli->error);
    }
    foreach ($position as $key => $value) {
      $query = 'UPDATE modules SET box_order = '.$value.' WHERE '.
        'user = "'.$owner.'" AND box_id = '.$key.' AND page = "'.$page.'"';
      if (!$mysqli->query($query)) {
        log_db('update_layout 5: '.$mysqli->error);
      }
    }
    // The new placement group.
    $position = array();
    $new_order = $order;
    $query = 'SELECT box_id FROM modules WHERE user = "'.$owner.'"'.
      ' AND page = "'.$page.'" AND placement = "'.$placement.'"'.
      ' AND box_id != '.$id.' AND box_order >= '.$new_order.
      ' AND deleted = 0 ORDER BY box_order';
    if ($result = $mysqli->query($query)) {
      while ($modules = $result->fetch_assoc()) {
        $position[$modules['box_id']] = ++$new_order;
      }
      $result->close();
    }
    else {
      log_db('update_layout 6: '.$mysqli->error);
    }
    foreach ($position as $key => $value) {
      $query = 'UPDATE modules SET box_order = '.$value.' WHERE '.
        'user = "'.$owner.'" AND box_id = '.$key.' AND page = "'.$page.'"';
      if (!$mysqli->query($query)) {
        log_db('update_layout 7: '.$mysqli->error);
      }
    }
  }

  // Update the current box with the given value and (possibly new) placement.
  $query = 'UPDATE modules SET box_order = '.$order.', '.
    'placement = "'.$placement.'" WHERE user = "'.$owner.'" AND box_id = '.$id.
    ' AND page = "'.$page.'"';
  if (!$mysqli->query($query)) {
    log_db('update_layout 8: '.$mysqli->error);
  }
  $mysqli->close();
}

