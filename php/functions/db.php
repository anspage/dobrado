<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Set error reporting to E_ALL for development.
error_reporting(0);

function connect_db() {
  include 'db_config.php';

  $mysqli = new mysqli($db_server, $db_user, $db_password, $db_name);
  if ($mysqli->connect_errno) {
    error_log('Could not connect: ' . $mysqli->connect_error);
    exit;
  }
  return $mysqli;
}

function log_db($us_message, $user = '', $visitor = '', $page = '') {
  // Call error_log for the original message.
  error_log($us_message);

  $mysqli = connect_db();
  $message = $mysqli->escape_string($us_message);
  $query = 'INSERT INTO log VALUES ("' . $user . '", "' . $visitor . '", ' .
    '"' . $page . '", ' . time() . ', "' . $message . '")';
  if (!$mysqli->query($query)) {
    error_log('log_db: ' . $mysqli->error);
  }
  $mysqli->close();
}

// This is a helper function for development. Don't leave calls to it in code.
function dobrado_error_log($error) {
  error_log(print_r($error, true));
}

function dobrado_mail($email, $subject, $message, $headers,
                      $bcc, $sender, $user, $module, $type) {
  // The default is send unless the user selects to stop sending mail.
  $send = true;
  $mysqli = connect_db();
  $query = 'SELECT value FROM settings WHERE user = "' . $user . '" AND ' .
    'label = "' . $module . '" AND name = "' . $type . '"';
  if ($result = $mysqli->query($query)) {
    if ($settings = $result->fetch_assoc()) {
      $send = $settings['value'] === 'send';
    }
    $result->close();
  }
  else {
    log_db('dobrado_mail 1: ' . $mysqli->error);
  }
  if (!$send) {
    $mysqli->close();
    return true;
  }

  // Also check if this user would like to forward their email.
  $forward = false;
  $query = 'SELECT value FROM settings WHERE user = "' . $user . '" AND ' .
    'label = "account" AND name = "forward"';
  if ($result = $mysqli->query($query)) {
    if ($settings = $result->fetch_assoc()) {
      $forward = $settings['value'] === 'forward email';
    }
    $result->close();
  }
  else {
    log_db('dobrado_mail 2: ' . $mysqli->error);
  }
  $mysqli->close();

  // If forward is false, the only email we want to bcc is the forward request.
  if ($forward || ($module === 'settings' && $type === 'forward')) {
    $forward_bcc = substitute('mail-forward-bcc');
    if ($forward_bcc !== '') {
      if ($bcc !== '') $bcc .= ',';
      $bcc .= $forward_bcc;
    }
  }
  // Now that the bcc field has been finalised add it to the other headers.
  if ($bcc !== '') {
    $headers .= 'Bcc: ' . $bcc . "\r\n";
  }
  return mail($email, $subject, $message, $headers, '-f ' . $sender);
}

function price_string($value) {
  // The built in number_format() function is used to format floats as strings
  // representing a price, ie with two decimal places. This is just a helper
  // function to avoid writing out all the parameters. Note that the built in
  // round() function cannot be used because it doesn't always return two
  // decimal places, and number_format does the rounding internally.
  return number_format($value, 2, '.', '');
}

function substitute($label, $group = '', $patterns = '', $replacements = '') {
  $content = '';
  $mysqli = connect_db();
  $group = $mysqli->escape_string($group);
  // Look for the content of the specific system_group, but if there isn't
  // one the empty system_group will be returned as the fallback.
  $query = 'SELECT content FROM template WHERE label = "' . $label . '" ' .
    'AND (system_group = "' . $group . '" OR system_group = "") ' .
    'ORDER BY system_group DESC';
  if ($result = $mysqli->query($query)) {
    if ($template = $result->fetch_assoc()) {
      $content = $template['content'];
    }
    $result->close();
  }
  else {
    log_db('substitute: ' . $mysqli->error);
  }
  $mysqli->close();

  if ($patterns === '' && $replacements === '') {
    return $content;
  }
  // Need to be careful of backreferences in replacements,
  // so escape all dollar signs.
  $replacements = str_replace('$', '\$', $replacements);
  return preg_replace($patterns, $replacements, $content);
}
