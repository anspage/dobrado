<?php
// Dobrado Content Management System
// Copyright (C) 2019 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function discover_endpoint($us_url, $rels) {
  $result = [];
  if (is_string($rels)) $rels = [$rels];
  $us_url = trim($us_url);
  if (stripos($us_url, 'http') !== 0) $us_url = 'http://' . $us_url;
  $ch = curl_init($us_url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
  curl_setopt($ch, CURLOPT_TIMEOUT, 20);
  curl_setopt($ch, CURLOPT_ENCODING, '');
  curl_setopt($ch, CURLOPT_HEADER, true);
  curl_setopt($ch, CURLOPT_HEADERFUNCTION,
    function($ch, $header) use($rels, &$result) {
      foreach ($rels as $name) {
        if (stripos($header, 'Link:') === 0 && stripos($header, $name)) {
          $regex = '/<([^>]+)>; rel="?(([^"]* )?' . $name . '( [^"]*)?)"?/i';
          if (preg_match($regex, $header, $match)) {
            if (in_array($name, explode(' ', strtolower($match[2])))) {
              // This makes sure the first discovered endpoint is used.
              if (!isset($result[$name])) $result[$name] = $match[1];
            }
          }
        }
      }
      return strlen($header);
    });
  log_db('discover_endpoint 1: curl ' . $us_url);
  $response = curl_exec($ch);
  // Need to remove headers from the response as it can mess with the parser.
  $body = substr($response, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
  $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
  curl_close($ch);

  // This is a special case for web actions, if the provided url returns JSON
  // then the endpoint was given and the config can be returned here.
  if ($rels[0] === 'webaction' && $content_type === 'application/json') {
    return $body;
  }

  // Check the body for requested endpoints that weren't discovered in headers.
  if (count($result) !== count($rels)) {
    $doc = new DOMDocument();
    @$doc->loadHTML($body);
    $xpath = new DOMXpath($doc);
    $query = '//a[@rel and @href] | //link[@rel and @href]';
    foreach ($xpath->query($query) as $link) {
      $rel_values = explode(' ', strtolower($link->getAttribute('rel')));
      foreach ($rels as $name) {
        if (!isset($result[$name]) && in_array($name, $rel_values)) {
          $result[$name] = trim($link->getAttribute('href'));
          if ($result[$name] === '') $result[$name] = $us_url;
        }
      }
    }
  }
  // Make sure all requests are returned and all discovered endpoints are valid.
  // Turn relative endpoints into absolute ones using the given url.
  $regex = '/^(?:https?:\/\/)?[\/]*([^\/]+)/i';
  foreach ($rels as $name) {
    if (!isset($result[$name])) {
      $result[$name] = '';
    }
    else if (stripos($result[$name], 'http') !== 0) {
      if (preg_match('/^(https?:\/\/[^\/]+)/i', $us_url, $match)) {
        if (strpos($result[$name], '/') === 0) {
          $result[$name] = $match[1] . $result[$name];
        }
        else {
          // No leading slash means the endpoint is relative to the url path,
          // so match everything except for the last path segment.
          if (preg_match('/^(https?:\/\/.+\/)[^\/]*$/i', $us_url, $match)) {
            $result[$name] = $match[1] . $result[$name];
          }
          else {
            // This catches a url without any path.
            $result[$name] = $us_url . '/' . $result[$name];
          }
        }
      }
    }
    else if (preg_match($regex, $result[$name], $match)) {
      if (strpos($match[1], '127.') === 0 ||
          strtolower($match[1]) === 'localhost') {
        $result[$name] = 'Invalid ' . $name . ' url found at ' . $us_url;
      }
    }
  }
  // Another special case for web actions, fetch the config from the endpoint.
  if ($rels[0] === 'webaction') {
    if ($result['webaction'] === '' ||
        stripos($result['webaction'], 'http') !== 0) {
      return json_encode('config not found.');
    }

    $ch = curl_init($result['webaction']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_ENCODING, '');
    log_db('discover_endpoint 2: curl ' . $result['webaction']);
    $body = curl_exec($ch);
    $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
    curl_close($ch);
    if ($content_type === 'application/json') {
      return $body;
    }
    return json_encode('config not found.');
  }

  // Also try looking for a canonical h-card to add to the nickname cache.
  include_once 'library/Masterminds/HTML5.auto.php';
  include_once 'library/Mf2/Parser.php';
  $mf = Mf2\parse($body, $us_url);
  // Use SimplePie to cache images.
  include_once 'autoloader.php';
  $simple_pie = new SimplePie();
  $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
    'https://' : 'http://';
  $handler = $scheme . $_SERVER['SERVER_NAME'] . '/php/image.php';
  // The full image handler url is required for Microsub clients.
  $simple_pie->set_image_handler($handler);
  $simple_pie->init();
  // Use the url without the scheme for more lenient matching.
  $domain = $us_url;
  if (preg_match('/^https?:\/\/(.+)/i', $us_url, $match)) {
    $domain = trim($match[1], ' /');
  }
  // Only interested in an h-card with a url property matching the url
  // provided as a parameter.
  foreach ($mf['items'] as $mf_item) {
    if (!isset($mf_item['type'])) continue;

    $hcard = NULL;
    if (in_array('h-card', $mf_item['type']) &&
        isset($mf_item['properties']['url'])) {
      $hcard = $mf_item;
    }
    else if (isset($mf_item['properties']['author'])) {
      foreach ($mf_item['properties']['author'] as $author) {
        if (isset($author['type']) &&
            in_array('h-card', $author['type']) &&
            isset($author['properties']['url'])) {
          $hcard = $author;
          break;
        }
      }
    }
    if (!isset($hcard)) continue;

    $match = false;
    foreach ($hcard['properties']['url'] as $canonical) {
      if (stripos($canonical, 'http://' . $domain) === 0 ||
          stripos($canonical, 'https://' . $domain) === 0) {
        $us_url = $canonical;
        $match = true;
        break;
      }
    }
    if (!$match) continue;

    $mysqli = connect_db();
    // Check if there's an existing photo to avoid caching it again.
    $us_photo = '';
    $url = $mysqli->escape_string(trim($us_url, ' /'));
    $query = 'SELECT photo FROM nickname WHERE url = "' . $url . '"';
    if ($mysqli_result = $mysqli->query($query)) {
      if ($nickname = $mysqli_result->fetch_assoc()) {
        $us_photo = $nickname['photo'];
      }
      $mysqli_result->close();
    }
    else {
      log_db('microformats->discover_endpoint 1: ' . $mysqli->error);
    }
    $properties = $hcard['properties'];
    $name = isset($properties['name'][0]) ?
      $mysqli->escape_string($properties['name'][0]) : '';
    $nickname = isset($properties['nickname'][0]) ?
      $mysqli->escape_string($properties['nickname'][0]) : '';
    if (isset($properties['photo'][0]) &&
        $us_photo !== $properties['photo'][0]) {
      $us_photo = $properties['photo'][0];
    }
    else {
      // Set the photo to an empty string to avoid re-caching and updating.
      $us_photo = '';
    }
    $us_cache = $us_photo === '' ? '' :
      $simple_pie->sanitize($us_photo, SIMPLEPIE_CONSTRUCT_IRI, '', true);
    $photo = $mysqli->escape_string($us_photo);
    $cache = $mysqli->escape_string($us_cache);
    $update_photo_query = $photo === '' || $cache === '' ? '' :
      ', photo = "' . $photo . '", cache = "' . $cache . '"';
    $reachable = stripos($result['webmention'], 'http') === 0 ? '1' : '0';
    $query = 'INSERT INTO nickname VALUES ("' . $name . '", ' .
      '"' . $url . '", "' . $photo . '", "' . $cache . '", ' .
      '"' . $nickname . '", ' . $reachable . ') ON DUPLICATE KEY UPDATE ' .
      'name = "' . $name . '", reachable = ' . $reachable . $update_photo_query;
    if (!$mysqli->query($query)) {
      log_db('microformats->discover_endpoint 2: ' . $mysqli->error);
    }
    $mysqli->close();
    break;
  }

  // If only one endpoint was requested just return the result.
  if (count($rels) === 1) return $result[$rels[0]];
  return $result;
}

function parse_comments($comment_list, $source) {
  include_once 'library/Masterminds/HTML5.auto.php';
  include_once 'library/Mf2/Parser.php';

  $result = [];
  if (is_array($comment_list)) {
    foreach ($comment_list as $comment) {
      if (isset($comment['type']) && (in_array('h-entry', $comment['type']) ||
                                      in_array('h-cite', $comment['type']))) {
        $author = '';
        $author_photo = '';
        $author_url = '';
        $url = '';
        $content = '';
        $date = '';
        $currency = '';
        $payment = '';
        $amount = '';
        if (isset($comment['properties']['author'][0])) {
          $author = $comment['properties']['author'][0];
          // author is a special case, it can be plain text or an h-card array.
          // If it's plain text it can also be a url that should be followed to
          // get the actual h-card.
          if (!is_string($author)) {
            list($author, $author_photo, $author_url) = parse_hcard($author);
          }
          else if (stripos($author, 'http') === 0) {
            $mf = Mf2\fetch($author);
            foreach ($mf['items'] as $hcard) {
              // Only interested in an h-card by itself in this case.
              if (!in_array('h-card', $hcard['type'])) {
                continue;
              }
              // It must have a url property matching what we fetched.
              if (!isset($hcard['properties']['url'][0]) ||
                  $hcard['properties']['url'][0] !== $author) {
                continue;
              }
              list($author, $author_photo, $author_url) = parse_hcard($hcard);
              break;
            }
          }
        }
        if (isset($comment['properties']['url'][0])) {
          $url = $comment['properties']['url'][0];
        }
        if (isset($comment['properties']['currency'][0])) {
          $currency = $comment['properties']['currency'][0];
        }
        if (isset($comment['properties']['published'][0])) {
          $date = $comment['properties']['published'][0];
        }
        if (isset($comment['properties']['content'][0])) {
          // Check e-content (html) over p-content (plain text).
          if (isset($comment['properties']['content'][0]['html'])) {
            $content = $comment['properties']['content'][0]['html'];
          }
          else if (is_string($comment['properties']['content'][0])) {
            $content = $comment['properties']['content'][0];
          }
        }
        if (isset($comment['properties']['debit'][0])) {
          $payment = 'debit';
          $amount = $comment['properties']['debit'][0];
        }
        else if (isset($comment['properties']['credit'][0])) {
          $payment = 'credit';
          $amount = $comment['properties']['credit'][0];
        }
        // Always return an author, use the source domain if not found.
        if ($author === '') {
          $author = $source;
          if (preg_match('/\/\/([^\/]+)/', $author, $match)) {
            $author = $match[1];
          }
        }
        $result[] = ['url' => $url, 'author-name' => $author,
                     'author-photo' => $author_photo,
                     'author-url' => $author_url, 'content' => $content,
                     'date' => $date, 'currency' => $currency,
                     'payment' => $payment, 'amount' => $amount];
      }
    }
  }
  else if (is_string($comment_list)) {
    $result[] = ['content' => $comment_list];
  }
  return $result;
}

function parse_happ($source) {
  include_once 'library/Masterminds/HTML5.auto.php';
  include_once 'library/Mf2/Parser.php';

  $mf = Mf2\fetch($source);
  $h_app = [];
  foreach ($mf['items'] as $mf_item) {
    if (!isset($mf_item['type'])) continue;

    if (in_array('h-app', $mf_item['type']) ||
        in_array('h-x-app', $mf_item['type'])) {
      $h_app = $mf_item;
      break;
    }
  }
  $name = isset($h_app['properties']['name'][0]) ?
    $h_app['properties']['name'][0] : '';
  $logo = isset($h_app['properties']['logo'][0]) ?
    $h_app['properties']['logo'][0] : '';
  $url = isset($h_app['properties']['url'][0]) ?
    $h_app['properties']['url'][0] : '';
  return ['name' => $name, 'logo' => $logo, 'url' => $url];
}

function parse_hcard($author, $show_repost = false) {
  $us_author_name = '';
  $us_old_name = '';
  $us_photo = '';
  $us_cache = '';
  $us_author_photo = '';
  $us_author_url = '';
  $us_repost_name = '';
  $us_repost_photo = '';
  $us_repost_cache = '';
  $us_repost_url = '';
  $url = '';
  $repost_url = '';

  $mysqli = connect_db();
  if (isset($author['type']) && in_array('h-card', $author['type'])) {
    if (isset($author['properties']['url'][0])) {
      $us_author_url = trim($author['properties']['url'][0], ' /');
      if (stripos($us_author_url, 'http') !== 0) {
        $us_author_url = 'http://' . $us_author_url;
      }
      $url = $mysqli->escape_string($us_author_url);
      // Check if there's an existing photo to avoid caching it again. Also
      // want to return the old name for the Reader module in case it changes.
      $query = 'SELECT name, photo, cache FROM nickname WHERE ' .
        'url = "' . $url . '"';
      if ($mysqli_result = $mysqli->query($query)) {
        if ($nickname = $mysqli_result->fetch_assoc()) {
          $us_old_name = $nickname['name'];
          $us_photo = $nickname['photo'];
          $us_cache = $nickname['cache'];
        }
        $mysqli_result->close();
      }
      else {
        log_db('microformats->parse_hcard 1: ' . $mysqli->error);
      }
      // A second author in an h-card is assumed to be a repost author.
      if (isset($author['properties']['url'][1])) {
        $us_repost_url = trim($author['properties']['url'][1], ' /');
        if (stripos($us_repost_url, 'http') !== 0) {
          $us_repost_url = 'http://' . $us_repost_url;
        }
        $repost_url = $mysqli->escape_string($us_repost_url);
        $query = 'SELECT photo FROM nickname WHERE url = "' . $repost_url . '"';
        if ($mysqli_result = $mysqli->query($query)) {
          if ($nickname = $mysqli_result->fetch_assoc()) {
            $us_repost_photo = $nickname['photo'];
          }
          $mysqli_result->close();
        }
        else {
          log_db('microformats->parse_hcard 2: ' . $mysqli->error);
        }
      }
    }
    if (isset($author['properties']['photo'][0]) &&
        $us_photo !== $author['properties']['photo'][0]) {
      $us_photo = $author['properties']['photo'][0];
      if ($us_photo !== '') {
        // Use SimplePie to cache images.
        include_once 'autoloader.php';
        $simple_pie = new SimplePie();
        $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
          'https://' : 'http://';
        $handler = $scheme . $_SERVER['SERVER_NAME'] . '/php/image.php';
        // The full image handler url is required for Microsub clients.
        $simple_pie->set_image_handler($handler);
        $simple_pie->init();
        $us_cache = $simple_pie->sanitize($us_photo, SIMPLEPIE_CONSTRUCT_IRI,
                                          '', true);
        if (isset($author['properties']['photo'][1]) &&
            $us_repost_photo !== $author['properties']['photo'][1]) {
          $us_repost_photo = $author['properties']['photo'][1];
          if ($us_repost_photo !== '') {
            $us_repost_cache = $simple_pie->sanitize($us_repost_photo,
                                                     SIMPLEPIE_CONSTRUCT_IRI,
                                                     '', true);
          }
        }
      }
    }
    if (isset($author['properties']['name'][0])) {
      // Can't contain commas because repost author may also be returned.
      $us_author_name = str_replace(',', '', $author['properties']['name'][0]);
      $mysqli = connect_db();
      $name = $mysqli->escape_string($us_author_name);
      $photo = $mysqli->escape_string($us_photo);
      $cache = $mysqli->escape_string($us_cache);
      $url = $mysqli->escape_string($us_author_url);
      $nickname = isset($properties['nickname'][0]) ?
        $mysqli->escape_string($properties['nickname'][0]) : '';
      // Don't replace the photo if not found on this h-card.
      $update_photo_query = $photo === '' || $cache === '' ? '' :
        ', photo = "' . $photo . '", cache = "' . $cache . '"';
      $query = 'INSERT INTO nickname VALUES ("' . $name . '", "' . $url . '", '.
        '"' . $photo . '", "' . $cache . '", "' . $nickname . '", 0) ' .
        'ON DUPLICATE KEY UPDATE name = "' . $name . '"' . $update_photo_query;
      if (!$mysqli->query($query)) {
        log_db('microformats->parse_hcard 3: ' . $mysqli->error);
      }
      if (isset($author['properties']['name'][1])) {
        $us_repost_name =
          str_replace(',', '', $author['properties']['name'][1]);
        $repost_name = $mysqli->escape_string($us_repost_name);
        $repost_photo = $mysqli->escape_string($us_repost_photo);
        $repost_cache = $mysqli->escape_string($us_repost_cache);
        $repost_url = $mysqli->escape_string($us_repost_url);
        // Don't replace the photo if not found on this h-card.
        $update_photo_query = $repost_photo === '' || $repost_cache === '' ? ''
          : ', photo = "' . $repost_photo . '", cache = "' . $repost_cache .'"';
        $query = 'INSERT INTO nickname VALUES ("' . $repost_name . '", ' .
          '"' . $repost_url . '", "' . $repost_photo . '", ' .
          '"' . $repost_cache . '", "", 0) ON DUPLICATE KEY UPDATE ' .
          'name = "' . $repost_name . '"' . $update_photo_query;
        if (!$mysqli->query($query)) {
          log_db('microformats->parse_hcard 4: ' . $mysqli->error);
        }
      }
    }
  }
  else if (isset($author['value'])) {
    $us_author_name = $author['value'];
  }
  $mysqli->close();

  if ($us_repost_name !== '') $us_repost_name = ',' . $us_repost_name;
  // img tag is stored to match Detail module for local accounts.
  if ($us_cache !== '') {
    $us_author_photo = '<img class="thumb u-photo" src="' . $us_cache . '">';
  }
  if ($show_repost) $us_author_name .= $us_repost_name;
  return [$us_author_name, $us_author_photo, $us_author_url, $us_old_name];
}

function parse_hcite($target, $hcite_list) {
  foreach ($hcite_list as $hcite) {
    if (isset($hcite['type']) && in_array('h-cite', $hcite['type']) &&
        isset($hcite['properties']['url']) &&
        in_array($target, $hcite['properties']['url'])) {
      return true;
    }
  }
  return false;
}

function parse_hentry($source, $target = '', $result = '') {
  include_once 'library/Masterminds/HTML5.auto.php';
  include_once 'library/Mf2/Parser.php';

  $mention = 'tag';
  $author = '';
  $author_photo = '';
  $author_url = '';
  $uid = '';
  $title = '';
  $content = '';
  $category = '';
  $currency = '';
  $date = '';
  $tag_author = '';
  $tag_photo = '';
  $tag_url = '';
  $payment = '';
  $amount = '';
  $in_reply_to = '';
  $comment = [];
  $like = [];
  $share = [];
  $mf = $result === '' ? Mf2\fetch($source) : Mf2\parse($result, $source);

  // First look for an h-feed.
  $h_feed = [];
  foreach ($mf['items'] as $mf_item) {
    if (!isset($mf_item['type'])) continue;

    if (in_array('h-feed', $mf_item['type'])) {
      $h_feed = $mf_item;
      break;
    }
    // Also look for an h-feed in the children of each top level item.
    if (!isset($mf_item['children'][0]['type'])) continue;
    if (in_array('h-feed', $mf_item['children'][0]['type'])) {
      $h_feed = $mf_item['children'][0];
      break;
    }
  }

  $entries = isset($h_feed['children']) ? $h_feed['children'] : $mf['items'];
  foreach ($entries as $entry) {
    if (!isset($entry['type']) || (!in_array('h-entry', $entry['type']) &&
                                   !in_array('h-cite', $entry['type']))) {
      continue;
    }

    if (isset($entry['properties']['like-of'])) {
      $like_of = $entry['properties']['like-of'];
      // like may not be an array of urls, so check for h-cite first.
      if (parse_hcite($target, $like_of) || in_array($target, $like_of)) {
        $mention = 'star';
      }
    }
    else if (isset($entry['properties']['repost-of'])) {
      $repost_of = $entry['properties']['repost-of'];
      if (parse_hcite($target, $repost_of) || in_array($target, $repost_of)) {
        $mention = 'share';
      }
    }
    else if (isset($entry['properties']['follow-of'])) {
      $follow_of = $entry['properties']['follow-of'];
      if (parse_hcite($target, $follow_of) || in_array($target, $follow_of)) {
        $mention = 'follow';
      }
    }
    else if (isset($entry['properties']['in-reply-to'])) {
      $reply = $entry['properties']['in-reply-to'];
      if (parse_hcite($target, $reply) || in_array($target, $reply)) {
        $mention = 'comment';
      }
      // Want to return the in-reply-to url but it doesn't have to be the
      // target, so just return the first value found.
      if (is_string($reply[0])) {
        $in_reply_to = $reply[0];
      }
      else if (isset($reply[0]['type']) &&
               in_array('h-cite', $reply[0]['type']) &&
               isset($reply[0]['properties']['url'][0])) {
        $in_reply_to = $reply[0]['properties']['url'][0];
      }
    }
    if (isset($entry['properties']['debit'][0])) {
      $payment = 'debit';
      $amount = $entry['properties']['debit'][0];
    }
    else if (isset($entry['properties']['credit'][0])) {
      $payment = 'credit';
      $amount = $entry['properties']['credit'][0];
    }
    if (isset($entry['properties']['author'][0])) {
      $author = $entry['properties']['author'][0];
      // author is a special case, it can be plain text or an h-card array.
      // If it's plain text it can also be a url that should be followed to
      // get the actual h-card.
      if (!is_string($author)) {
        list($author, $author_photo, $author_url) = parse_hcard($author);
      }
      else if (stripos($author, 'http') === 0) {
        $mf = Mf2\fetch($author);
        foreach ($mf['items'] as $hcard) {
          // Only interested in an h-card by itself in this case.
          if (!in_array('h-card', $hcard['type'])) continue;
          // It must have a url property matching what we fetched.
          if (!isset($hcard['properties']['url']) ||
              !in_array($author, $hcard['properties']['url'])) {
            continue;
          }
          list($author, $author_photo, $author_url) = parse_hcard($hcard);
          break;
        }
      }
    }
    if (isset($entry['properties']['url'][0])) {
      // Changing the source url should only be permitted by trusted sites.
      if (stripos($source, 'https://brid.gy') === 0) {
        $source = $entry['properties']['url'][0];
      }
    }
    if (isset($entry['properties']['name'][0])) {
      $title = $entry['properties']['name'][0];
    }
    if (isset($entry['properties']['uid'][0])) {
      $uid = $entry['properties']['uid'][0];
    }
    if (isset($entry['properties']['category'])) {
      foreach ($entry['properties']['category'] as $tag) {
        if ($category !== '') $category .= ',';
        if (is_string($tag)) {
          $category .= $tag;
        }
        else {
          if (isset($tag['value'])) $category .= $tag['value'];
          // Test this tag as an h-card to update the mention type, also want
          // to know about a person tag in case we get a reply from them.
          list($tag_author, $tag_photo, $tag_url) = parse_hcard($tag);
          if ($target !== '' && $tag_url === $target) {
            // Also need to know if this person tag should be shown as a
            // comment.
            $mention = $mention === 'comment' ? 'tag-comment' : 'tag-person';
          }
        }
      }
    }
    if (isset($entry['properties']['currency'][0])) {
      $currency = $entry['properties']['currency'][0];
    }
    if (isset($entry['properties']['published'][0])) {
      $date = $entry['properties']['published'][0];
    }
    $use_content = true;
    if (isset($entry['properties']['summary'][0])) {
      // Use summary as content if there are any unrecognized properties.
      $known_properties = ['like-of', 'repost-of', 'in-reply-to', 'url',
                           'debit', 'credit', 'author', 'name', 'uid',
                           'category', 'currency', 'published', 'summary',
                           'content', 'photo', 'comment', 'like', 'repost'];
      foreach ($entry['properties'] as $name => $property) {
        if (!in_array($name, $known_properties)) {
          $use_content = false;
          break;
        }
      }
      if (!$use_content) {
        if (isset($entry['properties']['summary'][0]['html'])) {
          $content = $entry['properties']['summary'][0]['html'];
        }
        else if (is_string($entry['properties']['summary'][0])) {
          $content = $entry['properties']['summary'][0];
        }
      }
    }
    if ($use_content && isset($entry['properties']['content'][0])) {
      // Check e-content (html) over p-content (plain text).
      if (isset($entry['properties']['content'][0]['html'])) {
        $content = $entry['properties']['content'][0]['html'];
      }
      else if (is_string($entry['properties']['content'][0])) {
        $content = $entry['properties']['content'][0];
      }
    }
    if (isset($entry['properties']['photo'][0])) {
      $photo_list = [];
      foreach ($entry['properties']['photo'] as $photo) {
        if (!empty($photo) && strpos($content, $photo) === false) {
          $photo_list[] = $photo;
        }
      }
      if (count($photo_list) === 0) {
        // All photos were found to be duplicated in content, so assume this
        // is a photo post and display the lightbox. Add a special wrapper
        // around the existing content so those photos can be hidden.
        $content = '<div class="photo-hidden">' . $content . '</div>';
        $photo_list = $entry['properties']['photo'];
      }
      $count = count($photo_list);
      if ($count > 1) {
        // Use SimplePie to cache images.
        include_once 'autoloader.php';
        $simple_pie = new SimplePie();
        $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== '' ?
          'https://' : 'http://';
        $handler = $scheme . $_SERVER['SERVER_NAME'] . '/php/image.php';
        $simple_pie->set_image_handler($handler);
        $simple_pie->init();
        // When there's more than one photo show the first two and use a
        // lightbox. Need a permanent, unique name for the image set, but
        // don't have anything unique except for photo urls, so use that.
        $image_set_id = preg_replace('/[[:^alnum:]]/', '', $photo_list[0]);
        $content .= '<p class="photo-list">';
        for ($i = 0; $i < $count; $i++) {
          $hidden = $i <= 1 ? '' : 'class="hidden" ';
          $cache = $simple_pie->sanitize($photo_list[$i],
                                         SIMPLEPIE_CONSTRUCT_IRI, '', true);
          $content .= '<a href="' . $cache . '" ' . $hidden .
            'data-lightbox="image-set-' . $image_set_id . '">' .
            '<img src="' . $cache . '"></a>';
        }
        $content .= '<br><b>' . $count . ' photos</b></p>';
      }
      else if ($count == 1) {
        $cache = $simple_pie->sanitize($photo_list[0],
                                       SIMPLEPIE_CONSTRUCT_IRI, '', true);

        $content .= '<p><img src="' . $photo_list[0] . '"></p>';
      }
    }
    // Check for children of this entry, either comment, like or repost.
    if (isset($entry['properties']['comment'])) {
      $comment = parse_comments($entry['properties']['comment'], $source);
    }
    if (isset($entry['properties']['like'])) {
      $like = parse_comments($entry['properties']['like'], $source);
    }
    if (isset($entry['properties']['repost'])) {
      $share = parse_comments($entry['properties']['repost'], $source);
    }
    break;
  }
  // Always return an author, use the source domain if not found.
  if ($author === '') {
    $author = $source;
    if (preg_match('/\/\/([^\/]+)/', $author, $match)) {
      $author = $match[1];
    }
  }
  return ['url' => $source, 'mention' => $mention, 'author-name' => $author,
          'author-photo' => $author_photo, 'author-url' => $author_url,
          'uid' => $uid, 'content' => $content, 'title' => $title,
          'category' => $category, 'date' => $date, 'comment' => $comment,
          'like' => $like, 'share' => $share, 'payment' => $payment,
          'amount' => $amount, 'currency' => $currency,
          'in-reply-to' => $in_reply_to];
}
