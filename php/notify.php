<?php
// Dobrado Content Management System
// Copyright (C) 2016 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

foreach (['timestamp', 'track', 'url'] as $name) {
  if (!isset($_POST[$name])) {
    echo json_encode(['error' => $name.' not provided']);
    exit;
  }
}

include 'functions/db.php';
include 'functions/page_owner.php';

$mysqli = connect_db();

$timestamp = (int)$_POST['timestamp'];
$url = $mysqli->escape_string($_POST['url']);
list($page, $owner) = page_owner($url);
if (isset($_SESSION['user'])) $owner = $_SESSION['user'];

$us_track = json_decode($_POST['track'], true);
$object = ['timestamp' => time(), 'track' => []];

if ($timestamp !== 0) {
  for ($i = 0; $i < count($us_track); $i++) {
    $action = $mysqli->escape_string($us_track[$i]);
    $query = 'SELECT box_id FROM notify WHERE user = "'.$owner.'"'.
      ' AND action = "'.$action.'" AND timestamp > '.$timestamp.
      ' AND (page = "'.$page.'" OR page = "")';
    if ($result = $mysqli->query($query)) {
      $object['track'][$action] = $result->num_rows > 0;
      $result->close();
    }
    else {
      log_db('notify: '.$mysqli->error);
    }
  }
}

$mysqli->close();

echo json_encode($object);
