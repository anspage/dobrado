<?php
// Dobrado Content Management System
// Copyright (C) 2017 Malcolm Blaney
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

include 'functions/session.php';

if (session_expired()) exit;

foreach (['id', 'label', 'url'] as $name) {
  if (!isset($_POST[$name])) {
    echo json_encode(['error' => $name.' not provided']);
    exit;
  }
}

include 'functions/copy_page.php';
include 'functions/db.php';
include 'functions/microformats.php';
include 'functions/new_module.php';
include 'functions/page_owner.php';
include 'functions/permission.php';
include 'functions/style.php';
include 'functions/write_style.php';

include 'config.php';
include 'module.php';
include 'user.php';

// Remove the '#dobrado-' prefix.
$mysqli = connect_db();
$id = (int)substr($_POST['id'], 9);
$label = $mysqli->escape_string($_POST['label']);
$url = $mysqli->escape_string($_POST['url']);
$mysqli->close();

list($page, $owner) = page_owner($url);
// Modules must do their own safety checks on content.
$us_content = json_decode($_POST['content'], true);
$user = new User();
$user->SetPermission($page, $owner);
$module = new Module($user, $owner, $label);
if ($module->CanEdit($id)) {
  // Post modules are a special case, as the Writer module can be used to
  // create posts with only view page permission.
  if ($user->canEditPage || ($label === 'post' && $user->canViewPage)) {
    $module->SetContent($id, $us_content);
    // Return the updated content.
    echo json_encode(['html' => $module->Content($id)]);
  }
  else {
    echo json_encode(['done' => true]);
  }
}
else {
  echo json_encode(['error' => 'could not edit module']);
}
